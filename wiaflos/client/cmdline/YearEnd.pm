# Year end procedures
# Copyright (C) 2009-2014, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.




package wiaflos::client::cmdline::YearEnd;

use strict;
use warnings;


use wiaflos::constants;
use wiaflos::client::misc;
use wiaflos::client::soap;
use wiaflos::client::reportwriter;


# Plugin info
our $pluginInfo = {
	Name 	=> "YearEnd",
	Menu 	=> [
		# Reporting Menu
		{
			MenuItem 	=> "YearEnd",
			Regex		=> "year(?:end)?",
			Children	=> [
				{
					MenuItem 	=> "Create",
					Regex		=> "create",
					Desc		=> "Create year end",
					Help		=> 'send start="<yyyy-mm-dd>" end="<yyyy-mm-dd>" account="<account number>" [type=<transaction type>]',
					Function	=> \&performYearEnd,
				},
			],
		},
	],
};




# Perform year end
sub performYearEnd
{
	my ($OUT,@args) = @_;


	my $parms = parseArgs(@args);

	if (!defined($parms->{'start'})) {
		print($OUT "  => ERROR: Parameter 'start' not defined\n");
		return ERR_C_PARAM;
	}

	if (!defined($parms->{'end'})) {
		print($OUT "  => ERROR: Parameter 'end' not defined\n");
		return ERR_C_PARAM;
	}

	if (!defined($parms->{'account'})) {
		print($OUT "  => ERROR: Parameter 'account' not defined\n");
		return ERR_C_PARAM;
	}


	my $detail;
	$detail->{'StartDate'} = $parms->{'start'};
	$detail->{'EndDate'} = $parms->{'end'};
	$detail->{'REAccountNumber'} = $parms->{'account'};
	$detail->{'Type'} = $parms->{'type'};
	my $res = soapCall($OUT,"wiaflos/server/api/YearEnd","performYearEnd",$detail);
	if ($res->{'Result'} != RES_OK) {
		soapDebug($OUT,$res);
	}

	return $res->{'Result'};
}



1;
# vim: ts=4
