# Inventory functions
# Copyright (C) 2009-2014, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.



package wiaflos::client::cmdline::Inventory;

use strict;
use warnings;

use wiaflos::constants;
use wiaflos::client::misc;
use wiaflos::client::soap;
use wiaflos::client::reportwriter;


# Plugin info
our $pluginInfo = {
	Name 	=> "Inventory",
	Menu 	=> [
		# Inventory main menu option
		{
			MenuItem 	=> "Inventory",
			Children 	=> [
				{
					MenuItem 	=> "Create",
					Regex		=> "create",
					Desc		=> "Create inventory item",
					Help		=> 'create code="<product code>" type="<product or service>" [mode="<bulk or track for products>"] '.
							'description="<description>" incomeaccount="<income account>" [assetaccount="<asset account>"] '.
							'[expenseaccount="<expense account>"] sellprice="<sell price>" taxtype="<tax type>" [unit="<unit>"] '.
							'taxmode="<price is incl or excl>" [discountable="<yes or no>"]',
					Function	=> \&create,
				},
				{
					MenuItem 	=> "Remove",
					Regex		=> "remove",
					Desc		=> "Remove inventory item",
					Help		=> 'remove item="<product code>"',
					Function	=> \&remove,
				},
				{
					MenuItem 	=> "Update",
					Regex		=> "update",
					Desc		=> "Update inventory item",
					Help		=> 'update item="<product code>" [description="<description>"] [sellprice="<sell price>"] [discountable="<yes or no>"] [unit="<unit>"] [incomeaccount="<income account>"] [assetaccount="<asset account>"] [expenseaccount="<expense account>"]',
					Function	=> \&update,
				},
				{
					MenuItem 	=> "List",
					Regex		=> "list",
					Desc		=> "Display list of inventory items",
					Help		=> 'list',
					Function	=> \&list,
				},
				{
					MenuItem 	=> "Show",
					Regex		=> "show",
					Desc		=> "Show inventory item",
					Help		=> 'show item="<product code>"',
					Function	=> \&show,
				},
				{
					MenuItem 	=> "ShowMovement",
					Regex		=> "showmovement",
					Desc		=> "Show inventory item movement",
					Help		=> 'show item="<product code>" [serial="<serial number>"]',
					Function	=> \&showMovement,
				},
				{
					MenuItem 	=> "LinkExpense",
					Regex		=> "linkexpense",
					Desc		=> "Link inventory item to expense account",
					Help		=> 'linkExpense date="<YYYY-MM-DD>" item="<product code>" reference="<refernce>" '.
							'[serial="<serial number>"] qty="<qty>" account="<GL expense account>"',
					Function	=> \&linkExpense,
				},
			],
		},
	],
};




# List inventory items
sub list
{
	my ($OUT,@args) = @_;

	if (@args > 0) {
		return ERR_C_PARAM;
	}

	my $res = soapCall($OUT,"wiaflos/server/api/Inventory","getInventory");

	if ($res->{'Result'} == RES_OK) {
		print $OUT swrite(<<'END', "ID", "Code", "Type", "Mode", "Description", "Income", "Asset", "Expense", "SellPrice");
+=======+===========+=========+========+==============================================+===============+==============+=============+===========+
| @|||| | @|||||||| | @|||||| | @||||| | @||||||||||||||||||||||||||||||||||||||||||| | @|||||||||||| | @||||||||||| | @|||||||||| | @|||||||| |
+=======+===========+=========+========+==============================================+===============+==============+=============+===========+
END
		foreach my $item (@{$res->{'Data'}}) {
			print $OUT swrite(<<'END', $item->{'ID'}, $item->{'Code'}, $item->{'Type'}, defined($item->{'Mode'}) ? $item->{'Mode'} : '-', $item->{'Description'}, $item->{'GLIncomeAccountNumber'} ? $item->{'GLIncomeAccountNumber'} : '-', defined($item->{'GLAssetAccountNumber'}) ? $item->{'GLAssetAccountNumber'} : '-', defined($item->{'GLExpenseAccountNumber'}) ? $item->{'GLExpenseAccountNumber'} : '-', sprintf('%.2f',$item->{'SellPrice'}));
| @<<<< | @<<<<<<<< | @|||||| | @||||| | @<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< | @<<<<<<<<<<<< | @<<<<<<<<<<< | @<<<<<<<<<< | @>>>>>>>> |
END
		}
		print $OUT swrite(<<'END');
+=======+===========+=========+========+==============================================+===============+==============+=============+===========+
END
	} else {
		soapDebug($OUT,$res);
	}

	return $res->{'Result'};
}


# Create inventory item
sub create
{
	my ($OUT,@args) = @_;


	my $parms = parseArgs(@args);

	if (!defined($parms->{'code'})) {
		print($OUT "  => ERROR: Parameter 'code' not defined\n");
		return ERR_C_PARAM;
	}

	if (!defined($parms->{'type'})) {
		print($OUT "  => ERROR: Parameter 'type' not defined\n");
		return ERR_C_PARAM;
	}

	if (!defined($parms->{'description'})) {
		print($OUT "  => ERROR: Parameter 'description' not defined\n");
		return ERR_C_PARAM;
	}

	if (!defined($parms->{'incomeaccount'})) {
		print($OUT "  => ERROR: Parameter 'incomeaccount' not defined\n");
		return ERR_C_PARAM;
	}

	if (!defined($parms->{'sellprice'})) {
		print($OUT "  => ERROR: Parameter 'sellprice' not defined\n");
		return ERR_C_PARAM;
	}

	if (!defined($parms->{'taxtype'})) {
		print($OUT "  => ERROR: Parameter 'taxtype' not defined\n");
		return ERR_C_PARAM;
	}

	if (!defined($parms->{'taxmode'})) {
		print($OUT "  => ERROR: Parameter 'taxmode' not defined\n");
		return ERR_C_PARAM;
	}

	my $detail;
	$detail->{'Code'} = $parms->{'code'};
	$detail->{'Type'} = $parms->{'type'};
	$detail->{'Mode'} = $parms->{'mode'};
	$detail->{'Description'} = $parms->{'description'};
	$detail->{'GLIncomeAccountNumber'} = $parms->{'incomeaccount'};
	$detail->{'GLAssetAccountNumber'} = $parms->{'assetaccount'};
	$detail->{'GLExpenseAccountNumber'} = $parms->{'expenseaccount'};
	$detail->{'SellPrice'} = $parms->{'sellprice'};
	$detail->{'TaxTypeID'} = $parms->{'taxtype'};
	$detail->{'TaxMode'} = $parms->{'taxmode'};
	$detail->{'Unit'} = $parms->{'unit'};
	$detail->{'Discountable'} = $parms->{'discoutable'};
	my $res = soapCall($OUT,"wiaflos/server/api/Inventory","createInventoryItem",$detail);
	if ($res->{'Result'} != RES_OK) {
		soapDebug($OUT,$res);
		return $res->{'Result'};
	}

	return $res->{'Data'};
}


# Remove inventory item
sub remove
{
	my ($OUT,@args) = @_;


	my $parms = parseArgs(@args);

	if (!defined($parms->{'item'})) {
		print($OUT "  => ERROR: Parameter 'item' not defined\n");
		return ERR_C_PARAM;
	}

	my $detail;
	$detail->{'Code'} = $parms->{'item'};
	my $res = soapCall($OUT,"wiaflos/server/api/Inventory","removeInventoryItem",$detail);
	if ($res->{'Result'} != RES_OK) {
		soapDebug($OUT,$res);
	}

	return $res->{'Result'};
}


# Update inventory item
sub update
{
	my ($OUT,@args) = @_;


	my $parms = parseArgs(@args);

	if (!defined($parms->{'item'})) {
		print($OUT "  => ERROR: Parameter 'item' not defined\n");
		return ERR_C_PARAM;
	}

	my $detail;
	$detail->{'Code'} = $parms->{'item'};
	$detail->{'Description'} = $parms->{'description'};
	$detail->{'GLIncomeAccountNumber'} = $parms->{'incomeaccount'};
	$detail->{'GLAssetAccountNumber'} = $parms->{'assetaccount'};
	$detail->{'GLExpenseAccountNumber'} = $parms->{'expenseaccount'};
	$detail->{'SellPrice'} = $parms->{'sellprice'};
	$detail->{'Unit'} = $parms->{'unit'};
	$detail->{'Discountable'} = $parms->{'discoutable'};
	my $res = soapCall($OUT,"wiaflos/server/api/Inventory","updateInventoryItem",$detail);
	if ($res->{'Result'} != RES_OK) {
		soapDebug($OUT,$res);
	}
	return $res->{'Result'};
}


# Show inventory item
sub show
{
	my ($OUT,@args) = @_;


	my $parms = parseArgs(@args);

	if (!defined($parms->{'item'})) {
		print($OUT "  => ERROR: Parameter 'item' not defined\n");
		return ERR_C_PARAM;
	}

	my $detail;
	$detail->{'Code'} = $parms->{'item'};
	my $res = soapCall($OUT,"wiaflos/server/api/Inventory","getInventoryItem",$detail);

	if ($res->{'Result'} == RES_OK) {
		my $data = $res->{'Data'};

		printf $OUT <<'END',$data->{'Code'},$data->{'Type'},$data->{'Mode'},$data->{'Description'},$data->{'GLIncomeAccountNumber'},$data->{'GLAssetAccountNumber'},$data->{'GLExpenseAccountNumber'},defined($data->{'Unit'}) ? $data->{'Unit'} : '-',$data->{'SellPrice'},$data->{'TaxTypeID'},$data->{'TaxMode'},$data->{'Discountable'};
Product Info
============
Product Code......: %s
Type..............: %s
Mode..............: %s
Description.......: %s
GLIncomeAccount...: %s
GLAssetAccount....: %s
GLExpenseAccount..: %s
Unit..............: %s
SellPrice.........: %s
TaxTypeID.........: %s
TaxMode...........: %s
Discountable......: %s

END

	} else {
		soapDebug($OUT,$res);
	}

	return $res->{'Result'};
}



# Show inventory item movement
sub showMovement
{
	my ($OUT,@args) = @_;


	my $parms = parseArgs(@args);

	if (!defined($parms->{'item'})) {
		print($OUT "  => ERROR: Parameter 'item' not defined\n");
		return ERR_C_PARAM;
	}

	my $detail;
	$detail->{'Code'} = $parms->{'item'};
	$detail->{'SerialNumber'} = $parms->{'serial'};
	my $res = soapCall($OUT,"wiaflos/server/api/Inventory","getInventoryItemMovement",$detail);

	if ($res->{'Result'} == RES_OK) {
		my $data = $res->{'Data'};

		print $OUT swrite(<<'END', "Timestamp", "GLTrans", "SerialNumber", "QtyChange", "Price");
Product Movement
+============+===========+=========================+===============+==============+
| @||||||||| | @|||||||| | @|||||||||||||||||||||| | @|||||||||||| | @||||||||||| |
+============+===========+=========================+===============+==============+
END
		foreach my $change (@{$data}) {
			print $OUT swrite(<<'END', $change->{'GLTransactionDate'}, $change->{'GLTransactionID'}, defined($change->{'SerialNumber'}) ? $change->{'SerialNumber'} : '-', $change->{'QtyChange'}, $change->{'Price'});
| @||||||||| | @|||||||| | @|||||||||||||||||||||| | @>>>>>>>>>>>> | @>>>>>>>>>>> |
END
		}
		print $OUT swrite(<<'END');
+============+===========+=========================+===============+==============+
END
	} else {
		soapDebug($OUT,$res);
	}

	return $res->{'Result'};
}


# Link inventory item as expense to GL account
sub linkExpense
{
	my ($OUT,@args) = @_;


	my $parms = parseArgs(@args);

	if (!defined($parms->{'date'})) {
		print($OUT "  => ERROR: Parameter 'date' not defined\n");
		return ERR_C_PARAM;
	}

	if (!defined($parms->{'item'})) {
		print($OUT "  => ERROR: Parameter 'item' not defined\n");
		return ERR_C_PARAM;
	}

	if (!defined($parms->{'reference'})) {
		print($OUT "  => ERROR: Parameter 'reference' not defined\n");
		return ERR_C_PARAM;
	}

	if (!defined($parms->{'account'})) {
		print($OUT "  => ERROR: Parameter 'account' not defined\n");
		return ERR_C_PARAM;
	}

	my $detail;
	$detail->{'Date'} = $parms->{'date'};
	$detail->{'Code'} = $parms->{'item'};
	$detail->{'Reference'} = $parms->{'reference'};
	$detail->{'GLAccountNumber'} = $parms->{'account'};
	$detail->{'SerialNumber'} = $parms->{'serial'};
	my $res = soapCall($OUT,"wiaflos/server/api/Inventory","linkInventoryItemToExpenseAccount",$detail);
	if ($res->{'Result'} != RES_OK) {
		soapDebug($OUT,$res);
		return $res->{'Result'};
	}

	return $res->{'Data'};
}




1;
# vim: ts=4
