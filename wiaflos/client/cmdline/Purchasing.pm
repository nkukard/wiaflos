# Purchasing functions
# Copyright (C) 2009-2014, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.




package wiaflos::client::cmdline::Purchasing;

use strict;
use warnings;

use wiaflos::constants;
use wiaflos::client::misc;
use wiaflos::client::soap;
use wiaflos::client::reportwriter;

use Math::BigFloat;
Math::BigFloat::precision(-2);


# Plugin info
our $pluginInfo = {
	Name 	=> "Purchasing",
	Menu 	=> [
		# Suppliers main menu option
		{
			MenuItem 	=> "Suppliers",
			Children 	=> [
				{
					MenuItem 	=> "Purchasing",
					Regex		=> "purchas(?:ing)?",
					Children	=> [
						{
							MenuItem 	=> "Create",
							Regex		=> "create",
							Desc		=> "Create a supplier invoice",
							Help		=> 'create supplier="<supplier ref>" invoice="<invoice number>" supplierinvoice="<suppliers invoice number>" issuedate="<issue date" duedate="<due date>" [orderm="<order number>"] [subtotal="<sub total>"] [taxtotal="<tax total>"] [total="<inv total>"] [note="<notes>]"',
							Function	=> \&createInvoice,
						},
						{
							MenuItem 	=> "LinkInventory",
							Regex		=> "linkinventory",
							Desc		=> "Link inventory item",
							Help		=> 'linkInventory invoice="<invoice number>" inventory="<inventory item code>" description="<description of item>" [serial="<item serial number>"] qty="<quantity>" [unit="<unit>"] unitprice="<unit price>" discount="<discount>" taxtypeid="<tax type id>" taxmode="<incl or excl>"',
							Function	=> \&linkInventoryItem,
						},
						{
							MenuItem 	=> "LinkExpense",
							Regex		=> "linkexpense",
							Desc		=> "Link expense item",
							Help		=> 'linkExpense invoice="<invoice number>" account="<expense account>" description="<description of item>" [serial="<item serial number>"] qty="<quantitiy>" [unit="<unit>"] unitprice="<unitprice>" discount="<discount in %>" taxtypeid="<tax type id>" taxmode="<incl or excl>"',
							Function	=> \&linkExpenseItem,
						},
#						{
#							MenuItem 	=> "RemoveItem",
#							Regex		=> "removeitem[ \$]",
#							Desc		=> "Remove item from invoice",
#							Help		=> 'removeitem invoice="<invoice number>" item="<item id>"',
#							Function	=> \&removeItem,
#						},
						{
							MenuItem 	=> "List",
							Regex		=> "list",
							Desc		=> "Display list of supplier invoices",
							Help		=> 'list [type="<all> or <unpaid>"]',
							Function	=> \&list,
						},
						{
							MenuItem 	=> "Show",
							Regex		=> "show",
							Desc		=> "Show supplier invoice",
							Help		=> 'show invoice="<invoice number>"',
							Function	=> \&show,
						},
						{
							MenuItem 	=> "ShowTransactions",
							Regex		=> "showtransactions",
							Desc		=> "Show supplier invoice transactions",
							Help		=> 'show invoice="<invoice number>"',
							Function	=> \&showTransactions,
						},
						{
							MenuItem 	=> "Post",
							Regex		=> "post",
							Desc		=> "Post supplier invoice",
							Help		=> 'post invoice="<invoice number>"',
							Function	=> \&post,
						},
					],

				},
			],
		},
	],
};



# List supplier invoices
sub list
{
	my ($OUT,@args) = @_;

	my $parms = parseArgs(@args);

	my $detail;
	$detail->{'Type'} = $parms->{'type'};
	my $res = soapCall($OUT,"wiaflos/server/api/Purchasing","getSupplierInvoices",$detail);

	if ($res->{'Result'} == RES_OK) {
		print swrite(<<'END', "ID", "InvNum", "SupplrID", "SupplierInvNum", "IssueDate", "DueDate", "TaxTotal", "InvTotal");
+========+==================+===========+======================================+===============+==============+=============+=============+
| @||||| | @||||||||||||||| | @|||||||| | @||||||||||||||||||||||||||||||||||| | @|||||||||||| | @||||||||||| | @|||||||||| | @|||||||||| |
+========+==================+===========+======================================+===============+==============+=============+=============+
END
		foreach my $item (@{$res->{'Data'}}) {
			print swrite(<<'END', $item->{'ID'}, $item->{'Number'}, $item->{'SupplierID'}, $item->{'SupplierInvoiceNumber'}, $item->{'IssueDate'}, defined($item->{'DueDate'}) ? $item->{'DueDate'} : '-', defined($item->{'TaxTotal'}) ? sprintf('%.2f',$item->{'TaxTotal'}) : '-', defined($item->{'Total'}) ? sprintf('%.2f',$item->{'Total'}) : '-');
| @<<<<< | @<<<<<<<<<<<<<<< | @<<<<<<<< | @<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< | @<<<<<<<<<<<< | @<<<<<<<<<<< | @<<<<<<<<<< | @>>>>>>>>>> |
END
			}
		print swrite(<<'END');
+========+==================+===========+======================================+===============+==============+=============+=============+
END
	} else {
		soapDebug($OUT,$res);
	}

	return $res->{'Result'};
}


# Create supplier invoice
sub createInvoice
{
	my ($OUT,@args) = @_;


	my $parms = parseArgs(@args);

	if (!defined($parms->{'supplier'})) {
		print($OUT "  => ERROR: Parameter 'supplier' not defined\n");
		return ERR_C_PARAM;
	}

	if (!defined($parms->{'invoice'})) {
		print($OUT "  => ERROR: Parameter 'invoice' not defined\n");
		return ERR_C_PARAM;
	}

	if (!defined($parms->{'supplierinvoice'})) {
		print($OUT "  => ERROR: Parameter 'supplierinvoice' not defined\n");
		return ERR_C_PARAM;
	}

	if (!defined($parms->{'issuedate'})) {
		print($OUT "  => ERROR: Parameter 'issuedate' not defined\n");
		return ERR_C_PARAM;
	}


	my $detail;
	$detail->{'SupplierCode'} = $parms->{'supplier'};
	$detail->{'Number'} = $parms->{'invoice'};
	$detail->{'SupplierInvoiceNumber'} = $parms->{'supplierinvoice'};
	$detail->{'IssueDate'} = $parms->{'issuedate'};
	$detail->{'DueDate'} = $parms->{'duedate'};
	$detail->{'OrderNumber'} = $parms->{'order'};
	$detail->{'Note'} = $parms->{'note'};
	my $res = soapCall($OUT,"wiaflos/server/api/Purchasing","createSupplierInvoice",$detail);
	if ($res->{'Result'} != RES_OK) {
		soapDebug($OUT,$res);
		return $res->{'Result'};
	}

	return $res->{'Data'};
}


# Link inventory item
sub linkInventoryItem
{
	my ($OUT,@args) = @_;


	my $parms = parseArgs(@args);

	if (!defined($parms->{'invoice'})) {
		print($OUT "  => ERROR: Parameter 'invoice' not defined\n");
		return ERR_C_PARAM;
	}

	if (!defined($parms->{'inventory'})) {
		print($OUT "  => ERROR: Parameter 'inventory' not defined\n");
		return ERR_C_PARAM;
	}

	if (!defined($parms->{'description'})) {
		print($OUT "  => ERROR: Parameter 'description' not defined\n");
		return ERR_C_PARAM;
	}

	if (!defined($parms->{'unitprice'})) {
		print($OUT "  => ERROR: Parameter 'unitprice' not defined\n");
		return ERR_C_PARAM;
	}

	if (!defined($parms->{'taxtypeid'})) {
		print($OUT "  => ERROR: Parameter 'taxtypeid' not defined\n");
		return ERR_C_PARAM;
	}

	if (!defined($parms->{'taxmode'})) {
		print($OUT "  => ERROR: Parameter 'taxmode' not defined\n");
		return ERR_C_PARAM;
	}


	my $detail;
	$detail->{'Number'} = $parms->{'invoice'};
	$detail->{'InventoryCode'} = $parms->{'inventory'};
	$detail->{'Description'} = $parms->{'description'};
	$detail->{'SerialNumber'} = $parms->{'serial'};
	$detail->{'Quantity'} = $parms->{'qty'};
	$detail->{'UnitPrice'} = $parms->{'unitprice'};
	$detail->{'Discount'} = $parms->{'discount'};
	$detail->{'TaxTypeID'} = $parms->{'taxtypeid'};
	$detail->{'TaxMode'} = $parms->{'taxmode'};
	my $res = soapCall($OUT,"wiaflos/server/api/Purchasing","linkSupplierInvoiceInventoryItem",$detail);
	if ($res->{'Result'} != RES_OK) {
		soapDebug($OUT,$res);
		return $res->{'Result'};
	}

	return $res->{'Data'};
}


# Link expsense item
sub linkExpenseItem
{
	my ($OUT,@args) = @_;


	my $parms = parseArgs(@args);

	if (!defined($parms->{'invoice'})) {
		print($OUT "  => ERROR: Parameter 'invoice' not defined\n");
		return ERR_C_PARAM;
	}

	if (!defined($parms->{'account'})) {
		print($OUT "  => ERROR: Parameter 'account' not defined\n");
		return ERR_C_PARAM;
	}

	if (!defined($parms->{'description'})) {
		print($OUT "  => ERROR: Parameter 'description' not defined\n");
		return ERR_C_PARAM;
	}

	if (!defined($parms->{'unitprice'})) {
		print($OUT "  => ERROR: Parameter 'unitprice' not defined\n");
		return ERR_C_PARAM;
	}

	if (!defined($parms->{'taxtypeid'})) {
		print($OUT "  => ERROR: Parameter 'taxtypeid' not defined\n");
		return ERR_C_PARAM;
	}

	if (!defined($parms->{'taxmode'})) {
		print($OUT "  => ERROR: Parameter 'taxmode' not defined\n");
		return ERR_C_PARAM;
	}


	my $detail;
	$detail->{'Number'} = $parms->{'invoice'};
	$detail->{'GLAccountNumber'} = $parms->{'account'};
	$detail->{'Description'} = $parms->{'description'};
	$detail->{'SerialNumber'} = $parms->{'serial'};
	$detail->{'Quantity'} = $parms->{'qty'};
	$detail->{'UnitPrice'} = $parms->{'unitprice'};
	$detail->{'Discount'} = $parms->{'discount'};
	$detail->{'TaxTypeID'} = $parms->{'taxtypeid'};
	$detail->{'TaxMode'} = $parms->{'taxmode'};
	my $res = soapCall($OUT,"wiaflos/server/api/Purchasing","linkSupplierInvoiceExpenseItem",$detail);
	if ($res->{'Result'} != RES_OK) {
		soapDebug($OUT,$res);
		return $res->{'Result'};
	}

	return $res->{'Data'};
}


# Show invoice
sub show
{
	my ($OUT,@args) = @_;


	my $parms = parseArgs(@args);

	if (!defined($parms->{'invoice'})) {
		print($OUT "  => ERROR: Parameter 'invoice' not defined\n");
		return ERR_C_PARAM;
	}

	my $detail;
	$detail->{'Number'} = $parms->{'invoice'};
	my $res = soapCall($OUT,"wiaflos/server/api/Purchasing","getSupplierInvoice",$detail);
	if ($res->{'Result'} != RES_OK) {
		soapDebug($OUT,$res);
		return $res->{'Result'};
	}
	my $invoice = $res->{'Data'};
	printf('Supplier invoice: %s
=========================
Tax Reference...........: %s
Supplier Invoice Number.: %s
Issue Date..............: %s
Due Date................: %s
Order Number............: %s
Discount Total..........: %s
Sub Total...............: %s
Tax Total...............: %s
Total...................: %s
GLTransactionID.........: %s
Paid....................: %s
Note:
%s

', $invoice->{'Number'}, $invoice->{'TaxReference'}, $invoice->{'SupplierInvoiceNumber'}, $invoice->{'IssueDate'}, $invoice->{'DueDate'},
		defined($invoice->{'OrderNumber'}) ? $invoice->{'OrderNumber'} : "", $invoice->{'DiscountTotal'}, $invoice->{'SubTotal'}, $invoice->{'TaxTotal'},
		$invoice->{'Total'}, defined($invoice->{'GLTransactionID'}) ? $invoice->{'GLTransactionID'} : "", ($invoice->{'Paid'} eq "1") ? "yes" : "no",
		defined($invoice->{'Note'}) ? $invoice->{'Note'} : "");


	my $subTotal = Math::BigFloat->new(0);
	my $taxTotal = Math::BigFloat->new(0);
	my $invTotal = Math::BigFloat->new(0);
	$res = soapCall($OUT,"wiaflos/server/api/Purchasing","getSupplierInvoiceItems",$detail);
	if ($res->{'Result'} == RES_OK) {
		print swrite(<<'END', "ID", "Description", "Invt/Exp", "Unit", "Qty", "UnitPrice", "Price", "TaxAmount", "Total");
Supplier invoice items....
+========+===========================================+===========+========+========+============+============+=============+==============+
| @||||| | @|||||||||||||||||||||||||||||||||||||||| | @|||||||| | @||||| | @||||| | @||||||||| | @||||||||| | @|||||||||| | @||||||||||| |
+========+===========================================+===========+========+========+============+============+=============+==============+
END
		foreach my $item (@{$res->{'Data'}}) {
			my $ref;
			if (defined($item->{'InventoryID'})) {
				$ref = "I:".$item->{'InventoryID'};
			} elsif (defined($item->{'GLAccountNumber'})) {
				$ref = "G:".$item->{'GLAccountNumber'};
			}


			print swrite(<<'END', $item->{'ID'}, $item->{'Description'}, $ref, defined($item->{'Unit'}) ? $item->{'Unit'} : '-', $item->{'Quantity'}, sprintf('%.2f',$item->{'UnitPrice'}), sprintf('%.2f',$item->{'Price'}), sprintf('%.2f',$item->{'TaxAmount'}), sprintf('%.2f',$item->{'Total'}));
| @<<<<< | @<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< | @<<<<<<<< | @||||| | @||||| | @>>>>>>>>> | @>>>>>>>>> | @>>>>>>>>>> | @>>>>>>>>>>> |
END
			$subTotal->badd($item->{'Price'});
			$taxTotal->badd($item->{'TaxAmount'});
			$invTotal->badd($item->{'Total'});
		}
		print swrite(<<'END', sprintf('%.2f',$subTotal->bstr()), sprintf('%.2f',$taxTotal->bstr()), sprintf('%.2f',$invTotal->bstr()));
+========+===========================================+===========+========+========+============+============+=============+==============+
|                                                                                               | @>>>>>>>>> | @>>>>>>>>>> | @>>>>>>>>>>> |
+===============================================================================================+============+=============+==============+
END
	} else {
		soapDebug($OUT,$res);
	}

	return $res->{'Result'};
}


# Show invoice transactions
sub showTransactions
{
	my ($OUT,@args) = @_;


	my $parms = parseArgs(@args);

	if (!defined($parms->{'invoice'})) {
		print($OUT "  => ERROR: Parameter 'invoice' not defined\n");
		return ERR_C_PARAM;
	}

	my $detail;
	$detail->{'Number'} = $parms->{'invoice'};

	# Pull in invoice
	my $res = soapCall($OUT,"wiaflos/server/api/Purchasing","getSupplierInvoice",$detail);
	if ($res->{'Result'} != RES_OK) {
		soapDebug($OUT,$res);
		return $res->{'Result'};
	}
	my $invoice = $res->{'Data'};

	$res = soapCall($OUT,"wiaflos/server/api/Purchasing","getSupplierInvoiceTransactions",$detail);
	if ($res->{'Result'} == RES_OK) {
		print swrite(<<'END', $detail->{'Number'}, "ID", "Info", "Amount", "Balance");
Invoice transactions for: @<<<<<<<<<<<<<<<<<<<
+========+===================================================+==============+
| @||||| | @|||||||||||||||||||||||||||||||||||||||||||||||| | @>>>>>>>>>>> |
+========+===================================================+==============+
END
		my $transBalance = Math::BigFloat->new();
		my $balance = Math::BigFloat->new($invoice->{'Total'});

		# Loop with items
		foreach my $item (@{$res->{'Data'}}) {
			my $extraInfo = "<UNKNOWN>";

			# Adjust balances
			$transBalance->badd($item->{'Amount'});

			# Get final balance
			$balance->badd($item->{'Amount'});

			# Check if this is a payment
			if (defined($item->{'Payment'})) {
				$extraInfo = sprintf("Payment '%s'",$item->{'Payment'}->{'Number'});

			# Or if this is a credit note
			} elsif (defined($item->{'SupplierCreditNote'})) {
				$extraInfo = sprintf("Credit note '%s'",$item->{'SupplierCreditNote'}->{'Number'});
			}

			print swrite(<<'END', $item->{'ID'}, $extraInfo, sprintf('%.2f',$item->{'Amount'}));
| @<<<<< | @<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< | @>>>>>>>>>>> |
END
		}

		print swrite(<<'END',sprintf('%.2f',$transBalance->bstr()),sprintf('%.2f',$invoice->{'Total'}),sprintf('%.2f',$balance->bstr()));
+========+===================================================+==============+
|                                        Transaction Balance | @>>>>>>>>>>> |
+------------------------------------------------------------+--------------+
|                                              Invoice Total | @>>>>>>>>>>> |
+------------------------------------------------------------+--------------+
|                                                    Balance | @>>>>>>>>>>> |
+========+===================================================+==============+
END
	} else {
		soapDebug($OUT,$res);
	}

	return $res->{'Result'};
}



# Post invoice
sub post
{
	my ($OUT,@args) = @_;


	my $parms = parseArgs(@args);

	if (!defined($parms->{'invoice'})) {
		print($OUT "  => ERROR: Parameter 'invoice' not defined\n");
		return ERR_C_PARAM;
	}
	my $detail;
	$detail->{'Number'} = $parms->{'invoice'};
	my $res = soapCall($OUT,"wiaflos/server/api/Purchasing","postSupplierInvoice",$detail);
	if ($res->{'Result'} != RES_OK) {
		soapDebug($OUT,$res);
	}

	return $res->{'Result'};
}





1;
# vim: ts=4
