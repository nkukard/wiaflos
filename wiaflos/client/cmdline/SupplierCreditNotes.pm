# Supplier credit notes functions
# Copyright (C) 2009-2014, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.




package wiaflos::client::cmdline::SupplierCreditNotes;

use strict;
use warnings;

use wiaflos::constants;
use wiaflos::client::misc;
use wiaflos::client::soap;
use wiaflos::client::reportwriter;

use Math::BigFloat;
Math::BigFloat::precision(-2);


# Plugin info
our $pluginInfo = {
	Name 	=> "SupplierCreditNotes",
	Menu 	=> [
		# Clients main menu option
		{
			MenuItem 	=> "Suppliers",
			Children 	=> [
				{
					MenuItem 	=> "CreditNotes",
					Children	=> [
						{
							MenuItem 	=> "Create",
							Regex		=> "create",
							Desc		=> "Create a supplier credit note",
							Help		=> 'create supplier="<supplier code>" number="<credit note number>" suppliercnnum=<supplier credit note number> issuedate="<issue date>" [supplierinvnum="<supplier invoice number>"] [note="<notes>]"',
							Function	=> \&create,
						},
						{
							MenuItem 	=> "LinkInventory",
							Regex		=> "linkinventory",
							Desc		=> "Link inventory item",
							Help		=> 'linkInventory creditnote="<credit note number>" inventory="<inventory item code>" [description="<item description>"] [serial="<item serial number>"] qty="<quantity>" [unit="<unit>"] unitprice="<unit price>" [expense="expense account number"]',
							Function	=> \&linkInventoryItem,
						},
						{
							MenuItem 	=> "LinkExpense",
							Regex		=> "linkexpense",
							Desc		=> "Link expense item",
							Help		=> 'linkExpense creditnote="<credit note number>" account="<expense account>" [description="<item description>"] [serial="<item serial number>"] qty="<quantitiy>" [unit="<unit>"] unitprice="<unitprice>" [expense="expense account number"]',
							Function	=> \&linkExpenseItem,
						},
						{
							MenuItem 	=> "Show",
							Regex		=> "show",
							Desc		=> "Show supplier credit note",
							Help		=> 'show creditnote="<credit note number>"',
							Function	=> \&show,
						},
						{
							MenuItem 	=> "Post",
							Regex		=> "post",
							Desc		=> "Post credit note",
							Help		=> 'post creditnote="<credit note number>"',
							Function	=> \&post,
						},
						{
							MenuItem 	=> "List",
							Regex		=> "list",
							Desc		=> "Display list of supplier credit notes",
							Help		=> 'list [type="<all> or <open>"]',
							Function	=> \&list,
						},
						{
							MenuItem 	=> "ShowTransactions",
							Regex		=> "showtransactions",
							Desc		=> "Show supplier credit note transactions",
							Help		=> 'show creditnote="<credit note number>"',
							Function	=> \&showTransactions,
						},
					],

				},
			],
		},
	],
};


# Create supplier credit note
sub create
{
	my ($OUT,@args) = @_;


	my $parms = parseArgs(@args);

	if (!defined($parms->{'supplier'})) {
		print($OUT "  => ERROR: Parameter 'supplier' not defined\n");
		return ERR_C_PARAM;
	}

	if (!defined($parms->{'number'})) {
		print($OUT "  => ERROR: Parameter 'number' not defined\n");
		return ERR_C_PARAM;
	}

	if (!defined($parms->{'suppliercnnum'})) {
		print($OUT "  => ERROR: Parameter 'suppliercnnum' not defined\n");
		return ERR_C_PARAM;
	}

	if (!defined($parms->{'issuedate'})) {
		print($OUT "  => ERROR: Parameter 'account' not defined\n");
		return ERR_C_PARAM;
	}


	my $detail;
	$detail->{'SupplierCode'} = $parms->{'supplier'};
	$detail->{'Number'} = $parms->{'number'};
	$detail->{'SupplierCreditNoteNumber'} = $parms->{'suppliercnnum'};
	$detail->{'IssueDate'} = $parms->{'issuedate'};
	$detail->{'SupplierInvoiceNumber'} = $parms->{'supplierinvnum'};
	$detail->{'Note'} = $parms->{'note'};
	my $res = soapCall($OUT,"wiaflos/server/api/SupplierCreditNotes","createSupplierCreditNote",$detail);
	if ($res->{'Result'} != RES_OK) {
		soapDebug($OUT,$res);
	}

	return $res->{'Result'};
}


# Link inventory item
sub linkInventoryItem
{
	my ($OUT,@args) = @_;


	my $parms = parseArgs(@args);

	if (!defined($parms->{'creditnote'})) {
		print($OUT "  => ERROR: Parameter 'creditnote' not defined\n");
		return ERR_C_PARAM;
	}

	if (!defined($parms->{'inventory'})) {
		print($OUT "  => ERROR: Parameter 'inventory' not defined\n");
		return ERR_C_PARAM;
	}

	if (!defined($parms->{'unitprice'})) {
		print($OUT "  => ERROR: Parameter 'unitprice' not defined\n");
		return ERR_C_PARAM;
	}


	my $detail;
	$detail->{'Number'} = $parms->{'creditnote'};
	$detail->{'InventoryCode'} = $parms->{'inventory'};
	$detail->{'Description'} = $parms->{'description'};
	$detail->{'SerialNumber'} = $parms->{'serial'};
	$detail->{'Quantity'} = $parms->{'qty'};
	$detail->{'UnitPrice'} = $parms->{'unitprice'};
	$detail->{'GLExpenseAccountNumber'} = $parms->{'expense'};
	my $res = soapCall($OUT,"wiaflos/server/api/SupplierCreditNotes","linkSupplierCreditNoteInventoryItem",$detail);
	if ($res->{'Result'} != RES_OK) {
		soapDebug($OUT,$res);
		return $res->{'Result'};
	}

	return $res->{'Data'};
}


# Link expsense item
sub linkExpenseItem
{
	my ($OUT,@args) = @_;


	my $parms = parseArgs(@args);

	if (!defined($parms->{'creditnote'})) {
		print($OUT "  => ERROR: Parameter 'creditnote' not defined\n");
		return ERR_C_PARAM;
	}

	if (!defined($parms->{'account'})) {
		print($OUT "  => ERROR: Parameter 'account' not defined\n");
		return ERR_C_PARAM;
	}

	if (!defined($parms->{'unitprice'})) {
		print($OUT "  => ERROR: Parameter 'unitprice' not defined\n");
		return ERR_C_PARAM;
	}


	my $detail;
	$detail->{'Number'} = $parms->{'creditnote'};
	$detail->{'GLAccountNumber'} = $parms->{'account'};
	$detail->{'Description'} = $parms->{'description'};
	$detail->{'SerialNumber'} = $parms->{'serial'};
	$detail->{'Quantity'} = $parms->{'qty'};
	$detail->{'UnitPrice'} = $parms->{'unitprice'};
	$detail->{'GLExpenseAccountNumber'} = $parms->{'expense'};
	my $res = soapCall($OUT,"wiaflos/server/api/SupplierCreditNotes","linkSupplierCreditNoteExpenseItem",$detail);
	if ($res->{'Result'} != RES_OK) {
		soapDebug($OUT,$res);
		return $res->{'Result'};
	}

	return $res->{'Data'};
}


# Post credit note
sub post
{
	my ($OUT,@args) = @_;


	my $parms = parseArgs(@args);

	if (!defined($parms->{'creditnote'})) {
		print($OUT "  => ERROR: Parameter 'creditnote' not defined\n");
		return ERR_C_PARAM;
	}
	my $detail;
	$detail->{'Number'} = $parms->{'creditnote'};
	my $res = soapCall($OUT,"wiaflos/server/api/SupplierCreditNotes","postSupplierCreditNote",$detail);
	if ($res->{'Result'} != RES_OK) {
		soapDebug($OUT,$res);
	}

	return $res->{'Result'};
}


# List supplier credit notes
sub list
{
	my ($OUT,@args) = @_;

	my $parms = parseArgs(@args);

	my $detail;
	$detail->{'Type'} = $parms->{'type'};
	my $res = soapCall($OUT,"wiaflos/server/api/SupplierCreditNotes","getSupplierCreditNotes",$detail);

	if ($res->{'Result'} == RES_OK) {
		print swrite(<<'END', "ID", "CN", "Supplier", "IssueDate", "TaxTotal", "CNTotal");
+========+===============+===========+===============+==============+=============+
| @||||| | @|||||||||||| | @|||||||| | @|||||||||||| | @||||||||||| | @|||||||||| |
+========+===============+===========+===============+==============+=============+
END
		foreach my $item (@{$res->{'Data'}}) {
			print swrite(<<'END', $item->{'ID'}, $item->{'Number'}, $item->{'SupplierCode'}, $item->{'IssueDate'}, defined($item->{'TaxTotal'}) ? sprintf('%.2f',$item->{'TaxTotal'}) : '-', defined($item->{'Total'}) ? sprintf('%.2f',$item->{'Total'}) : '-');
| @<<<<< | @<<<<<<<<<<<< | @<<<<<<<< | @<<<<<<<<<<<< | @<<<<<<<<<<< | @<<<<<<<<<< |
END
			}
		print swrite(<<'END');
+========+===============+===========+===============+==============+=============+
END
	} else {
		soapDebug($OUT,$res);
	}

	return $res->{'Result'};
}




# Show credit note
sub show
{
	my ($OUT,@args) = @_;


	my $parms = parseArgs(@args);

	if (!defined($parms->{'creditnote'})) {
		print($OUT "  => ERROR: Parameter 'creditnote' not defined\n");
		return ERR_C_PARAM;
	}

	my $detail;
	$detail->{'Number'} = $parms->{'creditnote'};
	my $res = soapCall($OUT,"wiaflos/server/api/SupplierCreditNotes","getSupplierCreditNote",$detail);
	if ($res->{'Result'} != RES_OK) {
		soapDebug($OUT,$res);
		return $res->{'Result'};
	}

	my $subTotal = Math::BigFloat->new();
	my $taxTotal = Math::BigFloat->new();
	my $invTotal = Math::BigFloat->new();
	$res = soapCall($OUT,"wiaflos/server/api/SupplierCreditNotes","getSupplierCreditNoteItems",$detail);
	if ($res->{'Result'} == RES_OK) {
		print swrite(<<'END', "ID", "Description", "Invt/Exp", "Unit", "Qty", "UnitPrice", "Price", "TaxAmount", "Total");
+========+===========================================+===========+========+========+============+============+=============+==============+
| @||||| | @|||||||||||||||||||||||||||||||||||||||| | @|||||||| | @||||| | @||||| | @||||||||| | @||||||||| | @|||||||||| | @||||||||||| |
+========+===========================================+===========+========+========+============+============+=============+==============+
END
		foreach my $item (@{$res->{'Data'}}) {
			my $ref;
			if (defined($item->{'InventoryID'})) {
				$ref = "I:".$item->{'InventoryID'};
			} elsif (defined($item->{'GLAccountNumber'})) {
				$ref = "G:".$item->{'GLAccountNumber'};
			}

			print swrite(<<'END', $item->{'ID'}, $item->{'Description'}, $ref, defined($item->{'Unit'}) ? $item->{'Unit'} : '-', $item->{'Quantity'}, sprintf('%.2f',$item->{'UnitPrice'}), sprintf('%.2f',$item->{'Price'}), sprintf('%.2f',$item->{'TaxAmount'}), sprintf('%.2f',$item->{'TotalPrice'}));
| @<<<<< | @<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< | @<<<<<<<< | @||||| | @||||| | @>>>>>>>>> | @>>>>>>>>> | @>>>>>>>>>> | @>>>>>>>>>>> |
END
			$subTotal->badd($item->{'Price'});
			$taxTotal->badd($item->{'TaxAmount'});
			$invTotal->badd($item->{'TotalPrice'});
			}
			print swrite(<<'END', sprintf('%.2f',$subTotal->bstr()), sprintf('%.2f',$taxTotal->bstr()), sprintf('%.2f',$invTotal->bstr()));
+========+===========================================+===========+========+========+============+============+=============+==============+
|                                                                                               | @>>>>>>>>> | @>>>>>>>>>> | @>>>>>>>>>>> |
+===============================================================================================+============+=============+==============+
END
	} else {
		soapDebug($OUT,$res);
	}

	return $res->{'Result'};
}


# Show invoice transactions
sub showTransactions
{
	my ($OUT,@args) = @_;


	my $parms = parseArgs(@args);

	if (!defined($parms->{'creditnote'})) {
		print($OUT "  => ERROR: Parameter 'creditnote' not defined\n");
		return ERR_C_PARAM;
	}

	my $detail;
	$detail->{'Number'} = $parms->{'creditnote'};

	# Pull in invoice
	my $res = soapCall($OUT,"wiaflos/server/api/SupplierCreditNotes","getSupplierCreditNote",$detail);
	if ($res->{'Result'} != RES_OK) {
		soapDebug($OUT,$res);
		return $res->{'Result'};
	}
	my $invoice = $res->{'Data'};

	$res = soapCall($OUT,"wiaflos/server/api/SupplierCreditNotes","getSupplierCreditNoteTransactions",$detail);
	if ($res->{'Result'} == RES_OK) {
		print swrite(<<'END', $detail->{'Number'}, "ID", "Info", "Amount", "Balance");
Credit note transactions for: @<<<<<<<<<<<<<<<<<<<
+========+===================================================+==============+
| @||||| | @|||||||||||||||||||||||||||||||||||||||||||||||| | @>>>>>>>>>>> |
+========+===================================================+==============+
END
		my $transBalance = Math::BigFloat->new();
		my $balance = Math::BigFloat->new($invoice->{'Total'});

		# Loop with items
		foreach my $item (@{$res->{'Data'}}) {
			my $extraInfo = "<UNKNOWN>";

			# Adjust balances
			$transBalance->badd($item->{'Amount'});

			# Get final balance
			$balance->badd($item->{'Amount'});

			# Check if this is a payment
			if (defined($item->{'SupplierReceipt'})) {
				$extraInfo = sprintf("Supplier receipt '%s'",$item->{'SupplierReceipt'}->{'Number'});

			# Or if this is a credit note
			} elsif (defined($item->{'SupplierInvoiceTransaction'})) {
				$extraInfo = sprintf("Supplier invoice transaction '%s'",$item->{'SupplierInvoiceTransactionID'});
			}

			print swrite(<<'END', $item->{'ID'}, $extraInfo, sprintf('%.2f',$item->{'Amount'}));
| @<<<<< | @<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< | @>>>>>>>>>>> |
END
		}

		print swrite(<<'END',sprintf('%.2f',$transBalance->bstr()),sprintf('%.2f',$invoice->{'Total'}),sprintf('%.2f',$balance->bstr()));
+========+===================================================+==============+
|                                        Transaction Balance | @>>>>>>>>>>> |
+------------------------------------------------------------+--------------+
|                                          Credit Note Total | @>>>>>>>>>>> |
+------------------------------------------------------------+--------------+
|                                                    Balance | @>>>>>>>>>>> |
+========+===================================================+==============+
END
	} else {
		soapDebug($OUT,$res);
	}

	return $res->{'Result'};
}





1;
# vim: ts=4

