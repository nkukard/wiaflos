# Supplier functions
# Copyright (C) 2009-2014, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.




package wiaflos::client::cmdline::Suppliers;

use strict;
use warnings;


use wiaflos::constants;
use wiaflos::client::misc;
use wiaflos::client::soap;
use wiaflos::client::reportwriter;

# Plugin info
our $pluginInfo = {
	Name 	=> "Suppliers",
	Menu 	=> [
		# Suppliers main menu option
		{
			MenuItem 	=> "Suppliers",
			Children 	=> [
				{
					MenuItem 	=> "Create",
					Regex		=> "create",
					Desc		=> "Create a supplier",
					Help		=> 'create code="<supplier code>" name="<supplier name>" [reg="<registration num>"] [taxref="<tax reference number>"] account="<GL account>" [contactperson="<contact person name>"] [createsubaccount="<y to create sub account>"]',
					Function	=> \&create,
				},
				{
					MenuItem 	=> "LinkAddr",
					Regex		=> "linkaddr(?:ess)?",
					Desc		=> "Link in an address",
					Help		=> 'linkAddress supplier="<supplier code>" type="<billing or shipping>" address="<address>"',
					Function	=> \&linkAddress,
				},
				{
					MenuItem 	=> "LinkEmail",
					Regex		=> "linkemail(?:address)?",
					Desc		=> "Link in an email address",
					Help		=> 'linkEmailAddress supplier="<supplier code>" type="<accounts or general>" address="<address>"',
					Function	=> \&linkEmailAddress,
				},
				{
					MenuItem 	=> "LinkPhNum",
					Regex		=> "link(?:phnum|phonenumber)",
					Desc		=> "Link in a phone number",
					Help		=> 'linkPhoneNumber supplier="<supplier code>" type="<phone or fax>" number="<phone number>" [name="<name>"]',
					Function	=> \&linkPhoneNumber,
				},
				{
					MenuItem 	=> "Remove",
					Regex		=> "remove",
					Desc		=> "Remove supplier",
					Help		=> 'remove supplier="<supplier code>"',
					Function	=> \&remove,
				},
				{
					MenuItem 	=> "List",
					Regex		=> "list",
					Desc		=> "Display list of suppliers",
					Help		=> 'list',
					Function	=> \&list,
				},
				{
					MenuItem 	=> "Show",
					Regex		=> "show",
					Desc		=> "Show supplier",
					Help		=> 'show supplier="<supplier code>"',
					Function	=> \&show,
				},
				{
					MenuItem 	=> "ShowGL",
					Regex		=> "showgl",
					Desc		=> "Show GL account entries",
					Help		=> 'showGL supplier="<supplier code>" [start="<start date>"] [end="<end date>"] [balance-brought-forward="<y|n>"]',
					Function	=> \&showGLAccountEntries,
				},
			],
		},
	],
};



# List suppliers
sub list
{
	my ($OUT,@args) = @_;

	if (@args > 0) {
		print($OUT "  => ERROR: Too many arguments provided\n");
		return ERR_C_PARAM;
	}

	my $res = soapCall($OUT,"wiaflos/server/api/Suppliers","getSuppliers");

	if ($res->{'Result'} == RES_OK) {
		print $OUT swrite(<<'END', "ID", "Code", "Name", "RegNumber", "TaxRef", "GLAccount");
+========+===========+===============================================+=====================+================+=================+
| @||||| | @|||||||| | @|||||||||||||||||||||||||||||||||||||||||||| | @|||||||||||||||||| | @||||||||||||| | @|||||||||||||| |
+========+===========+===============================================+=====================+================+=================+
END
		foreach my $item (@{$res->{'Data'}}) {
			print $OUT swrite(<<'END', $item->{'ID'}, $item->{'Code'}, $item->{'Name'}, defined($item->{'RegNumber'}) ? $item->{'RegNumber'} : '-', defined($item->{'TaxReference'}) ? $item->{'TaxReference'} : '-', $item->{'GLAccountNumber'});
| @<<<<< | @<<<<<<<< | @<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< | @<<<<<<<<<<<<<<<<<< | @<<<<<<<<<<<<< | @<<<<<<<<<<<<<< |
END
			}
			print $OUT swrite(<<'END');
+========+===========+===============================================+=====================+================+=================+
END
	} else {
		soapDebug($OUT,$res);
	}

	return $res->{'Result'};
}


# Create supplier
sub create
{
	my ($OUT,@args) = @_;


	my $parms = parseArgs(@args);

	if (!defined($parms->{'code'})) {
		print($OUT "  => ERROR: Parameter 'code' not defined\n");
		return ERR_C_PARAM;
	}

	if (!defined($parms->{'name'})) {
		print($OUT "  => ERROR: Parameter 'name' not defined\n");
		return ERR_C_PARAM;
	}

	if (!defined($parms->{'account'})) {
		print($OUT "  => ERROR: Parameter 'account' not defined\n");
		return ERR_C_PARAM;
	}


	my $detail;
	$detail->{'Code'} = $parms->{'code'};
	$detail->{'Name'} = $parms->{'name'};
	$detail->{'GLAccountNumber'} = $parms->{'account'};
	$detail->{'RegNumber'} = $parms->{'reg'};
	$detail->{'TaxReference'} = $parms->{'taxref'};
	$detail->{'CreateSubAccount'} = $parms->{'createsubaccount'};
	my $res = soapCall($OUT,"wiaflos/server/api/Suppliers","createSupplier",$detail);

	if ($res->{'Result'} != RES_OK) {
		soapDebug($OUT,$res);
		return $res->{'Result'};
	}

	return $res->{'Data'};
}


# Link address
sub linkAddress
{
	my ($OUT,@args) = @_;


	my $parms = parseArgs(@args);

	if (!defined($parms->{'supplier'})) {
		print($OUT "  => ERROR: Parameter 'supplier' not defined\n");
		return ERR_C_PARAM;
	}

	if (!defined($parms->{'type'})) {
		print($OUT "  => ERROR: Parameter 'type' not defined\n");
		return ERR_C_PARAM;
	}

	if (!defined($parms->{'address'})) {
		print($OUT "  => ERROR: Parameter 'address' not defined\n");
		return ERR_C_PARAM;
	}

	my $detail;
	$detail->{'Code'} = $parms->{'supplier'};
	$detail->{'Type'} = $parms->{'type'};
	$detail->{'Address'} = $parms->{'address'};
	my $res = soapCall($OUT,"wiaflos/server/api/Suppliers","linkSupplierAddress",$detail);

	if ($res->{'Result'} != RES_OK) {
		soapDebug($OUT,$res);
		return $res->{'Result'};
	}

	return $res->{'Data'};
}


# Link phone number
sub linkPhoneNumber
{
	my ($OUT,@args) = @_;


	my $parms = parseArgs(@args);

	if (!defined($parms->{'supplier'})) {
		print($OUT "  => ERROR: Parameter 'supplier' not defined\n");
		return ERR_C_PARAM;
	}

	if (!defined($parms->{'type'})) {
		print($OUT "  => ERROR: Parameter 'type' not defined\n");
		return ERR_C_PARAM;
	}

	if (!defined($parms->{'number'})) {
		print($OUT "  => ERROR: Parameter 'number' not defined\n");
		return ERR_C_PARAM;
	}


	my $detail;
	$detail->{'Code'} = $parms->{'supplier'};
	$detail->{'Type'} = $parms->{'type'};
	$detail->{'Number'} = $parms->{'number'};
	$detail->{'Name'} = $parms->{'name'};
	my $res = soapCall($OUT,"wiaflos/server/api/Suppliers","linkSupplierPhoneNumber",$detail);

	if ($res->{'Result'} != RES_OK) {
		soapDebug($OUT,$res);
		return $res->{'Result'};
	}

	return $res->{'Data'};
}


# Show supplier
sub show
{
	my ($OUT,@args) = @_;

	my $parms = parseArgs(@args);

	if (!defined($parms->{'supplier'})) {
		return ERR_C_PARAM;
	}

	my $detail;
	$detail->{'Code'} = $parms->{'supplier'};

	my $res;

	$res = soapCall($OUT,"wiaflos/server/api/Suppliers","getSupplier",$detail);
	if ($res->{'Result'} != RES_OK) {
		soapDebug($OUT,$res);
		return $res->{'Result'};
	}
	my $supplier = $res->{'Data'};

	$res = soapCall($OUT,"wiaflos/server/api/Suppliers","getSupplierAddresses",$detail);
	if ($res->{'Result'} != RES_OK) {
		soapDebug($OUT,$res);
		return $res->{'Result'};
	}
	my $addies = $res->{'Data'};

	$res = soapCall($OUT,"wiaflos/server/api/Suppliers","getSupplierPhoneNumbers",$detail);
	if ($res->{'Result'} != RES_OK) {
		soapDebug($OUT,$res);
		return $res->{'Result'};
	}
	my $phones = $res->{'Data'};

	$res = soapCall($OUT,"wiaflos/server/api/Suppliers","getSupplierEmailAddresses",$detail);
	if ($res->{'Result'} != RES_OK) {
		soapDebug($OUT,$res);
		return $res->{'Result'};
	}
	my $emailAddies = $res->{'Data'};

	print($OUT "
ID.....: ".$supplier->{'ID'}."
Code...: ".$supplier->{'Code'}."
Name...: ".$supplier->{'Name'}."
Reg....: ".(defined($supplier->{'RegNumber'}) ? $supplier->{'RegNumber'} : "-")."
TaxRef.: ".(defined($supplier->{'TaxReference'}) ? $supplier->{'TaxReference'} : "-")."
GLAcc..: ".$supplier->{'GLAccountNumber'}."

");

foreach my $item (@{$addies}) {
	print($OUT "
Address: ".$item->{'Type'}."
".$item->{'Address'}."
");
}

foreach my $item (@{$phones}) {
	print($OUT "
Phone Number: ".$item->{'Type'}."
".$item->{'Number'}."
");
}

foreach my $item (@{$emailAddies}) {
	print($OUT "
Email Address: ".$item->{'Type'}."
".$item->{'Address'}."
");
}

	return RES_OK;
}


# Remove supplier
sub remove
{
	my ($OUT,@args) = @_;


	my $parms = parseArgs(@args);

	if (!defined($parms->{'supplier'})) {
		print($OUT "  => ERROR: Parameter 'supplier' not defined\n");
		return ERR_C_PARAM;
	}

	my $detail;
	$detail->{'Code'} = $parms->{'supplier'};
	my $res = soapCall($OUT,"wiaflos/server/api/Suppliers","removeSupplier",$detail);
	if ($res->{'Result'} != RES_OK) {
		soapDebug($OUT,$res);
	}

	return $res->{'Result'};
}



# Show supplier GL account entries
sub showGLAccountEntries
{
	my ($OUT,@args) = @_;


	my $parms = parseArgs(@args);

	if (!defined($parms->{'supplier'})) {
		print($OUT "  => ERROR: Parameter 'supplier' not defined\n");
		return ERR_C_PARAM;
	}
	my $detail;
	$detail->{'Code'} = $parms->{'supplier'};
	$detail->{'StartDate'} = $parms->{'start'};
	$detail->{'EndDate'} = $parms->{'end'};
	$detail->{'BalanceBroughtForward'} = $parms->{'balance-brought-forward'};
	my $res = soapCall($OUT,"wiaflos/server/api/Suppliers","getSupplierGLAccountEntries",$detail);

	if ($res->{'Result'} == RES_OK) {
		print $OUT swrite(<<'END', "ID", "Date", "Ref", "Amount", "Balance");
+===========+============+================================================================================+==============+================+
| @|||||||| | @||||||||| | @||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| | @||||||||||| | @||||||||||||| |
+===========+============+================================================================================+==============+================+
END
		# Sort data
		my @sorted = sort {$a->{'TransactionDate'} cmp $b->{'TransactionDate'}} @{$res->{'Data'}};

		my $balance = Math::BigFloat->new(0);

		foreach my $entry (@sorted) {
			$balance->badd($entry->{'Amount'});
			print $OUT swrite(<<'END', $entry->{'ID'}, $entry->{'TransactionDate'}, $entry->{'Reference'} ? $entry->{'Reference'} : $entry->{'TransactionReference'}, sprintf('%.2f',$entry->{'Amount'}),sprintf('%.2f',$balance));
| @<<<<<<<< | @<<<<<<<<< | @<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< | @>>>>>>>>>>> | @>>>>>>>>>>>>> |
END
		}
		print $OUT swrite(<<'END');
+===========+============+================================================================================+==============+================+
END
	} else {
		soapDebug($OUT,$res);
	}

	return $res->{'Result'};
}





1;
# vim: ts=4
