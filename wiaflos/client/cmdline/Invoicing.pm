# Invoicing functions
# Copyright (C) 2009-2014, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.




package wiaflos::client::cmdline::Invoicing;

use strict;
use warnings;

use wiaflos::constants;
use wiaflos::client::misc;
use wiaflos::client::soap;
use wiaflos::client::reportwriter;

use Math::BigFloat;
Math::BigFloat::precision(-2);


# Plugin info
our $pluginInfo = {
	Name 	=> "Invoicing",
	Menu 	=> [
		# Clients main menu option
		{
			MenuItem 	=> "Clients",
			Children 	=> [
				{
					MenuItem 	=> "Invoicing",
					Children	=> [
						{
							MenuItem 	=> "Create",
							Regex		=> "create",
							Desc		=> "Create a client invoice",
							Help		=> 'create client="<client code>" invoice="<invoice number>" [shipaddress="<shipping address>"] issuedate="<issue date" duedate="<due date>" [ordernumber="<order number>"] [note="<notes>]"',
							Function	=> \&createInvoice,
						},
						{
							MenuItem 	=> "LinkInventory",
							Regex		=> "linkinventory",
							Desc		=> "Link inventory item",
							Help		=> 'linkInventory invoice="<invoice number>" inventory="<inventory item code>" [description="<description of item>"] [serial="<serial number of item>"] qty="<quantity>" [unitprice="<unit price>"] [discount="<discount>"] [taxtype="<tax type id>"]',
							Function	=> \&linkInventoryItem,
						},
#						{
#							MenuItem 	=> "RemoveItem",
#							Regex		=> "removeitem ",
#							Desc		=> "Remove item from invoice",
#							Help		=> 'removeitem invoice="<invoice number>" itemid="<item id>"',
#							Function	=> \&removeItem,
#						},
						{
							MenuItem 	=> "List",
							Regex		=> "list",
							Desc		=> "Display list of client invoices",
							Help		=> 'list [type="<all> or <unpaid>"]',
							Function	=> \&list,
						},
						{
							MenuItem 	=> "Send",
							Regex		=> "send",
							Desc		=> "Send client invoice",
							Help		=> 'send invoice="<invoice number>" sendto="<email[:addy1,addy2...] or file:filename>"',
							Function	=> \&send,
						},
						{
							MenuItem 	=> "Show",
							Regex		=> "show",
							Desc		=> "Show client invoice",
							Help		=> 'show invoice="<invoice number>"',
							Function	=> \&show,
						},
						{
							MenuItem 	=> "ShowTransactions",
							Regex		=> "showtransactions",
							Desc		=> "Show invoice transactions",
							Help		=> 'show invoice="<invoice number>"',
							Function	=> \&showTransactions,
						},
						{
							MenuItem 	=> "Post",
							Regex		=> "post",
							Desc		=> "Post client invoice",
							Help		=> 'post invoice="<invoice number>"',
							Function	=> \&post,
						},
					],

				},
			],
		},
	],
};




# List client invoices
sub list
{
	my ($OUT,@args) = @_;

	my $parms = parseArgs(@args);

	my $detail;
	$detail->{'Type'} = $parms->{'type'};
	my $res = soapCall($OUT,"wiaflos/server/api/Invoicing","getInvoices",$detail);

	if ($res->{'Result'} == RES_OK) {
		print swrite(<<'END', "ID", "Invoice", "Client", "IssueDate", "DueDate", "TaxTotal", "InvTotal");
+========+====================+===========+===============+==============+=============+===========+
| @||||| | @||||||||||||||||| | @|||||||| | @|||||||||||| | @||||||||||| | @|||||||||| | @|||||||| |
+========+====================+===========+===============+==============+=============+===========+
END
		foreach my $item (@{$res->{'Data'}}) {
			print swrite(<<'END', $item->{'ID'}, $item->{'Number'}, $item->{'ClientID'}, $item->{'IssueDate'}, defined($item->{'DueDate'}) ? $item->{'DueDate'} : '-', defined($item->{'TaxTotal'}) ? sprintf('%.2f',$item->{'TaxTotal'}) : '-', defined($item->{'Total'}) ? sprintf('%.2f',$item->{'Total'}) : '-');
| @<<<<< | @<<<<<<<<<<<<<<<<< | @<<<<<<<< | @<<<<<<<<<<<< | @<<<<<<<<<<< | @<<<<<<<<<< | @>>>>>>>> |
END
			}
		print swrite(<<'END');
+========+====================+===========+===============+==============+=============+===========+
END
	} else {
		soapDebug($OUT,$res);
	}

	return $res->{'Result'};
}


# Create client invoice
sub createInvoice
{
	my ($OUT,@args) = @_;


	my $parms = parseArgs(@args);

	if (!defined($parms->{'client'})) {
		print($OUT "  => ERROR: Parameter 'client' not defined\n");
		return ERR_C_PARAM;
	}

	if (!defined($parms->{'invoice'})) {
		print($OUT "  => ERROR: Parameter 'invoice' not defined\n");
		return ERR_C_PARAM;
	}

	if (!defined($parms->{'issuedate'})) {
		print($OUT "  => ERROR: Parameter 'account' not defined\n");
		return ERR_C_PARAM;
	}

	if (!defined($parms->{'duedate'})) {
		print($OUT "  => ERROR: Parameter 'account' not defined\n");
		return ERR_C_PARAM;
	}


	my $detail;
	$detail->{'ClientCode'} = $parms->{'client'};
	$detail->{'Number'} = $parms->{'invoice'};
	$detail->{'ShippingAddress'} = $parms->{'shipaddress'};
	$detail->{'IssueDate'} = $parms->{'issuedate'};
	$detail->{'DueDate'} = $parms->{'duedate'};
	$detail->{'OrderNumber'} = $parms->{'ordernumber'};
	$detail->{'Note'} = $parms->{'note'};
	my $res = soapCall($OUT,"wiaflos/server/api/Invoicing","createInvoice",$detail);
	if ($res->{'Result'} != RES_OK) {
		soapDebug($OUT,$res);
	}

	return $res->{'Result'};
}


# Link inventory item
sub linkInventoryItem
{
	my ($OUT,@args) = @_;


	my $parms = parseArgs(@args);

	if (!defined($parms->{'invoice'})) {
		print($OUT "  => ERROR: Parameter 'invoice' not defined\n");
		return ERR_C_PARAM;
	}

	if (!defined($parms->{'inventory'})) {
		print($OUT "  => ERROR: Parameter 'inventory' not defined\n");
		return ERR_C_PARAM;
	}


	my $detail;
	$detail->{'Number'} = $parms->{'invoice'};
	$detail->{'InventoryCode'} = $parms->{'inventory'};
	$detail->{'Description'} = $parms->{'description'};
	$detail->{'SerialNumber'} = $parms->{'serial'};
	$detail->{'Quantity'} = $parms->{'qty'};
	$detail->{'UnitPrice'} = $parms->{'unitprice'};
	$detail->{'Discount'} = $parms->{'discount'};
	$detail->{'TaxTypeID'} = $parms->{'taxtype'};
	my $res = soapCall($OUT,"wiaflos/server/api/Invoicing","linkInvoiceItem",$detail);
	if ($res->{'Result'} != RES_OK) {
		soapDebug($OUT,$res);
	}

	return $res->{'Result'};
}


# Show invoice
sub show
{
	my ($OUT,@args) = @_;


	my $parms = parseArgs(@args);

	if (!defined($parms->{'invoice'})) {
		print($OUT "  => ERROR: Parameter 'invoice' not defined\n");
		return ERR_C_PARAM;
	}

	my $detail;
	$detail->{'Number'} = $parms->{'invoice'};
	my $res = soapCall($OUT,"wiaflos/server/api/Invoicing","getInvoice",$detail);
	if ($res->{'Result'} != RES_OK) {
		soapDebug($OUT,$res);
		return $res->{'Result'};
	}

	my $invoice = $res->{'Data'};
	printf('Invoice: %s
=========================
Tax Reference...........: %s
Issue Date..............: %s
Due Date................: %s
Order Number............: %s
Discount Total..........: %s
Sub Total...............: %s
Tax Total...............: %s
Total...................: %s
GLTransactionID.........: %s
Paid....................: %s
Note:
%s

', $invoice->{'Number'}, $invoice->{'TaxReference'}, $invoice->{'IssueDate'}, $invoice->{'DueDate'},
		defined($invoice->{'OrderNumber'}) ? $invoice->{'OrderNumber'} : "", $invoice->{'DiscountTotal'}, $invoice->{'SubTotal'}, $invoice->{'TaxTotal'},
		$invoice->{'Total'}, defined($invoice->{'GLTransactionID'}) ? $invoice->{'GLTransactionID'} : "", ($invoice->{'Paid'} eq "1") ? "yes" : "no",
		defined($invoice->{'Note'}) ? $invoice->{'Note'} : "");

	my $subTotal = Math::BigFloat->new(0);
	my $taxTotal = Math::BigFloat->new(0);
	my $invTotal = Math::BigFloat->new(0);
	$res = soapCall($OUT,"wiaflos/server/api/Invoicing","getInvoiceItems",$detail);
	if ($res->{'Result'} == RES_OK) {
		print swrite(<<'END', "ID", "Description", "Invt/Exp", "Unit", "Qty", "UnitPrice", "Price", "TaxAmount", "Total");
+========+===========================================+===========+========+========+============+============+=============+==============+
| @||||| | @|||||||||||||||||||||||||||||||||||||||| | @|||||||| | @||||| | @||||| | @||||||||| | @||||||||| | @|||||||||| | @||||||||||| |
+========+===========================================+===========+========+========+============+============+=============+==============+
END
		foreach my $item (@{$res->{'Data'}}) {
			print swrite(<<'END', $item->{'ID'}, $item->{'Description'}, $item->{'InventoryID'}, defined($item->{'Unit'}) ? $item->{'Unit'} : '-', $item->{'Quantity'}, $item->{'UnitPrice'}, $item->{'Price'}, sprintf('%.2f',$item->{'TaxAmount'}), sprintf('%.2f',$item->{'TotalPrice'}));
| @<<<<< | @<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< | @<<<<<<<< | @||||| | @||||| | @>>>>>>>>> | @>>>>>>>>> | @>>>>>>>>>> | @>>>>>>>>>>> |
END
			$subTotal->badd($item->{'Price'});
			$taxTotal->badd($item->{'TaxAmount'});
			$invTotal->badd($item->{'TotalPrice'});
			}
			print swrite(<<'END', $subTotal->bstr(), $taxTotal->bstr(), $invTotal->bstr());
+========+===========================================+===========+========+========+============+============+=============+==============+
|                                                                                               | @>>>>>>>>> | @>>>>>>>>>> | @>>>>>>>>>>> |
+===============================================================================================+============+=============+==============+
END
	} else {
		soapDebug($OUT,$res);
	}

	return $res->{'Result'};
}


# Post invoice
sub post
{
	my ($OUT,@args) = @_;


	my $parms = parseArgs(@args);

	if (!defined($parms->{'invoice'})) {
		print($OUT "  => ERROR: Parameter 'invoice' not defined\n");
		return ERR_C_PARAM;
	}
	my $detail;
	$detail->{'Number'} = $parms->{'invoice'};
	my $res = soapCall($OUT,"wiaflos/server/api/Invoicing","postInvoice",$detail);
	if ($res->{'Result'} != RES_OK) {
		soapDebug($OUT,$res);
	}

	return $res->{'Result'};
}


# Send invoice
sub send
{
	my ($OUT,@args) = @_;


	my $parms = parseArgs(@args);

	if (!defined($parms->{'invoice'})) {
		print($OUT "  => ERROR: Parameter 'invoice' not defined\n");
		return ERR_C_PARAM;
	}

	if (!defined($parms->{'sendto'})) {
		print($OUT "  => ERROR: Parameter 'sendto' not defined\n");
		return ERR_C_PARAM;
	}

	my $detail;
	$detail->{'Number'} = $parms->{'invoice'};
	$detail->{'SendTo'} = $parms->{'sendto'};
	my $res = soapCall($OUT,"wiaflos/server/api/Invoicing","sendInvoice",$detail);
	if ($res->{'Result'} != RES_OK) {
		soapDebug($OUT,$res);
		return $res->{'Result'};
	}

	return $res->{'Data'};
}


# Show invoice transactions
sub showTransactions
{
	my ($OUT,@args) = @_;


	my $parms = parseArgs(@args);

	if (!defined($parms->{'invoice'})) {
		print($OUT "  => ERROR: Parameter 'invoice' not defined\n");
		return ERR_C_PARAM;
	}

	my $detail;
	$detail->{'Number'} = $parms->{'invoice'};

	# Pull in invoice
	my $res = soapCall($OUT,"wiaflos/server/api/Invoicing","getInvoice",$detail);
	if ($res->{'Result'} != RES_OK) {
		soapDebug($OUT,$res);
		return $res->{'Result'};
	}
	my $invoice = $res->{'Data'};

	$res = soapCall($OUT,"wiaflos/server/api/Invoicing","getInvoiceTransactions",$detail);
	if ($res->{'Result'} == RES_OK) {
		print swrite(<<'END', $detail->{'Number'}, "ID", "Info", "Amount", "Balance");
Invoice transactions for: @<<<<<<<<<<<<<<<<<<<
+========+===================================================+==============+
| @||||| | @|||||||||||||||||||||||||||||||||||||||||||||||| | @>>>>>>>>>>> |
+========+===================================================+==============+
END
		my $transBalance = Math::BigFloat->new();
		my $balance = Math::BigFloat->new($invoice->{'Total'});

		# Loop with items
		foreach my $item (@{$res->{'Data'}}) {
			my $extraInfo = "<UNKNOWN>";

			# Adjust balances
			$transBalance->badd($item->{'Amount'});

			# Get final balance
			$balance->badd($item->{'Amount'});

			# Check if this is a payment
			if (defined($item->{'Receipt'})) {
				$extraInfo = sprintf("Receipt '%s'",$item->{'Receipt'}->{'Number'});

			# Or if this is a credit note
			} elsif (defined($item->{'CreditNote'})) {
				$extraInfo = sprintf("Credit note '%s'",$item->{'CreditNote'}->{'Number'});
			}

			print swrite(<<'END', $item->{'ID'}, $extraInfo, sprintf('%.2f',$item->{'Amount'}));
| @<<<<< | @<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< | @>>>>>>>>>>> |
END
		}

		print swrite(<<'END',sprintf('%.2f',$transBalance->bstr()),sprintf('%.2f',$invoice->{'Total'}),sprintf('%.2f',$balance->bstr()));
+========+===================================================+==============+
|                                        Transaction Balance | @>>>>>>>>>>> |
+------------------------------------------------------------+--------------+
|                                              Invoice Total | @>>>>>>>>>>> |
+------------------------------------------------------------+--------------+
|                                                    Balance | @>>>>>>>>>>> |
+========+===================================================+==============+
END
	} else {
		soapDebug($OUT,$res);
	}

	return $res->{'Result'};
}







1;
# vim: ts=4
