# Purchasing functions
# Copyright (C) 2009-2014, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.




package wiaflos::server::core::Purchasing;

use strict;
use warnings;


use wiaflos::constants;
use awitpt::db::dblayer;
use wiaflos::server::core::GL;
use wiaflos::server::core::Inventory;
use wiaflos::server::core::Suppliers;
use wiaflos::server::core::Tax;

# Whole money transactions, precision is two
use Math::BigFloat;
Math::BigFloat::precision(-2);



# Our current error message
my $error = "";

# Set current error message
# Args: error_message
sub setError
{
	my $err = shift;
	my ($package,$filename,$line) = caller;
	my (undef,undef,undef,$subroutine) = caller(1);

	# Set error
	$error = "$subroutine($line): $err";
}

# Return current error message
# Args: none
sub Error
{
	my $err = $error;

	# Reset error
	$error = "";

	# Return error
	return $err;
}



# Backend function to build item hash
sub sanitizeRawItem
{
	my $rawData = shift;


	my $item;

	$item->{'ID'} = $rawData->{'ID'};

	$item->{'SupplierID'} = $rawData->{'SupplierID'};
	$item->{'Number'} = "PUR/".uc($rawData->{'Number'});

	$item->{'TaxReference'} = $rawData->{'TaxReference'};

	$item->{'SupplierInvoiceNumber'} = $rawData->{'SupplierInvoiceNumber'};
	$item->{'IssueDate'} = $rawData->{'IssueDate'};
	$item->{'DueDate'} = $rawData->{'DueDate'};
	$item->{'OrderNumber'} = $rawData->{'OrderNumber'};

	$item->{'DiscountTotal'} = $rawData->{'DiscountTotal'};
	$item->{'SubTotal'} = $rawData->{'SubTotal'};
	$item->{'TaxTotal'} = $rawData->{'TaxTotal'};
	$item->{'Total'} = $rawData->{'Total'};

	$item->{'GLTransactionID'} = $rawData->{'GLTransactionID'};
	$item->{'Posted'} = defined($rawData->{'GLTransactionID'}) ? 1 : 0;

	$item->{'Paid'} = $rawData->{'Paid'};

	return $item;
}



# Backend function to build supplier invoice item hash
sub sanitizeRawSupplierInvoiceItem
{
	my $rawData = shift;


	my $item;

	$item->{'ID'} = $rawData->{'ID'};

	$item->{'InventoryID'} = $rawData->{'InventoryID'};
	$item->{'GLAccountID'} = $rawData->{'GLAccountID'};
	$item->{'GLAccountNumber'} = wiaflos::server::core::GL::getGLAccountNumberFromID($rawData->{'GLAccountID'});
	$item->{'Description'} = $rawData->{'Description'};
	$item->{'SerialNumber'} = $rawData->{'SerialNumber'};
	$item->{'Quantity'} = $rawData->{'Quantity'};
	$item->{'Unit'} = $rawData->{'Unit'};
	$item->{'UnitPrice'} = $rawData->{'UnitPrice'};
	$item->{'Price'} = $rawData->{'Price'};
	$item->{'Discount'} = $rawData->{'Discount'};
	$item->{'DiscountAmount'} = $rawData->{'DiscountAmount'};
	$item->{'TaxRate'} = $rawData->{'TaxRate'};
	$item->{'TaxAmount'} = $rawData->{'TaxAmount'};
	$item->{'TaxTypeID'} = $rawData->{'TaxTypeID'};
	$item->{'TaxMode'} = $rawData->{'TaxMode'};

	# Calculate final price
	my $totalPrice = Math::BigFloat->new($rawData->{'Price'});
	$totalPrice->precision(-2);
	if ($rawData->{'TaxMode'} eq "2") {
		$totalPrice->badd($rawData->{'TaxAmount'});
	}
	$item->{'Total'} = $totalPrice->bstr();


	return $item;
}


# Check if invoice number exists
# Backend function, takes 1 parameter which is the invoice number
sub supplierInvoiceNumberExists
{
	my $invoiceNumber = shift;


	# Uppercase and sanitize purchase number
	$invoiceNumber = uc($invoiceNumber);
	$invoiceNumber =~ s#^PUR/##;

	# Select invoice count
	my $rows = DBSelectNumResults("FROM supplier_invoices WHERE Number = ".DBQuote($invoiceNumber));
	if (!defined($rows)) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	return $rows > 0 ? 1 : 0;
}


# Check if invoice exists
# Backend function, takes 1 parameter which is the invoice ID
sub supplierInvoiceIDExists
{
	my $invoiceID = shift;


	# Select invoice count
	my $rows = DBSelectNumResults("FROM supplier_invoices WHERE ID = ".DBQuote($invoiceID));
	if (!defined($rows)) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	return $rows > 0 ? 1 : 0;
}


# Return suppliers invoice id from invoice number
sub getSupplierInvoiceIDFromNumber
{
	my $invoiceNumber = shift;


	# Uppercase and sanitize purchase number
	$invoiceNumber = uc($invoiceNumber);
	$invoiceNumber =~ s#^PUR/##;

	# Select supplier invoice
	my $sth = DBSelect("
		SELECT
			ID
		FROM
			supplier_invoices
		WHERE
			Number = ".DBQuote($invoiceNumber)."
	");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	my $row = hashifyLCtoMC($sth->fetchrow_hashref(), qw( ID ));
	DBFreeRes($sth);

	# Check we got a result
	if (!defined($row)) {
		setError("Error finding supplier invoice '$invoiceNumber'");
		return ERR_NOTFOUND;
	}

	return $row->{'ID'};
}



# Return suppliers invoice id from supplier ID and suppliers' invoice number
sub getSupplierInvoiceIDFromSupplierInvoiceNumber
{
	my ($supplierID,$invoiceNumber) = @_;


	# Uppercase and sanitize purchase number
	$invoiceNumber = uc($invoiceNumber);
	$invoiceNumber =~ s#^PUR/##;

	# Select supplier invoice
	my $sth = DBSelect("
		SELECT
			ID
		FROM
			supplier_invoices
		WHERE
			SupplierID = ".DBQuote($supplierID)."
			AND SupplierInvoiceNumber = ".DBQuote($invoiceNumber)."
	");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	my $row = hashifyLCtoMC($sth->fetchrow_hashref(), qw( ID ));
	DBFreeRes($sth);

	# Check we got a result
	if (!defined($row)) {
		setError("Error finding supplier invoice '".$invoiceNumber."'");
		return ERR_NOTFOUND;
	}

	return $row->{'ID'};
}



# Return an array of supplier invoices
# Optional
#		Type - "unpaid", "all"
sub getSupplierInvoices
{
	my ($detail) = @_;
	my $type = defined($detail->{'Type'}) ? $detail->{'Type'} : "unpaid";
	my @invoices = ();


	# Return list of invoices
	my $sth = DBSelect("
		SELECT
			ID, SupplierID, Number, SupplierInvoiceNumber, IssueDate, DueDate, OrderNumber, DiscountTotal, SubTotal,
			TaxTotal, Total, GLTransactionID, Paid
		FROM
			supplier_invoices
	");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	# Fetch rows
	while (my $row = hashifyLCtoMC($sth->fetchrow_hashref(),
			qw( ID SupplierID Number SupplierInvoiceNumber IssueDate DueDate OrderNumber DiscountTotal SubTotal
				TaxTotal Total GLTransactionID Paid )
	)) {
		# Check what kind of payments we want
		if (($type eq "unpaid") && $row->{'Paid'} eq "0") {
			push(@invoices,sanitizeRawItem($row));
		} elsif ($type eq "all") {
			push(@invoices,sanitizeRawItem($row));
		}
	}

	DBFreeRes($sth);

	return \@invoices;
}


# Return a supplier invoice
# Optional:
#		ID		- Supplier invoice ID
#		Number	- Supplier invoice number
sub getSupplierInvoice
{
	my ($detail) = @_;


	my $invID;

	# Check which 'mode' we operating in
	if (!defined($detail->{'ID'}) || $detail->{'ID'} < 1) {
		# Verify supplier invoice number
		if (!defined($detail->{'Number'}) || $detail->{'Number'} eq "") {
			setError("No (or invalid) supplier invoice number provided");
			return ERR_PARAM;
		}

		# Check if supplier invoice number  exists
		if (($invID = getSupplierInvoiceIDFromNumber($detail->{'Number'})) < 1) {
			setError(Error());
			return $invID;
		}
	} else {
		$invID = $detail->{'ID'};
	}

	# Verify supplier invoice ID
	if (!$invID || $invID < 1) {
		setError("No (or invalid) supplier invoice number/id provided");
		return ERR_PARAM;
	}

	# Return invoice details
	my $sth = DBSelect("
		SELECT
			ID, SupplierID, Number, TaxReference, SupplierInvoiceNumber, IssueDate, DueDate, OrderNumber,
			DiscountTotal, SubTotal, TaxTotal, Total, GLTransactionID, Paid
		FROM
			supplier_invoices
		WHERE
			ID = ".DBQuote($invID)."
	");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	# Fetch row
	my $row = hashifyLCtoMC($sth->fetchrow_hashref(),
			qw( ID SupplierID Number TaxReference SupplierInvoiceNumber IssueDate DueDate OrderNumber
				DiscountTotal SubTotal TaxTotal Total GLTransactionID Paid )
	);
	DBFreeRes($sth);

	return sanitizeRawItem($row);
}


# Return an array of invoice items
# Optional:
#		ID		- Supplier invoice ID
#		Number	- Supplier invoice number
sub getSupplierInvoiceItems
{
	my ($detail) = @_;

	my @items = ();


	my $invID;

	# Check which 'mode' we operating in
	if (!defined($detail->{'ID'}) || $detail->{'ID'} < 1) {
		# Verify supplier invoice number
		if (!defined($detail->{'Number'}) || $detail->{'Number'} eq "") {
			setError("No (or invalid) supplier invoice number provided");
			return ERR_PARAM;
		}

		# Check if supplier invoice number  exists
		if (($invID = getSupplierInvoiceIDFromNumber($detail->{'Number'})) < 1) {
			setError(Error());
			return $invID;
		}
	} else {
		$invID = $detail->{'ID'};
	}

	# Verify supplier invoice ID
	if (!$invID || $invID < 1) {
		setError("No (or invalid) supplier invoice number/id provided");
		return ERR_PARAM;
	}

	# Return list of supplier invoice items
	my $sth = DBSelect("
		SELECT
			ID, InventoryID, GLAccountID, Description, SerialNumber, Quantity, Unit, UnitPrice, Price,
			Discount, DiscountAmount, TaxMode, TaxRate, TaxTypeID, TaxAmount
		FROM
			supplier_invoice_items
		WHERE
			SupplierInvoiceID = ".DBQuote($invID)."
	");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	# Fetch rows
	while (my $row = hashifyLCtoMC($sth->fetchrow_hashref(),
			qw( ID InventoryID GLAccountID Description SerialNumber Quantity Unit UnitPrice Price
				Discount DiscountAmount TaxMode TaxRate TaxTypeID TaxAmount )
	)) {
		my $item = sanitizeRawSupplierInvoiceItem($row);
		push(@items,$item);
	}

	DBFreeRes($sth);

	return \@items;
}



# Return an invoice item
# Optional:
#		ID		- Supplier invoice item ID
sub getSupplierInvoiceItem
{
	my ($detail) = @_;

	my @items = ();


	# Verify invoice item ID
	if (!defined($detail->{'ID'}) || $detail->{'ID'} eq "") {
		setError("No (or invalid) supplier invoice ID provided");
		return ERR_PARAM;
	}

	# Return list of supplier invoice items
	my $sth = DBSelect("
		SELECT
			ID, InventoryID, GLAccountID, Description, SerialNumber, Quantity, Unit, UnitPrice, Price, Discount,
			DiscountAmount, TaxMode, TaxRate, TaxTypeID, TaxAmount
		FROM
			supplier_invoice_items
		WHERE
			ID = ".DBQuote($detail->{'ID'})."
	");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	# Fetch row
	my $row = hashifyLCtoMC($sth->fetchrow_hashref(),
			qw( ID InventoryID GLAccountID Description SerialNumber Quantity Unit UnitPrice Price Discount
				DiscountAmount TaxMode TaxRate TaxTypeID TaxAmount )
	);
	DBFreeRes($sth);

	return sanitizeRawSupplierInvoiceItem($row);
}


# Create supplier invoice
# Parameters:
#		SupplierCode		- Supplier code
#		Number			- Invoice number
#		SupplierInvoiceNumber	- Suppliers invoice number
#		IssueDate		- Issue date
# Optional:
#		DueDate			- Due date
#		OrderNumber		- Order number
#		Note			- Notes
sub createSupplierInvoice
{
	my ($detail) = @_;


	my @extraCols = ();
	my @extraData = ();


	# Verify supplier code
	if (!defined($detail->{'SupplierCode'}) || $detail->{'SupplierCode'} eq "" ) {
		setError("No supplier code provided");
		return ERR_PARAM;
	}

	# Verify invoice numbers
	if (!defined($detail->{'Number'}) || $detail->{'Number'} eq "") {
		setError("No invoice number provided");
		return ERR_PARAM;
	}
	# Remove PUR/
	(my $invNumber = uc($detail->{'Number'})) =~ s#^PUR/##;

	if (!defined($detail->{'SupplierInvoiceNumber'}) || $detail->{'SupplierInvoiceNumber'} eq "") {
		setError("No supplier invoice number provided for supplier invoice '".$detail->{'Number'}."'");
		return ERR_PARAM;
	}

	# Verify issue date
	if (!defined($detail->{'IssueDate'}) || $detail->{'IssueDate'} eq "") {
		setError("No issue date provided for supplier invoice '".$detail->{'Number'}."'");
		return ERR_PARAM;
	}

	# Add due date if exists
	if (defined($detail->{'DueDate'}) && $detail->{'DueDate'} ne "") {
		push(@extraCols,'DueDate');
		push(@extraData,DBQuote($detail->{'DueDate'}));
	}

	# Add order number if exists
	if (defined($detail->{'OrderNumber'}) && $detail->{'OrderNumber'} ne "") {
		push(@extraCols,'OrderNumber');
		push(@extraData,DBQuote($detail->{'OrderNumber'}));
	}

	# Add note if it exists
	if (defined($detail->{'Note'}) && $detail->{'Note'} ne "") {
		push(@extraCols,'Note');
		push(@extraData,DBQuote($detail->{'Note'}));
	}

	# Check for conflicts
	if ((my $res = supplierInvoiceNumberExists($invNumber)) != 0) {
		# Err of exists
		if ($res == 1) {
			setError("Supplier invoice number '$invNumber' already exists");
			return ERR_CONFLICT;
		} else {
			setError(Error());
		}
		# else err with result
		return $res;
	}

	# Grab supplier
	my $data;
	$data->{'Code'} = $detail->{'SupplierCode'};
	my $supplier = wiaflos::server::core::Suppliers::getSupplier($data);
	if (ref $supplier ne "HASH") {
		setError(wiaflos::server::core::Suppliers::Error());
		return $supplier;
	}

	# Pull in stuff we need
	if (defined($supplier->{'TaxReference'}) && $supplier->{'TaxReference'} ne "") {
		push(@extraCols,'TaxReference');
		push(@extraData,DBQuote($supplier->{'TaxReference'}));
	}

	# Pull in extra data
	my $extraCols = "";
	my $extraData = "";
	if (@extraCols > 0 && @extraData > 0) {
		$extraCols .= ',' . join(',',@extraCols);
		$extraData .= ',' . join(',',@extraData);
	}

	# Create supplier invoice
	my $sth = DBDo("
		INSERT INTO supplier_invoices
				(SupplierID,Number,SupplierInvoiceNumber,IssueDate$extraCols)
			VALUES
				(
					".DBQuote($supplier->{'ID'}).",
					".DBQuote($invNumber).",
					".DBQuote($detail->{'SupplierInvoiceNumber'}).",
					".DBQuote($detail->{'IssueDate'})."
					$extraData
				)
	");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	# Grab last ID
	my $ID = DBLastInsertID("supplier_invoices","ID");

	return $ID;
}


# Link item to invoice
# Parameters:
#		Description			- Description
#		UnitPrice			- UnitPrice, this price is dependant on the TaxMode, excl = unitprice is excl, incl = unitprice is incl
#		TaxTypeID			- Tax type ID
#		TaxMode				- Tax mode
# Optional:
#		ID					- Supplier invoice ID
#		Number				- Supplier invoice number
#
#		InventoryCode		- Inventory code   - A: ONE OF THESE MUST BE SPECIFIED
#		SerialNumber		- Used by tracked inventory products, this is normally the serial number of the item. Or in any other case
#							can be used a a batch number, or anything as a matter of fact.
#		GLAccountNumber		- GL account number  - A: ONE OF THESE MUST BE SPECIFIED
#		Quantity			- Quantity
#		Discount			- Discount rate
#		Unit				- Unit
sub linkSupplierInvoiceItem
{
	my ($detail) = @_;

	my @extraCols = ();
	my @extraData = ();


	# Check if supplier invoice exists
	my $data;
	$data->{'ID'} = $detail->{'ID'};
	$data->{'Number'} = $detail->{'Number'};
	my $invoice = getSupplierInvoice($data);
	if (ref $invoice ne "HASH") {
		setError(Error());
		return $invoice;
	}

	# Verify description
	if (!defined($detail->{'Description'}) || $detail->{'Description'} eq "") {
		setError("No description provided for linking item to supplier invoice '".$invoice->{'Number'}."'");
		return ERR_PARAM;
	}

	# Add unit if it exists
	if (defined($detail->{'Unit'}) && $detail->{'Unit'} ne "") {
		push(@extraCols,'Unit');
		push(@extraData,DBQuote($detail->{'Unit'}));
	}

	# Verify price
	if (!defined($detail->{'UnitPrice'})) {
		setError("No price provided for linking item to supplier invoice '".$invoice->{'Number'}."'");
		return ERR_PARAM;
	}

	# Verify tax type ID
	if (!defined($detail->{'TaxTypeID'}) || $detail->{'TaxTypeID'} < 1) {
		setError("No tax type ID provided for linking item to supplier invoice '".$invoice->{'Number'}."'");
		return ERR_PARAM;
	}

	# Verify tax mode
	if (!defined($detail->{'TaxMode'}) || $detail->{'TaxMode'} eq "") {
		setError("No tax mode provided for linking item to supplier invoice '".$invoice->{'Number'}."'");
		return ERR_PARAM;
	}

	# Check for either inventory item or GL account
	my $inventoryItem;  # Set if we dealing with an inventory item
	if (defined($detail->{'InventoryCode'}) && $detail->{'InventoryCode'} ne "") {
		# Check if inventory item exists
		my $tmp;
		$tmp->{'Code'} = $detail->{'InventoryCode'};
		$inventoryItem = wiaflos::server::core::Inventory::getInventoryItem($tmp);
		if (ref $inventoryItem ne "HASH") {
			setError(wiaflos::server::core::Inventory::Error());
			return $inventoryItem;
		}

		push(@extraCols,'InventoryID');
		push(@extraData,DBQuote($inventoryItem->{'ID'}));


	} elsif (defined($detail->{'GLAccountNumber'}) && $detail->{'GLAccountNumber'} ne "") {
		# Check if GL account exists
		my $GLAccID = wiaflos::server::core::GL::getGLAccountIDFromNumber($detail->{'GLAccountNumber'});
		if ($GLAccID < 1) {
			setError(wiaflos::server::core::GL::Error());
			return $GLAccID;
		}

		# Check fincat
		my $catMatch = wiaflos::server::core::GL::checkGLAccountFinCat($GLAccID,"E01") || wiaflos::server::core::GL::checkGLAccountFinCat($GLAccID,"A01");

		if ($catMatch == 0) {
			setError("Expense GL account '".$detail->{'GLAccountNumber'}."' is not of type expense when linking item to supplier invoice '".
					$invoice->{'Number'}."'");
			return ERR_USAGE;
		} elsif ($catMatch < 0) {
			setError(wiaflos::server::core::GL::Error());
			return $catMatch;
		}

		push(@extraCols,'GLAccountID');
		push(@extraData,DBQuote($GLAccID));
	} else {
		setError("InventoryCode or GLAccountNumber must be provided");
		return ERR_USAGE;
	}

	# Pull discount
	my $discount = Math::BigFloat->new();
	if (defined($detail->{'Discount'})) {
		$discount->badd($detail->{'Discount'});
	}

	# If we have an item serial number, add it aswell
	if (defined($detail->{'SerialNumber'}) && $detail->{'SerialNumber'} ne "") {
		push(@extraCols,'SerialNumber');
		push(@extraData,DBQuote($detail->{'SerialNumber'}));
	} elsif (defined($inventoryItem)) {
		# Tracked items must have a serial number
		if ($inventoryItem->{'Mode'} eq "track") {
			setError("Inventory item '".$inventoryItem->{'Code'}."' is tracked, a SerialNumber MUST be provided");
			return ERR_USAGE;
		}
	}

	# Check if tax type exists
	$data = undef;
	$data->{'TaxTypeID'} = $detail->{'TaxTypeID'};
	my $taxType = wiaflos::server::core::Tax::getTaxType($data);
	if (ref $taxType ne "HASH") {
		setError(wiaflos::server::core::Tax::Error());
		return $taxType;
	}

	# Get quantity
	my $quantity = Math::BigFloat->new();
	$quantity->precision(-4);
	if (defined($detail->{'Quantity'})) {
		$quantity->badd($detail->{'Quantity'});
	} else {
		$quantity->badd(1);
	}

	# Pull in tax rate from the type we got above & set precision
	my $taxRate = Math::BigFloat->new($taxType->{'TaxRate'});

	# And pull in unit price
	my $unitPrice = Math::BigFloat->new($detail->{'UnitPrice'});

	# Total up final price
	my $totalPrice = $unitPrice->copy();
	$totalPrice->precision(-4);  # Increase precision for below calculations
	$totalPrice->bmul($quantity);

	# Get ready for calculating tax
	my ($taxAmount,$taxMode) = wiaflos::server::core::Tax::getTaxAmount($detail->{'TaxMode'},$taxRate,$totalPrice);

	# Calculate discount
	if (!$discount->is_zero()) {
		# Set discountAmount depending on tax mode
		my $discountAmount = $totalPrice->copy();
		$discountAmount->bsub($taxAmount) if ($detail->{'TaxMode'} eq "incl");

		# Get discount multiplier
		my $tmpDisc = $discount->copy();
		$tmpDisc->precision(-4);
		$tmpDisc->bdiv(100);

		# Get discount amount
		$discountAmount->bmul($tmpDisc);

		# Remove discount from total price
		$totalPrice->bsub($discountAmount);

		# Reduce precision
		$discountAmount->precision(-2);

		# All columns
		push(@extraCols,'Discount');
		push(@extraData,DBQuote($discount->bstr()));
		push(@extraCols,'DiscountAmount');
		push(@extraData,DBQuote($discountAmount->bstr()));
	}

	# Reduce precision now
	$totalPrice->precision(-2);

	# Pull in extra data
	my $extraCols = "";
	my $extraData = "";
	if (@extraCols > 0 && @extraData > 0) {
		$extraCols .= ',' . join(',',@extraCols);
		$extraData .= ',' . join(',',@extraData);
	}

	# Link in supplier address
	my $sth = DBDo("
		INSERT INTO supplier_invoice_items
				(SupplierInvoiceID,Description,Quantity,UnitPrice,Price,TaxTypeID,TaxMode,TaxRate,TaxAmount$extraCols)
			VALUES
				(
					".DBQuote($invoice->{'ID'}).",
					".DBQuote($detail->{'Description'}).",
					".DBQuote($quantity->bstr()).",
					".DBQuote($unitPrice->bstr()).",
					".DBQuote($totalPrice->bstr()).",
					".DBQuote($detail->{'TaxTypeID'}).",
					".DBQuote($taxMode).",
					".DBQuote($taxRate->bstr()).",
					".DBQuote($taxAmount)."
					$extraData
				)
	");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	# Grab last ID
	my $ID = DBLastInsertID("supplier_invoice_items","ID");

	return $ID;
}


# Post invoice
# Optional:
#		ID		- Supplier invoice ID
#		Number	- Supplier invoice number
sub postSupplierInvoice
{
	my ($detail) = @_;

	my $data;


	# Grab invoice
	$data = undef;
	$data->{'ID'} = $detail->{'ID'};
	$data->{'Number'} = $detail->{'Number'};
	my $invoice = getSupplierInvoice($data);
	if (ref $invoice ne "HASH") {
		setError(Error());
		return $invoice;
	}
	# Check if we already posted or not
	if ($invoice->{'GLTransactionID'}) {
		setError("Supplier invoice '".$invoice->{'Number'}."' already posted");
		return ERR_POSTED;
	}

	# Grab supplier
	$data = undef;
	$data->{'ID'} = $invoice->{'SupplierID'};
	my $supplier = wiaflos::server::core::Suppliers::getSupplier($data);
	if (ref $supplier ne "HASH") {
		setError(wiaflos::server::core::Suppliers::Error());
		return $supplier;
	}

	# Return list of supplier invoice items
	my $sth = DBSelect("
		SELECT
			supplier_invoice_items.ID,
			supplier_invoice_items.InventoryID, supplier_invoice_items.GLAccountID,
			supplier_invoice_items.SerialNumber,
			supplier_invoice_items.Quantity,
			supplier_invoice_items.UnitPrice, supplier_invoice_items.DiscountAmount, supplier_invoice_items.Price,
			supplier_invoice_items.TaxTypeID, supplier_invoice_items.TaxMode, supplier_invoice_items.TaxAmount,

			tax_types.GLAccountID AS TaxGLAccountID
		FROM
			supplier_invoice_items, tax_types
		WHERE
			supplier_invoice_items.SupplierInvoiceID = ".DBQuote($invoice->{'ID'})."
			AND tax_types.ID = supplier_invoice_items.TaxTypeID
	");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	DBBegin();

	# Create transaction
	my $transactionRef = sprintf('Purchase: %s',$invoice->{'Number'});
	$data = undef;
	$data->{'Date'} = $invoice->{'IssueDate'};
	$data->{'Reference'} = $transactionRef;
	my $GLTransactionID = wiaflos::server::core::GL::createGLTransaction($data);
	if ($GLTransactionID < 1) {
		setError(wiaflos::server::core::GL::Error());
		DBRollback();
		return $GLTransactionID;
	}

	# These are the totals that will be posted to the GL
	my %taxEntries;
	my %stockEntries;
	my %expEntries;
	my $invTotal = Math::BigFloat->new();
	my $subTotal = Math::BigFloat->new();
	my $discountTotal = Math::BigFloat->new();

	# Fetch rows
	while (my $row = hashifyLCtoMC($sth->fetchrow_hashref(),
			qw( ID InventoryID GLAccountID SerialNumber Quantity UnitPrice DiscountAmount Price
				TaxTypeID TaxMode TaxAmount TaxGLAccountID )
	)) {
		# Pull quantity as we do math below, else we wouldn't need to
		my $quantity = Math::BigFloat->new();
		$quantity->precision(-4);
		$quantity->badd($row->{'Quantity'});

		# Calculate final prices
		my $itemInclPrice = Math::BigFloat->new($row->{'Price'});
		$itemInclPrice->badd($row->{'TaxAmount'}) if ($row->{'TaxMode'} eq "2");

		my $itemExclPrice = Math::BigFloat->new($row->{'Price'});
		$itemExclPrice->bsub($row->{'TaxAmount'}) if ($row->{'TaxMode'} eq "1");

		# Pull in tax GL account and addup what we need to post
		if (defined($taxEntries{$row->{'TaxGLAccountID'}})) {
			$taxEntries{$row->{'TaxGLAccountID'}}->badd($row->{'TaxAmount'});
		} else {
			$taxEntries{$row->{'TaxGLAccountID'}} = Math::BigFloat->new($row->{'TaxAmount'});
		}

		# Pull in discount
		$discountTotal->badd($row->{'DiscountAmount'}) if (defined($row->{'DiscountAmount'}));

		# Check what we need to do
		if (defined($row->{'InventoryID'})) {
			$data = undef;
			$data->{'ID'} = $row->{'InventoryID'};
			my $inventoryItem = wiaflos::server::core::Inventory::getInventoryItem($data);
			if (ref $inventoryItem ne "HASH") {
				setError(wiaflos::server::core::Inventory::Error());
				return $inventoryItem;
			}

			# Pull in stock control account and add up what we need to post, if its zero, its probably discounted 100%
			if (defined($inventoryItem->{'GLAssetAccountID'}) && !$itemExclPrice->is_zero()) {
				if (defined($stockEntries{$inventoryItem->{'GLAssetAccountID'}})) {
					$stockEntries{$inventoryItem->{'GLAssetAccountID'}}->badd($itemExclPrice);
				} else {
					$stockEntries{$inventoryItem->{'GLAssetAccountID'}} = $itemExclPrice->copy();
				}
			}

			# Adjust stock
			$data = undef;
			$data->{'ID'} = $row->{'InventoryID'};
			$data->{'GLTransactionID'} = $GLTransactionID;
			$data->{'QtyChange'} = $quantity->bstr();
			$data->{'Cost'} = $itemExclPrice->bstr();
			$data->{'SerialNumber'} = $row->{'SerialNumber'};
			# Adjust stock and check for errors
			my $trackingInfo  = wiaflos::server::core::Inventory::adjustInventoryStock($data);
			if (ref $trackingInfo ne "ARRAY") {
				setError(wiaflos::server::core::Inventory::Error());
				DBRollback();
				return $trackingInfo;
			}

			# Loop with tracking info and record
			foreach my $trackingInfoItem (@{$trackingInfo}) {
				# Link in tracking information
				if ($trackingInfoItem->{'InventoryTrackingID'} > 0) {
					$data = undef;
					$data->{'ID'} = $row->{'ID'};
					$data->{'InventoryTrackingID'} = $trackingInfoItem->{'InventoryTrackingID'};
					$data->{'QtyChange'} = $trackingInfoItem->{'QtyChange'};
					$data->{'Price'} = $trackingInfoItem->{'Price'};
					my $itemEntryID = addSupplierInvoiceItemEntry($data);
					if ($itemEntryID < 1) {
						setError(Error());
						return $itemEntryID;
					}
				}
			}

		# Expense account
		} elsif (defined($row->{'GLAccountID'})) {
			# Pull in expense
			if (defined($expEntries{$row->{'GLAccountID'}})) {
				$expEntries{$row->{'GLAccountID'}}->badd($itemExclPrice);
			} else {
				$expEntries{$row->{'GLAccountID'}} = $itemExclPrice->copy();
			}

			$data = undef;
			$data->{'ID'} = $row->{'ID'};
			$data->{'QtyChange'} = $quantity->bstr();
			$data->{'Price'} = $itemExclPrice->bstr();
			my $itemEntryID = addSupplierInvoiceItemEntry($data);
			if ($itemEntryID < 1) {
				setError(Error());
				return $itemEntryID;
			}
		}

		# Addup inv totals
		$subTotal->badd($itemExclPrice);
		$invTotal->badd($itemInclPrice);
	}

	DBFreeRes($sth);

	# Link invoice total to suppliers GL account
	my $invLinkTotal = $invTotal->copy();
	$invLinkTotal->bneg(); # Negate
	$data = undef;
	$data->{'ID'} = $GLTransactionID;
	$data->{'GLAccountID'} = $supplier->{'GLAccountID'};
	$data->{'Amount'} = $invLinkTotal->bstr();
	if ((my $res = wiaflos::server::core::GL::linkGLTransaction($data)) < 1) {
		setError(wiaflos::server::core::GL::Error());
		DBRollback();
		return $res;
	}

	# Link in tax to GL
	foreach my $taxacc (keys %taxEntries) {
		$data->{'GLAccountID'} = $taxacc;
		$data->{'Amount'} = $taxEntries{$taxacc}->bstr();
		$data->{'Reference'} = sprintf("Tax: %s",$invoice->{'Number'});
		if ((my $res = wiaflos::server::core::GL::linkGLTransaction($data)) < 1) {
			setError(wiaflos::server::core::GL::Error());
			DBRollback();
			return $res;
		}
	}

	# Link in asset adjustment to GL
	foreach my $stock (keys %stockEntries) {
		$data->{'GLAccountID'} = $stock;
		$data->{'Amount'} = $stockEntries{$stock}->bstr();
		$data->{'Reference'} = sprintf("Stock control: %s",$invoice->{'Number'});
		if ((my $res = wiaflos::server::core::GL::linkGLTransaction($data)) < 1) {
			setError(wiaflos::server::core::GL::Error());
			DBRollback();
			return $res;
		}
	}

	# Link in expenses
	foreach my $exp (keys %expEntries) {
		$data->{'GLAccountID'} = $exp;
		$data->{'Amount'} = $expEntries{$exp}->bstr();
		$data->{'Reference'} = sprintf("Expense: %s",$invoice->{'Number'});
		if ((my $res = wiaflos::server::core::GL::linkGLTransaction($data)) < 1) {
			setError(wiaflos::server::core::GL::Error());
			DBRollback();
			return $res;
		}
	}

	# Update supplier invoice with transaction ID
	my $taxTotal = $invTotal->copy();
	$taxTotal->bsub($subTotal);
	$sth = DBDo("
		UPDATE supplier_invoices
			SET
				GLTransactionID = ".DBQuote($GLTransactionID).",
				DiscountTotal = ".DBQuote($discountTotal->bstr()).",
				SubTotal = ".DBQuote($subTotal->bstr()).",
				TaxTotal = ".DBQuote($taxTotal->bstr()).",
				Total = ".DBQuote($invTotal->bstr())."
			WHERE
				ID = ".DBQuote($invoice->{'ID'})."
	");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		DBRollback();
		return ERR_DB;
	}

	# Post transaction
	$data = undef;
	$data->{'ID'} = $GLTransactionID;
	if ((my $res = wiaflos::server::core::GL::postGLTransaction($data)) != 0) {
		setError(wiaflos::server::core::GL::Error());
		DBRollback();
		return $res;
	}

	DBCommit();

	return 0;
}



# Return an array of invoice transactions
# Parameters:
#		ID	- Supplier Invoice ID
# 		Number - Supplier Invoice Number  (optional instead of ID)
# Optional:
# 		Mode - Can be specified as "resolve" to add ALL info to the results.
sub getSupplierInvoiceTransactions
{
	my ($detail) = @_;

	my @allocations = ();


	# Grab invoice
	my $data;
	$data->{'ID'} = $detail->{'ID'};
	$data->{'Number'} = $detail->{'Number'};
	my $invoice = getSupplierInvoice($data);
	if (ref $invoice ne "HASH") {
		setError(Error());
		return $invoice;
	}

	# Return list of supplier invoice transactions
	my $sth = DBSelect("
		SELECT
			ID, Amount, PaymentAllocationID, SupplierCreditNoteID
		FROM
			supplier_invoice_transactions
		WHERE
			SupplierInvoiceID = ".DBQuote($invoice->{'ID'})."
	");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	# Fetch rows
	while (my $row = hashifyLCtoMC($sth->fetchrow_hashref(),
			qw( ID Amount PaymentAllocationID SupplierCreditNoteID )
	)) {
		my $item;

		$item->{'ID'} = $row->{'ID'};

		$item->{'Amount'} = $row->{'Amount'};

		# If its a payment, pull in all the details
		if (defined($row->{'PaymentAllocationID'})) {
			$item->{'PaymentAllocationID'} = $row->{'PaymentAllocationID'};

			# Check if we must resolve all the ID links or not
			if (defined($detail->{'Mode'}) && $detail->{'Mode'} eq "resolve_all") {
				# Pull in payment allocation
				$data = undef;
				$data->{'ID'} = $item->{'PaymentAllocationID'};
				$item->{'PaymentAllocation'} = wiaflos::server::core::Payments::getPaymentAllocation($data);
				if (ref $item->{'PaymentAllocation'} ne "HASH") {
					setError(wiaflos::server::core::Payments::Error());
					return $item->{'PaymentAllocation'};
				}

				# Pull in payment
				$data = undef;
				$data->{'ID'} = $item->{'PaymentAllocation'}->{'PaymentID'};
				$item->{'Payment'} = wiaflos::server::core::Payments::getPayment($data);
				if (ref $item->{'Payment'} ne "HASH") {
					setError(wiaflos::server::core::Payments::Error());
					return $item->{'Payment'};
				}
			}

		# If its a credit note pull in all the details for that
		} elsif (defined($row->{'SupplierCreditNoteID'})) {
			$item->{'SupplierCreditNoteID'} = $row->{'SupplierCreditNoteID'};

			# Check if we must resolve all the ID links or not
			if (defined($detail->{'Mode'}) && $detail->{'Mode'} eq "resolve_all") {
				# Pull in credit note
				$data = undef;
				$data->{'ID'} = $item->{'SupplierCreditNoteID'};
				$item->{'SupplierCreditNote'} = wiaflos::server::core::SupplierCreditNotes::getSupplierCreditNote($data);
				if (ref $item->{'SupplierCreditNote'} ne "HASH") {
					setError(wiaflos::server::core::SupplierCreditNotes::Error());
					return $item->{'SupplierCreditNote'};
				}
			}
		}


		push(@allocations,$item);
	}

	DBFreeRes($sth);

	return \@allocations;
}


# Return an invoice transaction
# Parameters:
#		ID	- Supplier invoice transaction ID
sub getSupplierInvoiceTransaction
{
	my ($detail) = @_;


	# Verify supplier invoice item ID
	if (!defined($detail->{'ID'}) || $detail->{'ID'} eq "") {
		setError("No (or invalid) supplier invoice item transaction ID provided");
		return ERR_PARAM;
	}

	# Return list of supplier invoice transactions
	my $sth = DBSelect("
		SELECT
			ID, Amount, SupplierInvoiceID, PaymentAllocationID, SupplierCreditNoteID
		FROM
			supplier_invoice_transactions
		WHERE
			ID = ".DBQuote($detail->{'ID'})."
	");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	my $row = hashifyLCtoMC($sth->fetchrow_hashref(),
			qw( ID Amount SupplierInvoiceID PaymentAllocationID SupplierCreditNoteID ));
	if (!$row) {
		setError("Invoice transaction '".$detail->{'ID'}."' not found");
		DBFreeRes($sth);
		return ERR_NOTFOUND;
	}

	my $item;

	$item->{'ID'} = $row->{'ID'};

	$item->{'Amount'} = $row->{'Amount'};

	$item->{'SupplierInvoiceID'} = $row->{'SupplierInvoiceID'};

	# If its a payment, pull in all the details
	if (defined($row->{'PaymentAllocationID'})) {
		$item->{'PaymentAllocationID'} = $row->{'PaymentAllocationID'};

	# If its a credit note pull in all the details for that
	} elsif (defined($row->{'SupplierCreditNoteID'})) {
		$item->{'SupplierCreditNoteID'} = $row->{'SupplierCreditNoteID'};
	}

	DBFreeRes($sth);

	return $item;
}



# Function to retrun current invoice transaction balance
# Parameters:
# 		ID	- Supplier invoice ID
# 		Number - or; optionally the number
sub getSupplierInvoiceTransactionBalance
{
	my $detail = shift;


	# Grab invoice
	my $data;
	$data->{'ID'} = $detail->{'ID'};
	$data->{'Number'} = $detail->{'Number'};
	my $invoice = getSupplierInvoice($data);
	if (ref $invoice ne "HASH") {
		setError(Error());
		return $invoice;
	}

	# Grab transactions
	$data = undef;
	$data->{'ID'} = $invoice->{'ID'};
	my $transactions = getSupplierInvoiceTransactions($data);
	if (ref $transactions ne "ARRAY") {
		setError(Error());
		return $transactions;
	}

	# Check the invoice total vs. the invoice transactions
	my $invBalance = Math::BigFloat->new($invoice->{'Total'});
	foreach my $trans (@{$transactions}) {
		$invBalance->badd($trans->{'Amount'});
	}

	# Hash up and return
	my $ret;
	$ret->{'Balance'} = $invBalance->bstr();
	return $ret;
}


# Backend function to create supplier invoice transaction
# Parameters:
#		ID	- Suppliers invoice ID, Option A
#		Number	- Suppliers invoice number, Option A
#
#		Amount		- Amount, payment = -xxxx, credit note = -xxxx, debit note = +xxxx
# Optional:
#		SupplierCreditNoteID - supplier credit note
#		PaymentAllocationID - payment allocation
sub allocateSupplierInvoiceTransaction
{
	my ($detail) = @_;


	my @extraCols = ();
	my @extraData = ();


	# Grab invoice
	my $data;
	$data->{'ID'} = $detail->{'ID'};
	$data->{'Number'} = $detail->{'Number'};
	my $invoice = getSupplierInvoice($data);
	if (ref $invoice ne "HASH") {
		setError(Error());
		return $invoice;
	}

	# We cannot allocate a transaction to a paid invoice??
	if ($invoice->{'Paid'} eq "1") {
		setError("Attempt to allocate a transaction to supplier invoice '".$invoice->{'Number'}."' which has already been paid");
		return ERR_PAID;
	}

	$data = undef;
	$data->{'ID'} = $invoice->{'ID'};
	my $res = getSupplierInvoiceTransactionBalance($data);
	if (ref $res ne "HASH") {
		setError(Error());
		return $res;
	}
	my $invBalance = Math::BigFloat->new($res->{'Balance'});

	# And the current transaction
	$invBalance->badd($detail->{'Amount'});

	# Make sure we're not overallocating the invoice
	if ($invBalance->is_neg()) {
		setError("Cannot allocate transaction against supplier invoice '".$invoice->{'Number'}.
				"' as it will cause a negative balance of '".$invBalance->bstr()."'");
	}

	DBBegin();


	# If invoice balances out, invoice is now paid
	if ($invBalance->is_zero()) {
		my $sth = DBDo("
			UPDATE supplier_invoices
			SET
				Paid = 1
			WHERE
				ID = ".DBQuote($invoice->{'ID'})."
		");
		if (!$sth) {
			setError(awitpt::db::dblayer::Error());
			DBRollback();
			return ERR_DB;
		}
	}

	# Check if this is a credit note
	if ($detail->{'SupplierCreditNoteID'}) {
		push(@extraCols,'SupplierCreditNoteID');
		push(@extraData,DBQuote($detail->{'SupplierCreditNoteID'}));

	# Its a payment allocation
	} elsif ($detail->{'PaymentAllocationID'}) {
		push(@extraCols,'PaymentAllocationID');
		push(@extraData,DBQuote($detail->{'PaymentAllocationID'}));
	}

	# Pull in extra data
	my $extraCols = "";
	my $extraData = "";
	if (@extraCols > 0 && @extraData > 0) {
		$extraCols .= ',' . join(',',@extraCols);
		$extraData .= ',' . join(',',@extraData);
	}

	# Add invoice trnsaction
	my $sth = DBDo("
		INSERT INTO supplier_invoice_transactions
				(SupplierInvoiceID,Amount$extraCols)
			VALUES
				(
					".DBQuote($invoice->{'ID'}).",
					".DBQuote($detail->{'Amount'})."
					$extraData
				)
	");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		DBRollback();
		return ERR_DB;
	}

	# Grab last ID
	my $ID = DBLastInsertID("supplier_invoice_transactions","ID");

	DBCommit();

	return $ID;
}


# Return an invoice item inventory tracking ID
# Params:
#		ID		- Supplier invoice item ID
# Returns:
# 	Array of hashes with these keys...
#		InventoryTrackingID - Inventory tracking ID of this supplier invoice item
#		QtyChange - Quantity change
#		Price - Value of this change
sub getSupplierInvoiceItemTracking
{
	my ($detail) = @_;

	my @items = ();


	# Verify supplier invoice item ID
	if (!defined($detail->{'ID'}) || $detail->{'ID'} eq "") {
		setError("No (or invalid) supplier invoice item ID provided");
		return ERR_PARAM;
	}

	# Return list of supplier invoice items
	my $sth = DBSelect("
			SELECT
				InventoryTrackingID, QtyChange, Price
			FROM
				supplier_invoice_item_tracking
			WHERE
				SupplierInvoiceItemID = ".DBQuote($detail->{'ID'})."
	");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	my @ret;

	# Fetch row
	while (my $row = hashifyLCtoMC($sth->fetchrow_hashref(), qw( InventoryTrackingID QtyChange Price ))) {
		my $tmp;
		$tmp->{'InventoryTrackingID'} = $row->{'InventoryTrackingID'};
		$tmp->{'QtyChange'} = $row->{'QtyChange'};
		$tmp->{'Price'} = $row->{'Price'};
		push(@ret,$tmp);
	}
	DBFreeRes($sth);

	# If no row, bork out
	if (@ret < 1) {
		setError("Failed to find inventory tracking ID for supplier item ID '".$detail->{'ID'}."'");
		return ERR_NOTFOUND;
	}

	return \@ret;
}


# Backend function to add supplier invoice item entry
# Parameters:
#	ID
# 	InventoryTrackingID
sub addSupplierInvoiceItemEntry
{
	my ($detail) = @_;


	# Verify supplier invoice item ID
	if (!defined($detail->{'ID'}) || $detail->{'ID'} eq "") {
		setError("No (or invalid) supplier invoice item ID provided");
		return ERR_PARAM;
	}

	# Verify qty change
	if (!defined($detail->{'QtyChange'}) || $detail->{'QtyChange'} eq "") {
		setError("No (or invalid) QtyChange provided for supplier invoice item entry");
		return ERR_PARAM;
	}

	# Verify price
	if (!defined($detail->{'Price'}) || $detail->{'Price'} eq "") {
		setError("No (or invalid) QtyChange provided for supplier invoice item entry");
		return ERR_PARAM;
	}

	# Add tracking item
	my $sth = DBDo("
		INSERT INTO supplier_invoice_item_tracking
				(SupplierInvoiceItemID,InventoryTrackingID,QtyChange,Price)
			VALUES
				(
					".DBQuote($detail->{'ID'}).",
					".DBQuote($detail->{'InventoryTrackingID'}).",
					".DBQuote($detail->{'QtyChange'}).",
					".DBQuote($detail->{'Price'})."
				)
	");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	# Grab last ID
	my $ID = DBLastInsertID("supplier_invoice_item_tracking","ID");

	return $ID;
}




1;
# vim: ts=4
