# Year end support
# Copyright (C) 2009-2014, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.



package wiaflos::server::core::YearEnd;

use strict;
use warnings;


use wiaflos::constants;
use awitpt::db::dblayer;
use wiaflos::server::core::GL;

use Math::BigFloat;



# Our current error message
my $error = "";

# Set current error message
# Args: error_message
sub setError
{
	my $err = shift;
	my ($package,$filename,$line) = caller;
	my (undef,undef,undef,$subroutine) = caller(1);

	# Set error
	$error = "$subroutine($line): $err";
}

# Return current error message
# Args: none
sub Error
{
	my $err = $error;

	# Reset error
	$error = "";

	# Return error
	return $err;
}


## @fn performYearEnd
# Function to perform a yearend on the set of books
#
# @param detail Parameter hash ref
# @li StartDate - Yearend start date
# @li EndDate - Yearend end date
# @li REAccountNumber - Retained earnings account number
# @li Type - GL entry types to run the year-end for
sub performYearEnd
{
	my $detail = shift;


	# Verify dates
	if (!defined($detail->{'StartDate'}) || $detail->{'StartDate'} eq "") {
		setError("No start date provided for year end");
		return ERR_PARAM;
	}
	if (!defined($detail->{'EndDate'}) || $detail->{'EndDate'} eq "") {
		setError("No end date provided for year end");
		return ERR_PARAM;
	}
	if (!defined($detail->{'REAccountNumber'}) || $detail->{'REAccountNumber'} eq "") {
		setError("No retained earnings account number provided for year end");
		return ERR_PARAM;
	}

	# Get our account tree
	my $accounts = wiaflos::server::core::GL::getGLAccounts();
	if (ref($accounts) ne "ARRAY") {
		setError(wiaflos::server::core::GL::Error());
		return $accounts;
	}

	# List of transactions to create, hashed by account ID
	my %entryList = ();

	# GL search criteria
	my $search;
	$search->{'StartDate'} = $detail->{'StartDate'};
	$search->{'EndDate'} = $detail->{'EndDate'};

	# Transaction balance
	my $transActBalance = Math::BigFloat->new();
	$transActBalance->precision(-2);

	# Pick out expense accounts and generate transaction list
	foreach my $account(@{$accounts}) {
		# If its not expense or income, ignore it
		if ($account->{'FinCatCode'} ne "E01" && $account->{'FinCatCode'} ne "D01") {
			next;
		}

		# Set account ID
		$search->{'AccountID'} = $account->{'ID'};
		$search->{'Type'} = $detail->{'Type'};

		# Get account balance
		my $glBalance = wiaflos::server::core::GL::getGLAccountBalance($search);
		if (ref($glBalance) ne "HASH") {
			setError(wiaflos::server::core::GL::Error());
			return $glBalance;
		}

		# These are our balances
		my $creditBalance = Math::BigFloat->new($glBalance->{'CreditBalance'});
		my $debitBalance = Math::BigFloat->new($glBalance->{'DebitBalance'});
		$creditBalance->precision(-2);
		$debitBalance->precision(-2);

		# Generate overall balance
		my $balance = $debitBalance->copy()->bsub($creditBalance);

		# Get opposite side of the transaction
		$balance->bmul(-1);

		# Record the balance for this account
		$entryList{$account->{'ID'}} = $balance->bstr();

		# Add up balance
		$transActBalance->bsub($balance);
	}


	DBBegin();

	my $data;

	# Transaction details
	$data->{'Date'} = $detail->{'EndDate'};
	$data->{'Reference'} = "Year End: ".$detail->{'StartDate'}." to ".$detail->{'EndDate'};
	# Is this an audited year-end adjustment?
	if (defined($detail->{'Type'}) && ($detail->{'Type'} & GL_TRANSTYPE_AUDIT) == GL_TRANSTYPE_AUDIT) {
		$data->{'Type'} = GL_TRANSTYPE_AUDIT_YEAREND;
	} else {
		$data->{'Type'} = GL_TRANSTYPE_YEAREND;
	}
	# Create transaction
	my $GLTransActID = wiaflos::server::core::GL::createGLTransaction($data);
	if ($GLTransActID < 1) {
		setError(wiaflos::server::core::GL::Error());
		DBRollback();
		return $GLTransActID;
	}

	# Set defaults for the transaction entries
	$data = undef;
	$data->{'ID'} = $GLTransActID;
	# Loop with transaction entry list
	foreach my $accountID (keys %entryList) {
		# Set the details specific to this entry
		$data->{'GLAccountID'} = $accountID;
		$data->{'Amount'} = $entryList{$accountID};
		# Create entry
		if ((my $res = wiaflos::server::core::GL::linkGLTransaction($data)) < 1) {
			setError(wiaflos::server::core::GL::Error());
			DBRollback();
			return $res;
		}
	}

	# Setup data for the retained earnings entry
	$data = undef;
	$data->{'ID'} = $GLTransActID;
	$data->{'GLAccountNumber'} = $detail->{'REAccountNumber'};
	$data->{'Amount'} = $transActBalance->bstr();
	# Create entry
	if ((my $res = wiaflos::server::core::GL::linkGLTransactionByAccountNumber($data)) < 1) {
		setError(wiaflos::server::core::GL::Error());
		DBRollback();
		return $res;
	}

	# Post transaction
	$data = undef;
	$data->{'ID'} = $GLTransActID;
	if ((my $res = wiaflos::server::core::GL::postGLTransaction($data)) != 0) {
		setError(wiaflos::server::core::GL::Error());
		DBRollback();
		return $res;
	}

	DBCommit();

	return RES_OK;
}




1;
# vim: ts=4
