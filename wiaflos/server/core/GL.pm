# General ledger functions
# Copyright (C) 2009-2014, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2006-2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.




package wiaflos::server::core::GL;

use strict;
use warnings;


use wiaflos::constants;
use awitpt::db::dblayer;
use awitpt::cache;

use Math::BigFloat;

# Exporter stuff
require Exporter;
our (@ISA,@EXPORT,@EXPORT_OK);
@ISA = qw(Exporter);
@EXPORT = qw(
	GL_TRANSTYPE_NORMAL
	GL_TRANSTYPE_YEAREND
	GL_TRANSTYPE_AUDIT
	GL_TRANSTYPE_AUDIT_YEAREND
);
@EXPORT_OK = ();


use constant {
	GL_TRANSTYPE_NORMAL	=>	1,
	GL_TRANSTYPE_YEAREND	=>	2,
	GL_TRANSTYPE_AUDIT	=>	4,
	GL_TRANSTYPE_AUDIT_YEAREND	=>	8
};


# Our current error message
my $error = "";

# Set current error message
# Args: error_message
sub setError
{
	my $err = shift;
	my ($package,$filename,$line) = caller;
	my (undef,undef,undef,$subroutine) = caller(1);

	# Set error
	$error = "$subroutine($line): $err";
}

# Return current error message
# Args: none
sub Error
{
	my $err = $error;

	# Reset error
	$error = "";

	# Return error
	return $err;
}


# Backend function to build account item hash
sub sanitizeRawGLAccountItem
{
	my $rawData = shift;


	my $item;
	$item->{'ID'} = $rawData->{'ID'};

	$item->{'Number'} = getGLAccountNumberFromID($rawData->{'ID'});

	$item->{'ParentGLAccountID'} = $rawData->{'ParentGLAccountID'};
	$item->{'Name'} = $rawData->{'Name'};
	$item->{'FinCatCode'} = $rawData->{'FinCatCode'};
	$item->{'FinCatDescription'} = $rawData->{'FinCatDescription'};
	$item->{'RwCatCode'} = $rawData->{'RwCatCode'};
	$item->{'RwCatDescription'} = $rawData->{'RwCatDescription'};

	return $item;
}



# Backend function to build GL transaction item hash
sub sanitizeRawGLTransactionItem
{
	my $rawData = shift;


	my $item;

	$item->{'ID'} = $rawData->{'ID'};
	$item->{'TransactionDate'} = $rawData->{'TransactionDate'};
	$item->{'Reference'} = $rawData->{'Reference'};
	$item->{'Type'} = $rawData->{'Type'};
	$item->{'Posted'} = $rawData->{'Posted'};

	return $item;
}



# Check if GL account ID exists
# Backend function, takes 1 parameter which is the GL account ID
sub GLAccountIDExists
{
	my $GLAccID = shift;


	# Select account count
	my $rows = DBSelectNumResults("FROM gl_accounts WHERE ID = ".DBQuote($GLAccID));
	if (!defined($rows)) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	# Check we got a result
	if ($rows < 1) {
		setError("Error finding GL account '$GLAccID'");
		return ERR_NOTFOUND;
	}

	return 1;
}


# Check if a GL account code exists
sub GLAccountCodeExists
{
	my ($parentID,$code) = @_;
	my $extra_sql = "";


	# If we have a parent use it
	if (defined($parentID) && $parentID ne "") {
		$extra_sql .= " AND ParentGLAccountID = ".DBQuote($parentID);
	} else {
		$extra_sql .= " AND ParentGLAccountID IS NULL";
	}

	# Select account count
	my $rows = DBSelectNumResults("FROM gl_accounts WHERE Code = ".DBQuote($code)." $extra_sql");
	if (!defined($rows)) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	return $rows > 0 ? 1 : 0;
}


# Check if transaction exists
# Backend function, takes 1 parameter which is the transaction ID
sub GLTransactionIDExists
{
	my $transActID = shift;


	# Select transaction count
	my $rows = DBSelectNumResults("FROM gl_transactions WHERE ID = ".DBQuote($transActID));
	if (!defined($rows)) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	return $rows > 0 ? 1 : 0;
}


# Get GL account ID from GL account number
# Backend function, takes a GL account number and returns the GL account ID
sub getGLAccountIDFromNumber
{
	my $GLAccNumber = shift;


	# Check cache
	my ($cache_res,$cache) = cacheGetKeyPair('GL/Number-to-AccountID',$GLAccNumber);
	if ($cache_res != RES_OK) {
		setError(awitpt::cache::Error());
		return $cache_res;
	}
	return $cache if (defined($cache));


	# Pull list of GL accounts
	my $sth = DBSelect("
		SELECT
			ID, Code
		FROM
			gl_accounts
	");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	# Fetch rows, while we not found anything
	my $GLAccID;
	while ((my $row = hashifyLCtoMC($sth->fetchrow_hashref(),qw( ID Code ))) && !defined($GLAccID)) {
		my $tmpGLAccNumber = getGLAccountNumberFromID($row->{'ID'});
		# If not defined b0rk out
		if (!defined($tmpGLAccNumber)) {
			DBFreeRes($sth);
			return ERR_UNKNOWN;
		}

		# Check if we found it
		if ($GLAccNumber eq $tmpGLAccNumber) {
			$GLAccID = $row->{'ID'};
		}
	}

	DBFreeRes($sth);

	if (!defined($GLAccID)) {
		setError("Error finding GL account '$GLAccNumber'");
		return ERR_NOTFOUND;
	}

	# Cache this
	$cache_res = cacheStoreKeyPair('GL/Number-to-AccountID',$GLAccNumber,$GLAccID);
	if ($cache_res != RES_OK) {
		setError(awitpt::cache::Error());
		return $cache_res;
	}

	return $GLAccID;
}


# Return financial category ID from code
sub getGLFinCatIDFromCode
{
	my $finCatCode = shift;


	# Select financial category
	my $sth = DBSelect("
		SELECT
			ID
		FROM
			gl_financial_categories
		WHERE
			Code = ".DBQuote($finCatCode)."
	");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	my $row = hashifyLCtoMC($sth->fetchrow_hashref(),qw( ID ));
	DBFreeRes($sth);

	# Check we got a result
	if (!defined($row)) {
		setError("Error finding financial category '$finCatCode'");
		return ERR_NOTFOUND;
	}

	return $row->{'ID'};
}


# Return reporting category ID from code
sub getGLRwCatIDFromCode
{
	my $rwCatCode = shift;


	# Select reporting category
	my $sth = DBSelect("
		SELECT
			ID
		FROM
			gl_reportwriter_categories
		WHERE
			Code = ".DBQuote($rwCatCode)."
	");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	my $row = hashifyLCtoMC($sth->fetchrow_hashref(),qw( ID ));
	DBFreeRes($sth);

	# Check we got a result
	if (!defined($row)) {
		setError("Error finding reporting category '$rwCatCode'");
		return ERR_NOTFOUND;
	}

	return $row->{'ID'};
}


# Return report writer categories
sub getGLRwCats
{
	# Select reporting category
	my $sth = DBSelect("
		SELECT
			ID, Code, Description
		FROM
			gl_reportwriter_categories
	");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	# Fetch rows
	my @entries;
	while (my $row = hashifyLCtoMC($sth->fetchrow_hashref(),qw( ID Code Description ))) {
		my $entry;

		$entry->{'ID'} = $row->{'ID'};
		$entry->{'Code'} = $row->{'Code'};
		$entry->{'Description'} = $row->{'Description'};

		push(@entries,$entry);
	}

	DBFreeRes($sth);

	return \@entries;
}


# Resolve the full GL account reference
sub getGLAccountNumberFromID
{
	my $accID = shift;


	# Check cache
	my ($cache_res,$cache) = cacheGetKeyPair('GL/AccountID-to-Number',$accID);
	if ($cache_res != RES_OK) {
		setError(awitpt::cache::Error());
		return $cache_res;
	}
	return $cache if (defined($cache));


	# Return account ref & parent
	my $sth = DBSelect("
		SELECT
			Code, ParentGLAccountID
		FROM
			gl_accounts
		WHERE
			ID = ".DBQuote($accID)."
	");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		return undef;
	}

	# Grab row & free
	my $row = hashifyLCtoMC($sth->fetchrow_hashref(),qw( Code ParentGLAccountID ));
	DBFreeRes($sth);

	# Get parent acc number ... or if we the parent, return our acc number
	my $accnum = "";
	# If we have parent, get its ref
	if (defined($row->{'ParentGLAccountID'})) {
		# Check return value
		if (my $ret = getGLAccountNumberFromID($row->{'ParentGLAccountID'})) {
			# And add ref to our ref
			$accnum = "$ret:".$row->{'Code'};
		} else {
			return undef;
		}
	} else {
		$accnum = $row->{'Code'};
	}

	# Cache this
	$cache_res = cacheStoreKeyPair('GL/AccountID-to-Number',$accID,$accnum);
	if ($cache_res != RES_OK) {
		setError(awitpt::cache::Error());
		return $cache_res;
	}


	return $accnum;
}


# Check GL account financial category
# Returns:
#	1	- match
#	0	- no match
sub checkGLAccountFinCat
{
	my ($GLAccID,$finCatCode) = @_;


	# Check and return GL account financial category
	my $sth = DBSelect("
		SELECT
			gl_financial_categories.Code
		FROM
			gl_financial_categories, gl_accounts
		WHERE
			gl_accounts.ID = ".DBQuote($GLAccID)."
			AND gl_financial_categories.ID = gl_accounts.FinCatID
	");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	# Grab row
	my $row = hashifyLCtoMC($sth->fetchrow_hashref(),qw( Code ));
	DBFreeRes($sth);

	# Check we got a result
	if (!defined($row)) {
		setError("Error finding GL account '$GLAccID'");
		return ERR_NOTFOUND;
	}

	return $row->{'Code'} eq $finCatCode ? 1 : 0;
}



## @fn getGLAccountTree
# Resolve the set of accounts into a tree
#
# @returns Array ref of hash refs, @see getGLAccounts with the additional items...
# @li Children Array ref of hash refs, children of this account
# @li Level Depth level of this account
sub getGLAccountTree
{
	# Grab account list
	my $accounts = getGLAccounts();
	if (ref($accounts) ne "ARRAY") {
		return $accounts;
	}

	my %accountsByID;
	my @paccounts; # Parent accounts
	foreach my $account (@{$accounts}) {
		# Generate our rows by ID table
		$accountsByID{$account->{'ID'}} = $account;
		# Check if we're a parent account
		push(@paccounts,$account) if (!defined($account->{'ParentGLAccountID'}));
	}
	# Resolve children
	foreach my $account (@{$accounts}) {
		# Make sure we're a child and not a parent
		if (defined($account->{'ParentGLAccountID'})) {
			# This is the account parent
			my $parent = $accountsByID{$account->{'ParentGLAccountID'}};
			# Attach us as a child
			push(@{$parent->{'Children'}}, $account);
		}
	}
	# Resolve levels
	foreach my $parent (@paccounts) {
		# Resolve the account level
		sub resolveLevel {
			my ($raccount,$plevel) = @_;

			# We 1 level down
			$raccount->{'Level'} = $plevel + 1;
			# Loop with children
			foreach my $caccount (@{$raccount->{'Children'}}) {
				resolveLevel($caccount,$raccount->{'Level'});
			}
		}
		# Parents are level 0, so we set this to -1
		resolveLevel($parent,-1);
	}

	return \@paccounts;
}


# Return an array of general ledger accounts
sub getGLAccounts
{
	my @accounts = ();

	# Return list of GL accounts
	my $sth = DBSelect("
		SELECT
			gl_accounts.ID, gl_accounts.ParentGLAccountID, gl_accounts.Code, gl_accounts.Name,

			gl_financial_categories.Code AS FinCatCode, gl_financial_categories.Description AS FinCatDescription,

			gl_reportwriter_categories.Code AS RwCatCode, gl_reportwriter_categories.Description AS RwCatDescription

		FROM
			gl_accounts, gl_financial_categories, gl_reportwriter_categories

		WHERE
			gl_financial_categories.ID = gl_accounts.FinCatID
			AND gl_reportwriter_categories.ID = gl_accounts.RwCatID
	");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	# Fetch rows
	while (my $row = hashifyLCtoMC($sth->fetchrow_hashref(),
			qw( ID ParentGLAccountID Code Name FinCatCode FinCatDescription RwCatCode RwCatDescription )
	)) {
		my $account = sanitizeRawGLAccountItem($row);

		push(@accounts,$account);
	}

	DBFreeRes($sth);

	return \@accounts;
}


# Return a hash containing account details
# Parameters:
#		AccountID		- GL account ID
#		AccountNumber	- GL account reference
sub getGLAccount
{
	my ($data) = @_;


	my $GLAccID;

	# Check which 'mode' we operating in
	if (!defined($data->{'AccountID'}) || $data->{'AccountID'} < 1) {
		# Verify GL account ref
		if (!defined($data->{'AccountNumber'}) || $data->{'AccountNumber'} eq "") {
			setError("No (or invalid) account number provided");
			return ERR_PARAM;
		}

		# Check if account number exists
		if (($GLAccID = getGLAccountIDFromNumber($data->{'AccountNumber'})) < 1) {
			setError(Error());
			return $GLAccID;
		}
	} else {
		$GLAccID = $data->{'AccountID'};
	}

	# Verify account ID
	if (!$GLAccID || $GLAccID < 1) {
		setError("No (or invalid) account number or ID provided");
		return ERR_PARAM;
	}

	my $sth = DBSelect("
		SELECT
			gl_accounts.ID, gl_accounts.ParentGLAccountID, gl_accounts.Code, gl_accounts.Name,

			gl_financial_categories.Code AS FinCatCode, gl_financial_categories.Description AS FinCatDescription,

			gl_reportwriter_categories.Code AS RwCatCode, gl_reportwriter_categories.Description AS RwCatDescription

		FROM
			gl_accounts, gl_financial_categories, gl_reportwriter_categories

		WHERE
			gl_accounts.ID = ".DBQuote($GLAccID)."
			AND gl_financial_categories.ID = gl_accounts.FinCatID
			AND gl_reportwriter_categories.ID = gl_accounts.RwCatID
	");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	# Fetch row
	my $row = hashifyLCtoMC($sth->fetchrow_hashref(),
			qw( ID ParentGLAccountID Code Name FinCatCode FinCatDescription RwCatCode RwCatDescription )
	);
	DBFreeRes($sth);

	# Check we got a result
	if (!defined($row)) {
		setError("Error finding account '$GLAccID'");
		return ERR_NOTFOUND;
	}


	return sanitizeRawGLAccountItem($row);
}


# Return the next sub account code
# Parameters:
#		AccountNumber	- GL account reference
sub getNextGLSubAccountCode
{
	my ($data) = @_;


	# Verify GL account number
	if (!defined($data->{'AccountNumber'}) || $data->{'AccountNumber'} eq "") {
		setError("No (or invalid) GL account number provided");
		return ERR_PARAM;
	}

	# Check GL account exists
	my $GLAccID;
	if (($GLAccID = getGLAccountIDFromNumber($data->{'AccountNumber'})) < 1) {
		setError(Error());
		return $GLAccID;
	}

	# Select last account
	my $sth = DBSelect("
		SELECT
			Code
		FROM
			gl_accounts
		WHERE
			ParentGLAccountID = ".DBQuote($GLAccID)."
		ORDER BY Code DESC
		LIMIT 1
	");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	# Fetch row
	my $row = hashifyLCtoMC($sth->fetchrow_hashref(), qw( Code ));
	DBFreeRes($sth);

	# Check we got a result, if not return 1
	if (!defined($row)) {
		return 1;
	}

	return $row->{'Code'} + 1;
}


## @fn getGLTransactions($data)
# Return an array of general ledger transactions
#
# @param data Parameter hash ref
# @li AccountID Limit transactions to those relating to this account
# @li StartDate	Optional start date
# @li EndDate Optional end date
# @li Type Optional transaction type
#
# @returns Array ref of hash refs
# @li ID GL entry ID
# @li TransactionDate Transaction date
# @li Reference GL entry reference
# @li Posted 0 if unposted, 1 if posted
# @li Type Transaction type
sub getGLTransactions
{
	my $data = shift;

	my @transactions = ();

	# Extra SQL we may need
	my $extraSQL = "";
	my $extraTables = "";
	my $extraEndSQL = "";

	# Check if we have an account ID
	if (defined($data->{'AccountID'}) && $data->{'AccountID'} > 0) {
		$extraTables .= ", gl_entries";
		$extraSQL .= "AND gl_entries.GLAccountID = ".DBQuote($data->{'AccountID'})." ";
		$extraSQL .= "AND gl_entries.GLTransactionID = gl_transactions.ID ";
		$extraEndSQL .= "GROUP BY gl_transactions.ID ";
	}

	# Check if we must use the start date
	if (defined($data->{'StartDate'}) && $data->{'StartDate'} ne "") {
		$extraSQL .= "AND gl_transactions.TransactionDate >= ".DBQuote($data->{'StartDate'})." ";
	}
	# Check if we must use the end date
	if (defined($data->{'EndDate'}) && $data->{'EndDate'} ne "") {
		$extraSQL .= "AND gl_transactions.TransactionDate <= ".DBQuote($data->{'EndDate'})." ";
	}

	# Check if we're filtering on type
	my $typeFilter = GL_TRANSTYPE_NORMAL;
	if (defined($data->{'Type'}) && $data->{'Type'} ne "") {
		$typeFilter = $data->{'Type'};
	}

	# Return list of GL transactions
	my $sth = DBSelect("
		SELECT
			ID, TransactionDate, Reference, Type, Posted
		FROM
			gl_transactions $extraTables
		WHERE
			1 = 1
			$extraSQL
		$extraEndSQL
	");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	# Fetch rows
	while (my $row = hashifyLCtoMC($sth->fetchrow_hashref(), qw( ID TransactionDate Reference Type Posted ))) {
		# Check filter, if this is not one of our items, continue
		if (! ($typeFilter & $row->{'Type'})) {
			next;
		}

		my $transaction = sanitizeRawGLTransactionItem($row);

		push(@transactions,$transaction);
	}

	DBFreeRes($sth);

	return \@transactions;
}


# Return a hash containing a GL transaction
# Parameters:
#		ID	- Transaction ID
sub getGLTransaction
{
	my ($detail) = @_;


	# Verify params
	if (!defined($detail->{'ID'}) || $detail->{'ID'} < 1) {
		setError("Transaction ID not provided");
		return ERR_PARAM;
	}

	# Return list of GL transactions
	my $sth = DBSelect("
		SELECT
			ID, TransactionDate, Reference, Posted
		FROM
			gl_transactions
		WHERE
			ID = ".DBQuote($detail->{'ID'})."
	");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	# Fetch rows
	my $row = hashifyLCtoMC($sth->fetchrow_hashref(),qw( ID TransactionDate Reference Posted ));
	DBFreeRes($sth);

	# Check we got a result
	if (!$row) {
		setError("GL transaction '".$detail->{'ID'}."' not found");
		return ERR_NOTFOUND;
	}

	return sanitizeRawGLTransactionItem($row);
}


## @fn createGLTransaction($data)
# Create GL transaction
#
# @param data Parameter hash ref
# @li Date Date of transaction
# @li Reference Reference of transaction
# @li Type Optional transaction type
#
# @returns Transaction ID
sub createGLTransaction
{
	my ($detail) = @_;


	# Extra SQL we may need
	my $extraColumns = "";
	my $extraValues = "";

	# Verify date
	if (!defined($detail->{'Date'}) || $detail->{'Date'} eq "") {
		setError("No date provided for GL transaction");
		return ERR_PARAM;
	}

	# Verify reference
	if (!defined($detail->{'Reference'}) || $detail->{'Reference'} eq "") {
		setError("No reference provided for GL transaction");
		return ERR_PARAM;
	}

	# Verify type
	if (defined($detail->{'Type'}) && $detail->{'Type'} ne "") {
		$extraColumns .= ",Type";
		$extraValues .= ",".DBQuote($detail->{'Type'});
	}

	# Create GL transaction
	my $sth = DBDo("
		INSERT INTO gl_transactions
				(TransactionDate,Reference$extraColumns)
			VALUES
				(
					".DBQuote($detail->{'Date'}).",
					".DBQuote($detail->{'Reference'})."
					$extraValues
				)
	");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	# Grab last ID
	my $ID = DBLastInsertID("gl_transactions","ID");

	return $ID;
}


# Link GL transaction using account number instead of ID
# Parameters:
#		ID				- Transaction ID
#		GLAccountNumber	- GL account number
#		Amount			- Amount to post
#		Reference		- Transaction reference
sub linkGLTransactionByAccountNumber
{
	my ($data) = @_;


	# Check GL account exists
	my $GLAccID;
	if (($GLAccID = getGLAccountIDFromNumber($data->{'GLAccountNumber'})) < 1) {
		setError(Error());
		return $GLAccID;
	}

	# Add ID
	$data->{'GLAccountID'} = $GLAccID;

	# And Link
	return linkGLTransaction($data);
}


# Link GL transaction
# Parameters:
#		ID				- Transaction ID
#		GLAccountID		- GL account ID
#		Amount			- Amount to post
#		Reference		- Transaction reference
sub linkGLTransaction
{
	my ($data) = @_;


	# Verify params
	if (!defined($data->{'ID'}) || $data->{'ID'} < 1) {
		setError("Transaction ID not provided");
		return ERR_PARAM;
	}

	if (!defined($data->{'GLAccountID'}) || $data->{'GLAccountID'} < 1) {
		setError("Account ID not provided for transaction '".$data->{'ID'}."'");
		return ERR_PARAM;
	}

	if (!defined($data->{'Amount'}) && !defined($data->{'Credit'}) && !defined($data->{'Debit'})) {
		setError("Amount or Credit or Debit must be specified");
		return ERR_PARAM;
	}
	if (defined($data->{'Credit'}) && defined($data->{'Debit'})) {
		setError("Parameter 'Credit' and 'Debit' cannot be specified for the same transcation");
		return ERR_PARAM;
	}
	if ((defined($data->{'Credit'}) && defined($data->{'Amount'})) || (defined($data->{'Debit'}) && defined($data->{'Amount'}))) {
		setError("Parameter 'Credit'/'Debit' is incompatible with 'Amount'");
		return ERR_PARAM;
	}

	# Lets get some account info
	my $params;
	$params->{'AccountID'} = $data->{'GLAccountID'};
	my $account = getGLAccount($params);
	if (ref $account ne "HASH") {
		return $account;
	}

	# Pull in amount
	my $cleanAmount = Math::BigFloat->new();
	$cleanAmount->precision(-2);

	# Lets see what we going to pull in...
	# We use ABS to get positive value no matte what
	# We use bneg() to negate this in case of a debit
	if (defined($data->{'Credit'})) {
		$cleanAmount->badd($data->{'Credit'})->babs();
		# NK: Credit vs. Debit based on account type, are we going to reverse this?
		foreach my $finCatCode ("A01", "B01", "D01", "E01") {
			# Check for match
			if ($account->{'FinCatCode'} eq $finCatCode) {
				$cleanAmount->bneg();
				last;
			}
		}
	} elsif (defined($data->{'Debit'})) {
		$cleanAmount->bsub($data->{'Debit'})->babs();

		# NK: Credit vs. Debit based on account type, are we going to reverse this?
		foreach my $finCatCode ("B01","C01","D01") {
			# Check for match
			if ($account->{'FinCatCode'} eq $finCatCode) {
				last;
			}
		}

	} elsif (defined($data->{'Amount'})) {
		$cleanAmount->badd($data->{'Amount'});
	}

	#NK - wtf .... should we or shouldn't we check?
	# still not sure, its disabled for a reason I guess ... ??
#	if ($cleanAmount->is_zero()) {
#		setError("Amount cannot be zero");
#		return ERR_AMTZERO;
#	}

	# Decide what to do with ref
	my $ref = defined($data->{'Reference'}) ? DBQuote($data->{'Reference'}) : "NULL";

	# Return list of GL transactions
	my $sth = DBSelect("
		SELECT
			Posted
		FROM
			gl_transactions
		WHERE
			ID = ".DBQuote($data->{'ID'})."
	");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	my $row = hashifyLCtoMC($sth->fetchrow_hashref(),qw( Posted ));
	DBFreeRes($sth);

	# Check we got a result
	if (!defined($row)) {
		setError("Error finding transaction '".$data->{'ID'}."'");
		return ERR_NOTFOUND;
	}

	# Check if we not posted
	if ($row->{'Posted'} == '1') {
		setError("Cannot link to a posted transaction '".$data->{'ID'}."'");
		return ERR_POSTED;
	}

	# Create GL entry
	$sth = DBDo("
		INSERT INTO gl_entries
				(GLTransactionID,GLAccountID,Reference,Amount)
			VALUES
				(
					".DBQuote($data->{'ID'}).",
					".DBQuote($data->{'GLAccountID'}).",
					$ref,
					".DBQuote($cleanAmount->bstr())."
				)
	");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	# Grab last ID
	my $ID = DBLastInsertID("gl_entries","ID");

	return $ID;
}


# Post GL transaction
# Parameters:
#		ID		- Transaction ID
sub postGLTransaction
{
	my ($data) = @_;


	# Verify params
	if (!defined($data->{'ID'}) || $data->{'ID'} < 1) {
		setError("No transaction ID given or invalid ID provided");
		return ERR_PARAM;
	}

	# Check transaction exists
	if ((my $res = GLTransactionIDExists($data->{'ID'})) != 1) {
		# If not found err
		if ($res == 0) {
			setError("GL transaction ID '".$data->{'ID'}."' does not exist");
			return ERR_NOTFOUND;
		} else {
			setError(Error());
		}
		return $res;
	}

	# Return list of GL transactions
	my $sth = DBSelect("
		SELECT
			ID, Amount
		FROM
			gl_entries
		WHERE
			GLTransactionID = ".DBQuote($data->{'ID'})."
	");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	# Fetch rows
	my $balance = Math::BigFloat->new();
	$balance->precision(-2);
	my $count = 0;
	while (my $row = hashifyLCtoMC($sth->fetchrow_hashref(), qw( ID Amount ))) {
		$balance->badd($row->{'Amount'});
		$count++;
	}

	# Check we got a proper result
	if ($count < 2) {
		setError("Transaction '".$data->{'ID'}."' needs at least two entries (to balance), $count entries found.");
		return ERR_NOBALANCE
	}

	DBFreeRes($sth);

	# Check we balance
	if (!$balance->is_zero()) {
		setError("Transaction '".$data->{'ID'}."' needs to balance before it can be posted, balance is: ".$balance->bstr());
		return ERR_NOBALANCE;
	}


	# Create GL transaction
	$sth = DBDo("
		UPDATE
			gl_transactions
		SET
			Posted = '1'
		WHERE
			ID = ".DBQuote($data->{'ID'})."
	");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	return 0;
}


# Remove GL transaction
# Parameters:
#		ID		- Transaction ID
sub removeGLTransaction
{
	my ($data) = @_;


	# Verify params
	if (!defined($data->{'ID'}) || $data->{'ID'} < 1) {
		setError("No transaction ID given or invalid ID provided");
		return ERR_PARAM;
	}

	# Check transaction exists
	if ((my $res = GLTransactionIDExists($data->{'ID'})) != 1) {
		# If not found err
		if ($res == 0) {
			setError("GL transaction ID '".$data->{'ID'}."' does not exist");
			return ERR_NOTFOUND;
		} else {
			setError(Error());
		}
		return $res;
	}

	# Remove GL transaction
	my $sth = DBDo("
		DELETE FROM
			gl_transactions
		WHERE
			ID = ".DBQuote($data->{'ID'})."
	");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	return 0;
}


# Return an array of transaction entries
# Parameters:
#		ID			- Transaction ID
sub getGLTransactionEntries
{
	my ($data) = @_;
	my @entries = ();


	# Verify params
	if (!defined($data->{'ID'}) || $data->{'ID'} < 1) {
		setError("No transaction ID given or invalid ID provided");
		return ERR_PARAM;
	}

	# Check transaction exists
	if ((my $res = GLTransactionIDExists($data->{'ID'})) != 1) {
		# If not found, err
		if ($res == 0) {
			setError("GL transaction ID '".$data->{'ID'}."' does not exist");
			return ERR_NOTFOUND;
		} else {
			setError(Error());
		}
		# else err with result
		return $res;
	}

	# Return list of transaction entries
	my $sth = DBSelect("
		SELECT
			gl_entries.ID, gl_entries.GLTransactionID, gl_entries.GLAccountID, gl_entries.Amount, gl_entries.Reference,
			gl_accounts.Name AS GLAccountName, gl_transactions.Reference AS GLTransactionRefernece
		FROM
			gl_entries, gl_accounts, gl_transactions
		WHERE
			gl_transactions.ID = ".DBQuote($data->{'ID'})."
			AND gl_entries.GLTransactionID = gl_transactions.ID
			AND gl_accounts.ID = gl_entries.GLAccountID
	");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	# Fetch rows
	while (my $row = hashifyLCtoMC($sth->fetchrow_hashref(),
			qw( ID GLTransactionID GLAccountID Amount Reference GLAccountName GLTransactionRefernece )
	)) {
		my $entry;

		$entry->{'ID'} = $row->{'ID'};
		$entry->{'TransactionID'} = $row->{'GLTransactionID'};
		$entry->{'TransactionReference'} = $row->{'GLTransactionRefernece'};
		$entry->{'GLAccountID'} = $row->{'GLAccountID'};
		$entry->{'GLAccountName'} = $row->{'GLAccountName'};
		$entry->{'GLAccountNumber'} = getGLAccountNumberFromID($row->{'GLAccountID'});
		# Pull in amount
		my $cleanAmount = Math::BigFloat->new($row->{'Amount'});
		$cleanAmount->precision(-2);
		$entry->{'Amount'} = $cleanAmount->bstr();
		$entry->{'Reference'} = $row->{'Reference'};

		push(@entries,$entry);
	}

	DBFreeRes($sth);

	return \@entries;
}


## @fn getGLAccountEntries($data)
# Return an array account entries
#
# @param data Parameter hash ref
# @li AccountID Optional account ID
# @li AccountNumber Optional account number
# @li StartDate	Optional start date
# @li EndDate Optional end date
# @li BalanceBroughtForward Optional flag to indicate if we must generate a balance brought forward entry, 'y[es]|1'
# @li Type Optional transaction type
#
# @returns Array ref of hash refs
# @li ID GL entry ID
# @li TransactionID GL transaction ID
# @li Reference GL entry reference
# @li Amount Amount
# @li TransactionDate Transaction date
# @li TransactionReference Transaction reference
sub getGLAccountEntries
{
	my ($data) = @_;
	my @entries = ();


	my $GLAccID;

	# Check which 'mode' we operating in
	if (!defined($data->{'AccountID'}) || $data->{'AccountID'} < 1) {
		# Verify GL account ref
		if (!defined($data->{'AccountNumber'}) || $data->{'AccountNumber'} eq "") {
			setError("No (or invalid) account number provided");
			return ERR_PARAM;
		}

		# Check if account number exists
		if (($GLAccID = getGLAccountIDFromNumber($data->{'AccountNumber'})) < 1) {
			setError(Error());
			return $GLAccID;
		}
	} else {
		$GLAccID = $data->{'AccountID'};
	}

	# Verify account ID
	if (!$GLAccID || $GLAccID < 1) {
		setError("No (or invalid) account number or ID provided");
		return ERR_PARAM;
	}

	# Check if we're filtering on type
	my $typeFilter = GL_TRANSTYPE_NORMAL;
	if (defined($data->{'Type'}) && $data->{'Type'} ne "") {
		$typeFilter = $data->{'Type'};
	}

	# Extra SQL we may need
	my $extraSQL = "";

	# Check if we must use the start date
	if (defined($data->{'StartDate'}) && $data->{'StartDate'} ne "") {
		# Check if we need to generate a BBF
		if (defined($data->{'BalanceBroughtForward'}) && (
				$data->{'BalanceBroughtForward'} =~ /^y(?:es)?$/i || $data->{'BalanceBroughtForward'} eq "1"
			)
		) {
			# Return list of transaction entries before the start date, these we add-up and generate a BBF (balance brought forward)
			my $sth = DBSelect("
				SELECT
					gl_entries.Amount, gl_transactions.Type
				FROM
					gl_entries, gl_transactions
				WHERE
					gl_entries.GLAccountID = ".DBQuote($GLAccID)."
					AND gl_transactions.ID = gl_entries.GLTransactionID
					AND gl_transactions.Posted = '1'
					AND gl_transactions.TransactionDate < ".DBQuote($data->{'StartDate'})."
			");

			if (!$sth) {
				setError(awitpt::db::dblayer::Error());
				return ERR_DB;
			}

			# Generate balance brought forward amount
			my $bbfAmount = Math::BigFloat->new();
			$bbfAmount->precision(-2);
			while (my $row = hashifyLCtoMC($sth->fetchrow_hashref(),qw( Amount Type ))) {
				# Check filter, if this is not one of our items, continue
				if (! ($typeFilter & $row->{'Type'})) {
					next;
				}
				$bbfAmount->badd($row->{'Amount'});
			}

			DBFreeRes($sth);

			# Generate entry
			my $entry;
			$entry->{'ID'} = -99;
			$entry->{'TransactionID'} = -99;
			$entry->{'Reference'} = "* Balance Brought Forward *";
			$entry->{'Amount'} = $bbfAmount->bstr();
			$entry->{'TransactionDate'} = $data->{'StartDate'};
			$entry->{'TransactionType'} = GL_TRANSTYPE_NORMAL;

			push(@entries,$entry);
		}

		$extraSQL .= " AND gl_transactions.TransactionDate >= ".DBQuote($data->{'StartDate'});
	}

	# Check if we must use an end date
	if (defined($data->{'EndDate'}) && $data->{'EndDate'} ne "") {
		$extraSQL .= " AND gl_transactions.TransactionDate <= ".DBQuote($data->{'EndDate'});
	}

	# Return list of transaction entries
	my $sth = DBSelect("
		SELECT
			gl_entries.ID, gl_entries.GLTransactionID, gl_entries.Amount, gl_entries.Reference,

			gl_transactions.TransactionDate, gl_transactions.Reference AS TransactionReference,
			gl_transactions.Type AS TransactionType

		FROM
			gl_entries, gl_transactions

		WHERE
			gl_entries.GLAccountID = ".DBQuote($GLAccID)."
			AND gl_transactions.ID = gl_entries.GLTransactionID
			AND gl_transactions.Posted = '1'
			$extraSQL
	");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	# Fetch rows
	while (my $row = hashifyLCtoMC($sth->fetchrow_hashref(),
			qw( ID GLTransactionID Amount Reference TransactionDate TransactionReference TransactionType )
	)) {
		# Check filter, if this is not one of our items, continue
		if (! ($typeFilter & $row->{'TransactionType'})) {
			next;
		}

		my $entry;
		$entry->{'ID'} = $row->{'ID'};
		$entry->{'TransactionID'} = $row->{'GLTransactionID'};
		$entry->{'Reference'} = $row->{'Reference'};

		# Pull in amount
		my $cleanAmount = Math::BigFloat->new($row->{'Amount'});
		$cleanAmount->precision(-2);
		$entry->{'Amount'} = $cleanAmount->bstr();

		$entry->{'TransactionDate'} = $row->{'TransactionDate'};
		$entry->{'TransactionReference'} = $row->{'TransactionReference'};
		$entry->{'TransactionType'} = $row->{'TransactionType'};

		push(@entries,$entry);
	}

	DBFreeRes($sth);

	return \@entries;
}


## @fn getGLAccountBalance($data)
# Return GL account balance
#
# @param data Parameter hash ref
# @li AccountID - Optional account ID
# @li AccountNumber - Optional account number
# @li StartDate	- Optional start date
# @li EndDate - Optional end date
# @li EndDateExcl - Exclude end date from search, 'y[es]|1'
# @li Type Optional transaction type
#
# @returns Hash of account balances
# @li CreditBalance - Credit balance
# @li DebitBalance - Debit balance
sub getGLAccountBalance
{
	my ($data) = @_;


	my $GLAccID;

	# Check which 'mode' we operating in
	if (!defined($data->{'AccountID'}) || $data->{'AccountID'} < 1) {
		# Verify GL account ref
		if (!defined($data->{'AccountNumber'}) || $data->{'AccountNumber'} eq "") {
			setError("No (or invalid) account number provided");
			return ERR_PARAM;
		}

		# Check if account number exists
		if (($GLAccID = getGLAccountIDFromNumber($data->{'AccountNumber'})) < 1) {
			setError(Error());
			return $GLAccID;
		}
	} else {
		$GLAccID = $data->{'AccountID'};
	}

	# Verify account ID
	if (!$GLAccID || $GLAccID < 1) {
		setError("No (or invalid) account number or ID provided");
		return ERR_PARAM;
	}

	# Check if we're filtering on type
	my $typeFilter = GL_TRANSTYPE_NORMAL;
	if (defined($data->{'Type'}) && $data->{'Type'} ne "") {
		$typeFilter = $data->{'Type'};
	}

	# Extra SQL we may need
	my $extraSQL = "";

	# Check if we must use the start date
	if (defined($data->{'StartDate'}) && $data->{'StartDate'} ne "") {
		$extraSQL .= " AND gl_transactions.TransactionDate >= ".DBQuote($data->{'StartDate'});
	}

	# Check if we must use an end date
	if (defined($data->{'EndDate'}) && $data->{'EndDate'} ne "") {
		if (defined($data->{'EndDateExcl'}) && ( $data->{'EndDateExcl'} =~ /^y(?:es)?$/i || $data->{'EndDateExcl'} eq "1" )) {
			$extraSQL .= " AND gl_transactions.TransactionDate < ".DBQuote($data->{'EndDate'});
		} else {
			$extraSQL .= " AND gl_transactions.TransactionDate <= ".DBQuote($data->{'EndDate'});
		}
	}

	# Return list of transaction entries
	my $sth = DBSelect("
		SELECT
			gl_entries.Amount, gl_transactions.Type
		FROM
			gl_entries, gl_transactions
		WHERE
			gl_entries.GLAccountID = ".DBQuote($GLAccID)."
			AND gl_transactions.ID = gl_entries.GLTransactionID
			AND gl_transactions.Posted = '1'
			$extraSQL
	");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	# These are our balances
	my $balance = Math::BigFloat->new();
	my $creditBalance = Math::BigFloat->new();
	my $debitBalance = Math::BigFloat->new();
	$balance->precision(-2);
	$creditBalance->precision(-2);
	$debitBalance->precision(-2);

	# Add the balances up
	while (my $row = hashifyLCtoMC($sth->fetchrow_hashref(),qw( Amount Type ))) {
		# Check filter, if this is not one of our items, continue
		if (! ($typeFilter & $row->{'Type'})) {
			next;
		}

		$balance->bzero()->badd($row->{'Amount'});

		# Check if its credit or debit
		if ($balance->is_neg()) {
			$balance->babs();
			$creditBalance->badd($balance);
		} else {
			$debitBalance->badd($balance);
		}
	}

	DBFreeRes($sth);

	# Build result
	my $res;
	$res->{'CreditBalance'} = $creditBalance->bstr();
	$res->{'DebitBalance'} = $debitBalance->bstr();

	return $res;
}


# Create account
# Parameters:
#		Code		- Account code
#		Name		- Account name
#		FinCatCode		- Financial category
#		RwCatCode		- Reporting category
# Optional:
#		ParentAccountNumber	- Parent account
sub createGLAccount
{
	my ($detail) = @_;


	# Verify ref
	if (!defined($detail->{'Code'}) || $detail->{'Code'} eq "") {
		setError("No (or invalid) ref provided");
		return ERR_PARAM;
	}

	# Verify name
	if (!defined($detail->{'Name'}) || $detail->{'Name'} eq "") {
		setError("No name provided for account '".$detail->{'Code'}."'");
		return ERR_PARAM;
	}

	# Verify financial category
	if (!defined($detail->{'FinCatCode'}) || $detail->{'FinCatCode'} eq "") {
		setError("No financial category provided for account '".$detail->{'Code'}."'");
		return ERR_PARAM;
	}

	# Verify reporting category
	if (!defined($detail->{'RwCatCode'}) || $detail->{'RwCatCode'} eq "") {
		setError("No reporting category provided for account '".$detail->{'Code'}."'");
		return ERR_PARAM;
	}

	# Check if we have a parent GL account
	my $parentGLAccID;
	if (defined($detail->{'ParentAccountNumber'}) && $detail->{'ParentAccountNumber'} ne "") {
		# Check GL account exists
		if (($parentGLAccID = getGLAccountIDFromNumber($detail->{'ParentAccountNumber'})) < 1) {
			setError(Error());
			return $parentGLAccID;
		}
	}

	# Check for conflicts
	if ((my $res = GLAccountCodeExists($parentGLAccID,$detail->{'Code'})) != 0) {
		# If we have a result err
		if ($res == 1) {
			setError("GL account code '".$detail->{'Code'}."' already exists");
			return ERR_CONFLICT;
		} else {
			setError(Error());
		}
		# else err with result
		return $res;
	}

	# Grab financial category
	my $fincatID;
	if (($fincatID = getGLFinCatIDFromCode($detail->{'FinCatCode'})) eq "") {
		setError(Error());
		return $fincatID;
	}

	# Grab reporting category
	my $rwcatID;
	if (($rwcatID = getGLRwCatIDFromCode($detail->{'RwCatCode'})) eq "") {
		setError(Error());
		return $rwcatID;
	}


	# Add GL account
	my $sth = DBDo("
		INSERT INTO gl_accounts
				(ParentGLAccountID,Code,Name,FinCatID,RwCatID)
			VALUES
				(
					".DBQuote($parentGLAccID).",
					".DBQuote($detail->{'Code'}).",
					".DBQuote($detail->{'Name'}).",
					".DBQuote($fincatID).",
					".DBQuote($rwcatID)."
				)
	");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	# Grab last ID
	my $ID = DBLastInsertID("gl_accounts","ID");

	return $ID;
}






1;
# vim: ts=4
