# Reporting functions
# Copyright (C) 2009-2014, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.




package wiaflos::server::core::Reporting;

use strict;
use warnings;


use wiaflos::version;
use wiaflos::constants;
use wiaflos::server::core::config;
use awitpt::db::dblayer;
use wiaflos::server::core::templating;
use wiaflos::server::core::GL;
use wiaflos::server::core::jobs;

use Math::BigFloat;

# Exporter stuff
require Exporter;
our (@ISA,@EXPORT,@EXPORT_OK);
@ISA = qw(Exporter);
@EXPORT = qw(
	REPORT_BALANCE_BF
	REPORT_INCLUDE_YEAREND
	REPORT_INCLUDE_AUDIT
	REPORT_INCLUDE_AUDIT_YEAREND
);
@EXPORT_OK = ();


use constant {
	REPORT_BALANCE_BF	=> 1,
	REPORT_INCLUDE_YEAREND	=> 2,
	REPORT_INCLUDE_AUDIT	=> 4,
	REPORT_INCLUDE_AUDIT_YEAREND	=> 8,
};




# Our current error message
my $error = "";

# Set current error message
# Args: error_message
sub setError
{
	my $err = shift;
	my ($package,$filename,$line) = caller;
	my (undef,undef,undef,$subroutine) = caller(1);

	# Set error
	$error = "$subroutine($line): $err";
}

# Return current error message
# Args: none
sub Error
{
	my $err = $error;

	# Reset error
	$error = "";

	# Return error
	return $err;
}


## @fn getAccountBalances
# Function to return the chart of accounts with balances
#
# @param flags Flags of what we returned, options are below, use || (OR)
# @li REPORT_BALANCE_BF - Return balance brought forward for each account
# @li REPORT_INCLUDE_YEAREND - Get balances and include yearend transactions
# @li REPORT_INCLUDE_AUDIT - Get balances and include audit adjustment transactions
# @li REPORT_INCLUDE_AUDIT_YEAREND - Get balances and include audited yearend adjustment transactions
#
# @param data Parameter hash ref
# @li StartDate - Optional statement start date
# @li EndDate - Optional statement end date
# @li Levels - Optional depth to go into on each parent account
#
# @returns Array ref of hash refs containing the accounts and their balances
# @li ID - Account ID
# @li Number - Account number
# @li Name - Name of account
#
# @li Balance - Account balance
# Balance of account, can either be - (credit) or + (debit)
#
# @li DebitBalance - Optional, this is the total of all debits
# @li CreditBalance - Optional, this is the total of all credits
#
# @li BalanceBroughtForward - Optional, returned when REPORT_BALANCE_BF is specified
# This is the balance of the account for the period up until this reporting period
sub getAccountBalances
{
	our $flags = shift;
	my $data = shift;


	# Get our account tree
	my $res = wiaflos::server::core::GL::getGLAccountTree();
	if (ref($res) ne "ARRAY") {
		setError(wiaflos::server::core::GL::Error());
		return $res;
	}

	# GL search criteria
	my $search;
	$search->{'StartDate'} = $data->{'StartDate'};
	$search->{'EndDate'} = $data->{'EndDate'};
	$search->{'Levels'} = $data->{'Levels'};
	$search->{'Type'} = GL_TRANSTYPE_NORMAL;

	# Check if we must include yearend transactions
	if (($flags & REPORT_INCLUDE_YEAREND) == REPORT_INCLUDE_YEAREND) {
		$search->{'Type'} |= GL_TRANSTYPE_YEAREND;
	}

	# Check if we must include audit adjustment transactions
	if (($flags & REPORT_INCLUDE_AUDIT) == REPORT_INCLUDE_AUDIT) {
		$search->{'Type'} |= GL_TRANSTYPE_AUDIT;
	}

	# Check if we must include audit yearend adjustment transactions
	if (($flags & REPORT_INCLUDE_AUDIT_YEAREND) == REPORT_INCLUDE_AUDIT_YEAREND) {
		$search->{'Type'} |= GL_TRANSTYPE_AUDIT_YEAREND;
	}

	# Loop with the parent accounts, remember they're top level
	foreach my $account (@{$res}) {

		# Process child accounts within parent
		sub processChildren
		{
			my ($paccount,$search) = @_;

			# This is our final balances
			my $creditBalance = Math::BigFloat->new();
			my $openingCreditBalance = Math::BigFloat->new();
			my $debitBalance = Math::BigFloat->new();
			my $openingDebitBalance = Math::BigFloat->new();
			$creditBalance->precision(-2);
			$openingCreditBalance->precision(-2);
			$debitBalance->precision(-2);
			$openingDebitBalance->precision(-2);

			# Loop with children
			foreach my $caccount (@{$paccount->{'Children'}}) {
				# Process child
				my $pcres = processChildren($caccount,$search);
				if ($pcres) {
					setError(wiaflos::server::core::GL::Error());
					return $pcres;
				}
				# Add up balance of child...
				$creditBalance->badd($caccount->{'CreditBalance'});
				$debitBalance->badd($caccount->{'DebitBalance'});
				# Check if we should add up opening balances
				if (($flags & REPORT_BALANCE_BF) == REPORT_BALANCE_BF) {
					$openingCreditBalance->badd($caccount->{'OpeningCreditBalance'});
					$openingDebitBalance->badd($caccount->{'OpeningDebitBalance'});
				}
			}

			# Build param we need to pass to get this accounts balance
			my $info;
			$info->{'StartDate'} = $search->{'StartDate'};
			$info->{'EndDate'} = $search->{'EndDate'};
			$info->{'Type'} = $search->{'Type'};
			$info->{'AccountID'} = $paccount->{'ID'};
			# Get our own account balance
			my $res = wiaflos::server::core::GL::getGLAccountBalance($info);
			if (ref($res) ne "HASH") {
				setError(wiaflos::server::core::GL::Error());
				return $res;
			}
			# Pull in balances...
			$creditBalance->badd($res->{'CreditBalance'});
			$debitBalance->badd($res->{'DebitBalance'});
			my $balance = $debitBalance->copy()->bsub($creditBalance);
			# Set parent account balances
			$paccount->{'CreditBalance'} = $creditBalance->bstr();
			$paccount->{'DebitBalance'} = $debitBalance->bstr();
			$paccount->{'Balance'} = $balance->bstr();

			# Check if we need to pull in balance brought forward
			if (($flags & REPORT_BALANCE_BF) == REPORT_BALANCE_BF) {
				# We can only do this if we have a start date, else its pointless?
				if ($search->{'StartDate'}) {
					my $bfinfo;
					$bfinfo->{'EndDate'} = $search->{'StartDate'};
					$bfinfo->{'EndDateExcl'} = 1;
					$bfinfo->{'Type'} = $search->{'Type'};
					$bfinfo->{'AccountID'} = $paccount->{'ID'};
					# Pull in balance brought forward
					$res = wiaflos::server::core::GL::getGLAccountBalance($bfinfo);
					if (ref($res) ne "HASH") {
						setError(wiaflos::server::core::GL::Error());
						return $res;
					}
					# Total up
					$openingCreditBalance->badd($res->{'CreditBalance'});
					$openingDebitBalance->badd($res->{'DebitBalance'});
				}
				# Set opening blalance
				my $openingBalance = $openingDebitBalance->copy()->bsub($openingCreditBalance);
				# Set parent account balances
				$paccount->{'OpeningCreditBalance'} = $openingCreditBalance->bstr();
				$paccount->{'OpeningDebitBalance'} = $openingDebitBalance->bstr();
				$paccount->{'OpeningBalance'} = $openingBalance->bstr();
				# Set closing balance
				$paccount->{'ClosingBalance'} = $openingBalance->copy()->badd($balance)->bstr();
			}

			return 0;
		}

		# Process this parent account
		my $pcres = processChildren($account,$search);
		if ($pcres) {
			setError(wiaflos::server::core::GL::Error());
			return $pcres;
		}
	}

	return $res;
}



## @fn sendReport($detail)
# Function to send a report
#
# @param detail Parameter hash ref
# @li Template - Tempalte to use
# @li GLAccountNumber - Optional GL account number(s) used for some reports
# @li SendTo - Send to,  email:  ,  file:  , return
# @li Subject - Optional Subject of the report
# @li StartDate - Optional statement start date
# @li EndDate - Optional statement end date
# @li Background - Optional, allow this report to be sent in the background
#
# @returns RES_OK on success, < 0 otherwise
sub sendReport
{
	our ($detail) = @_;


	# Variables used in the API functions
	our %variables = ();
	# Constants used in API functions
	use constant {
		API_FMT_REVERSE => 1,
		API_FMT_NEGBRACKET => 2,
	};

	# Reporting API function to retrieve account balances
	sub api_account_balances
	{
		our $flags = shift;


		# Set flags to 0 if we don't have any
		if (!defined($flags) || $flags eq "") {
			$flags = 0;
		}

		# GL search criteria
		my $search;
		$search->{'StartDate'} = $detail->{'StartDate'};
		$search->{'EndDate'} = $detail->{'EndDate'};

		my $entries = getAccountBalances($flags,$search);
		if (ref($entries) ne "ARRAY") {
			wiaflos::server::core::templating::abort('api_account_balances',$entries,Error());
		}

		our @accountList = ();
		if (defined($detail->{'GLAccountNumber'})) {
			@accountList = split(/[,;]/,$detail->{'GLAccountNumber'});
		}

		# Result to return
		our $resdata = undef;

		# Load line items for GL
		foreach my $item (@{$entries}) {

			# Process item
			sub processItem
			{
				my $account = shift;


				my $i;

				# Fix some stuff up
				$i->{'Number'} = $account->{'Number'};
				$i->{'Name'} = $account->{'Name'};

				# Pull in the normal balances
				$i->{'DebitBalance'} = $account->{'DebitBalance'};
				$i->{'CreditBalance'} = $account->{'CreditBalance'};
				$i->{'Balance'} = $account->{'Balance'};
				# Check if we need the opening balances
				if (($flags & REPORT_BALANCE_BF) == REPORT_BALANCE_BF) {
					$i->{'OpeningDebitBalance'} = $account->{'OpeningDebitBalance'};
					$i->{'OpeningCreditBalance'} = $account->{'OpeningCreditBalance'};
					$i->{'OpeningBalance'} = $account->{'OpeningBalance'};
					# And closing balance
					$i->{'ClosingBalance'} = $account->{'ClosingBalance'};
				}

				# Check balance
				my $balance = Math::BigFloat->new();
				$balance->precision(-2);
				$balance->badd($account->{'Balance'});
				# Check if its negative or positive
				if ($balance->is_neg()) {
					$balance->babs();
					$i->{'CreditAmount'} = sprintf('%.2f',$balance->bstr());
				} else {
					$i->{'DebitAmount'} = sprintf('%.2f',$balance->bstr());
				}

				$i->{'ReportWriterCategoryCode'} = $account->{'RwCatCode'};

				# If we have children, process them
				if ($account->{'Children'}) {
					# Loop with child accounts
					foreach my $caccount (@{$account->{'Children'}}) {
						# Process
						my $citem = processItem($caccount);
						# Add to child list
						push(@{$i->{'Children'}},$citem);
					}
				}

				my $add = 0;
				# Check if we have a limit list on the accounts to return
				if (@accountList > 0) {
					# Try find a match
					foreach my $j (@accountList) {
						if ($i->{'Number'} =~ /^$j:/) {
							$add = 1;
							last;
						}
					}
				} else {
					$add = 1;
				}

				# File balance under account number
 				if ($add) {
					$resdata->{'AccountBalances'}{$i->{'Number'}} = $i;
				}

				return $i;
			}

			# Process top level item
			my $pitem = processItem($item);

			my $add = 0;
			# Check if we have a limit list on the accounts to return
			if (@accountList > 0) {
				# Try find a match
				foreach my $j (@accountList) {
					if ($pitem->{'Number'} =~ /^$j:/) {
						$add = 1;
						last;
					}
				}
			} else {
				$add = 1;
			}

			# File under report write category code
			if ($add) {
				push(@{$resdata->{'ReportWriterCategoryToAccounts'}{$pitem->{'ReportWriterCategoryCode'}}},$pitem);
			}
		}

		# Load report write categories
		$entries = wiaflos::server::core::GL::getGLRwCats();
		if (ref($entries) ne "ARRAY") {
			setError(wiaflos::server::core::GL::Error());
			return $entries;
		}
		foreach my $item (@{$entries}) {
			$resdata->{'ReportWriterCategories'}{$item->{'Code'}} = $item;
		}

		return $resdata;
	}


	# Reporting API function to retrieve GL transactions
	sub api_gl_transactions
	{
		our $flags = shift;


		# Set flags to 0 if we don't have any
		if (!defined($flags) || $flags eq "") {
			$flags = 0;
		}

		my $types = GL_TRANSTYPE_NORMAL;

		# Check if we must include yearend transactions
		if (($flags & REPORT_INCLUDE_YEAREND) == REPORT_INCLUDE_YEAREND) {
			$types |= GL_TRANSTYPE_YEAREND;
		}

		# Check if we must include audit adjustment transactions
		if (($flags & REPORT_INCLUDE_AUDIT) == REPORT_INCLUDE_AUDIT) {
			$types |= GL_TRANSTYPE_AUDIT;
		}

		# Check if we must include audit yearend adjustment transactions
		if (($flags & REPORT_INCLUDE_AUDIT_YEAREND) == REPORT_INCLUDE_AUDIT_YEAREND) {
			$types |= GL_TRANSTYPE_AUDIT_YEAREND;
		}

		my $resdata = ();

		# Get account data
		my $accounts = wiaflos::server::core::GL::getGLAccounts();
		if (ref($accounts) ne "ARRAY") {
			wiaflos::server::core::templating::abort('api_gl_transactions',$accounts,wiaflos::server::core::GL::Error());
		}
		# Sort based on account number
		@{$accounts} = sort { $a->{'Number'} cmp $b->{'Number'} } @{$accounts};
		foreach my $account (@{$accounts}) {

			my $aitem;
			$aitem->{'ID'} = $account->{'ID'};
			$aitem->{'Number'} = $account->{'Number'};
			$aitem->{'Name'} = $account->{'Name'};

			# Grab transactions
			my $tmp;
			$tmp->{'AccountID'} = $account->{'ID'};
			$tmp->{'StartDate'} = $detail->{'StartDate'};
			$tmp->{'EndDate'} = $detail->{'EndDate'};
			$tmp->{'Type'} = $types;
			my $transactions = wiaflos::server::core::GL::getGLTransactions($tmp);
			# Sort based on transaction date
			@{$transactions} = sort { $a->{'TransactionDate'} cmp $b->{'TransactionDate'} } @{$transactions};
			# Loop and process
			foreach my $transaction (@{$transactions}) {
				# Only report on posted transactions
				next if (!$transaction->{'Posted'});

				# Setup our transaction item
				my $titem;
				$titem->{'ID'} = $transaction->{'ID'};
				$titem->{'TransactionDate'} = $transaction->{'TransactionDate'};
				$titem->{'Reference'} = $transaction->{'Reference'};
				$titem->{'Type'} = $transaction->{'Type'};

				# Pull in transaction entries
				$tmp = undef;
				$tmp->{'ID'} = $transaction->{'ID'};
				my $transactionEntries = wiaflos::server::core::GL::getGLTransactionEntries($tmp);
				# Loop and process
				foreach my $transactionEntry (@{$transactionEntries}) {
					my $eitem;

					# Start creating our transaction entry
					$eitem->{'ID'} = $transactionEntry->{'ID'};
					$eitem->{'GLAccountID'} = $transactionEntry->{'GLAccountID'};
					$eitem->{'GLAccountName'} = $transactionEntry->{'GLAccountName'};
					$eitem->{'GLAccountNumber'} = $transactionEntry->{'GLAccountNumber'};

					# Clean up amount
					my $cleanAmount = Math::BigFloat->new($transactionEntry->{'Amount'});
					$cleanAmount->precision(-2);
					$eitem->{'Amount'} = $cleanAmount->bstr();

					$eitem->{'Reference'} = $transactionEntry->{'Reference'};

					# Add entry to transaction
					push(@{$titem->{'Entries'}},$eitem);
				}
				# Add transaction to account
				push(@{$aitem->{'Transactions'}},$titem);
			}
			# Add account to return data
			push(@{$resdata},$aitem);
		}

		return $resdata;
	}


	# Reporting API function to retrieve account entries
	sub api_account_entries
	{
		my $resdata;

		# Get account data
		my $tmp;
		$tmp->{'AccountNumber'} = $detail->{'GLAccountNumber'};
		my $account = wiaflos::server::core::GL::getGLAccount($tmp);
		if (ref($account) ne "HASH") {
			wiaflos::server::core::templating::abort('api_account_entries',$account,wiaflos::server::core::GL::Error());
		}
		$resdata->{'Account'} = $account;

		$tmp = undef;
		$tmp->{'AccountID'} = $account->{'ID'};
		$tmp->{'StartDate'} = $detail->{'StartDate'};
		$tmp->{'EndDate'} = $detail->{'EndDate'};
		$tmp->{'BalanceBroughtForward'} = 1;
		my $res = wiaflos::server::core::GL::getGLAccountEntries($tmp);
		if (ref($res) ne "ARRAY") {
			wiaflos::server::core::templating::abort('api_account_entries',$res,wiaflos::server::core::GL::Error());
		}

		# Sort so our balance makes sense
		@{$res} = sort { $a->{'TransactionDate'} cmp $b->{'TransactionDate'} } @{$res};

		# Work out closing balance
		my $closingBalance = Math::BigFloat->new();
		$closingBalance->precision(-2);

		# Fetch items
		foreach my $item (@{$res}) {
			my $entry;

			$entry->{'ID'} = $item->{'ID'};
			$entry->{'GLTransactionID'} = $item->{'GLTransactionID'};
			$entry->{'Reference'} = defined($item->{'Reference'}) ? $item->{'Reference'} : $item->{'TransactionReference'};
			$entry->{'Amount'} = $item->{'Amount'};
			# Calculate balance so far
			$closingBalance->badd($entry->{'Amount'});
			# Make it pretty
			$entry->{'Balance'} = sprintf('%.2f',$closingBalance->bstr());

			$entry->{'TransactionDate'} = $item->{'TransactionDate'};
			$entry->{'TransactionReference'} = $item->{'TransactionReference'};

			push(@{$resdata->{'Entries'}},$entry);
		}
		# Generate closing balance entry
		my $entry;
		$entry->{'ID'} = -99;
		$entry->{'GLTransactionID'} = -99;
		$entry->{'Reference'} = "* Closing Balance *";
		$entry->{'Balance'} = sprintf('%.2f',$closingBalance->bstr());
		$entry->{'TransactionDate'} = $detail->{'EndDate'};
		push(@{$resdata->{'Entries'}},$entry);

		# Sort results
		@{$resdata->{'Entries'}} = sort { $a->{'TransactionDate'} cmp $b->{'TransactionDate'} } @{$resdata->{'Entries'}};

		return $resdata;
	}


	# Reporting API function to retrieve inventory stock balances
	sub api_inventory_stock_balances
	{
		my $resdata;

		# Get inventory data
		my $tmp;
		$tmp->{'EndDate'} = $detail->{'EndDate'};
		my $res = wiaflos::server::core::Inventory::getInventoryStockBalance($tmp);
		if (ref($res) ne "HASH") {
			wiaflos::server::core::templating::abort('api_inventory_stock_balances',$res,wiaflos::server::core::Inventory::Error());
		}

		# Work out closing balance
		my $totalValue = Math::BigFloat->new();
		$totalValue->precision(-2);

		# Loop with stock codes
		foreach my $itemCode (keys %{$res}) {
			my $entry;

			$entry->{'TotalQuantity'} = Math::BigFloat->new();
			$entry->{'TotalQuantity'}->precision(-4);
			$entry->{'TotalValue'} = Math::BigFloat->new();

			# Loop with stock items
			foreach my $itemSerial (keys %{$res->{$itemCode}}) {
				my $stockItem = $res->{$itemCode}->{$itemSerial};

				$entry->{'TotalQuantity'}->badd($stockItem->{'Quantity'});
				$entry->{'TotalValue'}->badd($stockItem->{'Value'});

				$totalValue->badd($stockItem->{'Value'});

				# If its zero, we don't want it
				if (!$stockItem->{'Quantity'}->is_zero() && !$stockItem->{'Value'}->is_zero()) {
					my $sentry;
					$sentry->{'TotalQuantity'} = sprintf('%.4f',$stockItem->{'Quantity'}->bstr());
					$sentry->{'TotalValue'} = sprintf('%.4f',$stockItem->{'Value'}->bstr());
					$resdata->{'StockItemBalances'}->{$itemCode}->{$itemSerial} = $sentry;
				}
			}
			# If its zero, we don't need it
			if (!$entry->{'TotalQuantity'}->is_zero() && !$entry->{'TotalValue'}->is_zero()) {
				$entry->{'TotalQuantity'} = sprintf('%.4f',$entry->{'TotalQuantity'}->bstr());
				$entry->{'TotalValue'} = sprintf('%.2f',$entry->{'TotalValue'}->bstr());
				$resdata->{'StockBalances'}->{$itemCode} = $entry;
			}
		}

		$resdata->{'TotalValue'} = sprintf('%.2f',$totalValue->bstr());

		return $resdata;
	}


	# Reporting API function to create a variable
	sub api_variable_new
	{
		my ($item,$type) = @_;

		# Check what type we are
		if (lc($type) eq "float") {
			$variables{$item}{'type'} = "float";
			$variables{$item}{'value'} = new Math::BigFloat();
			$variables{$item}{'value'}->precision(-2);
		}
		# We shouldn't return results
		return undef;
	}


	# Reporting API function to add something to a variable
	sub api_variable_add
	{
		my ($item,$value) = @_;


		# If we have a balance, add to it
		if (defined($variables{$item})) {
			# If its a float
			if ($variables{$item}{'type'} eq "float") {
				# Cannot add an undefined value
				if (defined($value) && $value ne "") {
					$variables{$item}{'value'}->badd($value);
				}
			} else {
				wiaflos::server::core::templating::abort('api_variable_add','Variable type not supported: "'.defined($value) ? $value : ''.'"');
			}
		} else {
			wiaflos::server::core::templating::abort('api_variable_add','Variable not defined: "'.defined($item) ? $item : ''.'"');
		}

		# We shouldn't return results
		return undef;
	}


	# Reporting API function to subtract something to a variable
	sub api_variable_subtract
	{
		my ($item,$value) = @_;


		# If we have a balance, subtract from it
		if (defined($variables{$item})) {
			# If its a float
			if ($variables{$item}{'type'} eq "float") {
				$variables{$item}{'value'}->bsub($value);
			} else {
				wiaflos::server::core::templating::abort('api_variable_subtract','Variable type not supported: "'.defined($value) ? $value : ''.'"');
			}
		} else {
			wiaflos::server::core::templating::abort('api_variable_subtract','Variable not defined: "'.defined($item) ? $item : ''.'"');
		}
		# We shouldn't return results
		return undef;
	}


	# Reporting API function to get variable
	sub api_variable_get
	{
		my $item = shift;


		my $res;

		# If we have a balance, return it
		if (defined($variables{$item})) {
			# If its a float
			if ($variables{$item}{'type'} eq "float") {
				$res = sprintf('%.2f',$variables{$item}{'value'}->bstr());
			} else {
				wiaflos::server::core::templating::abort('api_variable_get','Variable type not supported: "'.
					defined($variables{$item}{'type'}) ? $variables{$item}{'type'} : ''.'"');
			}
		} else {
			wiaflos::server::core::templating::abort('api_variable_get','Variable not defined: "'.defined($item) ? $item : ''.'"');
		}
		return $res;
	}


	# Reporting API function to get raw variable value
	sub api_variable_getraw
	{
		my $item = shift;


		my $res;

		# If we have a balance, return it
		if (defined($variables{$item})) {
			$res = $variables{$item}{'value'};
		} else {
			wiaflos::server::core::templating::abort('api_variable_get','Variable not defined: "'.defined($item) ? $item : ''.'"');
		}

		return $res;
	}


	# Reporting API function to logical OR all variables
	sub api_bitwise_or
	{
		my @vars = @_;

		# OR up all the values
		my $res = 0;
		foreach my $val (@vars) {
			$res |= $val;
		}

		return $res;
	}


	# Dump contents of parameters on server
	sub api_format_amount
	{
		my ($pamount,$options) = @_;

		my $prefix = "";
		my $suffix = "";

		# Pull in value
		my $amount = new Math::BigFloat();
		$amount->precision(-2);
		$amount->badd($pamount);

		# Check options
		if (($options & API_FMT_REVERSE) == API_FMT_REVERSE) {
			$amount->bmul(-1);
		}

		if (($options & API_FMT_NEGBRACKET) == API_FMT_NEGBRACKET) {
			# Check if neg, if it is, use brackets
			if ($amount->is_neg()) {
				$prefix = "(";
				$suffix = ")";
				$amount->babs();
			}
		}

		return $prefix . sprintf('%.2f',$amount->bstr()) . $suffix;
	}

	# Dump contents of parameters on server
	sub api_dump
	{
		use Data::Dumper;
		print(STDERR Dumper(\@_));
	}

	# Verify Template
	if (!defined($detail->{'Template'}) || $detail->{'Template'} eq "") {
		setError("Template was not provided");
		return ERR_PARAM;
	}

	# Verify SendTo
	if (!defined($detail->{'SendTo'}) || $detail->{'SendTo'} eq "") {
		setError("SendTo was not provided");
		return ERR_PARAM;
	}

	# Pull in config
	my $config = wiaflos::server::core::config::getConfig();

	# Check if subject was overridden
	my $subject = (defined($detail->{'Subject'}) && $detail->{'Subject'} ne "") ? $detail->{'Subject'} : "Report";

	# Build array of stuff we can use
	my $vars = {
		'WiaflosString' => $GENSTRING,

		# API
		'api_account_balances' => \&api_account_balances,
		'API_RPT_BALANCE_BF' => REPORT_BALANCE_BF,
		'API_RPT_INCLUDE_YEAREND' => REPORT_INCLUDE_YEAREND,
		'API_RPT_INCLUDE_AUDIT' => REPORT_INCLUDE_AUDIT,
		'API_RPT_INCLUDE_AUDIT_YEAREND' => REPORT_INCLUDE_AUDIT_YEAREND,

		'api_account_entries' => \&api_account_entries,

		'api_inventory_stock_balances' => \&api_inventory_stock_balances,
		'api_gl_transactions' => \&api_gl_transactions,

		'api_variable_new' => \&api_variable_new,
		'api_variable_add' => \&api_variable_add,
		'api_variable_subtract' => \&api_variable_subtract,
		'api_variable_get' => \&api_variable_get,
		'api_variable_getraw' => \&api_variable_getraw,

		'api_bitwise_or' => \&api_bitwise_or,

		'api_format_amount' => \&api_format_amount,
		'API_FMT_REVERSE' => API_FMT_REVERSE,
		'API_FMT_NEGBRACKET' => API_FMT_NEGBRACKET,

		'api_dump' => \&api_dump,

		# Client
		'StartDate' => defined($detail->{'StartDate'}) ? $detail->{'StartDate'} : "-",
		'EndDate' => defined($detail->{'EndDate'}) ? $detail->{'EndDate'} : "CURRENT",

	};

	# Should this be backgrounded?
	my $background = $detail->{'Background'};
	if ($background) {
		my $job = wiaflos::server::core::jobs::createJob();
		if ($job < RES_OK) {
			setError(wiaflos::jobs::Error());
			return $job;
		} elsif ($job > RES_OK) {
			# Parent job
			return RES_OK;
		}
	}

	# Set template to use
	my $template = $detail->{'Template'};

	# Check where report must go
	if ($detail->{'SendTo'} =~ /^file:(\S+)/i) {
		my $filename = $1;

		wiaflos::server::core::jobs::setStatus("Loading report template '$template'...");
		# Load template
		my $res = loadTemplate($template,$vars,$filename);
		if (!$res) {
			if (!$background) {
				setError("Failed to load template '$template': ".wiaflos::server::core::templating::Error());
				return ERR_SRVTEMPLATE;
			} else {
				wiaflos::server::core::jobs::setStatus("Failed to load template '$template': ".wiaflos::server::core::templating::Error());
				exit 0;
			}
		}

	# Write out using email
	} elsif ($detail->{'SendTo'} =~ /^email(?:\:(\S+))?/i) {
		# Pull in email address user specified
		my $emailAddy = $1;

		# Verify SMTP server is set
		my $server = $config->{'mail'}{'server'};
		if (!defined($server) || $server eq "") {
			if (!$background) {
				setError("Cannot use report emailing if we do not have an SMTP server defined");
				return ERR_SRVPARAM;
			} else {
				wiaflos::server::core::jobs::setStatus("Cannot use report emailing if we do not have an SMTP server defined");
				exit 0;
			}
		}

		# Check if we have a email addy
		if (!defined($emailAddy) || $emailAddy eq "") {
			if (!$background) {
				setError("No email address defined to send reports to");
				return ERR_PARAM;
			} else {
				wiaflos::server::core::jobs::setStatus("No email address defined to send reports to");
				exit 0;
			}
		}

		# Report filename
		(my $reportFilename = "report.html") =~ s,/,-,g;
		# Report signature filename
		my $rprtSignFilename = $reportFilename . ".asc";


		# If we must, pull in email body
		my $message_template = $config->{'reports'}{'email_message_template'};
		my $emailBody = "";
		if (defined($message_template) && $message_template ne "") {
			wiaflos::server::core::jobs::setStatus("Loading message template '$message_template'...");
			# Variables for our template
			my $vars2 = {
				'ReportFilename' => $reportFilename,
				'ReportSignatureFilename' => $rprtSignFilename,
			};
			# Load template
			my $res = loadTemplate($message_template,$vars2,\$emailBody);
			if (!$res) {
				if (!$background) {
					setError("Failed to load template '$message_template': ".wiaflos::server::core::templating::Error());
					return ERR_SRVTEMPLATE;
				} else {
					wiaflos::server::core::jobs::setStatus("Failed to load template '$message_template': ".wiaflos::server::core::templating::Error());
					exit 0;
				}
			}

			$emailBody =~ s/(?<!\r)\n/\r\n/sg; # Sanitize eol for crypt-gpg
		}

		wiaflos::server::core::jobs::setStatus("Loading report template '$template'...");

		# This is our entire report
		my $reportData = "";
		my $res = loadTemplate($template,$vars,\$reportData);
		if (!$res) {
			if (!$background) {
				setError("Failed to load template '$template': ".wiaflos::server::core::templating::Error());
				return ERR_SRVTEMPLATE;
			} else {
				wiaflos::server::core::jobs::setStatus("Failed to load template '$template': ".wiaflos::server::core::templating::Error());
				exit 0;
			}
		}
		$reportData =~ s/(?<!\r)\n/\r\n/sg; # Sanitize eol, needed to fix bug in crypt-gpg where it mangles \n

		# See if we must use GPG
		my $use_gpg_key = $config->{'reports'}{'use_gpg_key'};
		my $sign;
		if (defined($use_gpg_key) && $use_gpg_key ne "") {
			wiaflos::server::core::jobs::setStatus("Signing report...");
			# Setup GnuPG
			my $gpg = new Crypt::GPG;
			$gpg->gpgbin('/usr/bin/gpg');
			$gpg->secretkey($use_gpg_key);
			$gpg->armor(1);
			$gpg->comment("$APPNAME v$VERSION ($APPURL)");
			# Sign report
			$sign = $gpg->sign($reportData);
			if (!defined($sign)) {
				if (!$background) {
					setError("Failed to sign report");
					return ERR_SRVEXEC;
				} else {
					wiaflos::server::core::jobs::setStatus("Failed to sign report");
					exit 0;
				}
			}
		}

		wiaflos::server::core::jobs::setStatus("Composing mail...");
		# Create message
		my $msg = MIME::Lite->new(
				From	=> $config->{'reports'}{'email_from'},
				To		=> $emailAddy,
				Bcc		=> $config->{'reports'}{'email_bcc'},
				Subject	=> $subject,
				Type	=> 'multipart/mixed'
		);

		# Attach body
		$msg->attach(
				Type	=> 'TEXT',
				Encoding 	=> '8bit',
				Data	=> $emailBody,
		);

		# Attach report
		$msg->attach(
				Type	=> 'text/html',
				Encoding	=> 'base64',
				Data	=> $reportData,
				Disposition	=> 'attachment',
				Filename	=> $reportFilename
		);

		# If we have signature, sign report
		if (defined($sign)) {
			# Attach signature
			$msg->attach(
					Type	=> 'text/plain',
					Encoding	=> 'base64',
					Data	=> $sign,
					Disposition	=> 'attachment',
					Filename	=> $rprtSignFilename
			);
		}

		wiaflos::server::core::jobs::setStatus("Sending mail...");
		# Send email
		my @SMTPParams;
		if (!(my $res = $msg->send("smtp",$server))) {
			if (!$background) {
				setError("Failed to send report via email server '$server'");
				return ERR_SRVEXEC;
			} else {
				wiaflos::server::core::jobs::setStatus("Failed to send report via email server '$server'");
				exit 0;
			}
		}

	} else {
		if (!$background) {
			setError("Invalid SendTo method provided");
			return ERR_PARAM;
		} else {
			wiaflos::server::core::jobs::setStatus("Invalid SendTo method provided");
			exit 0;
		}
	}

	wiaflos::server::core::jobs::setStatus("Done");

	# Check how we going to exit
	if ($background) {
		exit 0;
	} else {
		return RES_OK;
	}
}




1;
# vim: ts=4
