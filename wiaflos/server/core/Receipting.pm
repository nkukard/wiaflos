# Receipting functions
# Copyright (C) 2009-2014, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.




package wiaflos::server::core::Receipting;

use strict;
use warnings;


use wiaflos::version;
use wiaflos::constants;
use wiaflos::server::core::config;
use awitpt::db::dblayer;
use awitpt::cache;
use wiaflos::server::core::templating;
use wiaflos::server::core::Clients;
use wiaflos::server::core::GL;

# Whole money transactions, precision is two
use Math::BigFloat;
Math::BigFloat::precision(-2);




# Our current error message
my $error = "";

# Set current error message
# Args: error_message
sub setError
{
	my $err = shift;
	my ($package,$filename,$line) = caller;
	my (undef,undef,undef,$subroutine) = caller(1);

	# Set error
	$error = "$subroutine($line): $err";
}

# Return current error message
# Args: none
sub Error
{
	my $err = $error;

	# Reset error
	$error = "";

	# Return error
	return $err;
}



# Check if receipt exists
# Backend function, takes 1 parameter which is the receipt ID
sub receiptIDExists
{
	my $receiptID = shift;


	# Select receipt count
	my $rows = DBSelectNumResults("FROM receipts WHERE ID = ".DBQuote($receiptID));
	if (!defined($rows)) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	return $rows > 0 ? 1 : 0;
}



# Check if receipt number exists
sub receiptNumberExists
{
	my $number = shift;


	# Sanitize
	$number = uc($number);
	$number =~ s#^RCT/##;

	# Select receipt count
	my $rows = DBSelectNumResults("FROM receipts WHERE Number = ".DBQuote($number));
	if (!defined($rows)) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	return $rows > 0 ? 1 : 0;
}



# Return receipt ID from number
sub getReceiptIDFromNumber
{
	my $number = shift;


	# Sanitize
	$number = uc($number);
	$number =~ s#^RCT/##;

	# Check cache
	my ($cache_res,$cache) = cacheGetKeyPair('Receipt/Number-to-ID',$number);
	if ($cache_res != RES_OK) {
		setError(awitpt::cache::Error());
		return $cache_res;
	}
	return $cache if (defined($cache));

	# Select receipt
	my $sth = DBSelect("
		SELECT
			ID
		FROM
			receipts
		WHERE
			Number = ".DBQuote($number)."
	");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	my $row = hashifyLCtoMC($sth->fetchrow_hashref(),qw( ID ));
	DBFreeRes($sth);

	# Check we got a result
	if (!defined($row)) {
		setError("Error finding receipt '$number'");
		return ERR_NOTFOUND;
	}

	# Cache this
	$cache_res = cacheStoreKeyPair('Receipt/Number-to-ID',$number,$row->{'ID'});
	if ($cache_res != RES_OK) {
		setError(awitpt::cache::Error());
		return $cache_res;
	}

	return $row->{'ID'};
}




# Backend function to build item hash
sub sanitizeRawItem
{
	my $rawData = shift;


	my $item;

	$item->{'ID'} = $rawData->{'ID'};

	my $data;

	# Pull in client data
	$item->{'ClientID'} = $rawData->{'ClientID'};

	$data = undef;
	$data->{'ID'} = $rawData->{'ClientID'};
	my $client = wiaflos::server::core::Clients::getClient($data);
	$item->{'ClientCode'} = $client->{'Code'};


	# Pull in GL account info
	$item->{'GLAccountID'} = $rawData->{'GLAccountID'};
	$item->{'GLAccountNumber'} = wiaflos::server::core::GL::getGLAccountNumberFromID($rawData->{'GLAccountID'});

	$item->{'Number'} = "RCT/".uc($rawData->{'Number'});

	$item->{'TransactionDate'} = $rawData->{'TransactionDate'};
	$item->{'Reference'} = $rawData->{'Reference'};
	$item->{'Amount'} = $rawData->{'Amount'};

	$item->{'GLTransactionID'} = $rawData->{'GLTransactionID'};
	$item->{'Posted'} = defined($rawData->{'GLTransactionID'}) ? 1 : 0;

	$item->{'Closed'} = $rawData->{'Closed'};

	return $item;
}



# Backend function to build item hash
sub sanitizeRawAllocationItem
{
	my $rawData = shift;


	my $item;

	$item->{'ID'} = $rawData->{'ID'};


   	# Pull in invoice data
	if (defined($rawData->{'InvoiceID'})) {
		$item->{'InvoiceID'} = $rawData->{'InvoiceID'};

		my $data;
		$data->{'ID'} = $rawData->{'InvoiceID'};
		my $invoice = wiaflos::server::core::Invoicing::getInvoice($data);
		$item->{'InvoiceNumber'} = $invoice->{'Number'};

	# Pull in transaction data
	} elsif (defined($rawData->{'AccountTransactionID'})) {
		$item->{'AccountTransactionID'} = $rawData->{'AccountTransactionID'};

		my $data;
		$data->{'ID'} = $rawData->{'AccountTransactionID'};
		my $transaction = wiaflos::server::core::Clients::getAccountTransaction($data);
		$item->{'AccountTransactionNumber'} = $transaction->{'Number'};
	}

	# And the receipt
	$item->{'ReceiptID'} = $rawData->{'ReceiptID'};

	my $data;
	$data->{'ID'} = $rawData->{'ReceiptID'};
	my $receipt = getReceipt($data);
	$item->{'ReceiptNumber'} = $receipt->{'Number'};


	$item->{'Amount'} = $rawData->{'Amount'};

	$item->{'Posted'} = (defined($rawData->{'InvoiceTransactionID'}) || defined($rawData->{'AccountTransactionAllocationID'})) ? 1 : 0;

	return $item;
}



# Create receipt
# Parameters:
#		ClientCode	- Client code
#		GLAccountNumber	- GL account where money was paid from
#		Number		- Number for this receipt
#		Date		- Date of receipt
#		Reference		- GL account entry reference (bank statement reference for example)
#		Amount		- Amount
sub createReceipt
{
	my ($detail) = @_;


	# Verify receipt number
	if (!defined($detail->{'Number'}) || $detail->{'Number'} eq "") {
		setError("No (or invalid) receipt number provided");
		return ERR_PARAM;
	}
	(my $receiptNumber = uc($detail->{'Number'})) =~ s#^RCT/##;

	# Verify client code
	if (!defined($detail->{'ClientCode'}) || $detail->{'ClientCode'} eq "") {
		setError("No (or invalid) client code provided for receipt '".$detail->{'Number'}."'");
		return ERR_PARAM;
	}

	# Verify GL account
	if (!defined($detail->{'GLAccountNumber'}) || $detail->{'GLAccountNumber'} eq "") {
		setError("No (or invalid) GL account provided for receipt '".$detail->{'Number'}."'");
		return ERR_PARAM;
	}

	# Verify date
	if (!defined($detail->{'Date'}) || $detail->{'Date'} eq "") {
		setError("No (or invalid) date provided for receipt '".$detail->{'Number'}."'");
		return ERR_PARAM;
	}

	# Verify reference
	if (!defined($detail->{'Reference'}) || $detail->{'Reference'} eq "") {
		setError("No (or invalid) reference provided for receipt '".$detail->{'Number'}."'");
		return ERR_PARAM;
	}

	# Amount
	if (!defined($detail->{'Amount'}) || $detail->{'Amount'} eq "") {
		setError("No (or invalid) amount account provided for receipt '".$detail->{'Number'}."'");
		return ERR_PARAM;
	}

	# Check if client exists
	my $clientID  = wiaflos::server::core::Clients::getClientIDFromCode($detail->{'ClientCode'});
	if ($clientID < 1) {
		setError(wiaflos::server::core::Clients::Error());
		return $clientID;
	}

	# Check GL account exists
	my $GLAccountID = wiaflos::server::core::GL::getGLAccountIDFromNumber($detail->{'GLAccountNumber'});
	if ($GLAccountID < 1) {
		setError(wiaflos::server::core::GL::Error());
		return $GLAccountID;
	}

	# Check for conflicts
	if (receiptNumberExists($receiptNumber)) {
		setError("Receipt number '$receiptNumber' already exists");
		return ERR_CONFLICT;
	}

	# Create receipt
	my $sth = DBDo("
		INSERT INTO receipts
				(ClientID,GLAccountID,Number,TransactionDate,Reference,Amount,Closed)
			VALUES
				(
					".DBQuote($clientID).",
					".DBQuote($GLAccountID).",
					".DBQuote($receiptNumber).",
					".DBQuote($detail->{'Date'}).",
					".DBQuote($detail->{'Reference'}).",
					".DBQuote($detail->{'Amount'}).",
					0
				)
	");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	# Grab last ID
	my $ID = DBLastInsertID("receipts","ID");

	return $ID;
}



# Return an array of receipts
sub getReceipts
{
	my ($detail) = @_;
	my $type = defined($detail->{'Type'}) ? $detail->{'Type'} : "open";
	my @receipts = ();


	# Return list of receipts
	my $sth = DBSelect("
		SELECT
			ID, ClientID, GLAccountID, Number, TransactionDate,
			Reference, Amount, Closed
		FROM
			receipts
	");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	# Fetch rows
	while (my $row = hashifyLCtoMC($sth->fetchrow_hashref(),
			qw( ID ClientID GLAccountID Number TransactionDate Reference Amount Closed )
	)) {
		# Check what kind of receipts do we want
		if (($type eq "open") && $row->{'Closed'} eq "0") {
			push(@receipts,sanitizeRawItem($row));
		} elsif ($type eq "all") {
			push(@receipts,sanitizeRawItem($row));
		}
	}

	DBFreeRes($sth);

	return \@receipts;
}



# Return an receipt hash
# Optional:
#		Number	- Receipt number
#		ID		- Receipt ID
sub getReceipt
{
	my ($detail) = @_;


	my $receiptID;

	# Check which 'mode' we operating in
	if (!defined($detail->{'ID'}) || $detail->{'ID'} < 1) {
		# Verify receipt number
		if (!defined($detail->{'Number'}) || $detail->{'Number'} eq "") {
			setError("No (or invalid) receipt number provided");
			return ERR_PARAM;
		}

		# Check if receipt exists
		if (($receiptID = getReceiptIDFromNumber($detail->{'Number'})) < 1) {
			setError(Error());
			return $receiptID;
		}
	} else {
		$receiptID = $detail->{'ID'};
	}

	# Verify receipt ID
	if (!$receiptID || $receiptID < 1) {
		setError("No (or invalid) receipt number/id provided");
		return ERR_PARAM;
	}

	# Return list of receipts
	my $sth = DBSelect("
		SELECT
			ID, ClientID, GLAccountID, Number, TransactionDate, Reference, Amount, GLTransactionID, Closed
		FROM
			receipts
		WHERE
			ID = ".DBQuote($receiptID)."
	");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	# Fetch rows
	my $row = hashifyLCtoMC($sth->fetchrow_hashref(),
			qw( ID ClientID GLAccountID Number TransactionDate Reference Amount GLTransactionID Closed )
	);
	DBFreeRes($sth);

	return sanitizeRawItem($row);
}



# One can now post the receipt, which sets GLTransactionID
# Parameters:
#		Number	- Receipt number
sub postReceipt
{
	my ($detail) = @_;


	my $data;

	# Grab receipt
	$data = undef;
	$data->{'Number'} = $detail->{'Number'};
	my $receipt = getReceipt($data);
	if (ref $receipt ne "HASH") {
		setError(Error());
		return $receipt;
	}

	# Make sure receipt is not posted
	if ($receipt->{'Posted'} eq "1") {
		setError("Receipt '".$receipt->{'Number'}."' already posted");
		return ERR_POSTED;
	}

	# Pull in client
	$data = undef;
	$data->{'ID'} = $receipt->{'ClientID'};
	my $client = wiaflos::server::core::Clients::getClient($data);
	if (ref $client ne "HASH") {
		setError(wiaflos::server::core::Clients::Error());
		return $client;
	}

	DBBegin();

	# Create transaction
	my $transactionRef = sprintf('Receipt: %s',$receipt->{'Number'});
	$data = undef;
	$data->{'Date'} = $receipt->{'TransactionDate'};
	$data->{'Reference'} = $transactionRef;
	my $GLTransactionID = wiaflos::server::core::GL::createGLTransaction($data);
	if ($GLTransactionID < 1) {
		setError(wiaflos::server::core::GL::Error());
		DBRollback();
		return $GLTransactionID;
	}

	# Pull in amount
	my $transValue = Math::BigFloat->new($receipt->{'Amount'});

	# Link from GL
	$data = undef;
	$data->{'ID'} = $GLTransactionID;
	$data->{'Reference'} = $receipt->{'Reference'};
	$data->{'GLAccountID'} = $receipt->{'GLAccountID'};
	$data->{'Amount'} = $transValue->bstr();
	if ((my $res = wiaflos::server::core::GL::linkGLTransaction($data)) < 1) {
		setError(wiaflos::server::core::GL::Error());
		DBRollback();
		return $res;
	}

	# Negate for other side
	$transValue->bneg();

	# Link to client GL account
	$data = undef;
	$data->{'ID'} = $GLTransactionID;
	$data->{'GLAccountID'} = $client->{'GLAccountID'};
	$data->{'Amount'} = $transValue->bstr();
	if ((my $res = wiaflos::server::core::GL::linkGLTransaction($data)) < 1) {
		setError(wiaflos::server::core::GL::Error());
		DBRollback();
		return $res;
	}

	# Post receipt
	my $sth = DBDo("
		UPDATE receipts
		SET
			GLTransactionID = ".DBQuote($GLTransactionID)."
		WHERE
			ID = ".DBQuote($receipt->{'ID'})."
	");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		DBRollback();
		return ERR_DB;
	}

	# Post transaction
	$data = undef;
	$data->{'ID'} = $GLTransactionID;
	if ((my $res = wiaflos::server::core::GL::postGLTransaction($data)) != 0) {
		setError(wiaflos::server::core::GL::Error());
		DBRollback();
		return $res;
	}

	DBCommit();

	return $GLTransactionID;
}


# @fn createReceiptAllocation($data)
# Create a receipt allocation
#
# @param data Hash ref with the below elements
# @li ReceiptNumber - Receipt number to create allocation for
# @li InvoiceNumber - Invoice number to allocate to
# @li TransactionNumber - Transaction number to allocate to
# @li Amount - Amount to allocate
#
# @return Receipt allocation ID on success < 1 on error
sub createReceiptAllocation
{
	my ($detail) = @_;


	# Verify receipt number
	if (!defined($detail->{'ReceiptNumber'}) || $detail->{'ReceiptNumber'} eq "") {
		setError("No (or invalid) receipt number provided for receipt allocation");
		return ERR_PARAM;
	}

	# Verify invoice number
	if (defined($detail->{'InvoiceNumber'})) {
		if ($detail->{'InvoiceNumber'} eq "") {
			setError("No (or invalid) invoice number provided for receipt allocation");
			return ERR_PARAM;
		}
	# or transaction number
	} elsif (defined($detail->{'TransactionNumber'})) {
		if ($detail->{'TransactionNumber'} eq "") {
			setError("No (or invalid) transaction number provided for receipt allocation");
			return ERR_PARAM;
		}
	} else {
		setError("No invoice or transaction number provided for receipt allocation");
		return ERR_PARAM;
	}

	# Amount
	if (!defined($detail->{'Amount'}) || $detail->{'Amount'} eq "") {
		setError("No (or invalid) amount account provided for receipt allocation");
		return ERR_PARAM;
	}
	my $allocAmount = Math::BigFloat->new($detail->{'Amount'});
	if ($allocAmount->is_zero()) {
		setError("Receipt allocation amount cannot be zero");
		return ERR_AMTZERO;
	}

	my $data;

	# Check if receipt exists & pull in
	$data = undef;
	$data->{'Number'} = $detail->{'ReceiptNumber'};
	my $receipt = getReceipt($data);
	if (ref $receipt ne "HASH" ) {
		setError(Error());
		return $receipt;
	}

	# Make sure receipt is posted
	if ($receipt->{'Posted'} ne "1") {
		setError("Receipt '".$receipt->{'Number'}."' must be posted before an allocation can be made");
		return ERR_POSTED;
	}

	$data = undef;
	my @extraCols;
	my @extraData;

	# Grab invoice
	if ($detail->{'InvoiceNumber'}) {
		$data->{'Number'} = $detail->{'InvoiceNumber'};
		my $invoice = wiaflos::server::core::Invoicing::getInvoice($data);
		if (ref($invoice) ne "HASH") {
			setError(wiaflos::server::core::Invoicing::Error());
			return $invoice;
		}

		# Check if invoice is posted or not
		if ($invoice->{'Posted'} ne "1") {
			setError("Cannot allocate an amount from receipt '".$receipt->{'Number'}."' to invoice '".$invoice->{'Number'}.
					"' as the invoice is not posted");
			return ERR_POSTED;
		}
		push(@extraCols,'InvoiceID');
		push(@extraData,DBQuote($invoice->{'ID'}));

	# Grab transaction
	} elsif ($detail->{'TransactionNumber'}) {
		$data->{'Number'} = $detail->{'TransactionNumber'};
		my $transaction = wiaflos::server::core::Clients::getAccountTransaction($data);
		if (ref($transaction) ne "HASH") {
			setError(wiaflos::server::core::Clients::Error());
			return $transaction;
		}

		# Check if transaction is posted or not
		if ($transaction->{'Posted'} ne "1") {
			setError("Cannot allocate an amount from receipt '".$receipt->{'Number'}."' to transaction '".$transaction->{'Number'}.
					"' as the transaction is not posted");
			return ERR_POSTED;
		}

		# Check if transaction is closed or not
		if ($transaction->{'Closed'} eq "1") {
			setError("Cannot allocate an amount from receipt '".$receipt->{'Number'}."' to transaction '".$transaction->{'Number'}.
					"' as the transaction is closed");
			return ERR_POSTED;
		}

		push(@extraCols,'AccountTransactionID');
		push(@extraData,DBQuote($transaction->{'ID'}));
	}


	# Grab allocations
	$data = undef;
	$data->{'ReceiptID'} = $receipt->{'ID'};
	my $allocs = getReceiptAllocations($data);
	if (ref $allocs ne "ARRAY") {
		setError(Error());
		return $allocs;
	}

	# Check if we either balance to 0 or have left over
	my $receiptBalance = Math::BigFloat->new($receipt->{'Amount'});
	foreach my $alloc (@{$allocs}) {
		$receiptBalance->bsub($alloc->{'Amount'});
	}
	$receiptBalance->bsub($detail->{'Amount'});
	if ($receiptBalance->is_neg()) {
		setError("Creating this allocation will exceed receipt '".$receipt->{'Number'}."' amount by ".$receiptBalance->bstr());
		return ERR_OVERALLOC;
	}

	# Pull in extra data
	my $extraCols = ',' . join(',',@extraCols);
	my $extraData = ',' . join(',',@extraData);

	# Create receipt allocation
	my $sth = DBDo("
		INSERT INTO receipt_allocations
				(ReceiptID,Amount$extraCols)
			VALUES
				(
					".DBQuote($receipt->{'ID'}).",
					".DBQuote($detail->{'Amount'})."
					$extraData
				)
	");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	# Grab last ID
	my $ID = DBLastInsertID("receipt_allocations","ID");

	return $ID;
}



# Return an array of receipt allocations
# Parameters:
#		ReceiptNumber	- Receipt number
#		ReceiptID	- Receipt ID
#
#		InvoiceID	-  invoice ID
#
#		AccountTransactionNumber - Transaction number
#		AccountTransactionID - Transaction ID
sub getReceiptAllocations
{
	my ($detail) = @_;

	my @allocations = ();


	# SQL query string to use
	my $query = "";

	# ID mode
	if (defined($detail->{'ReceiptID'}) && $detail->{'ReceiptID'} > 0) {
		$query .= "ReceiptID = ".$detail->{'ReceiptID'};

	# Receipt number mode
	} elsif (defined($detail->{'ReceiptNumber'}) && $detail->{'ReceiptNumber'} ne "") {
		my $receiptID;

		# Check if receipt exists
		if (($receiptID = getReceiptIDFromNumber($detail->{'ReceiptNumber'})) < 1) {
			setError(Error());
			return $receiptID;
		}

		$query .= "ReceiptID = ".DBQuote($receiptID);

	# Invoice ID mode
	} elsif (defined($detail->{'InvoiceID'}) && $detail->{'InvoiceID'} ne "") {
		$query .= "InvoiceID = ".DBQuote($detail->{'InvoiceID'});

	# Transaction ID mode
	} elsif (defined($detail->{'AccountTransactionID'}) && $detail->{'AccountTransactionID'} ne "") {
		$query .= "AccountTransactionID = ".DBQuote($detail->{'AccountTransactionID'});

	# Transaction number mode
	} elsif (defined($detail->{'AccountTransactionNumber'}) && $detail->{'AccountTransactionNumber'} ne "") {
		my $transactionID;

		# Check if transaction exists
		if (($transactionID = wiaflos::server::core::Clients::getTransactionIDFromNumber($detail->{'TransactionNumber'})) < 1) {
			setError(Error());
			return $transactionID;
		}

		$query .= "AccountTransactionID = ".DBQuote($transactionID);

	} else {
		setError("No acceptable parameters provided to return receipt allocations");
		return ERR_PARAM;
	}

	# Return list of receipt allocations
	my $sth = DBSelect("
		SELECT
			ID, ReceiptID, Amount, InvoiceID, InvoiceTransactionID, AccountTransactionID, AccountTransactionAllocationID
		FROM
			receipt_allocations
		WHERE
			$query
	");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	# Fetch rows
	while (my $row = hashifyLCtoMC($sth->fetchrow_hashref(),
			qw( ID ReceiptID Amount InvoiceID InvoiceTransactionID AccountTransactionID AccountTransactionAllocationID )
	)) {
		push(@allocations,sanitizeRawAllocationItem($row));
	}

	DBFreeRes($sth);

	return \@allocations;
}



# Return an hash containing a allocation
# Parameters:
#		ID	- Allocation ID
sub getReceiptAllocation
{
	my ($detail) = @_;


	# Verify allocation id
	if (!defined($detail->{'ID'}) || $detail->{'ID'} eq "") {
		setError("No (or invalid) allocation ID provided");
		return ERR_PARAM;
	}

	# Return allocation
	my $sth = DBSelect("
		SELECT
			ID, ReceiptID, Amount, InvoiceID, InvoiceTransactionID, AccountTransactionID, AccountTransactionAllocationID
		FROM
			receipt_allocations
		WHERE
			ID = ".DBQuote($detail->{'ID'})."
	");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	# Fetch rows
	my $row = hashifyLCtoMC($sth->fetchrow_hashref(),
			qw( ID ReceiptID Amount InvoiceID invoiceTransactionID AccountTransactionID AccountTransactionAllocationID )
	);
	DBFreeRes($sth);

	return sanitizeRawAllocationItem($row);
}



# One can now post the allocation, which checks the invoice if its totally paid
# Parameters:
#		ID	- Receipt allocation ID
sub postReceiptAllocation
{
	my ($detail) = @_;


	# Verify allocation ID
	if (!defined($detail->{'ID'}) || $detail->{'ID'} eq "") {
		setError("No (or invalid) allocation ID provided");
		return ERR_PARAM;
	}

	my $data;

	# Check if allocation exists & pull in
	$data = undef;
	$data->{'ID'} = $detail->{'ID'};
	my $allocation = getReceiptAllocation($data);
	if (ref $allocation ne "HASH") {
		setError(Error());
		return $allocation;
	}

	# Make sure allocation is not posted
	if ($allocation->{'Posted'} ne "0") {
		setError("Receipt allocation '".$allocation->{'ID'}."' already posted");
		return ERR_POSTED;
	}

	# Get corrosponding receipt
	$data = undef;
	$data->{'ID'} = $allocation->{'ReceiptID'};
	my $receipt = getReceipt($data);
	if (ref $receipt ne "HASH") {
		setError(Error());
		return $receipt;
	}

	# We use this at the end of the function to contain the ID of the thing we're posting the allocation against
	my $allocationAgainstID;

	# Check if its an invoice
	if (defined($allocation->{'InvoiceID'})) {
		# Grab invoice
		$data = undef;
		$data->{'ID'} = $allocation->{'InvoiceID'};
		my $invoice = wiaflos::server::core::Invoicing::getInvoice($data);
		if (ref $invoice ne "HASH") {
			setError(wiaflos::server::core::Invoicing::Error());
			return $invoice;
		}

		# Make sure invoice is not paid
		if ($invoice->{'Paid'} ne "0") {
			setError("Invoice '".$invoice->{'Number'}."' already paid");
			return ERR_PAID;
		}

		# Make sure invoice is posted
		if ($invoice->{'Posted'} ne "1") {
			setError("Invoice '".$invoice->{'Number'}."' not posted");
			return ERR_POSTED;
		}

		# Grab invoice allocations
		$data = undef;
		$data->{'InvoiceID'} = $allocation->{'InvoiceID'};
		my $invoiceAllocations = getReceiptAllocations($data);
		if (ref $invoiceAllocations ne "ARRAY") {
			setError(Error());
			return $invoiceAllocations;
		}
		# Add up invoice balance
		my $invoiceBalance = Math::BigFloat->new($invoice->{'Total'});
		foreach my $alloc (@{$invoiceAllocations}) {
			if ($alloc->{'Posted'} eq "1") {
				$invoiceBalance->bsub($alloc->{'Amount'});
			}
		}
		# Check invoice balance if its negative, this is if we've overallocated
		$invoiceBalance->bsub($allocation->{'Amount'});
		if ($invoiceBalance->is_neg()) {
			setError("Posting the allocation will end up in a balance of '".$invoiceBalance->bstr()."' on invoice '".$invoice->{'Number'}."'");
			return ERR_OVERALLOC;
		}

		$allocationAgainstID = $invoice->{'ID'};

	# Or if its a account transaction
	} elsif (defined($allocation->{'AccountTransactionID'})) {
		# Grab account transaction
		$data = undef;
		$data->{'ID'} = $allocation->{'AccountTransactionID'};
		my $transaction = wiaflos::server::core::Clients::getAccountTransaction($data);
		if (ref($transaction) ne "HASH") {
			setError(wiaflos::server::core::Invoicing::Error());
			return $transaction;
		}

		# Make sure transaction is posted
		if ($transaction->{'Posted'} ne "1") {
			setError("Account transaction '".$transaction->{'Number'}."' not posted");
			return ERR_POSTED;
		}

		# Make sure transaction is not closed
		if ($transaction->{'Closed'} eq "1") {
			setError("Account transaction '".$transaction->{'Number'}."' already closed");
			return ERR_PAID;
		}

		# Grab transaction allocations
		$data = undef;
		$data->{'AccountTransactionID'} = $transaction->{'ID'};
		my $transactionAllocations = getReceiptAllocations($data);
		if (ref($transactionAllocations) ne "ARRAY") {
			setError(Error());
			return $transactionAllocations;
		}
		# Add up transaction balance
		my $transactionBalance = Math::BigFloat->new($transaction->{'Amount'});
		foreach my $alloc (@{$transactionAllocations}) {
			if ($alloc->{'Posted'} eq "1") {
				$transactionBalance->bsub($alloc->{'Amount'});
			}
		}
		# Check transaction balance if its negative, this is if we've overallocated
		$transactionBalance->bsub($allocation->{'Amount'});
		if ($transactionBalance->is_neg()) {
			setError("Posting the allocation will end up in a balance of '".$transactionBalance->bstr().
					"' on account transaction '".$transaction->{'Number'}."'");
			return ERR_OVERALLOC;
		}

		$allocationAgainstID = $transaction->{'ID'};
	}


	# Grab receipt allocations
	$data = undef;
	$data->{'ReceiptID'} = $receipt->{'ID'};
	my $receiptAllocations = getReceiptAllocations($data);
	if (ref $receiptAllocations ne "ARRAY") {
		setError(Error());
		return $receiptAllocations;
	}
	# Add up receipt balance
	my $receiptBalance = Math::BigFloat->new($receipt->{'Amount'});
	foreach my $alloc (@{$receiptAllocations}) {
		if ($alloc->{'Posted'} eq "1") {
			$receiptBalance->bsub($alloc->{'Amount'});
		}
	}
	# Check receipt balance if its negative, this is if we've overallocated
	$receiptBalance->bsub($allocation->{'Amount'});
	if ($receiptBalance->is_neg()) {
		setError("Posting the allocation will end up in a balance of '".$receiptBalance->bstr()."' on receipt '".$receipt->{'Number'}."'");
		return ERR_OVERALLOC;
	}

	DBBegin();

	# If we equal out our payment, mark it as being closed
	if ($receiptBalance->is_zero()) {
		# Close it off
		my $sth = DBDo("
			UPDATE
				receipts
			SET
				Closed = 1
			WHERE
				ID = ".DBQuote($receipt->{'ID'})."
		");
		if (!$sth) {
			setError(awitpt::db::dblayer::Error());
			DBRollback();
			return ERR_DB;
		}
	}

	# Stuff we need for below
	$data = undef;
	$data->{'ID'} = $allocationAgainstID;
	$data->{'Amount'} = Math::BigFloat->new($allocation->{'Amount'})->bneg();
	$data->{'ReceiptAllocationID'} = $allocation->{'ID'};

	my $extraSQL;

	# Create invoice transaction
	if (defined($allocation->{'InvoiceID'})) {
		my $invoiceTransactionID = wiaflos::server::core::Invoicing::allocateInvoiceTransaction($data);
		if ($invoiceTransactionID < 1) {
			setError(wiaflos::server::core::Invoicing::Error());
			DBRollback();
			return $invoiceTransactionID;
		}
		$extraSQL = "InvoiceTransactionID = ".DBQuote($invoiceTransactionID);

	# Or, if its an account transaction, an account transaction allocation entry
	} elsif (defined($allocation->{'AccountTransactionID'})) {
		my $accountTransactionAllocationID = wiaflos::server::core::Clients::linkAccountTransactionAllocation($data);
		if ($accountTransactionAllocationID < 1) {
			setError(wiaflos::server::core::Invoicing::Error());
			DBRollback();
			return $accountTransactionAllocationID;
		}
		$extraSQL = "AccountTransactionAllocationID = ".DBQuote($accountTransactionAllocationID);
	}

	# Post allocation
	my $sth = DBDo("
		UPDATE
			receipt_allocations
		SET
			$extraSQL
		WHERE
			ID = ".DBQuote($allocation->{'ID'})."
	");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		DBRollback();
		return ERR_DB;
	}

	DBCommit();

	return RES_OK;
}



# Send receipt
# Parameters:
#		Number	- Receipt number
#		SendTo		- Send to,  email:  ,  file:  , return
sub sendReceipt
{
	my ($detail) = @_;


	# Verify SendTo
	if (!defined($detail->{'SendTo'}) || $detail->{'SendTo'} eq "") {
		setError("SendTo was not provided");
		return ERR_PARAM;
	}

	# Pull in config
	my $config = wiaflos::server::core::config::getConfig();

	# Grab receipt
	my $data;
	$data->{'Number'} = $detail->{'Number'};
	my $receipt = getReceipt($data);
	if (ref $receipt ne "HASH") {
		setError(Error());
		return $receipt;
	}

	# Grab items
	$data = undef;
	$data->{'ReceiptID'} = $receipt->{'ID'};
	my $allocations = getReceiptAllocations($data);
	if (ref $allocations ne "ARRAY") {
		setError(Error());
		return $allocations;
	}

	# Pull client
	$data = undef;
	$data->{'ID'} = $receipt->{'ClientID'};
	my $client = wiaflos::server::core::Clients::getClient($data);
	if (ref $client ne "HASH") {
		setError(wiaflos::server::core::Clients::Error());
		return $client;
	}

	# Pull in addresses
	$data = undef;
	$data->{'ID'} = $receipt->{'ClientID'};
	my $addresses = wiaflos::server::core::Clients::getClientAddresses($data);
	if (ref $addresses ne "ARRAY") {
		setError(wiaflos::server::core::Clients::Error());
		return $addresses;
	}

	# Pull in email addresses
	$data = undef;
	$data->{'ID'} = $receipt->{'ClientID'};
	my $emailAddies = wiaflos::server::core::Clients::getClientEmailAddresses($data);
	if (ref $emailAddies ne "ARRAY") {
		setError(wiaflos::server::core::Clients::Error());
		return $emailAddies;
	}

	# Parse in addresses
	my $billAddr;
	my $shipAddr = "";
	# Look for billing address
	foreach my $address (@{$addresses}) {
		if ($address->{'Type'} eq "billing") {
			$billAddr = $address->{'Address'};
		} elsif ($address->{'Type'} eq "shipping") {
			$shipAddr = $address->{'Address'};
		}
	}
	# If no billing address, use shipping address
	$billAddr = defined($billAddr) ? $billAddr : $shipAddr;
	$billAddr =~ s/\|/<br \/>/g;
	# Setup shipping address
	$shipAddr =~ s/\|/<br \/>/g if defined($shipAddr);

	# Parse in email addresses
	my $billEmailAddr;
	my @billEmailAddrs;
	my @genEmailAddrs;
	# Look for accounts address
	foreach my $address (@{$emailAddies}) {
		if ($address->{'Type'} eq "accounts") {
			push(@billEmailAddrs,$address->{'Address'});
		} elsif ($address->{'Type'} eq "general") {
			push(@genEmailAddrs,$address->{'Address'});
		}
	}
	# If no accounts address, use general address
	$billEmailAddr = @billEmailAddrs > 0 ? join(',',@billEmailAddrs) : join(',',@genEmailAddrs);

	# We change this as we go along...
	my $receiptBalance = Math::BigFloat->new($receipt->{'Amount'});

	# Build array of stuff we can use
	my $vars = {
		'WiaflosString' => $GENSTRING,

		# Client
		'ClientName' => $client->{'Name'},
		'ClientCode' => $client->{'Code'},
		'ClientBillingAddress' => $billAddr,
		'ClientShippingAddress' => $shipAddr,

		# Receipt
		'ClientTaxReference' => defined($receipt->{'TaxReference'}) ? $receipt->{'TaxReference'} : "",

		'ReceiptAmount' => sprintf('%.2f',$receipt->{'Amount'}),
		'ReceiptNumber' => $receipt->{'Number'},
		'ReceiptDate' => $receipt->{'TransactionDate'},
		# TODO: receipt note not implemented yet
		'ReceiptNote' => "",
	};

	# Load receipt line items
	foreach my $item (@{$allocations}) {

		# Pull invoice
		$data = undef;
		$data->{'ID'} = $item->{'InvoiceID'};
		my $invoice = wiaflos::server::core::Invoicing::getInvoice($data);
		if (ref $invoice ne "HASH") {
			setError(wiaflos::server::core::Invoicing::Error());
			return $invoice;
		}

		my $titem;

		# Fix some stuff up & sanitize
		$titem->{'ReceiptAllocatedAmount'} = sprintf('%.2f',$item->{'Amount'});
		# Keep balance
		$receiptBalance->bsub($item->{'Amount'});
		$titem->{'ReceiptBalance'} = sprintf('%.2f',$receiptBalance->bstr());

		$titem->{'InvoiceNumber'} = $invoice->{'Number'};
		$titem->{'InvoiceIssueDate'} = $invoice->{'IssueDate'};
		$titem->{'InvoiceDueDate'} = $invoice->{'DueDate'};
		$titem->{'InvoicePaid'} = $invoice->{'Paid'} ? 'yes' : 'no';
		$titem->{'InvoiceTotalIncl'} = sprintf('%.2f',$invoice->{'Total'});

		push(@{$vars->{'LineItems'}},$titem);
	}
	# This MUST be at the END of your template, AFTER the items above to be of any use
	$vars->{'ReceiptBalance'} = sprintf('%.2f',$receiptBalance->bstr());

	# Get receipt template file
	my $template = defined($config->{'receipting'}{'email_template'}) ? $config->{'receipting'}{'email_template'} : "receipts/receipt1.tt2";

	# Check where receipt must go
	if ($detail->{'SendTo'} =~ /^file:(\S+)/i) {
		my $filename = $1;

		# Load template
		my $res = loadTemplate($template,$vars,$filename);
		if (!$res) {
			setError("Failed to load template '$template': ".wiaflos::server::core::templating::Error());
			return ERR_SRVTEMPLATE;
		}

	# Write out using email
	} elsif ($detail->{'SendTo'} =~ /^email(?:\:(\S+))?/i) {
		# Pull email addr
		my $emailAddy = $1 ne "" ? $1 : $billEmailAddr;

		# Verify SMTP server is set
		my $server = $config->{'mail'}{'server'};
		if (!defined($server) || $server eq "") {
			setError("Cannot use receipt emailing if we do not have an SMTP server defined");
			return ERR_SRVPARAM;
		}

		# Check if we have a email addy
		if ($emailAddy eq "") {
			setError("No email address defined to send receipt '".$receipt->{'Number'}."' to");
			return ERR_PARAM;
		}

		# Receipt filename
		(my $receiptFilename = $receipt->{'Number'} . ".html") =~ s,/,-,g;
		# Invoice signature filename
		my $rctSignFilename = $receiptFilename . ".asc";

		# If we must, pull in email body
		my $message_template = $config->{'receipting'}{'email_message_template'};
		my $emailBody = "";
		if (defined($message_template) && $message_template ne "") {
			# Variables for our template
			my $vars2 = {
				'ReceiptFilename' => $receiptFilename,
				'ReceiptSignatureFilename' => $rctSignFilename,
			};
			# Load template
			my $res = loadTemplate($message_template,$vars2,\$emailBody);
			if (!$res) {
				setError("Failed to load template '$message_template': ".wiaflos::server::core::templating::Error());
				return ERR_SRVTEMPLATE;
			}

			$emailBody =~ s/(?<!\r)\n/\r\n/sg; # Sanitize eol for crypt-gpg
		}

		# Load template
		my $receiptData = "";
		my $res = loadTemplate($template,$vars,\$receiptData);
		if (!$res) {
			setError("Failed to load template '$template': ".wiaflos::server::core::templating::Error());
			return ERR_SRVTEMPLATE;
		}
		$receiptData =~ s/(?<!\r)\n/\r\n/sg; # Sanitize eol, needed to fix bug in crypt-gpg where it mangles \n

		# See if we must use GPG
		my $use_gpg_key = $config->{'receipting'}{'use_gpg_key'};
		my $sign;
		if (defined($use_gpg_key) && $use_gpg_key ne "") {
			# Setup GnuPG
			my $gpg = new Crypt::GPG;
			$gpg->gpgbin('/usr/bin/gpg');
			$gpg->secretkey($use_gpg_key);
			$gpg->armor(1);
			$gpg->comment("$APPNAME v$VERSION ($APPURL)");
			# Sign receipt
			$sign = $gpg->sign($receiptData);
			if (!defined($sign)) {
				setError("Failed to sign receipt");
				return ERR_SRVEXEC;
			}
		}

		# Create message
		my $msg = MIME::Lite->new(
				From	=> $config->{'receipting'}{'email_from'},
				To		=> $emailAddy,
				Bcc		=> $config->{'receipting'}{'email_bcc'},
				Subject	=> "Receipt: ".$receipt->{'Number'},
				Type	=> 'multipart/mixed'
		);

		# Attach body
		$msg->attach(
				Type	=> 'TEXT',
				Encoding 	=> '8bit',
				Data	=> $emailBody,
		);

		# Attach receipt
		$msg->attach(
				Type	=> 'text/html',
				Encoding	=> 'base64',
				Data	=> $receiptData,
				Disposition	=> 'attachment',
				Filename	=> $receiptFilename
		);

		# If we have signature, sign receipt
		if (defined($sign)) {
			# Attach signature
			$msg->attach(
					Type	=> 'text/plain',
					Encoding	=> 'base64',
					Data	=> $sign,
					Disposition	=> 'attachment',
					Filename	=> $rctSignFilename
			);
		}

		# Send email
		my @SMTPParams;
		$res = $msg->send("smtp",$server);
		if (!$res) {
			setError("Failed to send receipt via email server '$server'");
			return ERR_SRVEXEC;
		}

	} else {
		setError("Invalid SendTo method provided: '".$detail->{'SendTo'}."'");
		return ERR_PARAM;
	}


	return RES_OK;
}





1;
# vim: ts=4
