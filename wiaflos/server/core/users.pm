# User functions
# Copyright (C) 2009-2014, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2005-2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.




package wiaflos::server::core::users;

use strict;
use warnings;


use wiaflos::constants;
use wiaflos::server::core::config;
use awitpt::db::dblayer;
use wiaflos::server::core::system qw(hashPassword);

# Our current error message
my $error = "";

# Set current error message
# Args: error_message
sub setError
{
	my $err = shift;
	my ($package,$filename,$line) = caller;
	my (undef,undef,undef,$subroutine) = caller(1);

	# Set error
	$error = "$subroutine($line): $err";
}

# Return current error message
# Args: none
sub Error
{
	my $err = $error;

	# Reset error
	$error = "";

	# Return error
	return $err;
}



# Function to authenticate a user
# Args: <username> <password>
sub authenticate
{
	my ($username,$password) = @_;
	my $res = -1;


	if (!defined($username) || !defined($password)) {
		setError("Username and password not given");
		return ERR_PARAM;
	}

	# Select user details from database
	my $sth = DBSelect("
			SELECT
				ID, Username, Password, Disabled
			FROM
				soap_users
			WHERE
				Username = ".DBQuote($username)."
				AND Disabled = 0
	");
	if (!defined($sth)) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	# Grab result & check if we got something
	my $row = hashifyLCtoMC($sth->fetchrow_hashref(),
		qw( ID Username Password Disabled )
	);
	if (!defined($row)) {
		DBFreeRes($sth);
		setError("Username not found");
		return ERR_NOTFOUND;
	}

	# Equal both hashes
	my $cpass = hashPassword($password);
	(my $dpass = $row->{'Password'}) =~ s/^\{(\S+)\}/{\U$1}/;

	# Compare
	if ($cpass eq $dpass) {
		$res = $row->{'ID'};
	} else {
		setError("Passwords do not match");
	}

	DBFreeRes($sth);

	return $res;
}



# Function to get capabilities of a user
# Args: <user id>
sub getCapabilities
{
	my ($userID) = @_;
	my @capabilities = ();


	if (!defined($userID) || $userID < 1) {
		setError("UserID invalid");
		return undef;
	}

	# Select user details from database
	my $sth = DBSelect("
			SELECT
				soap_caps.Capability
			FROM
				soap_users, soap_grptousr, soap_caps
			WHERE
				soap_users.ID = ".DBQuote($userID)."
				AND soap_grptousr.UserID = soap_users.ID
				AND soap_caps.GroupID = soap_grptousr.GroupID
	");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		return undef;
	}

	# Grab results
	while (my $row = hashifyLCtoMC($sth->fetchrow_hashref(),
		qw( Capability )
	)) {
		push(@capabilities,$row->{'Capability'});
	}


	DBFreeRes($sth);

	return \@capabilities;
}




1;
# vim: ts=4
