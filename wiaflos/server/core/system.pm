# System functions
# Copyright (C) 2009, AllWorldiT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2005-2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.




package wiaflos::server::core::system;

use strict;
use warnings;


# Exporter stuff
require Exporter;
our (@ISA,@EXPORT_OK);
@ISA = qw(Exporter);
@EXPORT_OK = qw(
	hashPassword

	OpenMax

	SOAP_Param_Escape
	SOAP_Param_UnEscape
	$SOAP_Param_Regex
);

our (
	$SOAP_Param_Regex
);

use awitpt::db::dblayer;

use Digest::MD5;
use URI::Escape;
use POSIX;


# Set globals
$SOAP_Param_Regex = "[A-Za-z0-9\%]";

# Our current error message
my $error = "";

# Set current error message
# Args: error_message
sub setError
{
	my $err = shift;
	my ($package,$filename,$line) = caller;
	my (undef,undef,undef,$subroutine) = caller(1);

	# Set error
	$error = "$subroutine($line): $err";
}

# Return current error message
# Args: none
sub Error
{
	my $err = $error;

	# Reset error
	$error = "";

	# Return error
	return $err;
}


# Hash a plaintext password
# Args: <password>
sub hashPassword
{
	my $plain = shift;
	my $ctx = Digest::MD5->new();


	$ctx->add($plain);

	# Prefix '{MD5}' digest name and suffix with '==' to pad on 4 byte boundary
	return "{MD5}".$ctx->b64digest()."==";
}


# Make a SOAP param safe to be passed
# Args: <param>
sub SOAP_Param_Escape
{
	my $param = shift;
	# All chars except alphanumeric is escaped
	return uri_escape($param,'^A-Za-z0-9');
}


# Decipher a SOAP param which was escaped
# Args: <param>
sub SOAP_Param_UnEscape
{
	my $param = shift;
	# Unescape
	return uri_unescape($param);
}

# Return the maximum number of possible file descriptors
sub OpenMax {
    my $openmax = POSIX::sysconf( &POSIX::_SC_OPEN_MAX );
    (!defined($openmax) || $openmax < 0) ? 1024 : $openmax;
}


1;
# vim: ts=4
