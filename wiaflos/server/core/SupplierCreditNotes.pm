# Supplier credit note functions
# Copyright (C) 2009-2014, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.




package wiaflos::server::core::SupplierCreditNotes;

use strict;
use warnings;


use wiaflos::constants;
use awitpt::db::dblayer;
use wiaflos::server::core::GL;
use wiaflos::server::core::Inventory;
use wiaflos::server::core::Suppliers;
use wiaflos::server::core::Tax;

# Whole money transactions, precision is two
use Math::BigFloat;
Math::BigFloat::precision(-2);



# Our current error message
my $error = "";

# Set current error message
# Args: error_message
sub setError
{
	my $err = shift;
	my ($package,$filename,$line) = caller;
	my (undef,undef,undef,$subroutine) = caller(1);

	# Set error
	$error = "$subroutine($line): $err";
}

# Return current error message
# Args: none
sub Error
{
	my $err = $error;

	# Reset error
	$error = "";

	# Return error
	return $err;
}


# Backend function to build item hash
sub sanitizeRawItem
{
	my $rawData = shift;


	my $item;

	$item->{'ID'} = $rawData->{'ID'};

	$item->{'SupplierID'} = $rawData->{'SupplierID'};
	$item->{'SupplierCode'} = wiaflos::server::core::Suppliers::getSupplierCodeFromID($rawData->{'SupplierID'});
	$item->{'Number'} = "SCN/".uc($rawData->{'Number'});
	$item->{'SupplierInvoiceID'} = $rawData->{'SupplierInvoiceID'};

	$item->{'TaxReference'} = $rawData->{'TaxReference'};

	$item->{'SupplierCreditNoteNumber'} = $rawData->{'SupplierCreditNoteNumber'};
	$item->{'IssueDate'} = $rawData->{'IssueDate'};

	$item->{'SubTotal'} = $rawData->{'SubTotal'};
	$item->{'TaxTotal'} = $rawData->{'TaxTotal'};
	$item->{'Total'} = $rawData->{'Total'};

	$item->{'GLTransactionID'} = $rawData->{'GLTransactionID'};
	$item->{'Posted'} = defined($rawData->{'GLTransactionID'}) ? 1 : 0;

	$item->{'Closed'} = $rawData->{'Closed'};


	return $item;
}


# Check if credit note ID exists
# Backend function, takes 1 parameter which is the credit note ID
sub supplierCreditNoteIDExists
{
	my $creditNoteID = shift;


	# Select credit note count
	my $rows = DBSelectNumResults("FROM supplier_creditnotes WHERE ID = ".DBQuote($creditNoteID));
	if (!defined($rows)) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	return $rows > 0 ? 1 : 0;
}


# Check if credit note number exists
# Backend function, takes 1 parameter which is the credit note ID
sub supplierCreditNoteNumberExists
{
	my $creditNoteNumber = shift;


	# Uppercase and remove SCN/
	$creditNoteNumber = uc($creditNoteNumber);
	$creditNoteNumber =~ s#^SCN/##;

	# Select credit note count
	my $rows = DBSelectNumResults("FROM supplier_creditnotes WHERE Number = ".DBQuote($creditNoteNumber));
	if (!defined($rows)) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	return $rows > 0 ? 1 : 0;
}


# Return suppliers credit note id from ref
sub getSupplierCreditNoteIDFromNumber
{
	my $creditNoteNumber = shift;


	# Uppercase and remove SCN/
	$creditNoteNumber = uc($creditNoteNumber);
	$creditNoteNumber =~ s#^SCN/##;

	# Select supplier credit note
	my $sth = DBSelect("
		SELECT
			ID
		FROM
			supplier_creditnotes
		WHERE
			Number = ".DBQuote($creditNoteNumber)."
	");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	my $row = hashifyLCtoMC($sth->fetchrow_hashref(),qw( ID ));
	DBFreeRes($sth);

	# Check we got a result
	if (!defined($row)) {
		setError("Error finding supplier credit note '$creditNoteNumber'");
		return ERR_NOTFOUND;
	}

	return $row->{'ID'};
}



# Return an array of supplier credit notes
# Optional
#		Type - "open", "all"
sub getSupplierCreditNotes
{
	my ($detail) = @_;
	my $type = defined($detail->{'Type'}) ? $detail->{'Type'} : "open";
	my @creditNotes = ();


	# Return list of credit notes
	my $sth = DBSelect("
		SELECT
			ID, SupplierID, Number, SupplierInvoiceID, SupplierCreditNoteNumber, IssueDate, SubTotal,
			TaxTotal, Total, GLTransactionID, Closed
		FROM
			supplier_creditnotes
	");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	# Fetch rows
	while (my $row = hashifyLCtoMC($sth->fetchrow_hashref(),
			qw( ID SupplierID Number SupplierInvoiceID SupplierCreditNoteNumber IssueDate SubTotal
				TaxTotal Total GLTransactionID Closed )
	)) {
		# Check what kind of payments we want
		if (($type eq "open") && $row->{'Closed'} eq "0") {
			push(@creditNotes,sanitizeRawItem($row));
		} elsif ($type eq "all") {
			push(@creditNotes,sanitizeRawItem($row));
		}
	}

	DBFreeRes($sth);

	return \@creditNotes;
}


# Return a supplier credit note
# Optional:
#		ID	- Supplier credit note ID
#		Number	- Supplier credit note number
sub getSupplierCreditNote
{
	my ($detail) = @_;


	my $supplierCreditNoteID;

	# Check which 'mode' we operating in
	if (!defined($detail->{'ID'}) || $detail->{'ID'} < 1) {
		# Verify supplier credit note number
		if (!defined($detail->{'Number'}) || $detail->{'Number'} eq "") {
			setError("No (or invalid) supplier credit note number provided");
			return ERR_PARAM;
		}

		# Check if supplier credit note exists
		if (($supplierCreditNoteID = getSupplierCreditNoteIDFromNumber($detail->{'Number'})) < 1) {
			setError(Error());
			return $supplierCreditNoteID;
		}
	} else {
		$supplierCreditNoteID = $detail->{'ID'};
	}

	# Verify supplier credit note ID
	if (!$supplierCreditNoteID || $supplierCreditNoteID < 1) {
		setError("No (or invalid) supplier credit note number/id provided");
		return ERR_PARAM;
	}

	# Return credit note details
	my $sth = DBSelect("
		SELECT
			ID, SupplierID, Number, SupplierInvoiceID, TaxReference, SupplierCreditNoteNumber, IssueDate, SubTotal,
			TaxTotal, Total, GLTransactionID, Closed
		FROM
			supplier_creditnotes
		WHERE
			ID = ".DBQuote($supplierCreditNoteID)."
	");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	# Fetch row
	my $row = hashifyLCtoMC($sth->fetchrow_hashref(),
			qw( ID SupplierID Number SupplierInvoiceID TaxReference SupplierCreditNoteNumber IssueDate SubTotal
				TaxTotal Total GLTransactionID Closed )
	);
	DBFreeRes($sth);

	return sanitizeRawItem($row);
}


# Return an array of credit note items
# Parameters:
#		Number	- Supplier credit note number
sub getSupplierCreditNoteItems
{
	my ($detail) = @_;

	my @items = ();


	# Grab credit note
	my $data;
	$data->{'Number'} = $detail->{'Number'};
	my $creditNote = getSupplierCreditNote($data);
	if (ref $creditNote ne "HASH") {
		setError(Error());
		return $creditNote;
	}

	# Return list of supplier credit note items
	my $sth = DBSelect("
		SELECT
			ID, Description, Quantity, UnitPrice, Price
		FROM
			supplier_creditnote_items
		WHERE
			ID = ".DBQuote($creditNote->{'ID'})."
	");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	# Pull in items
	my @creditNoteItems;
	while (my $row = hashifyLCtoMC($sth->fetchrow_hashref(),
			qw( ID Description Quantity UnitPrice Price )
	)) {
		push(@creditNoteItems,$row);
	}

	DBFreeRes($sth);

	# Fetch rows
	foreach my $row (@creditNoteItems) {
		my $item;

		# Pull in linking info
		$sth = DBSelect("
			SELECT
				SupplierInvoiceItemID, Description, TaxTypeID, TaxMode, TaxRate, TaxAmount
			FROM
				supplier_creditnote_item_linking
			WHERE
				SupplierCreditNoteItemID = ".DBQuote($row->{'ID'})."
		");
		if (!$sth) {
			setError(awitpt::db::dblayer::Error());
			return ERR_DB;
		}

		# Pull in items from main query
		$item->{'ID'} = $row->{'ID'};
		$item->{'SerialNumber'} = $row->{'SerialNumber'};
		$item->{'Quantity'} = $row->{'Quantity'};
		$item->{'Unit'} = $row->{'Unit'};
		$item->{'UnitPrice'} = $row->{'UnitPrice'};
		$item->{'Price'} = $row->{'Price'};

		# Loop with linking items
		while (my $linkRow = hashifyLCtoMC($sth->fetchrow_hashref(),
				qw( SupplierInvoiceItemID Description TaxTypeID TaxMode TaxRate TaxAmount )
		)) {
			$item->{'SupplierInvoiceItemID'} = $linkRow->{'SupplierInvoiceItemID'};
			$item->{'Description'} = $linkRow->{'Description'};

			# Pull in inventory item
			$data = undef;
			$data->{'ID'} = $linkRow->{'SupplierInvoiceItemID'};
			my $invoiceItem = wiaflos::server::core::Purchasing::getSupplierInvoiceItem($data);

			if (defined($invoiceItem->{'InventoryID'})) {
				$item->{'InventoryID'} = $invoiceItem->{'InventoryID'};
			} elsif (defined($invoiceItem->{'GLAccountID'})) {
				$item->{'GLAccountID'} = $invoiceItem->{'GLAccountID'};
				$item->{'GLAccountNumber'} = $invoiceItem->{'GLAccountNumber'};
			}

			$item->{'TaxRate'} = $linkRow->{'TaxRate'};
			$item->{'TaxAmount'} = $linkRow->{'TaxAmount'};

			# Calculate final price
			my $totalPrice = Math::BigFloat->new($row->{'Price'});
			$totalPrice->precision(-2);
			if ($linkRow->{'TaxMode'} eq "2") {
				$totalPrice->badd($linkRow->{'TaxAmount'});
			}
			$item->{'TotalPrice'} = $totalPrice->bstr();

			push(@items,$item);
		}

		DBFreeRes($sth);
	}


	return \@items;
}


# Create supplier credit note
# Parameters:
#		SupplierCode		- Supplier code
#		Number				- Credit note ref
#		SupplierCreditNoteNumber	- Suppliers credit note number
#		SupplierInvoiceNumber	- Supplier invoice ref, invoice against which this credit note is
#		IssueDate		- Issue date
# Optional:
#		Note			- Notes
sub createSupplierCreditNote
{
	my ($detail) = @_;


	my @extraCols = ();
	my @extraData = ();


	# Verify supplier code
	if (!defined($detail->{'SupplierCode'}) || $detail->{'SupplierCode'} eq "" ) {
		setError("No supplier ID provided");
		return ERR_PARAM;
	}

	# Verify credit note number
	if (!defined($detail->{'Number'}) || $detail->{'Number'} eq "") {
		setError("No credit note number provided");
		return ERR_PARAM;
	}
	# Strip off prefix
	(my $creditNoteNumber = uc($detail->{'Number'})) =~ s#^SCN/##i;

	# Make sure we have a supplier credit note number
	if (!defined($detail->{'SupplierCreditNoteNumber'}) || $detail->{'SupplierCreditNoteNumber'} eq "") {
		setError("No supplier credit note number provided");
		return ERR_PARAM;
	}

	# Make sure we have a supplier credit invoice number
	if (!defined($detail->{'SupplierInvoiceNumber'}) || $detail->{'SupplierInvoiceNumber'} eq "") {
		setError("No supplier invoice number provided");
		return ERR_PARAM;
	}

	# Verify issue date
	if (!defined($detail->{'IssueDate'}) || $detail->{'IssueDate'} eq "") {
		setError("No issue date provided");
		return ERR_PARAM;
	}

	# Add note if it exists
	if (defined($detail->{'Note'}) && $detail->{'Note'} ne "") {
		push(@extraCols,'Note');
		push(@extraData,DBQuote($detail->{'Note'}));
	}

	# Grab supplier
	my $data;
	$data->{'Code'} = $detail->{'SupplierCode'};
	my $supplier = wiaflos::server::core::Suppliers::getSupplier($data);
	if (ref $supplier ne "HASH") {
		setError(wiaflos::server::core::Suppliers::Error());
		return $supplier;
	}


	# Check for conflicts
	if ((my $res = supplierCreditNoteNumberExists($creditNoteNumber)) != 0) {
		# Err of exists
		if ($res == 1) {
			setError("Supplier credit note number already exists");
			return ERR_CONFLICT;
		} else {
			setError(Error());
		}
		# else err with result
		return $res;
	}

	# Check if supplier invoice was provided
	my $supplierInvoiceID = wiaflos::server::core::Purchasing::getSupplierInvoiceIDFromSupplierInvoiceNumber($supplier->{'ID'},$detail->{'SupplierInvoiceNumber'});
	if ($supplierInvoiceID < 1) {
		setError(wiaflos::server::core::Purchasing::Error());
		return $supplierInvoiceID;
	}

	# Pull in stuff we need
	if (defined($supplier->{'TaxReference'}) && $supplier->{'TaxReference'} ne "") {
		push(@extraCols,'TaxReference');
		push(@extraData,DBQuote($supplier->{'TaxReference'}));
	}

	# Pull in extra data
	my $extraCols = "";
	my $extraData = "";
	if (@extraCols > 0 && @extraData > 0) {
		$extraCols .= ',' . join(',',@extraCols);
		$extraData .= ',' . join(',',@extraData);
	}

	# Create supplier credit note
	my $sth = DBDo("
		INSERT INTO supplier_creditnotes
			(SupplierID,Number,SupplierCreditNoteNumber,SupplierInvoiceID,IssueDate$extraCols)
		VALUES
			(
				".DBQuote($supplier->{'ID'}).",
				".DBQuote($creditNoteNumber).",
				".DBQuote($detail->{'SupplierCreditNoteNumber'}).",
				".DBQuote($supplierInvoiceID).",
				".DBQuote($detail->{'IssueDate'})."
				$extraData
			)
	");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	# Grab last ID
	my $ID = DBLastInsertID("supplier_creditnotes","ID");

	return $ID;
}



# Link supplier credite note item
# Parameters:
#		Number				- Supplier credit note number
#		UnitPrice			- UnitPrice
# Optional:
#		InventoryCode		- Inventory code   - A: ONE OF THESE MUST BE SPECIFIED
#		GLAccountNumber		- GL account number  - A: ONE OF THESE MUST BE SPECIFIED
#
#		GLExpenseAccountNumber		- GL account number for stock qtys = 0 and cost is still > 0
#
#		SerialNumber		- Used by tracked inventory products, this is normally the serial number of the item. Or in any other case
#							can be used a a batch number, or anything as a matter of fact.
#		Description			- Description
#		Quantity			- Quantity, NOTE: Positive means that stock will NOT be deducted, Negative means stock WILL be deducted
sub linkSupplierCreditNoteItem
{
	my ($detail) = @_;

	my @extraCols = ();
	my @extraData = ();


	# Verify supplier credit note number
	if (!defined($detail->{'Number'}) || $detail->{'Number'} eq "") {
		setError("No supplier credit note number provided");
		return ERR_PARAM;
	}

	if (!defined($detail->{'UnitPrice'})) {
		setError("No UnitPrice provided");
		return ERR_PARAM;
	}

	# Grab credit note
	my $data;
	$data->{'Number'} = $detail->{'Number'};
	my $creditNote = getSupplierCreditNote($data);
	if (ref $creditNote ne "HASH") {
		setError(Error());
		return $creditNote;
	}


	#
	# Parse our args and get the vars we need
	#

	# Check unitprice is > 0
	my $unitPrice = Math::BigFloat->new($detail->{'UnitPrice'});
	$unitPrice->precision(-2);
	if ($unitPrice->is_neg() || $unitPrice->is_zero()) {
		setError("UnitPrice must be > 0");
		return ERR_PARAM;
	}

	# Get quantity
	my $quantity = Math::BigFloat->new();
	$quantity->precision(-4);
	if (defined($detail->{'Quantity'})) {
		$quantity->badd($detail->{'Quantity'});
	} else {
		$quantity->badd(1);
	}
	# Used for calculations
	my $absQuantity = $quantity->copy()->babs();

	# Total up final price
	my $totalPrice = $unitPrice->copy();
	$totalPrice->precision(-4);  # Increase precision for below calculations
	$totalPrice->bmul($absQuantity);

	#
	# Check if its a inventory item or a GL account
	#

	if (defined($detail->{'InventoryCode'}) && $detail->{'InventoryCode'} ne "") {
		# Check if inventory item exists
		my $inventoryID = wiaflos::server::core::Inventory::getInventoryItemIDFromCode($detail->{'InventoryCode'});
		if ($inventoryID < 1) {
			setError(wiaflos::server::core::Inventory::Error());
			return $inventoryID;
		}
		# Add to db
		push(@extraCols,"InventoryID");
		push(@extraData,DBQuote($inventoryID));

	} elsif (defined($detail->{'GLAccountNumber'}) && $detail->{'GLAccountNumber'} ne "") {
		# Check if GL account exists
		my $GLAccountID = wiaflos::server::core::GL::getGLAccountIDFromNumber($detail->{'GLAccountNumber'});
		if ($GLAccountID < 1) {
			setError(wiaflos::server::core::GL::Error());
			return $GLAccountID;
		}

		push(@extraCols,"GLAccountID");
		push(@extraData,DBQuote($GLAccountID));

	} else {
		setError("InventoryCode or GLAccountNumber must be provided");
		return ERR_USAGE;
	}

	# Check if we were given an expense account to use, this is used when the original expense is not matched by the
	# credit note.   qty = 0, price > 0 ... we then allocate this difference to this account
	if (defined($detail->{'GLExpenseAccountNumber'}) && $detail->{'GLExpenseAccountNumber'} ne "") {
		# Check if GL account exists
		my $GLExpenseAccountID = wiaflos::server::core::GL::getGLAccountIDFromNumber($detail->{'GLExpenseAccountNumber'});
		if ($GLExpenseAccountID < 1) {
			setError(wiaflos::server::core::GL::Error());
			return $GLExpenseAccountID;
		}

		# Check we an expense account
		my $catMatch = wiaflos::server::core::GL::checkGLAccountFinCat($GLExpenseAccountID,"E01");
		if ($catMatch == 0) {
			setError("Expense account '".$detail->{'GLExpenseAccountNumber'}."' is not of type expense");
			return ERR_USAGE;
		} elsif ($catMatch < 0) {
			setError(wiaflos::server::core::GL::Error());
			return $catMatch;
		}

		push(@extraCols,"GLExpenseAccountID");
		push(@extraData,DBQuote($GLExpenseAccountID));
	}

	# Pull in extra data
	my $extraCols = "";
	my $extraData = "";
	if (@extraCols > 0 && @extraData > 0) {
		$extraCols .= ',' . join(',',@extraCols);
		$extraData .= ',' . join(',',@extraData);
	}

	# Link in item
	my $sth = DBDo("
		INSERT INTO supplier_creditnote_items
			(SupplierCreditNoteID,SerialNumber,Description,Quantity,UnitPrice,Price$extraCols)
		VALUES
			(
				".DBQuote($creditNote->{'ID'}).",
				".DBQuote($detail->{'SerialNumber'}).",
				".DBQuote($detail->{'Description'}).",
				".DBQuote($quantity->bstr()).",
				".DBQuote($unitPrice->bstr()).",
				".DBQuote($totalPrice->bstr())."
				$extraData
			)
	");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	# Grab last ID
	my $ID = DBLastInsertID("supplier_creditnote_items","ID");

	return $ID;
}



# Post credit note
# Parameters:
#		Number	- Credit note number
sub postSupplierCreditNote
{
	my ($detail) = @_;

	my $data;


	# Grab invoice
	$data = undef;
	$data->{'Number'} = $detail->{'Number'};
	my $creditNote = getSupplierCreditNote($data);
	if (ref $creditNote ne "HASH") {
		setError(Error());
		return $creditNote;
	}
	# Check if we already posted or not
	if ($creditNote->{'GLTransactionID'}) {
		setError("Supplier credit note '".$creditNote->{'Number'}."' already posted");
		return ERR_POSTED;
	}

	# Grab supplier
	$data = undef;
	$data->{'ID'} = $creditNote->{'SupplierID'};
	my $supplier = wiaflos::server::core::Suppliers::getSupplier($data);
	if (ref $supplier ne "HASH") {
		setError(Error());
		return $supplier;
	}

	# Grab supplier invoice items
	$data = undef;
	$data->{'ID'} = $creditNote->{'SupplierInvoiceID'};
	my $supplierInvoiceItems =  wiaflos::server::core::Purchasing::getSupplierInvoiceItems($data);
	if (ref $supplierInvoiceItems ne "ARRAY") {
		setError(wiaflos::server::core::Purchasing::Error());
		return $supplierInvoiceItems;
	}


	# Return list of supplier credit note items
	my $sth = DBSelect("
		SELECT
			ID, SupplierCreditNoteID, InventoryID, GLAccountID, GLExpenseAccountID, Description, SerialNumber, Quantity,
			UnitPrice, Price
		FROM
			supplier_creditnote_items

		WHERE
			SupplierCreditNoteID = ".DBQuote($creditNote->{'ID'})."
	");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	# Pull in all items
	my @supplierCreditNoteItems;
	while (my $row = hashifyLCtoMC($sth->fetchrow_hashref(),
			qw( ID SupplierCreditNoteID InventoryID GLAccountID GLExpenseAccountID Description SerialNumber Quantity
				UnitPrice Price )
	)) {
		push(@supplierCreditNoteItems,$row);
	}

	DBFreeRes($sth);



	# These are the totals that will be posted to the GL
	my %GLTaxEntries;
	my %GLAssetEntries;
	my %GLExpenseEntries;
	my %GLOrigExpenseAdjustEntries;
	my %GLAssetAdjustEntries;
	my %GLExpenseAdjustEntries;
	my $crnTotal = Math::BigFloat->new();
	my $subTotal = Math::BigFloat->new();

	DBBegin();

	# Create transaction
	my $transactionRef = sprintf('Credit note: %s',$creditNote->{'Number'});
	$data = undef;
	$data->{'Date'} = $creditNote->{'IssueDate'};
	$data->{'Reference'} = $transactionRef;
	my $GLTransactionID = wiaflos::server::core::GL::createGLTransaction($data);
	if ($GLTransactionID < 1) {
		setError(wiaflos::server::core::GL::Error());
		DBRollback();
		return $GLTransactionID;
	}

	# Loop with each credit note item
	foreach my $crnItem (@supplierCreditNoteItems) {
		# Item info
		my $inventoryItem;
		my $GLAccount;
		# If its a GL account, get the GL account info
		if (defined($crnItem->{'GLAccountID'})) {
			# Build GL account request
			$data = undef;
			$data->{'AccountID'} = $crnItem->{'GLAccountID'};
			$GLAccount = wiaflos::server::core::GL::getGLAccount($data);
			if (ref $GLAccount ne "HASH") {
				setError(wiaflos::server::core::GL::Error());
				DBRollback();
				return $GLAccount;
			}
		# If its an inventory item, get the inventory item info
		} elsif (defined($crnItem->{'InventoryID'})) {
			# Build inventory item request
			$data = undef;
			$data->{'ID'} = $crnItem->{'InventoryID'};
			$inventoryItem = wiaflos::server::core::Inventory::getInventoryItem($data);
			if (ref $inventoryItem ne "HASH") {
				setError(wiaflos::server::core::Inventory::Error());
				DBRollback();
				return $inventoryItem;
			}
		}

		#
		# Get details from credit note
		#
		my $crnQuantity = Math::BigFloat->new();
		$crnQuantity->precision(-4);
		$crnQuantity->badd($crnItem->{'Quantity'});

		# Check if we're only doing a price adjustment or not
		my $onlyPriceAdjustment = 0;
		if ($crnQuantity->is_pos()) {
			$onlyPriceAdjustment = 1;
		}

		my $crnUnitPrice = Math::BigFloat->new($crnItem->{'UnitPrice'});
		my $crnTotalPrice = Math::BigFloat->new($crnItem->{'Price'});

		#
		# Find items on supplier invoice
		#

		# Check for either inventory item or GL account
		my @foundItems;
		if (defined($crnItem->{'InventoryID'})) {
			# Look for invoice item
			foreach my $sii (@{$supplierInvoiceItems}) {
				# Check if we match the inventory ID
				if (defined($sii->{'InventoryID'}) && $sii->{'InventoryID'} eq $crnItem->{'InventoryID'}) {
					# Make sure unit price doesn't exceed
					my $priceBal = Math::BigFloat->new($sii->{'UnitPrice'});
					$priceBal->bsub($crnItem->{'UnitPrice'});
					if ($priceBal->is_neg()) {
						next;
					}
					# Check if we have a serial number and if it matches
					if (defined($sii->{'SerialNumber'})) {
						# We can only match on serial number if we have been given one
						if (defined($crnItem->{'SerialNumber'}) && $crnItem->{'SerialNumber'} eq $sii->{'SerialNumber'}) {
							push(@foundItems,$sii);
						}
					# If this is a non-serialnumber item...
					} else {
						push(@foundItems,$sii);
					}
				}
			}
			# Check if we found it or not
			if (@foundItems < 1) {
				setError("Inventory item '".$inventoryItem->{'Code'}."' has no suitable matches on supplier invoice, check amounts and try again");
				DBRollback();
				return ERR_NOTFOUND;
			}


		# Its a GL account
		} elsif (defined($crnItem->{'GLAccountID'})) {
			# Look for invoice item
			foreach my $sii (@{$supplierInvoiceItems}) {
				# Check if we match the GL account ID
				if (defined($sii->{'GLAccountID'}) && $sii->{'GLAccountID'} eq $sii->{'GLAccountID'}) {
					# Make sure unit price doesn't exceed
					my $priceBal = Math::BigFloat->new($sii->{'UnitPrice'});
					$priceBal->bsub($crnItem->{'UnitPrice'});
					if ($priceBal->is_neg()) {
						next;
					}
					# Check if we have a serial number and if it matches
					if (defined($sii->{'SerialNumber'})) {
						# We can only match on serial number if we have been given one
						if (defined($crnItem->{'SerialNumber'}) && $crnItem->{'SerialNumber'} eq $sii->{'SerialNumber'}) {
							push(@foundItems,$sii);
						}
					# If this is a non-serialnumber item...
					} else {
						push(@foundItems,$sii);
					}
				}
			}
			# Check if we found it or not
			if (@foundItems < 1) {
				setError("GL account '".$GLAccount->{'Number'}."' not found on supplier invoice");
				DBRollback();
				return ERR_NOTFOUND;
			}


		} else {
			setError("InventoryID or GLAccountID not found");
			DBRollback();
			return ERR_UNKNOWN;
		}



		#
		# Check if the items we found are still available
		#
		my $crnItemQtyBalance = $crnQuantity->copy()->babs();
		my $crnItemPriceBalance = $crnTotalPrice->copy();

		my @allocationList;

		# Loop with found items...
		foreach my $fi (@foundItems) {
			# Grab tax type
			$data = undef;
			$data->{'TaxTypeID'} = $fi->{'TaxTypeID'};
			my $taxType = wiaflos::server::core::Tax::getTaxType($data);
			if (ref $taxType ne "HASH") {
				setError(wiaflos::server::core::Tax::Error());
				return $taxType;
			}

			# Get supplier invoice item tracking, this will tell us first hand if we have enough items
			# available for the credit. For GL account this is enough, for inventory we go one step further
			# and cross reference the inventoryItemTrackingID to inventory tracking.
			$data = undef;
			$data->{'ID'} = $fi->{'ID'};
			my $invoiceItemMovement = wiaflos::server::core::Purchasing::getSupplierInvoiceItemTracking($data);
			if (ref $invoiceItemMovement ne "ARRAY") {
				setError(wiaflos::server::core::Purchasing::Error());
				DBRollback();
				return $invoiceItemMovement;
			}

			# Setup balance...
			my $itemQtyBalance = Math::BigFloat->new();
			$itemQtyBalance->precision(-4);
			my $itemPriceBalance = Math::BigFloat->new();
			# Tracked item balances
			my @inventoryTracking;
			# Loop with inventory item movement
			foreach my $item (@{$invoiceItemMovement}) {
				$itemQtyBalance->badd($item->{'QtyChange'});
				$itemPriceBalance->badd($item->{'Price'});

				# Check if we have inventory item tracking information
				if (defined($item->{'InventoryTrackingID'})) {
					# This will only be set for an inventory item
					my $trackingQtyBalance = Math::BigFloat->new();
					$trackingQtyBalance->precision(-4);
					my $trackingPriceBalance = Math::BigFloat->new();
					# Build request
					$data = undef;
					$data->{'ID'} = $item->{'InventoryTrackingID'};
					my $trackingHistory = wiaflos::server::core::Inventory::getInventoryTrackingItemHistory($data);
					if (ref $trackingHistory ne "ARRAY") {
						setError(wiaflos::server::core::Inventory::Error());
						DBRollback();
						return $trackingHistory;
					}
					# Loop with inventory tracking history
					foreach my $iti (@{$trackingHistory}) {
						$trackingQtyBalance->badd($iti->{'QtyChange'});
						$trackingPriceBalance->badd($iti->{'Price'});
					}
					# Build list of available inventory items
					$data = undef;
					$data->{'InventoryTrackingID'} = $item->{'InventoryTrackingID'};
					$data->{'QtyBalance'} = $trackingQtyBalance;
					$data->{'Price'} = $trackingPriceBalance;
					push(@inventoryTracking,$data);
				}
			}

			# If supplier invoice item tracking balance is not positive, its fully allocated, continue
			if (!$itemQtyBalance->is_pos() || $itemQtyBalance->is_zero()) {
				next;
			}

			# Work out incl, excl & tax amount
			my $itemInclPrice = $crnUnitPrice->copy();
			my $itemExclPrice = $crnUnitPrice->copy();
			# Work out tax amount
			my ($taxAmount) = wiaflos::server::core::Tax::getTaxAmount($fi->{'TaxMode'},$fi->{'TaxRate'},$crnUnitPrice->bstr());
			# Adjust tax on item if its already including
			if ($fi->{'TaxMode'} eq "2") {
				$itemInclPrice->badd($taxAmount);
			} elsif ($fi->{'TaxMode'} eq "1") {
				$itemExclPrice->bsub($taxAmount);
			} else {
				setError("TaxMode '".$fi->{'TaxMode'}."' invalid");
				DBRollback();
				return ERR_UNKNOWN;
			}
			my $itemTaxAmount = Math::BigFloat->new($taxAmount);

			# This is a GL account
			if (defined($fi->{'GLAccountID'})) {
				my $bal = $crnItemQtyBalance->copy()->bsub($itemQtyBalance);

				my $allocateQty;
				my $allocatePrice;

				# We have enough items now
				if ($bal->is_zero() || $bal->is_neg()) {
					$allocateQty = $crnItemQtyBalance->copy();

				# We need more items
				} elsif ($bal->is_pos()) {
					$allocateQty = $itemQtyBalance->copy();
				}

				# Calculate balances
				$crnItemQtyBalance->bsub($allocateQty);
				$allocatePrice = $itemExclPrice->copy()->bmul($allocateQty);
				$crnItemPriceBalance->bsub($allocatePrice);
				my $totalTaxAmount = Math::BigFloat->new($itemTaxAmount)->bmul($allocateQty);
				# Tax hack, if the price is including tax, we need to equal out the item price balance
				if ($fi->{'TaxMode'} eq "1") {
					$crnItemPriceBalance->bsub($totalTaxAmount);
				}

				# Calc cost balance, we cannot just adjust the qty and leave the asset account with $$
				my $GLItemCostBalance = $itemPriceBalance->copy()->bsub($allocatePrice);
				# Check if qty = 0 and price balance > 0, in this case the difference must be posted to an expense account
				if (!$onlyPriceAdjustment && $itemQtyBalance->copy()->bsub($allocateQty)->is_zero()) {
					if ($GLItemCostBalance->is_pos() && !$GLItemCostBalance->is_zero()) {
						# Make sure we have an expense account
						if (defined($crnItem->{'GLExpenseAccountID'}) && $crnItem->{'GLExpenseAccountID'} > 0) {
							# Adjust the original expense account
							if (defined($GLOrigExpenseAdjustEntries{$GLAccount->{'ID'}})) {
								$GLOrigExpenseAdjustEntries{$GLAccount->{'ID'}}->badd($GLItemCostBalance);
							} else {
								$GLOrigExpenseAdjustEntries{$GLAccount->{'ID'}} = $GLItemCostBalance->copy();
							}
							# Add to expense account user provided
							if (defined($GLExpenseAdjustEntries{$crnItem->{'GLExpenseAccountID'}})) {
								$GLExpenseAdjustEntries{$crnItem->{'GLExpenseAccountID'}}->badd($GLItemCostBalance);
							} else {
								$GLExpenseAdjustEntries{$crnItem->{'GLExpenseAccountID'}} = $GLItemCostBalance->copy();
							}
						# ERR if user didn't give us an expense account
						} else {
							setError("Supplier credit note item 'GL/".$GLAccount->{'Number'}."' will bring supplier invoice qty to zero, ".
									"but there is a expense amount left of '".$GLItemCostBalance->bstr()."' and no ExpenseGLAccountNumber ".
									"was provided to allocate the difference to");
							DBRollback();
							return ERR_NOBALANCE;
						}
					}
				}
				# Check cost balance
				if ($GLItemCostBalance->is_neg()) {
					setError("Supplier credit note item 'GL/".$GLAccount->{'Number'}."' will bring supplier invoice qty to zero, ".
							"but it would also over allocate by '".$GLItemCostBalance->bstr()."' ");
					DBRollback();
					return ERR_NOBALANCE;
				}

				my $allocation;
				$allocation->{'SupplierInvoiceItem'} = $fi;
				$allocation->{'Qty'} = $allocateQty->copy();
				$allocation->{'Price'} = $allocatePrice;
				$allocation->{'TaxAmount'} = $totalTaxAmount;
				$allocation->{'TaxGLAccountID'} = $taxType->{'GLAccountID'};
				push(@allocationList,$allocation);

				# If we've hit target, last item
				last if ($crnItemQtyBalance->is_zero());

			#
			# Process Inventory item credit
			#
			} elsif (defined($fi->{'InventoryID'})) {
				# Loop with the tracking info we just found, and work out if we can use any of it
				foreach my $item (@inventoryTracking) {
					# Check if we actually have something
					if ($item->{'QtyBalance'}->is_pos() && !$item->{'QtyBalance'}->is_zero()) {
						my $bal = $crnItemQtyBalance->copy()->bsub($itemQtyBalance);

						my $allocateQty;
						my $allocatePrice;

						# We have enough items now
						if ($bal->is_zero() || $bal->is_neg()) {
							$allocateQty = $crnItemQtyBalance->copy();

						# We need more items
						} elsif ($bal->is_pos()) {
							$allocateQty = $itemQtyBalance->copy();
						}

						# Calculate balances
						$crnItemQtyBalance->bsub($allocateQty);
						$allocatePrice = $itemExclPrice->copy()->bmul($allocateQty);
						$crnItemPriceBalance->bsub($allocatePrice);
						my $totalTaxAmount = Math::BigFloat->new($itemTaxAmount)->bmul($allocateQty);
						# Tax hack, if the price is including tax, we need to equal out the item price balance
						if ($fi->{'TaxMode'} eq "1") {
							$crnItemPriceBalance->bsub($totalTaxAmount);
						}

						# Calc cost balance, we cannot just adjust the qty and leave the asset account with $$
						my $inventoryItemCostBalance = $item->{'Price'}->copy()->bsub($allocatePrice);
						# Check if we have a hanging cost of goods
						if (!$onlyPriceAdjustment && $item->{'QtyBalance'}->copy()->bsub($allocateQty)->is_zero()) {
							# Check balance
							if ($inventoryItemCostBalance->is_pos() && !$inventoryItemCostBalance->is_zero()) {
								if (defined($crnItem->{'GLExpenseAccountID'}) && $crnItem->{'GLExpenseAccountID'} > 0) {
									# Remove stock value from asset account
									if (defined($GLAssetAdjustEntries{$inventoryItem->{'GLAssetAccountID'}})) {
										$GLAssetAdjustEntries{$inventoryItem->{'GLAssetAccountID'}}->badd($inventoryItemCostBalance);
									} else {
										$GLAssetAdjustEntries{$inventoryItem->{'GLAssetAccountID'}} = $inventoryItemCostBalance->copy();
									}
									# Add to expense account
									if (defined($GLExpenseAdjustEntries{$crnItem->{'GLExpenseAccountID'}})) {
										$GLExpenseAdjustEntries{$crnItem->{'GLExpenseAccountID'}}->badd($inventoryItemCostBalance);
									} else {
										$GLExpenseAdjustEntries{$crnItem->{'GLExpenseAccountID'}} = $inventoryItemCostBalance->copy();
									}
								# ERR if user didn't give us an expense account
								} else {
									setError("Supplier credit note item '".$inventoryItem->{'Code'}."' will bring stock on hand to zero, ".
											"but the stock is still worth '".$inventoryItemCostBalance->bstr()."' and no ExpenseGLAccountNumber ".
											"was provided to allocate the difference to");
									DBRollback();
									return ERR_NOBALANCE;
								}
							}
						}
						# Cost left is negative, did we over-allocate?
						if ($inventoryItemCostBalance->is_neg()) {
							setError("Supplier credit note item '".$inventoryItem->{'Code'}."' will bring supplier invoice qty to zero, ".
									"but it would also over allocate by '".$inventoryItemCostBalance->bstr()."' ");
							DBRollback();
							return ERR_NOBALANCE;
						}

						my $allocation;
						$allocation->{'SupplierInvoiceItem'} = $fi;
						$allocation->{'InventoryTrackingID'} = $item->{'InventoryTrackingID'};
						$allocation->{'Qty'} = $allocateQty->copy();
						$allocation->{'Price'} = $allocatePrice;
						$allocation->{'TaxAmount'} = $totalTaxAmount;
						$allocation->{'TaxGLAccountID'} = $taxType->{'GLAccountID'};
						push(@allocationList,$allocation);

						# If we've hit target, last item
						last if ($crnItemQtyBalance->is_zero());
					}
				}
			}
		}

		# Check if we met our quantity balance
		if (!$crnItemQtyBalance->is_zero()) {
			setError("Credit note item quantity does not balance, off by '".$crnItemQtyBalance->bstr()."'");
			DBRollback();
			return ERR_NOTFOUND;
		}

		# Check if we met our price balance
		if (!$crnItemPriceBalance->is_zero()) {
			setError("Credit note item price does not balance, off by '".$crnItemPriceBalance->bstr()."'");
			DBRollback();
			return ERR_NOTFOUND;
		}

		# Loop with allocation list
		foreach my $allocationItem (@allocationList) {
			# Pull in tax GL account and addup what we need to post
			if (defined($GLTaxEntries{$allocationItem->{'TaxGLAccountID'}})) {
				$GLTaxEntries{$allocationItem->{'TaxGLAccountID'}}->badd($allocationItem->{'TaxAmount'});
			} else {
				$GLTaxEntries{$allocationItem->{'TaxGLAccountID'}} = $allocationItem->{'TaxAmount'}->copy();
			}

			# Pull in expense if its a GL account
			if (defined($allocationItem->{'SupplierInvoiceItem'}->{'GLAccountID'})) {
				if (defined($GLExpenseEntries{$crnItem->{'GLAccountID'}})) {
					$GLExpenseEntries{$crnItem->{'GLAccountID'}}->badd($allocationItem->{'Price'});
				} else {
					$GLExpenseEntries{$crnItem->{'GLAccountID'}} = $allocationItem->{'Price'}->copy();
				}
				# Adjust supplier invoice item
				$data = undef;
				$data->{'ID'} = $allocationItem->{'SupplierInvoiceItem'}->{'ID'};
				# Check if we not only a price adjustment
				if (!$onlyPriceAdjustment) {
					$data->{'QtyChange'} = $allocationItem->{'Qty'}->copy()->bneg()->bstr();
				} else {
					$data->{'QtyChange'} = 0;
				}
				$data->{'Price'} = $allocationItem->{'Price'}->copy()->bneg()->bstr();
				my $itemEntryID = wiaflos::server::core::Purchasing::addSupplierInvoiceItemEntry($data);
				if ($itemEntryID < 1) {
					setError(wiaflos::server::core::Purchasing::Error());
					return $itemEntryID;
				}

			# Pull in stock control if its an inventory item
			} elsif (defined($allocationItem->{'SupplierInvoiceItem'}->{'InventoryID'})) {
				# Check if we have a asset account first of all
				if (defined($inventoryItem->{'GLAssetAccountID'})) {
					if (defined($GLAssetEntries{$inventoryItem->{'GLAssetAccountID'}})) {
						$GLAssetEntries{$inventoryItem->{'GLAssetAccountID'}}->badd($allocationItem->{'Price'});
					} else {
						$GLAssetEntries{$inventoryItem->{'GLAssetAccountID'}} = $allocationItem->{'Price'}->copy();
					}
				}

				# Calculate adjustment quantity
				my $quantity = $allocationItem->{'Qty'}->copy()->bneg();
				# If its a stock reduction
				if ($quantity->is_neg()) {

					# Adjust stock
					$data = undef;
					$data->{'ID'} = $allocationItem->{'SupplierInvoiceItem'}->{'InventoryID'};
					$data->{'GLTransactionID'} = $GLTransactionID;
					# Check if we not only a price adjustment
					if (!$onlyPriceAdjustment) {
						# Setup stock adjustment list
						my @stockAdjustmentList;
						my $stockItem;
						$stockItem->{'InventoryTrackingID'} = $allocationItem->{'InventoryTrackingID'};
						$stockItem->{'Quantity'} = $quantity->copy()->babs()->bstr();
						$stockItem->{'Cost'} = $allocationItem->{'Price'}->copy()->bneg()->bstr();
						push(@stockAdjustmentList,$stockItem);
						# Create params
						$data->{'QtyChange'} = $quantity->bstr();
						$data->{'StockAdjustmentList'} = \@stockAdjustmentList;
					} else {
						$data->{'ParentInventoryTrackingID'} = $allocationItem->{'InventoryTrackingID'};
						$data->{'QtyChange'} = 0;
						$data->{'Cost'} = $allocationItem->{'Price'}->copy()->bneg()->bstr();
					}
					$data->{'SerialNumber'} = $allocationItem->{'SupplierInvoiceItem'}->{'SerialNumber'};
					my $adjustmentTracking = wiaflos::server::core::Inventory::adjustInventoryStock($data);
					if (ref $adjustmentTracking ne "ARRAY") {
						setError(wiaflos::server::core::Inventory::Error());
						DBRollback();
						return $adjustmentTracking;
					}
					# Loop with adjustment tracking items
					foreach my $item (@{$adjustmentTracking}) {
						# Adjust supplier invoice item
						$data = undef;
						$data->{'ID'} = $allocationItem->{'SupplierInvoiceItem'}->{'ID'};
						# Check if we not only a price adjustment
						if (!$onlyPriceAdjustment) {
							$data->{'QtyChange'} = $item->{'Qty'}->copy()->bneg()->bstr();
						} else {
							$data->{'QtyChange'} = 0;
						}
						$data->{'Price'} = $item->{'Price'};
						$data->{'InventoryTrackingID'} = $item->{'InventoryTrackingID'};
						my $itemEntryID = wiaflos::server::core::Purchasing::addSupplierInvoiceItemEntry($data);
						if ($itemEntryID < 1) {
							setError(wiaflos::server::core::Purchasing::Error());
							return $itemEntryID;
						}
					}
				}

			}

			# Check if we've overridden the description
			my $description = defined($crnItem->{'Description'}) ? $crnItem->{'Description'} : $allocationItem->{'SupplierInvoiceItem'}->{'Description'};

			# Create linking item on credit note
			$sth = DBDo("
				INSERT INTO supplier_creditnote_item_linking
					(SupplierCreditNoteItemID,SupplierInvoiceItemID,Description,TaxTypeID,TaxMode,TaxRate,TaxAmount)
				VALUES
					(
						".DBQuote($crnItem->{'ID'}).",
						".DBQuote($allocationItem->{'SupplierInvoiceItem'}->{'ID'}).",
						".DBQuote($description).",
						".DBQuote($allocationItem->{'SupplierInvoiceItem'}->{'TaxTypeID'}).",
						".DBQuote($allocationItem->{'SupplierInvoiceItem'}->{'TaxMode'}).",
						".DBQuote($allocationItem->{'SupplierInvoiceItem'}->{'TaxRate'}).",
						".DBQuote($allocationItem->{'TaxAmount'})."
					)
			");
			if (!$sth) {
				setError(awitpt::db::dblayer::Error());
				DBRollback();
				return ERR_DB;
			}

			# Addup crn totals
			$subTotal->badd($allocationItem->{'Price'});
			# Add total tax to cn grand total
			$crnTotal->badd($allocationItem->{'Price'});
			$crnTotal->badd($allocationItem->{'TaxAmount'});
		}

#			# FIXME: Postivie vs. Negative stock adjustment
#			# FIXME: MODIFY COGS IF ITEM IS ALREADY SOLD!!

	}  # crnItem loop

	# Link credit note total to suppliers GL account
	$data = undef;
	$data->{'ID'} = $GLTransactionID;
	$data->{'GLAccountID'} = $supplier->{'GLAccountID'};
	$data->{'Amount'} = $crnTotal->bstr();
	if ((my $res = wiaflos::server::core::GL::linkGLTransaction($data)) < 1) {
		setError(wiaflos::server::core::GL::Error());
		DBRollback();
		return $res;
	}

	# Link in tax to GL
	foreach my $entry (keys %GLTaxEntries) {
		$data->{'GLAccountID'} = $entry;
		$data->{'Amount'} = $GLTaxEntries{$entry}->copy()->bneg()->bstr();
		$data->{'Reference'} = sprintf("Reverse Tax: %s",$creditNote->{'Number'});
		if ((my $res = wiaflos::server::core::GL::linkGLTransaction($data)) < 1) {
			setError(wiaflos::server::core::GL::Error());
			DBRollback();
			return $res;
		}
	}

	# Link in inventory to GL
	foreach my $entry (keys %GLAssetEntries) {
		$data->{'GLAccountID'} = $entry;
		$data->{'Amount'} = $GLAssetEntries{$entry}->copy()->bneg()->bstr();
		$data->{'Reference'} = sprintf("Reverse stock control: %s",$creditNote->{'Number'});
		if ((my $res = wiaflos::server::core::GL::linkGLTransaction($data)) < 1) {
			setError(wiaflos::server::core::GL::Error());
			DBRollback();
			return $res;
		}
	}

	# Link in expenses
	foreach my $entry (keys %GLExpenseEntries) {
		$data->{'GLAccountID'} = $entry;
		$data->{'Amount'} = $GLExpenseEntries{$entry}->copy()->bneg()->bstr();
		$data->{'Reference'} = sprintf("Reverse expense: %s",$creditNote->{'Number'});
		if ((my $res = wiaflos::server::core::GL::linkGLTransaction($data)) < 1) {
			setError(wiaflos::server::core::GL::Error());
			DBRollback();
			return $res;
		}
	}

	# Link in stock asset adjustments
	foreach my $entry (keys %GLOrigExpenseAdjustEntries) {
		$data->{'GLAccountID'} = $entry;
		$data->{'Amount'} = $GLOrigExpenseAdjustEntries{$entry}->copy()->bneg()->bstr();
		$data->{'Reference'} = sprintf("Original expense adjustment: %s",$creditNote->{'Number'});
		if ((my $res = wiaflos::server::core::GL::linkGLTransaction($data)) < 1) {
			setError(wiaflos::server::core::GL::Error());
			DBRollback();
			return $res;
		}
	}

	# Link in stock asset adjustments
	foreach my $entry (keys %GLAssetAdjustEntries) {
		$data->{'GLAccountID'} = $entry;
		$data->{'Amount'} = $GLAssetAdjustEntries{$entry}->copy()->bneg()->bstr();
		$data->{'Reference'} = sprintf("Stock control adjustment: %s",$creditNote->{'Number'});
		if ((my $res = wiaflos::server::core::GL::linkGLTransaction($data)) < 1) {
			setError(wiaflos::server::core::GL::Error());
			DBRollback();
			return $res;
		}
	}

	# Link in stock expense adjustments
	foreach my $entry (keys %GLExpenseAdjustEntries) {
		$data->{'GLAccountID'} = $entry;
		$data->{'Amount'} = $GLExpenseAdjustEntries{$entry}->bstr();
		$data->{'Reference'} = sprintf("Expense adjustment: %s",$creditNote->{'Number'});
		if ((my $res = wiaflos::server::core::GL::linkGLTransaction($data)) < 1) {
			setError(wiaflos::server::core::GL::Error());
			DBRollback();
			return $res;
		}
	}

	# Update supplier credit note
	my $taxTotal = $crnTotal->copy();
	$taxTotal->bsub($subTotal);
	$sth = DBDo("
		UPDATE supplier_creditnotes
			SET
				GLTransactionID = ".DBQuote($GLTransactionID).",
				SubTotal = ".DBQuote($subTotal->bstr()).",
				TaxTotal = ".DBQuote($taxTotal->bstr()).",
				Total = ".DBQuote($crnTotal->bstr())."
			WHERE
				ID = ".DBQuote($creditNote->{'ID'})."
	");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		DBRollback();
		return ERR_DB;
	}

	# Get supplier invoice transaction balance
	$data = undef;
	$data->{'ID'} = $creditNote->{'SupplierInvoiceID'};
	my $res = wiaflos::server::core::Purchasing::getSupplierInvoiceTransactionBalance($data);
	if (ref $res ne "HASH") {
		setError(wiaflos::server::core::Purchasing::Error());
		DBRollback();
		return $res;
	}
	my $invBalance = Math::BigFloat->new($res->{'Balance'});

	# b0rk out ... we cannot be neg
	if ($invBalance->is_neg()) {
		setError("Invoice balance cannot be negative");
		DBRollback();
		return ERR_UNKNOWN;
	}

	# If invoice balance is not zero, its positive
	if (!$invBalance->is_zero()) {
		my $allocAmount = $crnTotal->copy()->bneg()->bstr();  # Negate balance

		# Create supplier invoice transaction
		$data = undef;
		$data->{'ID'} = $creditNote->{'SupplierInvoiceID'};
		$data->{'Amount'} = $allocAmount;
		$data->{'SupplierCreditNoteID'} = $creditNote->{'ID'};
		my $supplierInvoiceTransactionID = wiaflos::server::core::Purchasing::allocateSupplierInvoiceTransaction($data);
		if ($supplierInvoiceTransactionID < 1) {
			setError(wiaflos::server::core::Purchasing::Error());
			DBRollback();
			return $supplierInvoiceTransactionID;
		}
		# Create supplier credit note transaction
		$data = undef;
		$data->{'ID'} = $creditNote->{'ID'};
		$data->{'Amount'} = $allocAmount;
		$data->{'SupplierInvoiceTransactionID'} = $supplierInvoiceTransactionID;
		my $supplierCreditNoteTransactionID = allocateSupplierCreditNoteTransaction($data);
		if ($supplierCreditNoteTransactionID < 1) {
			setError(Error());
			DBRollback();
			return $supplierCreditNoteTransactionID;
		}

	}

	# Post transaction
	$data = undef;
	$data->{'ID'} = $GLTransactionID;
	if ((my $res = wiaflos::server::core::GL::postGLTransaction($data)) != 0) {
		setError(wiaflos::server::core::GL::Error());
		DBRollback();
		return $res;
	}

	DBCommit();

	return RES_OK;
}



# Return an array of credit note transactions
# Parameters:
#		ID	- Supplier credit note ID
# 		Number - Supplier credit note number  (optional instead of ID)
# Optional:
# 		Mode - Can be specified as "resolve" to add ALL info to the results.
sub getSupplierCreditNoteTransactions
{
	my ($detail) = @_;

	my @allocations = ();


	# Grab credit note
	my $data;
	$data->{'ID'} = $detail->{'ID'};
	$data->{'Number'} = $detail->{'Number'};
	my $creditNote = getSupplierCreditNote($data);
	if (ref $creditNote ne "HASH") {
		setError(Error());
		return $creditNote;
	}

	# Return list of supplier invoice transactions
	my $sth = DBSelect("
		SELECT
			ID, Amount, SupplierInvoiceTransactionID, SupplierReceiptAllocationID
		FROM
			supplier_creditnote_transactions
		WHERE
			SupplierCreditNoteID = ".DBQuote($creditNote->{'ID'})."
	");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	# Fetch rows
	while (my $row = hashifyLCtoMC($sth->fetchrow_hashref(),
			qw( ID Amount SupplierInvoiceTransactionID SupplierReceiptAllocationID )
	)) {
		my $item;

		$item->{'ID'} = $row->{'ID'};

		$item->{'Amount'} = $row->{'Amount'};

		# If its a receipt allocation, pull in all the details
		if (defined($row->{'SupplierReceiptAllocationID'})) {
			$item->{'SupplierReceiptAllocationID'} = $row->{'SupplierReceiptAllocationID'};

			# Check if we must resolve all the ID links or not
			if (defined($detail->{'Mode'}) && $detail->{'Mode'} eq "resolve_all") {
				# Pull in receipt allocation
				$data = undef;
				$data->{'ID'} = $item->{'SupplierReceiptAllocationID'};
				$item->{'SupplierReceiptAllocation'} = wiaflos::server::core::SupplierReceipts::getSupplierReceiptAllocation($data);
				if (ref $item->{'SupplierReceiptAllocation'} ne "HASH") {
					setError(wiaflos::server::core::SupplierReceipts::Error());
					return $item->{'SupplierReceiptAllocation'};
				}

				# Pull in receipt
				$data = undef;
				$data->{'ID'} = $item->{'SupplierReceiptAllocation'}->{'SupplierReceiptID'};
				$item->{'SupplierReceipt'} = wiaflos::server::core::SupplierReceipts::getSupplierReceipt($data);
				if (ref $item->{'SupplierReceipt'} ne "HASH") {
					setError(wiaflos::server::core::SupplierReceipts::Error());
					return $item->{'SupplierReceipt'};
				}
			}

		# If its a credit note pull in all the details for that
		} elsif (defined($row->{'SupplierInvoiceTransactionID'})) {
			$item->{'SupplierInvoiceTransactionID'} = $row->{'SupplierInvoiceTransactionID'};

			# Check if we must resolve all the ID links or not
			if (defined($detail->{'Mode'}) && $detail->{'Mode'} eq "resolve_all") {
				# Pull in supplier invoice transaction
				$data = undef;
				$data->{'ID'} = $item->{'SupplierInvoiceTransactionID'};
				$item->{'SupplierInvoiceTransaction'} = wiaflos::server::core::Purchasing::getSupplierInvoiceTransaction($data);
				if (ref $item->{'SupplierInvoiceTransaction'} ne "HASH") {
					setError(wiaflos::server::core::Purchasing::Error());
					return $item->{'SupplierInvoiceTransaction'};
				}
				# Pull in supplier invoice
				$data = undef;
				$data->{'ID'} = $item->{'SupplierInvoiceTransaction'}->{'SupplierInvoiceID'};
				$item->{'SupplierInvoice'} = wiaflos::server::core::Purchasing::getSupplierInvoice($data);
				if (ref $item->{'SupplierInvoice'} ne "HASH") {
					setError(wiaflos::server::core::Purchasing::Error());
					return $item->{'SupplierInvoice'};
				}
			}
		}


		push(@allocations,$item);
	}

	DBFreeRes($sth);

	return \@allocations;
}


# Function to retrun current credit note transaction balance
# Parameters:
# 		ID	- Supplier credit note ID
# 		Number - or; optionally the number
sub getSupplierCreditNoteTransactionBalance
{
	my $detail = shift;


	# Grab credit note
	my $data;
	$data->{'ID'} = $detail->{'ID'};
	$data->{'Number'} = $detail->{'Number'};
	my $creditNote = getSupplierCreditNote($data);
	if (ref $creditNote ne "HASH") {
		setError(Error());
		return $creditNote;
	}

	# Grab transactions
	$data = undef;
	$data->{'ID'} = $creditNote->{'ID'};
	my $transactions = getSupplierCreditNoteTransactions($data);
	if (ref $transactions ne "ARRAY") {
		setError(Error());
		return $transactions;
	}

	# Check the invoice total vs. the invoice transactions
	my $crnBalance = Math::BigFloat->new($creditNote->{'Total'});
	foreach my $trans (@{$transactions}) {
		$crnBalance->badd($trans->{'Amount'});
	}

	# Hash up and return
	my $ret;
	$ret->{'Balance'} = $crnBalance->bstr();
	return $ret;
}



# Backend function to create supplier credit note transaction
# Parameters:
#		ID	- Credit note ID, Option A
#		Number	- Credit note number, Option A
#
#		Amount		- Amount, payment = -xxxx, credit note = -xxxx, debit note = +xxxx
sub allocateSupplierCreditNoteTransaction
{
	my ($detail) = @_;


	my @extraCols = ();
	my @extraData = ();


	# Grab invoice
	my $data;
	$data->{'ID'} = $detail->{'ID'};
	$data->{'Number'} = $detail->{'Number'};
	my $creditNote = getSupplierCreditNote($data);
	if (ref $creditNote ne "HASH") {
		setError(Error());
		return $creditNote;
	}

	# We cannot allocate a transaction to a closed credit note
	if ($creditNote->{'Closed'} eq "1") {
		setError("Attempt to allocate a transaction to supplier creditenote '".$creditNote->{'Number'}."' which has already been closed");
		return ERR_PAID;
	}

	$data = undef;
	$data->{'ID'} = $creditNote->{'ID'};
	my $res = getSupplierCreditNoteTransactionBalance($data);
	if (ref $res ne "HASH") {
		setError(Error());
		return $res;
	}
	my $crnBalance = Math::BigFloat->new($res->{'Balance'});

	# And the current transaction
	$crnBalance->badd($detail->{'Amount'});

	# Make sure we're not overallocating the credit note
	if ($crnBalance->is_neg()) {
		setError("Cannot allocate transaction against supplier credit note '".$creditNote->{'Number'}.
				"' as it will cause a negative balance of '".$crnBalance->bstr()."'");
	}

	DBBegin();


	# If credit note balances out, credit note is now closed
	if ($crnBalance->is_zero()) {
		my $sth = DBDo("
			UPDATE supplier_creditnotes
			SET
				Closed = 1
			WHERE
				ID = ".DBQuote($creditNote->{'ID'})."
		");
		if (!$sth) {
			setError(awitpt::db::dblayer::Error());
			DBRollback();
			return ERR_DB;
		}
	}

	# Check if this is a credit note
	if ($detail->{'SupplierInvoiceTransactionID'}) {
		push(@extraCols,'SupplierInvoiceTransactionID');
		push(@extraData,DBQuote($detail->{'SupplierInvoiceTransactionID'}));

	# Its a receipt allocation
	} elsif ($detail->{'SupplierReceiptAllocationID'}) {
		push(@extraCols,'SupplierReceiptAllocationID');
		push(@extraData,DBQuote($detail->{'SupplierReceiptAllocationID'}));
	}

	# Pull in extra data
	my $extraCols = "";
	my $extraData = "";
	if (@extraCols > 0 && @extraData > 0) {
		$extraCols .= ',' . join(',',@extraCols);
		$extraData .= ',' . join(',',@extraData);
	}

	# Add invoice trnsaction
	my $sth = DBDo("
		INSERT INTO supplier_creditnote_transactions
				(SupplierCreditNoteID,Amount$extraCols)
			VALUES
				(
					".DBQuote($creditNote->{'ID'}).",
					".DBQuote($detail->{'Amount'})."
					$extraData
				)
	");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		DBRollback();
		return ERR_DB;
	}

	# Grab last ID
	my $ID = DBLastInsertID("supplier_creditnote_transactions","ID");

	DBCommit();

	return $ID;
}




1;
# vim: ts=4
