# Statement functions
# Copyright (C) 2009-2014, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.




package wiaflos::server::core::Statements;

use strict;
use warnings;


use wiaflos::version;
use wiaflos::constants;
use wiaflos::server::core::config;
use awitpt::db::dblayer;
use wiaflos::server::core::templating;
use wiaflos::server::core::Clients;
use wiaflos::server::core::GL;

use Math::BigFloat;




# Our current error message
my $error = "";

# Set current error message
# Args: error_message
sub setError
{
	my $err = shift;
	my ($package,$filename,$line) = caller;
	my (undef,undef,undef,$subroutine) = caller(1);

	# Set error
	$error = "$subroutine($line): $err";
}

# Return current error message
# Args: none
sub Error
{
	my $err = $error;

	# Reset error
	$error = "";

	# Return error
	return $err;
}


## @fn getStatement
# Function to return a client account statement
#
# @param detail Parameter hash ref
# @li ClientCode Client code
# @li StartDate Optional statement start date
# @li EndDate Optional statement end date
#
# @returns Hash ref of account transactions
# @li ID Entry ID
# @li GLTransactionID GL account transaction ID
# @li Reference Entry reference
# @li Amount Amount
# @li TransactionDate Transaction date
# @li TransactionReference Transaction reference
sub getStatement
{
	my ($detail) = @_;
	my @entries = ();


	# Verify client
	if (!defined($detail->{'ClientCode'}) || $detail->{'ClientCode'} eq "") {
		setError("No client code provided");
		return ERR_PARAM;
	}

	# Check if client exists & pull
	my $tmp;
	$tmp->{'Code'} = $detail->{'ClientCode'};
	my $client = wiaflos::server::core::Clients::getClient($tmp);
	if (ref $client ne "HASH") {
		setError(wiaflos::server::core::Clients::Error());
		return $client;
	}

	$tmp = undef;
	$tmp->{'AccountID'} = $client->{'GLAccountID'};
	$tmp->{'StartDate'} = $detail->{'StartDate'};
	$tmp->{'EndDate'} = $detail->{'EndDate'};
	$tmp->{'BalanceBroughtForward'} = 1;
	my $res = wiaflos::server::core::GL::getGLAccountEntries($tmp);

	# Fetch items
	foreach my $item (@{$res}) {
		my $entry;

		$entry->{'ID'} = $item->{'ID'};
		$entry->{'GLTransactionID'} = $item->{'GLTransactionID'};
		$entry->{'Reference'} = $item->{'Reference'};
		$entry->{'Amount'} = $item->{'Amount'};

		$entry->{'TransactionDate'} = $item->{'TransactionDate'};
		$entry->{'TransactionReference'} = $item->{'TransactionReference'};

		push(@entries,$entry);
	}

	# Sort results
	@entries = sort { $a->{'TransactionDate'} cmp $b->{'TransactionDate'} } @entries;

	return \@entries;
}



# Send statement
# Parameters:
#		ClientCode	- Client code
#		StartDate	- Start date
#		SendTo		- Send to,  email:  ,  file:  , return
# Optional:
#		Subject		- Subject of the statement
sub sendStatement
{
	my ($detail) = @_;


	# Verify SendTo
	if (!defined($detail->{'SendTo'}) || $detail->{'SendTo'} eq "") {
		setError("SendTo was not provided");
		return ERR_PARAM;
	}

	# Grab items
	my $data;
	$data->{'ClientCode'} = $detail->{'ClientCode'};
	$data->{'StartDate'} = $detail->{'StartDate'};
	my $entries = getStatement($data);
	if (ref $entries ne "ARRAY") {
		setError(Error());
		return $entries;
	}

	# Pull in config
	my $config = wiaflos::server::core::config::getConfig();

	# Pull client
	$data = undef;
	$data->{'Code'} = $detail->{'ClientCode'};
	my $client = wiaflos::server::core::Clients::getClient($data);
	if (ref $client ne "HASH") {
		setError(wiaflos::server::core::Clients::Error());
		return $client;
	}

	# Check if subject was overridden
	my $subject = (defined($detail->{'Subject'}) && $detail->{'Subject'} ne "") ? $detail->{'Subject'} : "Statement: ".$client->{'Code'};

	# Setup request data
	$data = undef;
	$data->{'ID'} = $client->{'ID'};

	# Pull in addresses
	my $addresses = wiaflos::server::core::Clients::getClientAddresses($data);
	if (ref $addresses ne "ARRAY") {
		setError(wiaflos::server::core::Clients::Error());
		return $addresses;
	}
	# Pull in email addresses
	my $emailAddies = wiaflos::server::core::Clients::getClientEmailAddresses($data);
	if (ref $emailAddies ne "ARRAY") {
		setError(wiaflos::server::core::Clients::Error());
		return $emailAddies;
	}

	# Parse in addresses
	my $billAddr;
	my $shipAddr = "";
	# Look for billing address
	foreach my $address (@{$addresses}) {
		if ($address->{'Type'} eq "billing") {
			$billAddr = $address->{'Address'};
		} elsif ($address->{'Type'} eq "shipping") {
			$shipAddr = $address->{'Address'};
		}
	}
	# If no billing address, use shipping address
	$billAddr = defined($billAddr) ? $billAddr : $shipAddr;
	$billAddr =~ s/\|/<br \/>/g;
	# Setup shipping address
	$shipAddr =~ s/\|/<br \/>/g;

	# Parse in email addresses
	my $billEmailAddr;
	my @billEmailAddrs;
	my @genEmailAddrs;
	# Look for accounts address
	foreach my $address (@{$emailAddies}) {
		if ($address->{'Type'} eq "accounts") {
			push(@billEmailAddrs,$address->{'Address'});
		} elsif ($address->{'Type'} eq "general") {
			push(@genEmailAddrs,$address->{'Address'});
		}
	}
	# If no accounts address, use general address
	$billEmailAddr = @billEmailAddrs > 0 ? join(',',@billEmailAddrs) : join(',',@genEmailAddrs);

	# Build array of stuff we can use
	my $vars = {
		'WiaflosString' => $GENSTRING,

		# Client
		'ClientName' => $client->{'Name'},
		'ClientCode' => $client->{'Code'},
		'ClientBillingAddress' => $billAddr,
		'ClientShippingAddress' => $shipAddr,

		# Statement
		'StatementDate' => DateTime->from_epoch( epoch => time() )->ymd(),
		# FIXME - not implemented yet
		'StatementNote' => "",
	};

	# Tally up a running balance
	my $runningBalance = Math::BigFloat->new();
	$runningBalance->precision(-2);

	# Load invoice line items
	foreach my $item (@{$entries}) {
		my $titem;

		# Keep balance
		$runningBalance->badd($item->{'Amount'});

		# Fix some stuff up
		$titem->{'TransactionDescription'} = defined($item->{'Reference'}) ? $item->{'Reference'} : $item->{'TransactionReference'};
		$titem->{'TransactionDate'} = $item->{'TransactionDate'};
		$titem->{'TransactionAmount'} = sprintf('%.2f',$item->{'Amount'});
		$titem->{'StatementBalance'} = sprintf('%.2f',$runningBalance->bstr());

		# Various fixups & amount sanitizations
		push(@{$vars->{'LineItems'}},$titem);
	}

	# Get statement balance
	my $statementBalance = Math::BigFloat->new();
	$statementBalance->precision(-2);
	foreach my $item (@{$entries}) {
		$statementBalance->badd($item->{'Amount'});
	}
	$vars->{'StatementBalance'} = $statementBalance->bstr();

	# Get statement template file
	my $template = defined($config->{'statements'}{'email_template'}) ? $config->{'statements'}{'email_template'} : "statements/statement1.tt2";

	# Check where statement must go
	if ($detail->{'SendTo'} =~ /^file:(\S+)/i) {
		my $filename = $1;

		# Load template
		my $res = loadTemplate($template,$vars,$filename);
		if (!$res) {
			setError("Failed to load template '$template': ".wiaflos::server::core::templating::Error());
			return ERR_SRVTEMPLATE;
		}

	# Write out using email
	} elsif ($detail->{'SendTo'} =~ /^email(?:\:(\S+))?/i) {
		# Pull email address user specified if its defined and not blank, or use billing email address
		my $emailAddy = (defined($1) && $1 ne "") ? $1 : $billEmailAddr;

		# Verify SMTP server is set
		my $server = $config->{'mail'}{'server'};
		if (!defined($server) || $server eq "") {
			setError("Cannot use statement emailing if we do not have an SMTP server defined");
			return ERR_SRVPARAM;
		}

		# Check if we have a email addy
		if ($emailAddy eq "") {
			setError("No email address defined to send statement to");
			return ERR_PARAM;
		}

		# Statement filename
		(my $statementFilename = "statement.html") =~ s,/,-,g;
		# Statement signature filename
		my $stmtSignFilename = $statementFilename . ".asc";


		# If we must, pull in email body
		my $message_template = $config->{'statements'}{'email_message_template'};
		my $emailBody = "";
		if (defined($message_template) && $message_template ne "") {
			# Variables for our template
			my $vars2 = {
				'StatementFilename' => $statementFilename,
				'StatementSignatureFilename' => $stmtSignFilename,
			};
			# Load template
			my $res = loadTemplate($message_template,$vars2,\$emailBody);
			if (!$res) {
				setError("Failed to load template '$message_template': ".wiaflos::server::core::templating::Error());
				return ERR_SRVTEMPLATE;
			}

			$emailBody =~ s/(?<!\r)\n/\r\n/sg; # Sanitize eol for crypt-gpg
		}

		# This is our entire statement
		my $statementData = "";
		my $res = loadTemplate($template,$vars,\$statementData);
		if (!$res) {
			setError("Failed to load template '$template': ".wiaflos::server::core::templating::Error());
			return ERR_SRVTEMPLATE;
		}
		$statementData =~ s/(?<!\r)\n/\r\n/sg; # Sanitize eol, needed to fix bug in crypt-gpg where it mangles \n

		# See if we must use GPG
		my $use_gpg_key = $config->{'statements'}{'use_gpg_key'};
		my $sign;
		if (defined($use_gpg_key) && $use_gpg_key ne "") {
			# Setup GnuPG
			my $gpg = new Crypt::GPG;
			$gpg->gpgbin('/usr/bin/gpg');
			$gpg->secretkey($use_gpg_key);
			$gpg->armor(1);
			$gpg->comment("$APPNAME v$VERSION ($APPURL)");
			# Sign statement
			$sign = $gpg->sign($statementData);
			if (!defined($sign)) {
				setError("Failed to sign statement");
				return ERR_SRVEXEC;
			}
		}

		# Create message
		my $msg = MIME::Lite->new(
				From	=> $config->{'statements'}{'email_from'},
				To		=> $emailAddy,
				Bcc		=> $config->{'statements'}{'email_bcc'},
				Subject	=> $subject,
				Type	=> 'multipart/mixed'
		);

		# Attach body
		$msg->attach(
				Type	=> 'TEXT',
				Encoding 	=> '8bit',
				Data	=> $emailBody,
		);

		# Attach statement
		$msg->attach(
				Type	=> 'text/html',
				Encoding	=> 'base64',
				Data	=> $statementData,
				Disposition	=> 'attachment',
				Filename	=> $statementFilename
		);

		# If we have signature, sign statement
		if (defined($sign)) {
			# Attach signature
			$msg->attach(
					Type	=> 'text/plain',
					Encoding	=> 'base64',
					Data	=> $sign,
					Disposition	=> 'attachment',
					Filename	=> $stmtSignFilename
			);
		}

		# Send email
		my @SMTPParams;
		if (!(my $res = $msg->send("smtp",$server))) {
			setError("Failed to send statement via email server '$server'");
			return ERR_SRVEXEC;
		}

	} else {
		setError("Invalid SendTo method provided");
		return ERR_PARAM;
	}


	return RES_OK;
}





1;
# vim: ts=4
