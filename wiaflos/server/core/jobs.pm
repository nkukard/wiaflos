# Job scheduling and control
# Copyright (C) 2009-2014, AllWorldIT
# Copyright (C) 2009, LinuxRulz
# Copyright (C) 2005-2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.




package wiaflos::server::core::jobs;

use strict;

# Exporter stuff
require Exporter;
our (@ISA,@EXPORT);
@ISA = qw(Exporter);
@EXPORT = qw(
);

use wiaflos::constants;
use awitpt::db::dblayer;
use awitpt::db::dbilayer;
use awitpt::cache;
use wiaflos::server::core::logging;
use wiaflos::server::core::system qw(OpenMax);



# Our current error message
my $error = "";

# Set current error message
# Args: error_message
sub setError
{
	my $err = shift;
	my ($package,$filename,$line) = caller;
	my (undef,undef,undef,$subroutine) = caller(1);

	# Set error
	$error = "$subroutine($line): $err";
}

# Return current error message
# Args: none
sub Error
{
	my $err = $error;

	# Reset error
	$error = "";

	# Return error
	return $err;
}



# This is the server
my $serverHandle = undef;
# Jobs we currently handling
my %jobs;  # FIXME - this must be a shared variable




# Initialize
sub Init
{
	my $server = shift;


	$serverHandle = $server;

	_initJob();
}


# Finish off job control
sub Finish
{
	my $server = $serverHandle;

	$server->log(LOG_INFO,"[JOBS] Unloading caching engine.");
	awitpt::cache::disconnect($server);
}


# Init job
sub _initJob
{
	my $server = $serverHandle;


	$server->log(LOG_INFO,"[JOBS] Loading caching engine...");
	awitpt::cache::connect($server);
	$server->log(LOG_INFO,"[JOBS] Caching engine loaded.");

	$server->log(LOG_INFO,"[JOBS] Connecting to database...");
	# Init system stuff
	$server->{'client'}->{'dbh'} = awitpt::db::dbilayer::Init($server,'wiaflos');
	if (defined($server->{'client'}->{'dbh'})) {
		# Check if we succeeded
		if (!($server->{'client'}->{'dbh'}->connect())) {
			# If we succeeded, record OK
			$server->{'client'}->{'dbh_status'} = 0;
			$server->log(LOG_INFO,"[JOBS] Connected to database.");
		} else {
			$server->log(LOG_WARN,"[JOBS] Failed to connect to database: ".$server->{'client'}->{'dbh'}->Error());
		}
	} else {
		$server->log(LOG_WARN,"[JOBS] Failed to initialize: ".awitpt::db::dbilayer::internalError());
	}
}


# Background child job
sub createJob
{
	$SIG{CHLD} = \&REAPER;


	# Try fork
	my $pid = fork();
	if (!defined($pid)) {
		setError("Failed to fork(): $!");
		return ERR_SRVFORK;
	}
	if ($pid) {
		# This is the parent
		return $pid;

	} else {
		# This is the child
		setStatus("Initializing...");
		## Close open file descriptors
		# TODO - this breaks sqlite
	#	foreach my $i (0, 1, 3 .. OpenMax) { POSIX::close($i); }
		# TODO - we're going to leave half a gazillion fh's open?
		foreach my $i (0, 1, 3) { POSIX::close($i); }

		# Reopen stdout, stdin to /dev/null
		open(STDIN,  "+>/dev/null");
		open(STDOUT, "+>&STDIN");
	}


	_initJob();

	return 0;
}


# Set job status
sub setStatus
{
	my @params = @_;


	my $server = $serverHandle;

	# Set default pid and see if we were given one
	my $pid = $$;
	if (@params > 1) {
		$pid = shift;
	}

	my $txt = shift;
	# Set job status
	$jobs{$pid} = $txt;

	$server->log(LOG_INFO,"[JOBS] Setting status of $pid: $txt");
}




# The grim reaper
sub REAPER
{
	my $server = $serverHandle;

	my $waitedpid = waitpid(-1,POSIX::WNOHANG);

	delete($jobs{$waitedpid});

	$server->log(LOG_INFO,"[JOBS] Child $waitedpid exiting.");
}


1;
# vim: ts=4
