# Supplier receipting functions
# Copyright (C) 2009-2014, AllWorldIT
# Copyright (C) 2008, LinuxRulz
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.




package wiaflos::server::core::SupplierReceipting;

use strict;
use warnings;


use wiaflos::version;
use wiaflos::constants;
use wiaflos::server::core::config;
use awitpt::db::dblayer;
use awitpt::cache;
use wiaflos::server::core::templating;
use wiaflos::server::core::Suppliers;
use wiaflos::server::core::GL;

# Whole money transactions, precision is two
use Math::BigFloat;
Math::BigFloat::precision(-2);




# Our current error message
my $error = "";

# Set current error message
# Args: error_message
sub setError
{
	my $err = shift;
	my ($package,$filename,$line) = caller;
	my (undef,undef,undef,$subroutine) = caller(1);

	# Set error
	$error = "$subroutine($line): $err";
}

# Return current error message
# Args: none
sub Error
{
	my $err = $error;

	# Reset error
	$error = "";

	# Return error
	return $err;
}



# Check if receipt exists
# Backend function, takes 1 parameter which is the receipt ID
sub supplierReceiptIDExists
{
	my $receiptID = shift;


	# Select receipt count
	my $rows = DBSelectNumResults("FROM supplier_receipts WHERE ID = ".DBQuote($receiptID));
	if (!defined($rows)) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	return $rows > 0 ? 1 : 0;
}



# Check if receipt number exists
sub supplierReceiptNumberExists
{
	my $number = shift;


	# Sanitize
	$number = uc($number);
	$number =~ s#^SRT/##;

	# Select receipt count
	my $rows = DBSelectNumResults("FROM supplier_receipts WHERE Number = ".DBQuote($number));
	if (!defined($rows)) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	return $rows > 0 ? 1 : 0;
}



# Return receipt ID from number
sub getSupplierReceiptIDFromNumber
{
	my $number = shift;


	# Sanitize
	$number = uc($number);
	$number =~ s#^SRT/##;

	# Check cache
	my ($cache_res,$cache) = cacheGetKeyPair('SupplierReceipt/Number-to-ID',$number);
	if ($cache_res != RES_OK) {
		setError(awitpt::cache::Error());
		return $cache_res;
	}
	return $cache if (defined($cache));

	# Select receipt
	my $sth = DBSelect("SELECT ID FROM supplier_receipts WHERE Number = ".DBQuote($number));
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	my $row = hashifyLCtoMC($sth->fetchrow_hashref(),qw( ID ));
	DBFreeRes($sth);

	# Check we got a result
	if (!defined($row)) {
		setError("Error finding supplier receipt '$number'");
		return ERR_NOTFOUND;
	}

	# Cache this
	$cache_res = cacheStoreKeyPair('SupplierReceipt/Number-to-ID',$number,$row->{'ID'});
	if ($cache_res != RES_OK) {
		setError(awitpt::cache::Error());
		return $cache_res;
	}

	return $row->{'ID'};
}




# Backend function to build item hash
sub sanitizeRawItem
{
	my $rawData = shift;


	my $item;

	$item->{'ID'} = $rawData->{'ID'};

	my $data;

	# Pull in supplier data
	$item->{'SupplierID'} = $rawData->{'SupplierID'};

	$data = undef;
	$data->{'ID'} = $rawData->{'SupplierID'};
	my $supplier = wiaflos::server::core::Suppliers::getSupplier($data);
	$item->{'SupplierCode'} = $supplier->{'Code'};


	# Pull in GL account info
	$item->{'GLAccountID'} = $rawData->{'GLAccountID'};
	$item->{'GLAccountNumber'} = wiaflos::server::core::GL::getGLAccountNumberFromID($rawData->{'GLAccountID'});

	$item->{'Number'} = "SRT/".uc($rawData->{'Number'});

	$item->{'TransactionDate'} = $rawData->{'TransactionDate'};
	$item->{'Reference'} = $rawData->{'Reference'};
	$item->{'Amount'} = $rawData->{'Amount'};

	$item->{'GLTransactionID'} = $rawData->{'GLTransactionID'};
	$item->{'Posted'} = defined($rawData->{'GLTransactionID'}) ? 1 : 0;

	$item->{'Closed'} = $rawData->{'Closed'};

	return $item;
}



# Backend function to build item hash
sub sanitizeRawAllocationItem
{
	my $rawData = shift;


	my $item;

	$item->{'ID'} = $rawData->{'ID'};


   	# Pull in credit note data
	$item->{'SupplierCreditNoteID'} = $rawData->{'SupplierCreditNoteID'};

	my $data;
	$data->{'ID'} = $rawData->{'SupplierCreditNoteID'};
	my $creditnote = wiaflos::server::core::SupplierCreditNotes::getSupplierCreditNote($data);
	$item->{'SupplierCreditNoteNumber'} = $creditnote->{'Number'};

	# And the receipt
	$item->{'SupplierReceiptID'} = $rawData->{'SupplierReceiptID'};

	$data = undef;
	$data->{'ID'} = $rawData->{'SupplierReceiptID'};
	my $receipt = getSupplierReceipt($data);
	$item->{'SupplierReceiptNumber'} = $receipt->{'Number'};


	$item->{'Amount'} = $rawData->{'Amount'};

	$item->{'Posted'} = defined($rawData->{'SupplierCreditNoteTransactionID'}) ? 1 : 0;

	return $item;
}



# Create receipt
# Parameters:
#		SupplierCode	- Supplier code
#		GLAccountNumber	- GL account where money was paid from
#		Number		- Number for this receipt
#		Date		- Date of receipt
#		Reference		- GL account entry reference (bank statement reference for example)
#		Amount		- Amount
sub createSupplierReceipt
{
	my ($detail) = @_;


	# Verify receipt number
	if (!defined($detail->{'Number'}) || $detail->{'Number'} eq "") {
		setError("No (or invalid) supplier receipt number provided");
		return ERR_PARAM;
	}
	(my $receiptNumber = uc($detail->{'Number'})) =~ s#^SRT/##;

	# Verify supplier code
	if (!defined($detail->{'SupplierCode'}) || $detail->{'SupplierCode'} eq "") {
		setError("No (or invalid) supplier code provided for supplier receipt '".$detail->{'Number'}."'");
		return ERR_PARAM;
	}

	# Verify GL account
	if (!defined($detail->{'GLAccountNumber'}) || $detail->{'GLAccountNumber'} eq "") {
		setError("No (or invalid) GL account provided for supplier receipt '".$detail->{'Number'}."'");
		return ERR_PARAM;
	}

	# Verify date
	if (!defined($detail->{'Date'}) || $detail->{'Date'} eq "") {
		setError("No (or invalid) date provided for supplier receipt '".$detail->{'Number'}."'");
		return ERR_PARAM;
	}

	# Verify reference
	if (!defined($detail->{'Reference'}) || $detail->{'Reference'} eq "") {
		setError("No (or invalid) reference provided for supplier receipt '".$detail->{'Number'}."'");
		return ERR_PARAM;
	}

	# Amount
	if (!defined($detail->{'Amount'}) || $detail->{'Amount'} eq "") {
		setError("No (or invalid) amount account provided for supplier receipt '".$detail->{'Number'}."'");
		return ERR_PARAM;
	}

	# Check if supplier exists
	my $supplierID  = wiaflos::server::core::Suppliers::getSupplierIDFromCode($detail->{'SupplierCode'});
	if ($supplierID < 1) {
		setError(wiaflos::server::core::Suppliers::Error());
		return $supplierID;
	}

	# Check GL account exists
	my $GLAccountID = wiaflos::server::core::GL::getGLAccountIDFromNumber($detail->{'GLAccountNumber'});
	if ($GLAccountID < 1) {
		setError(wiaflos::server::core::GL::Error());
		return $GLAccountID;
	}

	# Check for conflicts
	if (supplierReceiptNumberExists($receiptNumber)) {
		setError("Supplier receipt number '$receiptNumber' already exists");
		return ERR_CONFLICT;
	}

	# Create receipt
	my $sth = DBDo("
		INSERT INTO supplier_receipts
				(SupplierID,GLAccountID,Number,TransactionDate,Reference,Amount,Closed)
			VALUES
				(
					".DBQuote($supplierID).",
					".DBQuote($GLAccountID).",
					".DBQuote($receiptNumber).",
					".DBQuote($detail->{'Date'}).",
					".DBQuote($detail->{'Reference'}).",
					".DBQuote($detail->{'Amount'}).",
					0
				)
	");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	# Grab last ID
	my $ID = DBLastInsertID("supplier_receipts","ID");

	return $ID;
}



# Return an array of receipts
sub getSupplierReceipts
{
	my ($detail) = @_;
	my $type = defined($detail->{'Type'}) ? $detail->{'Type'} : "open";
	my @receipts = ();


	# Return list of receipts
	my $sth = DBSelect("
		SELECT
			ID, SupplierID, GLAccountID, Number, TransactionDate, Reference, Amount, Closed
		FROM
			supplier_receipts
	");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	# Fetch rows
	while (my $row = hashifyLCtoMC($sth->fetchrow_hashref(),
			qw( ID SupplierID GLAccountID Number TransactionDate Reference Amount Closed )
	)) {
		# Check what kind of receipts do we want
		if (($type eq "open") && $row->{'Closed'} eq "0") {
			push(@receipts,sanitizeRawItem($row));
		} elsif ($type eq "all") {
			push(@receipts,sanitizeRawItem($row));
		}
	}

	DBFreeRes($sth);

	return \@receipts;
}



# Return an receipt hash
# Optional:
#		Number	- Receipt number
#		ID		- Receipt ID
sub getSupplierReceipt
{
	my ($detail) = @_;


	my $receiptID;

	# Check which 'mode' we operating in
	if (!defined($detail->{'ID'}) || $detail->{'ID'} < 1) {
		# Verify receipt number
		if (!defined($detail->{'Number'}) || $detail->{'Number'} eq "") {
			setError("No (or invalid) supplier receipt number provided");
			return ERR_PARAM;
		}

		# Check if receipt exists
		if (($receiptID = getSupplierReceiptIDFromNumber($detail->{'Number'})) < 1) {
			setError(Error());
			return $receiptID;
		}
	} else {
		$receiptID = $detail->{'ID'};
	}

	# Verify receipt ID
	if (!$receiptID || $receiptID < 1) {
		setError("No (or invalid) supplier receipt number/id provided");
		return ERR_PARAM;
	}

	# Return list of receipts
	my $sth = DBSelect("
		SELECT
			ID, SupplierID, GLAccountID, Number, TransactionDate, Reference, Amount, GLTransactionID, Closed
		FROM
			supplier_receipts
		WHERE
			ID = ".DBQuote($receiptID)."
	");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	# Fetch rows
	my $row = hashifyLCtoMC($sth->fetchrow_hashref(),
			qw( ID SupplierID GLAccountID Number TransactionDate Reference Amount GLTransactionID Closed ));
	DBFreeRes($sth);

	return sanitizeRawItem($row);
}



# One can now post the receipt, which sets GLTransactionID
# Parameters:
#		Number	- Receipt number
sub postSupplierReceipt
{
	my ($detail) = @_;


	my $data;

	# Grab receipt
	$data = undef;
	$data->{'Number'} = $detail->{'Number'};
	my $receipt = getSupplierReceipt($data);
	if (ref $receipt ne "HASH") {
		setError(Error());
		return $receipt;
	}

	# Make sure receipt is not posted
	if ($receipt->{'Posted'} eq "1") {
		setError("Receipt '".$receipt->{'Number'}."' already posted");
		return ERR_POSTED;
	}

	# Pull in supplier
	$data = undef;
	$data->{'ID'} = $receipt->{'SupplierID'};
	my $supplier = wiaflos::server::core::Suppliers::getSupplier($data);
	if (ref $supplier ne "HASH") {
		setError(wiaflos::server::core::Suppliers::Error());
		return $supplier;
	}

	DBBegin();

	# Create transaction
	my $transactionRef = sprintf('Receipt: %s',$receipt->{'Number'});
	$data = undef;
	$data->{'Date'} = $receipt->{'TransactionDate'};
	$data->{'Reference'} = $transactionRef;
	my $GLTransactionID = wiaflos::server::core::GL::createGLTransaction($data);
	if ($GLTransactionID < 1) {
		setError(wiaflos::server::core::GL::Error());
		DBRollback();
		return $GLTransactionID;
	}

	# Pull in amount
	my $transValue = Math::BigFloat->new($receipt->{'Amount'});

	# Link from GL
	$data = undef;
	$data->{'ID'} = $GLTransactionID;
#	$data->{'Reference'} = $receipt->{'Reference'};
	$data->{'GLAccountID'} = $receipt->{'GLAccountID'};
	$data->{'Amount'} = $transValue->bstr();
	if ((my $res = wiaflos::server::core::GL::linkGLTransaction($data)) < 1) {
		setError(wiaflos::server::core::GL::Error());
		DBRollback();
		return $res;
	}

	# Negate for other side
	$transValue->bneg();

	# Link to supplier GL account
	$data = undef;
	$data->{'ID'} = $GLTransactionID;
	$data->{'GLAccountID'} = $supplier->{'GLAccountID'};
	$data->{'Amount'} = $transValue->bstr();
	if ((my $res = wiaflos::server::core::GL::linkGLTransaction($data)) < 1) {
		setError(wiaflos::server::core::GL::Error());
		DBRollback();
		return $res;
	}

	# Post receipt
	my $sth = DBDo("
		UPDATE supplier_receipts
		SET
			GLTransactionID = ".DBQuote($GLTransactionID)."
		WHERE
			ID = ".DBQuote($receipt->{'ID'})."
	");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		DBRollback();
		return ERR_DB;
	}

	# Post transaction
	$data = undef;
	$data->{'ID'} = $GLTransactionID;
	if ((my $res = wiaflos::server::core::GL::postGLTransaction($data)) != 0) {
		setError(wiaflos::server::core::GL::Error());
		DBRollback();
		return $res;
	}

	DBCommit();

	return $GLTransactionID;
}



# Create allocation
# Parameters:
#		ReceiptNumber	- Receipt number
#		SupplierCreditNoteNumber	- Credit note number
#		Amount		- Amount
sub createSupplierReceiptAllocation
{
	my ($detail) = @_;


	# Verify receipt number
	if (!defined($detail->{'ReceiptNumber'}) || $detail->{'ReceiptNumber'} eq "") {
		setError("No (or invalid) supplier receipt number provided for supplier receipt allocation");
		return ERR_PARAM;
	}

	# Verify credit note number
	if (!defined($detail->{'SupplierCreditNoteNumber'}) || $detail->{'SupplierCreditNoteNumber'} eq "") {
		setError("No (or invalid) supplier credit note number provided for supplier receipt allocation");
		return ERR_PARAM;
	}

	# Amount
	if (!defined($detail->{'Amount'}) || $detail->{'Amount'} eq "") {
		setError("No (or invalid) amount account provided for supplier receipt allocation");
		return ERR_PARAM;
	}
	my $allocAmount = Math::BigFloat->new($detail->{'Amount'});
	if ($allocAmount->is_zero()) {
		setError("Supplier receipt allocation amount cannot be zero");
		return ERR_AMTZERO;
	}

	my $data;

	# Check if receipt exists & pull in
	$data = undef;
	$data->{'Number'} = $detail->{'ReceiptNumber'};
	my $receipt = getSupplierReceipt($data);
	if (ref $receipt ne "HASH" ) {
		setError(Error());
		return $receipt;
	}

	# Make sure receipt is posted
	if ($receipt->{'Posted'} ne "1") {
		setError("Receipt '".$receipt->{'Number'}."' must be posted before an allocation can be made");
		return ERR_POSTED;
	}

	# Grab credit note
	$data = undef;
	$data->{'Number'} = $detail->{'SupplierCreditNoteNumber'};
	my $creditnote = wiaflos::server::core::SupplierCreditNotes::getSupplierCreditNote($data);
	if (ref $creditnote ne "HASH") {
		setError(wiaflos::server::core::SupplierCreditNotes::Error());
		return $creditnote;
	}

	# Check if credit note is posted
	if ($creditnote->{'Posted'} ne "1") {
		setError("Cannot allocate an amount from supplier receipt '".$receipt->{'Number'}."' to creditnote '".
				$creditnote->{'Number'}."' as the credit note is not posted");
		return ERR_POSTED;
	}


	# Grab allocations
	$data = undef;
	$data->{'SupplierReceiptID'} = $receipt->{'ID'};
	my $allocs = getSupplierReceiptAllocations($data);
	if (ref $allocs ne "ARRAY") {
		setError(Error());
		return $allocs;
	}

	# Check if we either balance to 0 or have left over
	my $receiptBalance = Math::BigFloat->new($receipt->{'Amount'});
	foreach my $alloc (@{$allocs}) {
		$receiptBalance->bsub($alloc->{'Amount'});
	}
	$receiptBalance->bsub($detail->{'Amount'});
	if ($receiptBalance->is_neg()) {
		setError("Creating this allocation will exceed supplier receipt '".$receipt->{'Number'}."' amount by ".$receiptBalance->bstr());
		return ERR_OVERALLOC;
	}

	# Create supplier receipt allocation
	my $sth = DBDo("
		INSERT INTO supplier_receipt_allocations
				(SupplierReceiptID,SupplierCreditNoteID,Amount)
			VALUES
				(
					".DBQuote($receipt->{'ID'}).",
					".DBQuote($creditnote->{'ID'}).",
					".DBQuote($detail->{'Amount'})."
				)
	");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	# Grab last ID
	my $ID = DBLastInsertID("supplier_receipt_allocations","ID");

	return $ID;
}



# Return an array of receipt allocations
# Parameters:
#		SupplierReceiptNumber	- Receipt number
#		SupplierReceiptID	- Receipt ID
#		SupplierCreditNoteID	-  Credit note ID
sub getSupplierReceiptAllocations
{
	my ($detail) = @_;

	my @allocations = ();


	# SQL query string to use
	my $query = "";

	# ID mode
	if (defined($detail->{'SupplierReceiptID'}) && $detail->{'SupplierReceiptID'} > 0) {
		$query .= "SupplierReceiptID = ".$detail->{'SupplierReceiptID'};

	# Receipt number mode
	} elsif (defined($detail->{'SupplierReceiptNumber'}) && $detail->{'SupplierReceiptNumber'} ne "") {
		my $receiptID;

		# Check if receipt exists
		if (($receiptID = getSupplierReceiptIDFromNumber($detail->{'SupplierReceiptNumber'})) < 1) {
			setError(Error());
			return $receiptID;
		}

		$query .= "SupplierReceiptID = ".DBQuote($receiptID);

	# Credit note ID mode
	} elsif (defined($detail->{'SupplierCreditNoteID'}) && $detail->{'SupplierCreditNoteID'} ne "") {
		$query .= "SupplierCreditNoteID = ".DBQuote($detail->{'SupplierCreditNoteID'});

	} else {
		setError("No acceptable parameters provided to return supplier receipts allocations");
		return ERR_PARAM;
	}

	# Return list of receipt allocations
	my $sth = DBSelect("
		SELECT
			ID, SupplierCreditNoteID, SupplierReceiptID, Amount, SupplierCreditNoteTransactionID
		FROM
			supplier_receipt_allocations
		WHERE
			$query
	");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	# Fetch rows
	while (my $row = hashifyLCtoMC($sth->fetchrow_hashref(),
			qw( ID SupplierCreditNoteID SupplierReceiptID Amount SupplierCreditNoteTransactionID )
	)) {
		push(@allocations,sanitizeRawAllocationItem($row));
	}

	DBFreeRes($sth);

	return \@allocations;
}



# Return an hash containing a allocation
# Parameters:
#		ID	- Allocation ID
sub getSupplierReceiptAllocation
{
	my ($detail) = @_;


	# Verify allocation id
	if (!defined($detail->{'ID'}) || $detail->{'ID'} eq "") {
		setError("No (or invalid) allocation ID provided");
		return ERR_PARAM;
	}

	# Return allocation
	my $sth = DBSelect("
		SELECT
			ID, SupplierReceiptID, SupplierCreditNoteID, Amount, SupplierCreditNoteTransactionID
		FROM
			supplier_receipt_allocations
		WHERE
			ID = ".DBQuote($detail->{'ID'})."
	");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	# Fetch rows
	my $row = hashifyLCtoMC($sth->fetchrow_hashref(),
			qw( ID SupplierReceiptID SupplierCreditNoteID Amount SupplierCreditNoteTransactionID ));
	DBFreeRes($sth);

	return sanitizeRawAllocationItem($row);
}



# One can now post the allocation, which checks the invoice if its totally paid
# Parameters:
#		ID	- Receipt allocation ID
sub postSupplierReceiptAllocation
{
	my ($detail) = @_;


	# Verify allocation ID
	if (!defined($detail->{'ID'}) || $detail->{'ID'} eq "") {
		setError("No (or invalid) allocation ID provided");
		return ERR_PARAM;
	}

	my $data;

	# Check if allocation exists & pull in
	$data = undef;
	$data->{'ID'} = $detail->{'ID'};
	my $allocation = getSupplierReceiptAllocation($data);
	if (ref $allocation ne "HASH") {
		setError(Error());
		return $allocation;
	}

	# Make sure allocation is not posted
	if ($allocation->{'Posted'} ne "0") {
		setError("Supplier receipt allocation '".$allocation->{'ID'}."' already posted");
		return ERR_POSTED;
	}

	# Get corrosponding receipt
	$data = undef;
	$data->{'ID'} = $allocation->{'SupplierReceiptID'};
	my $receipt = getSupplierReceipt($data);
	if (ref $receipt ne "HASH") {
		setError(Error());
		return $receipt;
	}

	# Grab credit note
	$data = undef;
	$data->{'ID'} = $allocation->{'SupplierCreditNoteID'};
	my $creditnote = wiaflos::server::core::SupplierCreditNotes::getSupplierCreditNote($data);
	if (ref $creditnote ne "HASH") {
		setError(wiaflos::server::core::SupplierCreditNotes::Error());
		return $creditnote;
	}

	# Make sure credit note is posted
	if ($creditnote->{'Posted'} ne "1") {
		setError("Supplier credit note '".$creditnote->{'Number'}."' not posted");
		return ERR_POSTED;
	}

	# Grab credit note allocations
	$data = undef;
	$data->{'SupplierCreditNoteID'} = $allocation->{'SupplierCreditNoteID'};
	my $creditnoteAllocations = getSupplierReceiptAllocations($data);
	if (ref $creditnoteAllocations ne "ARRAY") {
		setError(Error());
		return $creditnoteAllocations;
	}
	# Add up credit note balance
	my $creditnoteBalance = Math::BigFloat->new($creditnote->{'Total'});
	foreach my $alloc (@{$creditnoteAllocations}) {
		if ($alloc->{'Posted'} eq "1") {
			$creditnoteBalance->bsub($alloc->{'Amount'});
		}
	}
	# Check credit note balance if its negative, this is if we've overallocated
	$creditnoteBalance->bsub($allocation->{'Amount'});
	if ($creditnoteBalance->is_neg()) {
		setError("Posting the allocation will end up in a balance of '".$creditnoteBalance->bstr()."' on credit note '".$creditnote->{'Number'}."'");
		return ERR_OVERALLOC;
	}

	# Grab receipt allocations
	$data = undef;
	$data->{'SupplierReceiptID'} = $receipt->{'ID'};
	my $receiptAllocations = getSupplierReceiptAllocations($data);
	if (ref $receiptAllocations ne "ARRAY") {
		setError(Error());
		return $receiptAllocations;
	}
	# Add up receipt balance
	my $receiptBalance = Math::BigFloat->new($receipt->{'Amount'});
	foreach my $alloc (@{$receiptAllocations}) {
		if ($alloc->{'Posted'} eq "1") {
			$receiptBalance->bsub($alloc->{'Amount'});
		}
	}
	# Check receipt balance if its negative, this is if we've overallocated
	$receiptBalance->bsub($allocation->{'Amount'});
	if ($receiptBalance->is_neg()) {
		setError("Posting the allocation will end up in a balance of '".$receiptBalance->bstr()."' on supplier receipt '".$receipt->{'Number'}."'");
		return ERR_OVERALLOC;
	}

	DBBegin();

	# If we equal out, mark it as being closed
	if ($receiptBalance->is_zero()) {
		# Close it off
		my $sth = DBDo("
			UPDATE
				supplier_receipts
			SET
				Closed = 1
			WHERE
				ID = ".DBQuote($receipt->{'ID'})."
		");
		if (!$sth) {
			setError(awitpt::db::dblayer::Error());
			DBRollback();
			return ERR_DB;
		}
	}


	# Create transaction
	$data = undef;
	$data->{'ID'} = $creditnote->{'ID'};
	$data->{'Amount'} = Math::BigFloat->new($allocation->{'Amount'})->bneg();
	$data->{'SupplierReceiptAllocationID'} = $allocation->{'ID'};
	my $creditNoteTransactionID = wiaflos::server::core::SupplierCreditNotes::allocateSupplierCreditNoteTransaction($data);
	if ($creditNoteTransactionID < 1) {
		setError(wiaflos::server::core::Invoicing::Error());
		DBRollback();
		return $creditNoteTransactionID;
	}

	# Post allocation
	my $sth = DBDo("
		UPDATE supplier_receipt_allocations
		SET
			SupplierCreditNoteTransactionID = ".DBQuote($creditNoteTransactionID)."
		WHERE
			ID = ".DBQuote($allocation->{'ID'})."
	");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		DBRollback();
		return ERR_DB;
	}


	DBCommit();

	return RES_OK;
}



# Send receipt
# Parameters:
#		Number	- Receipt number
#		SendTo		- Send to,  email:  ,  file:  , return
sub sendSupplierReceipt
{
	my ($detail) = @_;


	# Verify SendTo
	if (!defined($detail->{'SendTo'}) || $detail->{'SendTo'} eq "") {
		setError("SendTo was not provided");
		return ERR_PARAM;
	}

	# Pull in config
	my $config = wiaflos::server::core::config::getConfig();

	# Grab receipt
	my $data;
	$data->{'Number'} = $detail->{'Number'};
	my $receipt = getSupplierReceipt($data);
	if (ref $receipt ne "HASH") {
		setError(Error());
		return $receipt;
	}

	# Grab items
	$data = undef;
	$data->{'SupplierReceiptID'} = $receipt->{'ID'};
	my $allocations = getSupplierReceiptAllocations($data);
	if (ref $allocations ne "ARRAY") {
		setError(Error());
		return $allocations;
	}

	# Setup request data
	$data = undef;
	$data->{'ID'} = $receipt->{'SupplierID'};

	# Pull supplier
	my $supplier = wiaflos::server::core::Suppliers::getSupplier($data);
	if (ref $supplier ne "HASH") {
		setError(wiaflos::server::core::Suppliers::Error());
		return $supplier;
	}

	# Pull in addresses
	my $addresses = wiaflos::server::core::Suppliers::getSupplierAddresses($data);
	if (ref $addresses ne "ARRAY") {
		setError(wiaflos::server::core::Suppliers::Error());
		return $addresses;
	}

	# Pull in email addresses
	my $emailAddies = wiaflos::server::core::Suppliers::getSupplierEmailAddresses($data);
	if (ref $emailAddies ne "ARRAY") {
		setError(wiaflos::server::core::Suppliers::Error());
		return $emailAddies;
	}

	# Parse in addresses
	my $billAddr;
	my $shipAddr = "";
	# Look for billing address
	foreach my $address (@{$addresses}) {
		if ($address->{'Type'} eq "billing") {
			$billAddr = $address->{'Address'};
		} elsif ($address->{'Type'} eq "shipping") {
			$shipAddr = $address->{'Address'};
		}
	}
	# If no billing address, use shipping address
	$billAddr = defined($billAddr) ? $billAddr : $shipAddr;
	$billAddr =~ s/\|/<br \/>/g;
	# Setup shipping address
	$shipAddr =~ s/\|/<br \/>/g if defined($shipAddr);

	# Parse in email addresses
	my $billEmailAddr;
	my @billEmailAddrs;
	my @genEmailAddrs;
	# Look for accounts address
	foreach my $address (@{$emailAddies}) {
		if ($address->{'Type'} eq "accounts") {
			push(@billEmailAddrs,$address->{'Address'});
		} elsif ($address->{'Type'} eq "general") {
			push(@genEmailAddrs,$address->{'Address'});
		}
	}
	# If no accounts address, use general address
	$billEmailAddr = @billEmailAddrs > 0 ? join(',',@billEmailAddrs) : join(',',@genEmailAddrs);

	# Build array of stuff we can use
	my $vars = {
		'WiaflosString' => $GENSTRING,

		# Supplier
		'SupplierCode' => $supplier->{'Code'},
		'SupplierName' => $supplier->{'Name'},
		'SupplierBillingAddress' => $billAddr,
		'SupplierShippingAddress' => $shipAddr,

		# Receipt
		'ReceiptNumber' => $receipt->{'Number'},
		'ReceiptDate' => $receipt->{'TransactionDate'},
		'ReceiptAmount' => sprintf('%.2f',$receipt->{'Amount'}),
		# FIXME - not implemented
		'ReceiptNote' => "",
	};

	# Keep balance
	my $receiptBalance = Math::BigFloat->new($receipt->{'Amount'});

	# Load receipt line items
	foreach my $item (@{$allocations}) {
		my $titem;

		# Pull credit note
		$data = undef;
		$data->{'ID'} = $item->{'SupplierCreditNoteID'};
		my $creditnote = wiaflos::server::core::SupplierCreditNotes::getSupplierCreditNote($data);
		if (ref $creditnote ne "HASH") {
			setError(wiaflos::server::core::SupplierCreditNotes::Error());
			return $creditnote;
		}

		# Keep balance
		$receiptBalance->bsub($item->{'Amount'});

		# Fix some stuff up
		$titem->{'CreditNoteNumber'} = $creditnote->{'Number'};
		$titem->{'CreditNoteIssueDate'} = $creditnote->{'IssueDate'};
		$titem->{'CreditNoteAmount'} = $creditnote->{'Total'};
		$titem->{'CreditNoteAmountAllocated'} = $item->{'Amount'};
		$titem->{'CreditNoteClosed'} = $creditnote->{'Closed'} ? 'yes' : 'no';

		$titem->{'ReceiptBalance'} = sprintf('%.2f',$receiptBalance->bstr());

		# Various fixups & amount sanitizations
		push(@{$vars->{'LineItems'}},$titem);
	}
	# Get end receipt balance
	$vars->{'ReceiptBalance'} = $receiptBalance->bstr();

	# Get receipt template file
	my $template = defined($config->{'supplierreceipting'}{'email_template'}) ?
			$config->{'supplierreceipting'}{'email_template'} : "supplier_receipts/supplier_receipt1.tt2";

	# Check where receipt must go
	if ($detail->{'SendTo'} =~ /^file:(\S+)/i) {
		my $filename = $1;

		# Load template
		my $res = loadTemplate($template,$vars,$filename);
		if (!$res) {
			setError("Failed to load template '$template': ".wiaflos::server::core::templating::Error());
			return ERR_SRVTEMPLATE;
		}

	# Write out using email
	} elsif ($detail->{'SendTo'} =~ /^email(?:\:(\S+))?/i) {
		# Pull email addr
		my $emailAddy = $1 ne "" ? $1 : $billEmailAddr;

		# Verify SMTP server is set
		my $server = $config->{'mail'}{'server'};
		if (!defined($server) || $server eq "") {
			setError("Cannot use supplier receipt emailing if we do not have an SMTP server defined");
			return ERR_SRVPARAM;
		}

		# Check if we have a email addy
		if ($emailAddy eq "") {
			setError("No email address defined to send supplier receipt '".$receipt->{'Number'}."' to");
			return ERR_PARAM;
		}

		# Receipt filename
		(my $receiptFilename = $receipt->{'Number'} . ".html") =~ s,/,-,g;
		# Signature filename
		my $rctSignFilename = $receiptFilename . ".asc";

		# If we must, pull in email body
		my $message_template = $config->{'supplierreceipting'}{'email_message_template'};
		my $emailBody = "";
		if (defined($message_template) && $message_template ne "") {
			# Variables for our template
			my $vars2 = {
				'ReceiptFilename' => $receiptFilename,
				'ReceiptSignatureFilename' => $rctSignFilename,
			};
			# Load template
			my $res = loadTemplate($message_template,$vars2,\$emailBody);
			if (!$res) {
				setError("Failed to load template '$message_template': ".wiaflos::server::core::templating::Error());
				return ERR_SRVTEMPLATE;
			}

			$emailBody =~ s/(?<!\r)\n/\r\n/sg; # Sanitize eol
		}


		# Load template
		my $receiptData = "";
		my $res = loadTemplate($template,$vars,\$receiptData);
		if (!$res) {
			setError("Failed to load template '$template': ".wiaflos::server::core::templating::Error());
			return ERR_SRVTEMPLATE;
		}
		$receiptData =~ s/(?<!\r)\n/\r\n/sg; # Sanitize eol, needed to fix bug in crypt-gpg where it mangles \n

		# See if we must use GPG
		my $use_gpg_key = $config->{'supplierreceipting'}{'use_gpg_key'};
		my $sign;
		if (defined($use_gpg_key) && $use_gpg_key ne "") {
			# Setup GnuPG
			my $gpg = new Crypt::GPG;
			$gpg->gpgbin('/usr/bin/gpg');
			$gpg->secretkey($use_gpg_key);
			$gpg->armor(1);
			$gpg->comment("$APPNAME v$VERSION ($APPURL)");
			# Sign receipt
			$sign = $gpg->sign($receiptData);
			if (!defined($sign)) {
				setError("Failed to sign receipt");
				return ERR_SRVEXEC;
			}
		}

		# Create message
		my $msg = MIME::Lite->new(
				From	=> $config->{'supplierreceipting'}{'email_from'},
				To		=> $emailAddy,
				Bcc		=> $config->{'supplierreceipting'}{'email_bcc'},
				Subject	=> "Receipt: ".$receipt->{'Number'},
				Type	=> 'multipart/mixed'
		);

		# Attach body
		$msg->attach(
				Type	=> 'TEXT',
				Encoding 	=> '8bit',
				Data	=> $emailBody,
		);

		# Attach receipt
		$msg->attach(
				Type	=> 'text/html',
				Encoding	=> 'base64',
				Data	=> $receiptData,
				Disposition	=> 'attachment',
				Filename	=> $receiptFilename
		);

		# If we have signature, sign receipt
		if (defined($sign)) {
			# Attach signature
			$msg->attach(
					Type	=> 'text/plain',
					Encoding	=> 'base64',
					Data	=> $sign,
					Disposition	=> 'attachment',
					Filename	=> $rctSignFilename
			);
		}

		# Send email
		my @SMTPParams;
		if (!(my $res = $msg->send("smtp",$server))) {
			setError("Failed to send supplier receipt via email server '$server'");
			return ERR_SRVEXEC;
		}

	} else {
		setError("Invalid SendTo method provided: '".$detail->{'SendTo'}."'");
		return ERR_PARAM;
	}


	return RES_OK;
}





1;
# vim: ts=4
