# Supplier functions
# Copyright (C) 2009-2014, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.




package wiaflos::server::core::Suppliers;

use strict;
use warnings;


use wiaflos::constants;
use awitpt::db::dblayer;
use awitpt::cache;
use wiaflos::server::core::GL;

# Whole money transactions, precision is two
use Math::BigFloat;
Math::BigFloat::precision(-2);



# Our current error message
my $error = "";

# Set current error message
# Args: error_message
sub setError
{
	my $err = shift;
	my ($package,$filename,$line) = caller;
	my (undef,undef,undef,$subroutine) = caller(1);

	# Set error
	$error = "$subroutine($line): $err";
}

# Return current error message
# Args: none
sub Error
{
	my $err = $error;

	# Reset error
	$error = "";

	# Return error
	return $err;
}


# Check if supplier exists
sub supplierIDExists
{
	my $supplierID = shift;


	# Select supplier count
	my $rows = DBSelectNumResults("FROM suppliers WHERE ID = ".DBQuote($supplierID));
	if (!defined($rows)) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	return $rows > 0 ? 1 : 0;
}


# Check if supplier code exists
sub supplierCodeExists
{
	my $code = shift;


	# Uppercase and add remove client identifier
	$code = uc($code);
	$code =~ s#^S-##;

	# Select supplier count
	my $rows = DBSelectNumResults("FROM suppliers WHERE Code = ".DBQuote($code));
	if (!defined($rows)) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	return $rows > 0 ? 1 : 0;
}



# Return supplier id from ref
sub getSupplierIDFromCode
{
	my $supplierCode = shift;


	# Uppercase and add remove client identifier
	$supplierCode = uc($supplierCode);
	$supplierCode =~ s#^S-##;

	# Check cache
	my ($cache_res,$cache) = cacheGetKeyPair('Suppliers/Code-to-ID',$supplierCode);
	if ($cache_res != RES_OK) {
		setError(awitpt::cache::Error());
		return $cache_res;
	}
	return $cache if (defined($cache));


	# Select ID
	my $sth = DBSelect("SELECT ID FROM suppliers WHERE Code = ".DBQuote($supplierCode));
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	my $row = hashifyLCtoMC($sth->fetchrow_hashref(),qw( ID ));
	DBFreeRes($sth);

	# Check we got a result
	if (!defined($row)) {
		setError("Error finding supplier '$supplierCode'");
		return ERR_NOTFOUND;
	}

	# Cache this
	$cache_res = cacheStoreKeyPair('Suppliers/Code-to-ID',$supplierCode,$row->{'ID'});
	if ($cache_res != RES_OK) {
		setError(awitpt::cache::Error());
		return $cache_res;
	}


	return $row->{'ID'};
}


# Return supplier code from ID
sub getSupplierCodeFromID
{
	my $supplierID = shift;


	# Check cache
	my ($cache_res,$cache) = cacheGetKeyPair('Suppliers/ID-to-Code',$supplierID);
	if ($cache_res != RES_OK) {
		setError(awitpt::cache::Error());
		return $cache_res;
	}
	return $cache if (defined($cache));


	# Select code
	my $sth = DBSelect("
		SELECT
			Code
		FROM
			suppliers
		WHERE
			ID = ".DBQuote($supplierID)."
	");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	my $row = hashifyLCtoMC($sth->fetchrow_hashref(),qw( Code ));
	DBFreeRes($sth);

	# Check we got a result
	if (!defined($row)) {
		setError("Error finding supplier from supplier ID '$supplierID'");
		return ERR_NOTFOUND;
	}

	# Cache this
	$cache_res = cacheStoreKeyPair('Suppliers/ID-to-Code',$supplierID,$row->{'Code'});
	if ($cache_res != RES_OK) {
		setError(awitpt::cache::Error());
		return $cache_res;
	}


	return $row->{'Code'};
}


# Backend function to build item hash
sub sanitizeRawItem
{
	my $rawData = shift;


	my $item;

	$item->{'ID'} = $rawData->{'ID'};

	$item->{'Code'} = "S-".uc($rawData->{'Code'});
	$item->{'Name'} = $rawData->{'Name'};
	$item->{'RegNumber'} = $rawData->{'RegNumber'};
	$item->{'TaxReference'} = $rawData->{'TaxReference'};
	$item->{'GLAccountID'} = $rawData->{'GLAccountID'};
	$item->{'GLAccountNumber'} = wiaflos::server::core::GL::getGLAccountNumberFromID($rawData->{'GLAccountID'});
	$item->{'ContactPerson'} = $rawData->{'ContactPerson'};

	return $item;
}


# Return a hash containing the supplier
# Optional:
#		ID		- Supplier id
#		Code	- Supplier code
sub getSupplier
{
	my ($detail) = @_;


	my $supplierID;

	# Check which 'mode' we operating in
	if (!defined($detail->{'ID'}) || $detail->{'ID'} < 1) {
		# Verify supplier code
		if (!defined($detail->{'Code'}) || $detail->{'Code'} eq "") {
			setError("No (or invalid) supplier code provided");
			return ERR_PARAM;
		}

		# Check if supplier exists
		if (($supplierID = getSupplierIDFromCode($detail->{'Code'})) < 1) {
			setError(Error());
			return $supplierID;
		}
	} else {
		$supplierID = $detail->{'ID'};
	}

	# Verify supplier ID
	if (!$supplierID || $supplierID < 1) {
		setError("No (or invalid) supplier code/id provided");
		return ERR_USAGE;
	}


	# Return list of suppliers
	my $sth = DBSelect("
		SELECT
			ID, Code, Name, RegNumber, TaxReference, GLAccountID, ContactPerson
		FROM
			suppliers
		WHERE
			ID = ".DBQuote($supplierID)."
	");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	# Fetch rows
	my $row = hashifyLCtoMC($sth->fetchrow_hashref(),qw( ID Code Name RegNumber TaxReference GLAccountID ContactPerson ));
	DBFreeRes($sth);

	return sanitizeRawItem($row);
}




# Return an array of suppliers
sub getSuppliers
{
	my @suppliers = ();


	# Return list of suppliers
	my $sth = DBSelect("
		SELECT
			ID, Code, Name, RegNumber, TaxReference, GLAccountID, ContactPerson
		FROM
			suppliers
	");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	# Fetch rows
	while (my $row = hashifyLCtoMC($sth->fetchrow_hashref(),
			qw( ID Code Name RegNumber TaxReference GLAccountID ContactPerson )
	)) {
		my $item = sanitizeRawItem($row);

		push(@suppliers,$item);
	}

	DBFreeRes($sth);

	return \@suppliers;
}


# Create supplier
# Parameters:
#		Code		- Supplier code
#		Name		- Supplier name
#		GLAccountNumber	- General ledger account
# Optional:
#		RegNumber - Company registration number / ID number of person
#		TaxReference - Company tax reference number
#		CreateSubAccount - Create sub GL account for this client
sub createSupplier
{
	my ($detail) = @_;


	my @extraCols = ();
	my @extraData = ();


	# Verify reference
	if (!defined($detail->{'Code'}) || $detail->{'Code'} eq "") {
		setError("No code provided");
		return ERR_PARAM;
	}
	# Uppercase and add remove supplier identifier
	(my $supplierCode = uc($detail->{'Code'})) =~ s#^S-##;

	# Verify Name
	if (!defined($detail->{'Name'}) || $detail->{'Name'} eq "") {
		setError("No name provided for supplier '".$detail->{'Code'}."'");
		return ERR_PARAM;
	}

	# Verify GL account
	if (!defined($detail->{'GLAccountNumber'}) || $detail->{'GLAccountNumber'} eq "") {
		setError("No GL account provided for supplier '".$detail->{'Code'}."'");
		return ERR_PARAM;
	}

	# Grab GL account ID
	my $GLAccID;
	if (($GLAccID = wiaflos::server::core::GL::getGLAccountIDFromNumber($detail->{'GLAccountNumber'})) < 1) {
		setError(wiaflos::server::core::GL::Error());
		return $GLAccID;
	}

	# Check we a liability account
	my $catMatch = wiaflos::server::core::GL::checkGLAccountFinCat($GLAccID,"B01");
	if ($catMatch == 0) {
		setError("GL account for supplier '".$detail->{'Code'}."' is not of type liability");
		return ERR_USAGE;
	} elsif ($catMatch < 0) {
		setError(wiaflos::server::core::GL::Error());
		return $catMatch;
	}

	# Check for conflicts
	if ((my $res = supplierCodeExists($detail->{'Code'})) != 0) {
		# Err if exists
		if ($res == 1) {
			setError("Supplier code '$supplierCode' already exists");
			return ERR_CONFLICT;
		} else {
			setError(Error());
		}
		# else err
		return $res;
	}

	# See if we got optional params
	if (defined($detail->{'RegNumber'})) {
		push(@extraCols,'RegNumber');
		push(@extraData,DBQuote($detail->{'RegNumber'}));
	}
	if (defined($detail->{'TaxReference'})) {
		push(@extraCols,'TaxReference');
		push(@extraData,DBQuote($detail->{'TaxReference'}));
	}


	# Pull in extra data
	my $extraCols = "";
	my $extraData = "";
	if (@extraCols > 0 && @extraData > 0) {
		$extraCols .= ',' . join(',',@extraCols);
		$extraData .= ',' . join(',',@extraData);
	}

	DBBegin();

	# Check if we should create a sub account
	if (defined($detail->{'CreateSubAccount'}) && $detail->{'CreateSubAccount'} eq 'y') {

		# Get GL account ID
		my $tmp;
		$tmp->{'AccountNumber'} = $detail->{'GLAccountNumber'};
		my $subGLAcc = wiaflos::server::core::GL::getGLAccount($tmp);
		if (ref $subGLAcc ne "HASH") {
			setError(wiaflos::server::core::GL::Error());
			return $subGLAcc;
		}

		# Create new account
		my $newAcc;
		$newAcc->{'Code'} = wiaflos::server::core::GL::getNextGLSubAccountCode($tmp);
		$newAcc->{'Name'} = "Supplier: $supplierCode";
		$newAcc->{'FinCatCode'} = $subGLAcc->{'FinCatCode'};
		$newAcc->{'RwCatCode'} = $subGLAcc->{'RwCatCode'};
		$newAcc->{'ParentAccountNumber'} = $detail->{'GLAccountNumber'};
		$GLAccID = wiaflos::server::core::GL::createGLAccount($newAcc);
		# Check for error
		if ($GLAccID < 1) {
			setError(wiaflos::server::core::GL::Error());
			return $GLAccID;
		}
	}

	# Create supplier
	my $sth = DBDo("
		INSERT INTO suppliers
				(Code,Name,GLAccountID$extraCols)
			VALUES
				(
					".DBQuote($supplierCode).",
					".DBQuote($detail->{'Name'}).",
					".DBQuote($GLAccID)."
					$extraData
				)
	");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		DBRollback();
		return ERR_DB;
	}

	# Grab last ID
	my $ID = DBLastInsertID("suppliers","ID");

	DBCommit();

	return $ID;
}


# Link Address
# Parameters:
#		Code		- Supplier code
#		Type		- Address type, billing or shipping
#		Address		- Address
sub linkSupplierAddress
{
	my ($detail) = @_;


	# Verify supplier code
	if (!defined($detail->{'Code'}) || $detail->{'Code'} eq "") {
		setError("No (or invalid) supplier code provided for supplier address");
		return ERR_PARAM;
	}

	# Verify type
	if (!defined($detail->{'Type'}) || $detail->{'Type'} eq "") {
		setError("No address type provided for supplier '".$detail->{'Code'}."' address");
		return ERR_PARAM;
	}

	# Verify address
	if (!defined($detail->{'Address'}) || $detail->{'Address'} eq "") {
		setError("No address provided for supplier '".$detail->{'Code'}."' address");
		return ERR_PARAM;
	}

	# Decide on address type
	my $typeID;
	if ($detail->{'Type'} eq "billing") {
		$typeID = 1;
	} elsif ($detail->{'Type'} eq "shipping") {
		$typeID = 2;
	} else {
		setError("Invalid address type provided for supplier '".$detail->{'Code'}."' address");
		return ERR_USAGE;
	}

	# Check if supplier exists
	my $supplierID;
	if (($supplierID = getSupplierIDFromCode($detail->{'Code'})) < 1) {
		setError(Error());
		return $supplierID;
	}

	# Link in supplier address
	my $sth = DBDo("
		INSERT INTO supplier_addresses
				(SupplierID,Type,Address)
			VALUES
				(
					".DBQuote($supplierID).",
					".DBQuote($typeID).",
					".DBQuote($detail->{'Address'})."
				)
	");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	# Grab last ID
	my $ID = DBLastInsertID("supplier_addresses","ID");

	return $ID;
}


# Link email address
# Parameters:
#		Code		- Supplier code
#		Type		- Address type, accounts or ?
#		Address		- Address
sub linkSupplierEmailAddress
{
	my ($detail) = @_;


	# Verify supplier code
	if (!defined($detail->{'Code'}) || $detail->{'Code'} eq "") {
		setError("No (or invalid) supplier code provided for supplier email address");
		return ERR_PARAM;
	}

	# Verify type
	if (!defined($detail->{'Type'}) || $detail->{'Type'} eq "") {
		setError("No email address type provided for supplier '".$detail->{'Code'}."' email address");
		return ERR_PARAM;
	}

	# Verify address
	if (!defined($detail->{'Address'}) || $detail->{'Address'} eq "") {
		setError("No email address provided for supplier '".$detail->{'Code'}."' email address");
		return ERR_PARAM;
	}

	# Decide on address type
	my $typeID;
	if ($detail->{'Type'} eq "accounts") {
		$typeID = 1;
	} elsif ($detail->{'Type'} eq "general") {
		$typeID = 2;
	} else {
		setError("Invalid email address type provided for supplier '".$detail->{'Code'}."'");
		return ERR_USAGE;
	}

	# Check if supplier exists
	my $supplierID;
	if (($supplierID = getSupplierIDFromCode($detail->{'Code'})) < 1) {
		setError(Error());
		return $supplierID;
	}

	# Link in supplier address
	my $sth = DBDo("
		INSERT INTO supplier_email_addresses
				(SupplierID,Type,Address)
			VALUES
				(
					".DBQuote($supplierID).",
					".DBQuote($typeID).",
					".DBQuote($detail->{'Address'})."
				)
	");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	# Grab last ID
	my $ID = DBLastInsertID("supplier_email_addresses","ID");

	return $ID;
}


# Link PhNumber
# Parameters:
#		Code		- Supplier code
#		Type		- Number type, phone or fax
#		Number		- Phone number
# Optional:
#		Name		- Name of person/org
sub linkSupplierPhoneNumber
{
	my ($detail) = @_;


	my @extraCols = ();
	my @extraData = ();


	# Verify supplier code
	if (!defined($detail->{'Code'}) || $detail->{'Code'} eq "") {
		setError("No (or invalid) supplier code provided for supplier phone number");
		return ERR_PARAM;
	}

	# Verify type
	if (!defined($detail->{'Type'}) || $detail->{'Type'} eq "") {
		setError("No phone number type provided for supplier '".$detail->{'Code'}."' phone number");
		return ERR_PARAM;
	}

	# Verify phone number
	if (!defined($detail->{'Number'}) || $detail->{'Number'} eq "") {
		setError("No phone number provided for supplier '".$detail->{'Code'}."' phone number");
		return ERR_PARAM;
	}

	# Decide on phone number type
	my $typeID;
	if ($detail->{'Type'} eq "phone") {
		$typeID = 1;
	} elsif ($detail->{'Type'} eq "fax") {
		$typeID = 2;
	} else {
		setError("Invalid phone number type provided for supplier '".$detail->{'Code'}."' phone number");
		return ERR_USAGE;
	}

	# Check if supplier exists
	my $supplierID;
	if (($supplierID = getSupplierIDFromCode($detail->{'Code'})) < 1) {
		setError(Error());
		return $supplierID;
	}

	if (defined($detail->{'Name'}) && $detail->{'Name'} ne "") {
		push(@extraCols,"Name");
		push(@extraData,DBQuote($detail->{'Name'}));
	}


	# Pull in extra data
	my $extraCols = "";
	my $extraData = "";
	if (@extraCols > 0 && @extraData > 0) {
		$extraCols .= ',' . join(',',@extraCols);
		$extraData .= ',' . join(',',@extraData);
	}

	# Link in supplier address
	my $sth = DBDo("
		INSERT INTO supplier_phone_numbers
				(SupplierID,Type,Number$extraCols)
			VALUES
				(
					".DBQuote($supplierID).",
					".DBQuote($typeID).",
					".DBQuote($detail->{'Number'})."
					$extraData
				)
	");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	# Grab last ID
	my $ID = DBLastInsertID("supplier_phone_numbers","ID");

	return $ID;
}


# Return an array of supplier addresses
# Parameters:
#		Code		- Supplier code
# Optional:
#		ID			- Instead of ref, lets work on an ID
sub getSupplierAddresses
{
	my ($detail) = @_;

	my @addresses = ();


	my $supplierID;

	# Check which 'mode' we operating in
	if (!defined($detail->{'ID'}) || $detail->{'ID'} < 1) {
		# Verify supplier code
		if (!defined($detail->{'Code'}) || $detail->{'Code'} eq "") {
			setError("No (or invalid) supplier code provided");
			return ERR_PARAM;
		}

		# Check if supplier exists
		if (($supplierID = getSupplierIDFromCode($detail->{'Code'})) < 1) {
			setError(Error());
			return $supplierID;
		}
	} else {
		$supplierID = $detail->{'ID'};
	}

	# Return list of addresses
	my $sth = DBSelect("
		SELECT
			ID, SupplierID, Type, Address
		FROM
			supplier_addresses
		WHERE
			SupplierID = ".DBQuote($supplierID)."
	");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	# Fetch rows
	while (my $row = hashifyLCtoMC($sth->fetchrow_hashref(),
			qw( ID SupplierID Type Address )
			)) {
		my $item;

		$item->{'ID'} = $row->{'ID'};

		# Resolve type
		if ($row->{'Type'} == 1) {
			$item->{'Type'} = "billing";
		} elsif ($row->{'Type'} == 2) {
			$item->{'Type'} = "shipping";
		} else {
			$item->{'Type'} = "unknown";
		}

		$item->{'Address'} = $row->{'Address'};

		push(@addresses,$item);
	}

	DBFreeRes($sth);

	return \@addresses;
}


# Return an array of supplier email addresses
# Parameters:
#		Code		- Supplier code
# Optional:
#		ID			- Instead of code, lets work on an ID
sub getSupplierEmailAddresses
{
	my ($detail) = @_;

	my @addresses = ();


	my $supplierID;

	# Check which 'mode' we operating in
	if (!defined($detail->{'ID'}) || $detail->{'ID'} < 1) {
		# Verify supplier code
		if (!defined($detail->{'Code'}) || $detail->{'Code'} eq "") {
			setError("No (or invalid) supplier code provided");
			return ERR_PARAM;
		}

		# Check if supplier exists
		if (($supplierID = getSupplierIDFromCode($detail->{'Code'})) < 1) {
			setError(Error());
			return $supplierID;
		}
	} else {
		$supplierID = $detail->{'ID'};
	}


	# Return list of email addresses
	my $sth = DBSelect("
		SELECT
			ID, SupplierID, Type, Address
		FROM
			supplier_email_addresses
		WHERE
			SupplierID = ".DBQuote($supplierID)."
	");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	# Fetch rows
	while (my $row = hashifyLCtoMC($sth->fetchrow_hashref(),
			qw( ID SupplierID Type Address )
	)) {
		my $item;

		$item->{'ID'} = $row->{'ID'};

		# Resolve type
		if ($row->{'Type'} == 1) {
			$item->{'Type'} = "accounts";
		} elsif ($row->{'Type'} == 2) {
			$item->{'Type'} = "general";
		} else {
			$item->{'Type'} = "unknown";
		}

		$item->{'Address'} = $row->{'Address'};

		push(@addresses,$item);
	}

	DBFreeRes($sth);

	return \@addresses;
}


# Return an array of supplier email phone numbers
# Parameters:
#		Code		- Supplier code
# Optional:
#		ID			- Instead of ref, lets work on an ID
sub getSupplierPhoneNumbers
{
	my ($detail) = @_;

	my @numbers = ();


	my $supplierID;

	# Check which 'mode' we operating in
	if (!defined($detail->{'ID'}) || $detail->{'ID'} < 1) {
		# Verify supplier code
		if (!defined($detail->{'Code'}) || $detail->{'Code'} eq "") {
			setError("No (or invalid) supplier code provided");
			return ERR_PARAM;
		}

		# Check if supplier exists
		if (($supplierID = getSupplierIDFromCode($detail->{'Code'})) < 1) {
			setError(Error());
			return $supplierID;
		}
	} else {
		$supplierID = $detail->{'ID'};
	}

	# Return list of numbers
	my $sth = DBSelect("
		SELECT
			ID, SupplierID, Type, Number
		FROM
			supplier_phone_numbers
		WHERE
			SupplierID = ".DBQuote($supplierID)."
	");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	# Fetch rows
	while (my $row = hashifyLCtoMC($sth->fetchrow_hashref(),
			qw( ID SupplierID Type Number )
	)) {
		my $item;

		$item->{'ID'} = $row->{'ID'};

		# Resolve type
		if ($row->{'Type'} == 1) {
			$item->{'Type'} = "phone";
		} elsif ($row->{'Type'} == 2) {
			$item->{'Type'} = "fax";
		} else {
			$item->{'Type'} = "unknown";
		}

		$item->{'Number'} = $row->{'Number'};

		push(@numbers,$item);
	}

	DBFreeRes($sth);

	return \@numbers;
}


# Remove supplier
# Parameters:
#		Code		- Supplier code
sub removeSupplier
{
	my ($detail) = @_;


	# Verify supplier code
	if (!defined($detail->{'Code'}) || $detail->{'Code'} eq "") {
		setError("No (or invalid) supplier code provided");
		return ERR_PARAM;
	}

	# Check if supplier exists & pull
	my $tmp;
	$tmp->{'Code'} = $detail->{'Code'};
	my $supplier = getSupplier($tmp);
	if (ref $supplier ne "HASH") {
		setError(Error());
		return $supplier;
	}

	DBBegin();

	# Remove supplier
	my $sth = DBDo("DELETE FROM supplier_addresses WHERE SupplierID = ".DBQuote($supplier->{'ID'})."");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		DBRollback();
		return ERR_DB;
	}

	$sth = DBDo("DELETE FROM supplier_phone_numbers WHERE SupplierID = ".DBQuote($supplier->{'ID'})."");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		DBRollback();
		return ERR_DB;
	}

	$sth = DBDo("DELETE FROM supplier_email_addresses WHERE SupplierID = ".DBQuote($supplier->{'ID'})."");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		DBRollback();
		return ERR_DB;
	}

	$sth = DBDo("DELETE FROM suppliers WHERE ID = ".DBQuote($supplier->{'ID'})."");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		DBRollback();
		return ERR_DB;
	}


	DBCommit();

	return 0;
}



## @fn getGLAccountEntries($detail)
# Function to return a suppliers GL account entries
#
# @param detail Parameter hash ref
# @li Code Supplier code
# @li StartDate Optional start date
# @li EndDate Optional end date
# @li BalanceBroughtForward Optional balance brought forward
#
# @returns GL account entries, @see wiaflos::server::core::GL::getGLAccountEntries
sub getGLAccountEntries
{
	my ($detail) = @_;


	# Verify supplier code
	if (!defined($detail->{'Code'}) || $detail->{'Code'} eq "") {
		setError("No (or invalid) supplier code provided");
		return ERR_PARAM;
	}

	# Check if supplier exists & pull
	my $tmp;
	$tmp->{'Code'} = $detail->{'Code'};
	my $supplier = getSupplier($tmp);
	if (ref $supplier ne "HASH") {
		setError(Error());
		return $supplier;
	}

	# Build our request
	$tmp = undef;
	$tmp->{'AccountID'} = $supplier->{'GLAccountID'};
	$tmp->{'StartDate'} = $detail->{'StartDate'};
	$tmp->{'EndDate'} = $detail->{'EndDate'};
	$tmp->{'BalanceBroughtForward'} = $detail->{'BalanceBroughtForward'};

	# Get results
	my $res = wiaflos::server::core::GL::getGLAccountEntries($tmp);
	if (ref $res ne "ARRAY") {
		setError(wiaflos::server::core::GL::Error());
	}

	return $res;
}



1;
# vim: ts=4
