# Inventory functions
# Copyright (C) 2009-2014, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.




package wiaflos::server::core::Inventory;

use strict;
use warnings;


use wiaflos::constants;
use awitpt::db::dblayer;
use wiaflos::server::core::GL;
use wiaflos::server::core::Tax;

# Whole money transactions, precision is two
use Math::BigFloat;
Math::BigFloat::precision(-2);



# Our current error message
my $error = "";

# Set current error message
# Args: error_message
sub setError
{
	my $err = shift;
	my ($package,$filename,$line) = caller;
	my (undef,undef,undef,$subroutine) = caller(1);

	# Set error
	$error = "$subroutine($line): $err";
}

# Return current error message
# Args: none
sub Error
{
	my $err = $error;

	# Reset error
	$error = "";

	# Return error
	return $err;
}



# Check if inventory item exists
# Backend function, takes 1 parameter which is the inventory item ID
sub inventoryIDExists
{
	my $inventoryID = shift;


	# Select inventory item rows
	my $rows = DBSelectNumResults(" FROM inventory WHERE ID = ".DBQuote($inventoryID));
	if (!$rows) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	return $rows > 0 ? 1 : 0;
}



# Check if inventory item ref exists
sub inventoryCodeExists
{
	my $code = shift;


	# Select item
	my $rows = DBSelectNumResults("FROM inventory WHERE Code = ".DBQuote($code));
	if (!defined($rows)) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	return $rows > 0 ? 1 : 0;
}



# Get inventory ID from code
sub getInventoryItemIDFromCode
{
	my $code = shift;


	# Select inventory item
	my $sth = DBSelect("
		SELECT
			ID
		FROM
			inventory
		WHERE
			Code = ".DBQuote($code)."
	");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	my $row = hashifyLCtoMC($sth->fetchrow_hashref(),qw( ID ));
	DBFreeRes($sth);

	# Check we got a result
	if (!defined($row)) {
		setError("Error finding inventory item '$code'");
		return ERR_NOTFOUND;
	}

	return $row->{'ID'};
}



# Backend function to build item hash
sub sanitizeRawInventoryItem
{
	my $rawData = shift;


	my $item;

	$item->{'ID'} = $rawData->{'ID'};
	$item->{'Code'} = $rawData->{'Code'};

	# Decide what the type should be
	if ($rawData->{'Type'} eq "1") {
		$item->{'Type'} = "product";
		# Pull in product mode
		if ($rawData->{'Mode'} eq "1") {
			$item->{'Mode'} = "bulk";
		} elsif ($rawData->{'Mode'} eq "2") {
			$item->{'Mode'} = "track";
		} else {
			$item->{'Mode'} = "unknown";
		}
	} elsif ($rawData->{'Type'} eq "2") {
		$item->{'Type'} = "service";
		$item->{'Mode'} = "";
	} else {
		$item->{'Type'} = "unknown";
	}

	$item->{'Description'} = $rawData->{'Description'};

	$item->{'GLIncomeAccountID'} = $rawData->{'GLIncomeAccountID'};
	$item->{'GLIncomeAccountNumber'} = defined($rawData->{'GLIncomeAccountID'}) ?
			wiaflos::server::core::GL::getGLAccountNumberFromID($rawData->{'GLIncomeAccountID'}) :
			undef;

	$item->{'GLAssetAccountID'} = $rawData->{'GLAssetAccountID'};
	$item->{'GLAssetAccountNumber'} = defined($rawData->{'GLAssetAccountID'}) ?
			wiaflos::server::core::GL::getGLAccountNumberFromID($rawData->{'GLAssetAccountID'}) :
			undef;

	$item->{'GLExpenseAccountID'} = $rawData->{'GLExpenseAccountID'};
	$item->{'GLExpenseAccountNumber'} = defined($rawData->{'GLExpenseAccountID'}) ?
			wiaflos::server::core::GL::getGLAccountNumberFromID($rawData->{'GLExpenseAccountID'}) :
			undef;

	$item->{'SerialNumber'} = $rawData->{'SerialNumber'};

	$item->{'Unit'} = $rawData->{'Unit'};
	$item->{'SellPrice'} = $rawData->{'SellPrice'};
	$item->{'TaxTypeID'} = $rawData->{'TaxTypeID'};

	# Decide if we discountable or not
	if ($rawData->{'Discountable'} eq "1") {
		$item->{'Discountable'} = "yes";
	} elsif ($rawData->{'Discountable'} eq "0") {
		$item->{'Discountable'} = "no";
	} else {
		$item->{'Discountable'} = "unknown";
	}

	$item->{'TaxMode'} = wiaflos::server::core::Tax::getTaxModeStr($rawData->{'TaxMode'});

	return $item;
}



# Backend function to build item hash for a tracking item
sub sanitizeRawTrackingItem
{
	my $rawData = shift;


	my $item;

	$item->{'ID'} = $rawData->{'ID'};

	$item->{'ParentInventoryTrackingID'} = $rawData->{'ParentInventoryTrackingID'};

	$item->{'InventoryID'} = $rawData->{'InventoryID'};

	$item->{'GLTransactionID'} = $rawData->{'GLTransactionID'};

	# If we have a transaction ID, use it
	if (defined($rawData->{'GLTransactionID'}) && $rawData->{'GLTransactionID'} > 0) {
		my $data;
		$data->{'ID'} = $rawData->{'GLTransactionID'};
		my $gltrans = wiaflos::server::core::GL::getGLTransaction($data);
		$item->{'GLTransactionDate'} = $gltrans->{'TransactionDate'};
	}

	$item->{'SerialNumber'} = $rawData->{'SerialNumber'};

	$item->{'QtyChange'} = $rawData->{'QtyChange'};
	$item->{'Price'} = $rawData->{'Price'};

	$item->{'Closed'} = $rawData->{'Closed'};

	return $item;
}



# Backend to get a list of stock for a specific inventory item
# Params:
# 	<an inventory item returned by getInventoryItem()>
# 	or
# 		ID - ID of inventory item
# 		Mode - track or bulk
# 		SerialNumber - Serial number of item
#
sub getInventoryStockList
{
	my $detail = shift;


	# List of stock items required
	my %stockList;

	# Check item mode
	if ($detail->{'Mode'} eq "track") {
		# Make sure we have a serial number
		if (!defined($detail->{'SerialNumber'})) {
			setError("No serial number provided for stock listing request");
			return ERR_PARAM;
		}

		# Grab free inventory item list
		my $sth = DBSelect("
				SELECT
					ID, QtyChange, Price
				FROM
					inventory_tracking
				WHERE
					InventoryID = ".DBQuote($detail->{'ID'})."
					AND Closed = 0
					AND ParentInventoryTrackingID IS NULL
					AND SerialNumber = ".DBQuote($detail->{'SerialNumber'})."
		");

		if (!$sth) {
			setError(awitpt::db::dblayer::Error());
			return ERR_DB;
		}

		# Look for free inventory items
		while (my $row = hashifyLCtoMC($sth->fetchrow_hashref(),qw( ID QtyChange Price ))) {
			# Total stock quantity
			$stockList{$row->{'ID'}}{'TotalQuantity'} = Math::BigFloat->new();
			$stockList{$row->{'ID'}}{'TotalQuantity'}->precision(-4);
			$stockList{$row->{'ID'}}{'TotalQuantity'}->badd($row->{'QtyChange'});

			# Total quantity value
			$stockList{$row->{'ID'}}{'TotalValue'} = Math::BigFloat->new();
			$stockList{$row->{'ID'}}{'TotalValue'}->badd($row->{'Price'});

			# This is adjusted below if we have any allocations
			$stockList{$row->{'ID'}}{'CurrentQuantity'} = $stockList{$row->{'ID'}}{'TotalQuantity'}->copy();
			$stockList{$row->{'ID'}}{'CurrentValue'} = $stockList{$row->{'ID'}}{'TotalValue'}->copy();
		}
		DBFreeRes($sth);

		# Loop with stock list and get current quantity and value of that quantity
		foreach my $it (keys %stockList) {
			# Process inventory tracking items
			$sth = DBSelect("
					SELECT
						QtyChange, Price
					FROM
						inventory_tracking
					WHERE
						ParentInventoryTrackingID = ".DBQuote($it)."
			");
			# Check for error
			if (!$sth) {
				setError(awitpt::db::dblayer::Error());
				return ERR_DB;
			}

			# Loop
			while (my $row = hashifyLCtoMC($sth->fetchrow_hashref(),qw( QtyChange Price ))) {
				# This will probably be reductions...   a - added to a + is a -, so we use badd()
				$stockList{$it}{'CurrentQuantity'}->badd($row->{'QtyChange'});
				$stockList{$it}{'CurrentValue'}->badd($row->{'Price'});
			}
			DBFreeRes($sth);

			# Sanity check, the quantity on an unclosed item can NEVER be < 1
			if ($stockList{$it}{'CurrentQuantity'}->is_zero() || $stockList{$it}{'CurrentQuantity'}->is_neg()) {
				setError("Quantity balance on an unclosed inventory tracking item CANNOT be < 1");
				return ERR_INVALID_QTY;
			}
		}


	# Bulk calculation
	} elsif ($detail->{'Mode'} eq "bulk") {
		# Grab free inventory item list
		my $sth = DBSelect("
				SELECT
					ID, QtyChange, Price
				FROM
					inventory_tracking
				WHERE
					InventoryID = ".DBQuote($detail->{'ID'})."
					AND Closed = 0
		");

		if (!$sth) {
			setError(awitpt::db::dblayer::Error());
			return ERR_DB;
		}

		# Loop and add up
		while (my $row = hashifyLCtoMC($sth->fetchrow_hashref(),qw( ID QtyChange Price ))) {
			# Current stock quantity
			$stockList{'0'}{'CurrentQuantity'} = Math::BigFloat->new();
			$stockList{'0'}{'CurrentQuantity'}->precision(-4);
			$stockList{'0'}{'CurrentQuantity'}->badd($row->{'QtyChange'});

			# Current quantity value
			$stockList{'0'}{'CurrentValue'} = Math::BigFloat->new();
			$stockList{'0'}{'CurrentValue'}->badd($row->{'Price'});
		}
		DBFreeRes($sth);


	# Unknown stock mode
	} else {
			setError("Unknown inventory item mode when getting stock listing");
			return ERR_PARAM;
	}

	return \%stockList;
}



# Backend function to adjust stock
# Parameters:
#		ID		- Inventory item ID
#		GLTransactionID	- GL transaction ID
#		QtyChange	- Quantity change. This will be zero if its an override/update.
#
#
# 	- Method of calling 1: Positive adjustment (purchase)
#		Cost	- Cost of item
#
#
#	- Method of calling 2: Negative adjustment (invoice or supplier creditnote)
#		StockAdjustmentList - Array of stock items, these items have a QuantityNeeded which is POSITIVE!!!!
#				reason for this is its the stock needed to complete the adjustment, not the
#				adjustment itself.
#
#				Format for this is:
#							'InventoryTrackingID' => 'x',
#							'Quantity' => 'x.yyyy',
#							'Cost' => 'x.yy'
#
#	- Method of calling 3: Zero qty change (supplier credit note)
#		This basically just adjusts the price of the item...
#		Required:
#			ParentInventoryTrackingID 	- must be provided, which is the parent (or inbound) inventory item
#			Cost 	- Cost to adjust this by
#		Optional:
#			GLTransactionID
#			SerialNumber
#
#
# Returns:
# 		An array of hashes containing inventory tracking ID's
sub adjustInventoryStock
{
	my ($detail) = @_;


	# Verify params
	if (!defined($detail->{'ID'}) || $detail->{'ID'} < 1) {
		setError("No item ID given or invalid ID provided");
		return ERR_PARAM;
	}

	if (!defined($detail->{'GLTransactionID'}) || $detail->{'GLTransactionID'} < 1) {
		setError("No GL transaction ID provided or invalid ID provided for stock adjustment on item ID '".$detail->{'ID'}."'");
		return ERR_PARAM;
	}

	if (!defined($detail->{'QtyChange'})) {
		setError("No qty change provided or invalid qty change provided for stock adjustment on item ID '".$detail->{'ID'}."'");
		return ERR_PARAM;
	}

	# Return hashref
	my @ret;

	# Pull in inventory item
	my $data;
	$data->{'ID'} = $detail->{'ID'};
	my $inventoryItem = getInventoryItem($data);
	if (ref $inventoryItem ne "HASH") {
		setError(Error());
		return $inventoryItem;
	}

	# We only adjust products
	if ($inventoryItem->{'Type'} ne "product") {
		# Forge this...
		my $tmp;
		$tmp->{'InventoryTrackingID'} = 0;
		push(@ret,$tmp);
		return \@ret;
	}

	# Pull in with math library
	my $qtyChange = Math::BigFloat->new();
	$qtyChange->precision(-4);
	$qtyChange->badd($detail->{'QtyChange'});

	DBBegin();

	# Positive number, means this is stock comming in
	if ($qtyChange->is_pos()) {

		# We need a cost
		if (!defined($detail->{'Cost'})) {
			setError("Cost was not provided for positive stock adjustment on item ID '".$detail->{'ID'}."'");
			return ERR_PARAM;
		}

		# Insert into inventory tracking table
		my $qtyChangeStr = $qtyChange->bstr();
		my $res = DBDo("
				INSERT INTO inventory_tracking
						(ParentInventoryTrackingID,InventoryID,GLTransactionID,SerialNumber,QtyChange,Price,Closed)
					VALUES
						(
							NULL,
							".DBQuote($inventoryItem->{'ID'}).",
							".DBQuote($detail->{'GLTransactionID'}).",
							".DBQuote($detail->{'SerialNumber'}).",
							".DBQuote($qtyChangeStr).",
							".DBQuote($detail->{'Cost'}).",
							0
						)
		");

		if (!$res) {
			setError(awitpt::db::dblayer::Error());
			DBRollback();
			return ERR_DB;
		}

		# Grab last ID
		my $tmp;
		$tmp->{'InventoryTrackingID'} = DBLastInsertID("inventory_tracking","ID");
		$tmp->{'QtyChange'} = $qtyChangeStr;
		$tmp->{'Price'} = $detail->{'Cost'};
		push(@ret,$tmp);


	# If change is not zero its inbound stock
	} elsif ($qtyChange->is_zero()) {
		# We need a cost
		if (!defined($detail->{'Cost'})) {
			setError("Cost was not provided for zero quantity stock adjustment on item ID '".$detail->{'ID'}."'");
			return ERR_PARAM;
		}

		# We need a parent inventory tracking ID
		if (!defined($detail->{'ParentInventoryTrackingID'})) {
			setError("ParentInventoryTrackingID was not provided for zero quantity stock adjustment on item ID '".$detail->{'ID'}."'");
			return ERR_PARAM;
		}

		# Insert into inventory tracking table
		my $res = DBDo("
				INSERT INTO inventory_tracking
						(ParentInventoryTrackingID,InventoryID,GLTransactionID,SerialNumber,QtyChange,Price,Closed)
					VALUES
						(
							".DBQuote($detail->{'ParentInventoryTrackingID'}).",
							".DBQuote($inventoryItem->{'ID'}).",
							".DBQuote($detail->{'GLTransactionID'}).",
							".DBQuote($detail->{'SerialNumber'}).",
							0,
							".DBQuote($detail->{'Cost'}).",
							0
						)
		");

		if (!$res) {
			setError(awitpt::db::dblayer::Error());
			DBRollback();
			return ERR_DB;
		}

		# Grab last ID
		my $tmp;
		$tmp->{'InventoryTrackingID'} = DBLastInsertID("inventory_tracking","ID");
		$tmp->{'QtyChange'} = "0";
		$tmp->{'Price'} = $detail->{'Cost'};
		push(@ret,$tmp);


	# This is a stock reduction
	} elsif ($qtyChange->is_neg()) {
		# We need a stock adjustment list
		if (!defined($detail->{'StockAdjustmentList'})) {
			setError("StockAdjustmentList was not provided for negative stock adjustment on item ID '".$detail->{'ID'}."'");
			return ERR_PARAM;
		}

		# Grab stock list, add in serial number so we get right one
		my $tmp = $inventoryItem;
		$tmp->{'SerialNumber'} = $detail->{'SerialNumber'};
		my $stockList = getInventoryStockList($tmp);
		if (ref $stockList ne "HASH") {
			setError(Error());
			return $stockList;
		}

		# Check if we still have all the stock
		foreach my $stockAdjustItem (@{$detail->{'StockAdjustmentList'}}) {
				my $inventoryTrackingID = $stockAdjustItem->{'InventoryTrackingID'};

				# Pull in stock adjustment quantity
				my $stockAdjustQuantity = Math::BigFloat->new();
				$stockAdjustQuantity->precision(-4);
				$stockAdjustQuantity->badd($stockAdjustItem->{'Quantity'});

				# Make sure we still have stock!
				my $stockBalance = $stockList->{$inventoryTrackingID}{'CurrentQuantity'}->copy();
				$stockBalance->bsub($stockAdjustQuantity);
				if ($stockBalance->is_neg()) {
					setError("Stock adjustment would exceed available stock quantity on '".$inventoryItem->{'Code'}.
							"' by '".$stockBalance->copy()->babs()->bstr()."'");
					DBRollback();
					return ERR_OVERALLOC_QTY;
				}

				# If we've maxed the stock close the inventory tracking
				#  > 0 so we don't include bulk items
				if ($stockBalance->is_zero() && $inventoryTrackingID > 0) {
					# Close parent tracking item
					my $res = DBDo("
							UPDATE
								inventory_tracking
							SET
								Closed = 1
							WHERE
								ID = ".DBQuote($inventoryTrackingID)."
					");
					if (!$res) {
						setError(awitpt::db::dblayer::Error());
						DBRollback();
						return ERR_DB;
					}
				}

				# Get total cost
				my $stockAdjustCostPrice =  Math::BigFloat->new($stockAdjustItem->{'Cost'});

				# Record stock change
				my $qtyChange = $stockAdjustQuantity->copy->bneg()->bstr();
				my $totalPrice = $stockAdjustCostPrice->copy->bneg()->bstr();
				my $res = DBDo("
						INSERT INTO inventory_tracking
								(ParentInventoryTrackingID,InventoryID,GLTransactionID,SerialNumber,QtyChange,Price)
							VALUES
								(
									".DBQuote($inventoryTrackingID).",
									".DBQuote($inventoryItem->{'ID'}).",
									".DBQuote($detail->{'GLTransactionID'}).",
									".DBQuote($detail->{'SerialNumber'}).",
									".DBQuote($qtyChange).",
									".DBQuote($totalPrice)."
								)
				");

				if (!$res) {
					setError(awitpt::db::dblayer::Error());
					DBRollback();
					return ERR_DB;
				}

				# Grab last ID
				my $tmp;
				$tmp->{'InventoryTrackingID'} = DBLastInsertID("inventory_tracking","ID");
				$tmp->{'QtyChange'} = $qtyChange;
				$tmp->{'Price'} = $totalPrice;
				push(@ret,$tmp);
		}
	}

	DBCommit();

	return \@ret;
}



# Return an array of inventory items
sub getInventory
{
	my @inventory = ();


	# Return list of inventory items
	my $sth = DBSelect("
		SELECT
			ID, Code, Type, Mode, Description, GLIncomeAccountID, GLAssetAccountID, GLExpenseAccountID, Unit,
			SellPrice, Discountable, TaxTypeID, TaxMode
		FROM
			inventory
	");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	# Fetch rows
	while (my $row = hashifyLCtoMC($sth->fetchrow_hashref(),
			qw( ID Code Type Mode Description GLIncomeAccountID GLAssetAccountID GLExpenseAccountID Unit
				SellPrice Discountable TaxTypeID TaxMode )
	)) {
		push(@inventory,sanitizeRawInventoryItem($row));
	}

	DBFreeRes($sth);

	return \@inventory;
}



# Return an array containing a list of inventory stock items to match the Qty we require
# Optional:
#		ID		- Item ID
#		Code	- Item code
#		SerialNumber	- Item serial number
#
#		Quantity - Quantity we need, this means we want to actually return items
sub getInventoryStockItem
{
	my ($detail) = @_;


	# Fetch inventory item
	my $tmp;
	$tmp->{'ID'} = $detail->{'ID'};
	$tmp->{'Code'} = $detail->{'Code'};
	my $inventoryItem = getInventoryItem($tmp);
	if (ref $inventoryItem ne "HASH") {
		setError(Error());
		return $inventoryItem;
	}

	# Decide on cost price
	if ($inventoryItem->{'Type'} eq "product") { # Product

		# This is the quantity we need
		my $quantity = Math::BigFloat->new();
		$quantity->precision(-4);
		$quantity->badd($detail->{'Quantity'});

		# Grab stock list
		my $tmp = $inventoryItem;
		$tmp->{'SerialNumber'} = $detail->{'SerialNumber'};
		my $stockList = getInventoryStockList($tmp);
		if (ref $stockList ne "HASH") {
			setError(Error());
			return $stockList;
		}

		# Loop with stock list and decide what we need
		foreach my $it (keys %{$stockList}) {
			# Work out cost price
			my $unitCostPrice = $stockList->{$it}{'CurrentValue'}->copy();
			$unitCostPrice->precision(-4);
			$unitCostPrice->bdiv($stockList->{$it}{'CurrentQuantity'});

			# Build stock item to add
			my %stockItem = (
					'InventoryTrackingID' => $it,
					'QuantityNeeded' => $stockList->{$it}{'CurrentQuantity'}->bstr(),
					'UnitCost' => $unitCostPrice->bstr(),
					'Cost' => $stockList->{$it}{'CurrentValue'}->bstr(),
			);

			# Check how much of the quantity required we can fill
			my $qbalance = $quantity->copy()->bsub($stockList->{$it}{'CurrentQuantity'});

			# We have an exact match
			if ($qbalance->is_zero()) {
				$quantity->bsub($stockList->{$it}{'CurrentQuantity'});

			# We need more
			} elsif ($qbalance->is_pos()) {
				$quantity->bsub($stockList->{$it}{'CurrentQuantity'});

			# This item will fill up our requirement
			} elsif ($qbalance->is_neg()) {
				# Special case, adjust how much stock we need & balance
				$stockItem{'QuantityNeeded'} = $quantity->copy()->bstr();
				$quantity->bzero();
				# Recalculate costing
				my $newCostPrice = $unitCostPrice->copy()->bmul($stockItem{'QuantityNeeded'});
				$newCostPrice->precision(-2);
				$stockItem{'Cost'} = $newCostPrice->bstr();
			}

			# Add to list
			push(@{$inventoryItem->{'StockRequired'}},\%stockItem);

			# Last if we have a zero balance
			last if ($quantity->is_zero());
		}

		# Check if we don't have enough items
		if (!$quantity->is_zero()) {
			my $serial = $detail->{'SerialNumber'} ? $detail->{'SerialNumber'} : "";
			setError("Cannot find tracked inventory item '".$inventoryItem->{'Code'}."' with serial number '$serial' in stock. We need '".
					$quantity->bstr()."' item(s) more.");
			return ERR_NOTFOUND;
		}


	} elsif ($inventoryItem->{'Type'} eq "service") { # Service
		# No such thing as a cost price here, so leave undef

	} else { # Unknown
		setError("Invalid item type for item '".$detail->{'Code'}."' found");
		return ERR_UNKNOWN;
	}

	return $inventoryItem;
}



# Return a hash containing an inventory item
# Optional:
#		ID		- Item ID
#		Code	- Item code
sub getInventoryItem
{
	my ($data) = @_;


	my $itemID;

	# Check which 'mode' we operating in
	if (!defined($data->{'ID'}) || $data->{'ID'} < 1) {
		# Verify item ref
		if (!defined($data->{'Code'}) || $data->{'Code'} eq "") {
			setError("No (or invalid) item code provided");
			return ERR_PARAM;
		}

		# Check if item exists
		if (($itemID = getInventoryItemIDFromCode($data->{'Code'})) < 1) {
			setError(Error());
			return $itemID;
		}
	} else {
		$itemID = $data->{'ID'};
	}

	# Verify inventory item ID
	if (!$itemID || $itemID < 1) {
		setError("No (or invalid) inventory code/id provided");
		return ERR_PARAM;
	}

	# Return list of inventory items
	my $sth = DBSelect("
		SELECT
			ID, Code, Type, Mode, Description, GLIncomeAccountID, GLAssetAccountID, GLExpenseAccountID, Unit,
			SellPrice, Discountable, TaxTypeID, TaxMode
		FROM
			inventory
		WHERE
			ID = ".DBQuote($itemID)."
	");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	# Fetch row
	my $item = hashifyLCtoMC($sth->fetchrow_hashref(),
			qw( ID Code Type Mode Description GLIncomeAccountID GLAssetAccountID GLExpenseAccountID Unit
				SellPrice Discountable TaxTypeID TaxMode )
	);
	DBFreeRes($sth);

	return sanitizeRawInventoryItem($item);
}



# Function to get tracking information for a specific inventory item
# Optional:
#		ID		- Item ID
#		Code	- Item code
#
#		SerialNumber - Limit to a specific serial number
sub getInventoryItemMovement
{
	my $data = shift;


	my $itemID;

	# Check which 'mode' we operating in
	if (!defined($data->{'ID'}) || $data->{'ID'} < 1) {
		# Verify item ref
		if (!defined($data->{'Code'}) || $data->{'Code'} eq "") {
			setError("No (or invalid) item code provided");
			return ERR_PARAM;
		}

		# Check if item exists
		if (($itemID = getInventoryItemIDFromCode($data->{'Code'})) < 1) {
			setError(Error());
			return $itemID;
		}
	} else {
		$itemID = $data->{'ID'};
	}

	# Verify inventory item ID
	if (!$itemID || $itemID < 1) {
		setError("No (or invalid) inventory code/id provided");
		return ERR_PARAM;
	}

	# List of stock items
	my @resultList;

	my $extraSQL = "";

	# Make sure we have a serial number
	if (defined($data->{'SerialNumber'})) {
		$extraSQL = " AND inventory_tracking.SerialNumber = ".DBQuote($data->{'SerialNumber'});
	}

	# Grab free inventory item list
	my $sth = DBSelect("
			SELECT
				ID, GLTransactionID, SerialNumber, QtyChange, Price
			FROM
				inventory_tracking
			WHERE
				InventoryID = ".DBQuote($itemID)."
				$extraSQL
	");

	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	# Build result list
	while (my $row = hashifyLCtoMC($sth->fetchrow_hashref(), qw( ID GLTransactionID SerialNumber QtyChange Price ))) {
		push(@resultList,sanitizeRawTrackingItem($row));
	}
	DBFreeRes($sth);


	return \@resultList;
}



# Create inventory item
# Parameters:
#		Code			- Code
#		Type			- Account type, either "product" or "service"
#		Mode			- If Type == product, then this must be set to "bulk" or "track". Ignored for service.
#		Description		- Description
#		GLIncomeAccountNumber	- GL income account
#		GLAssetAccountNumber - GL asset account
#		GLExpenseAccountNumber	- GL expense account
#		SellPrice		- Selling price
#		TaxTypeID		- Tax type ID
#		TaxMode			- Tax mode, "incl" or "excl"
# Optional:
#		Unit			- Uni
#		Discountable	- Either service is discountable or not
sub createInventoryItem
{
	my ($data) = @_;


	my @extraCols = ();
	my @extraData = ();


	# Verify ref
	if (!defined($data->{'Code'}) || $data->{'Code'} eq "") {
		setError("No item code provided");
		return ERR_PARAM;
	}

	# Verify type
	if (!defined($data->{'Type'}) || $data->{'Type'} eq "") {
		setError("No type provided for item '".$data->{'Code'}."'");
		return ERR_PARAM;
	}

	# Verify description
	if (!defined($data->{'Description'}) || $data->{'Description'} eq "") {
		setError("No description provided for item '".$data->{'Code'}."'");
		return ERR_PARAM;
	}

	# Verify income accout number
	if (!defined($data->{'GLIncomeAccountNumber'}) || $data->{'GLIncomeAccountNumber'} eq "") {
		setError("Income account number not provided for item '".$data->{'Code'}."'");
		return ERR_PARAM;
	}

	# Verify sell price
	if (!defined($data->{'SellPrice'}) || $data->{'SellPrice'} eq "") {
		setError("Sell price not provided for item '".$data->{'Code'}."'");
		return ERR_PARAM;
	}

	# Decide on the type id
	my $typeID;
	my $modeID;
	if (lc($data->{'Type'}) eq "product") {
		$typeID = 1;
		# Decide on mode
		if (defined($data->{'Mode'})) {
			if ($data->{'Mode'} eq "bulk") {
				$modeID = DBQuote(1);
			} elsif ($data->{'Mode'} eq "track") {
				$modeID = DBQuote(2);
			} else {
				setError("Product mode '".lc($data->{'Type'})."' not supported for item '".$data->{'Code'}."'");
				return ERR_PARAM;
			}
		} else {
			setError("Product mode not provided for item '".$data->{'Code'}."'");
			return ERR_PARAM;
		}
	} elsif (lc($data->{'Type'}) eq "service") {
		$typeID = 2;
		$modeID = "NULL";
	} else {
		setError("Unknown inventory item type '".lc($data->{'Type'})."' for item '".$data->{'Code'}."'");
		return ERR_PARAM;
	}

	# Check for conflicts
	if ((my $res = inventoryCodeExists($data->{'Code'})) != 0) {
		# If found err
		if ($res == 1) {
			setError("Inventory item code '".$data->{'Code'}."' already exists");
			return ERR_CONFLICT;
		} else {
			setError(Error());
		}
		# else err with result
		return $res;
	}

	# Pull in amount
	my $cleanAmount = Math::BigFloat->new($data->{'SellPrice'});

	# Verify tax type id
	if (!defined($data->{'TaxTypeID'}) || $data->{'TaxTypeID'} < 1) {
		setError("Tax type ID not provided for item '".$data->{'Code'}."'");
		return ERR_PARAM;
	}

	# Grab income account ID
	my $incomeGLAccID;
	if (($incomeGLAccID = wiaflos::server::core::GL::getGLAccountIDFromNumber($data->{'GLIncomeAccountNumber'})) < 1) {
		setError(wiaflos::server::core::GL::Error());
		return $incomeGLAccID;
	}

	# Check we an income account
	my $catMatch = wiaflos::server::core::GL::checkGLAccountFinCat($incomeGLAccID,"D01");
	if ($catMatch == 0) {
		setError("Inventory income account '".$data->{'GLIncomeAccountNumber'}."' is not of type income for item '".$data->{'Code'}."'");
		return ERR_USAGE;
	} elsif ($catMatch < 0) {
		setError(wiaflos::server::core::GL::Error());
		return $catMatch;
	}

	# Make sure inventory GL account is a asset account
	if (defined($data->{'GLAssetAccountNumber'}) && $data->{'GLAssetAccountNumber'} ne "") {
		# Grab inventory account ID
		my $assetGLAccID;
		if (($assetGLAccID = wiaflos::server::core::GL::getGLAccountIDFromNumber($data->{'GLAssetAccountNumber'})) < 1) {
			setError(wiaflos::server::core::GL::Error());
			return $assetGLAccID;
		}

		# Check we an asset account
		my $catMatch = wiaflos::server::core::GL::checkGLAccountFinCat($assetGLAccID,"A01");
		if ($catMatch == 0) {
			setError("Inventory asset account '".$data->{'GLAssetAccountNumber'}."' is not of type asset for item '".$data->{'Code'}."'");
			return ERR_USAGE;
		} elsif ($catMatch < 0) {
			setError(wiaflos::server::core::GL::Error());
			return $catMatch;
		}

		push(@extraCols,'GLAssetAccountID');
		push(@extraData,DBQuote($assetGLAccID));
	}


	# Make sure expense GL account is a expense account
	if (defined($data->{'GLExpenseAccountNumber'}) && $data->{'GLExpenseAccountNumber'} ne "") {
		# Grab expense account ID
		my $expenseGLAccID;
		if (($expenseGLAccID = wiaflos::server::core::GL::getGLAccountIDFromNumber($data->{'GLExpenseAccountNumber'})) < 1) {
			setError(wiaflos::server::core::GL::Error());
			return $expenseGLAccID;
		}

		# Check we an expense account
		my $catMatch = wiaflos::server::core::GL::checkGLAccountFinCat($expenseGLAccID,"E01");
		if ($catMatch == 0) {
			setError("Inventory expense account '".$data->{'GLExpenseAccountNumber'}."' is not of type expense for item '".$data->{'Code'}."'");
			return ERR_USAGE;
		} elsif ($catMatch < 0) {
			setError(wiaflos::server::core::GL::Error());
			return $catMatch;
		}

		push(@extraCols,'GLExpenseAccountID');
		push(@extraData,DBQuote($expenseGLAccID));
	}

	# Check if tax type exists
	if ((my $res = wiaflos::server::core::Tax::taxTypeIDExists($data->{'TaxTypeID'})) != 1) {
		# If not exist, err
		if ($res == 0) {
			setError("Tax type '".$data->{'TaxTypeId'}."' does not exist");
			return ERR_NOTFOUND;
		}
		# else err with result & set error
		setError(wiaflos::server::core::Tax::Error());
		return $res;
	}

	# Decide on the tax mode
	my $taxMode;
	if (lc($data->{'TaxMode'}) eq "incl") {
		$taxMode = 1;
	} elsif (lc($data->{'TaxMode'}) eq "excl") {
		$taxMode = 2;
	} else {
		setError("Unknown inventory tax mode '".$data->{'TaxMode'}."' for item '".$data->{'Code'}."'");
		return ERR_USAGE;
	}

	# If we have an item reference, add it aswell
	if (defined($data->{'Unit'}) && $data->{'Unit'} ne "") {
		push(@extraCols,'Unit');
		push(@extraData,DBQuote($data->{'Unit'}));
	}

	# Decide if discountable or not
	if (defined($data->{'Discountable'})) {
		push(@extraCols,'Discountable');
		if (lc($data->{'Discountable'}) eq "yes") {
			push(@extraData,DBQuote(1));
		} elsif (lc($data->{'Discountable'}) eq "no") {
			push(@extraData,DBQuote(0));
		} else {
			setError("Unknown value for Discountable '".$data->{'Discountable'}."' regarding item '".$data->{'Code'}."'");
			return ERR_USAGE;
		}
	}


	# Pull in extra data
	my $extraCols = "";
	my $extraData = "";
	if (@extraCols > 0 && @extraData > 0) {
		$extraCols .= ',' . join(',',@extraCols);
		$extraData .= ',' . join(',',@extraData);
	}

	# Create inventory item ID
	my $sql = "
		INSERT INTO inventory
				(Code,Type,Mode,Description,GLIncomeAccountID,SellPrice,TaxTypeID,TaxMode$extraCols)
			VALUES
				(
					".DBQuote($data->{'Code'}).",
					".DBQuote($typeID).",
					$modeID,
					".DBQuote($data->{'Description'}).",
					".DBQuote($incomeGLAccID).",
					".DBQuote($cleanAmount->bstr()).",
					".DBQuote($data->{'TaxTypeID'}).",
					".DBQuote($taxMode)."
					$extraData
				)
	";
	if (!(my $sth = DBDo($sql))) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	# Grab last ID
	my $ID = DBLastInsertID("inventory","ID");

	return $ID;
}



# Remove inventory item
# Parameters:
#		Code	- Inventory item reference to remove
sub removeInventoryItem
{
	my ($data) = @_;


	# Verify params
	if (!defined($data->{'Code'}) || $data->{'Code'} eq "") {
		setError("No item code provided");
		return ERR_PARAM;
	}

	# Check if inventory item exists
	my $inventoryID;
	if (($inventoryID = getInventoryItemIDFromCode($data->{'Code'})) < 1) {
		setError(Error());
		return $inventoryID;
	}

	# Remove inventory item
	my $sth = DBDo("
		DELETE FROM
			inventory
		WHERE
			ID = ".DBQuote($inventoryID)."
	");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	return 0;
}



# Update inventory item
# Parameters:
#		Code			- Inventory item reference to update
# Optional:
#		Description		- Description
#		GLIncomeAccountNumber	- GL income account
#		GLAssetAccountNumber - GL asset account
#		GLExpenseAccountNumber	- GL expense account
#		SellPrice		- Selling price
#		Unit			- Unit
#		Discountable	- Either service is discountable or not
sub updateInventoryItem
{
	my ($data) = @_;


	# Verify params
	if (!defined($data->{'Code'}) || $data->{'Code'} eq "") {
		setError("No item code provided");
		return ERR_PARAM;
	}

	my @updates;

	# Check for what we updating
	if (defined($data->{'Description'})) {
		push(@updates,sprintf('Description = %s',DBQuote($data->{'Description'})));
	}
	# Check we an income account
	if (defined($data->{'GLIncomeAccountNumber'}) && $data->{'GLIncomeAccountNumber'} ne "") {
		my $incomeGLAccID;
		if (($incomeGLAccID = wiaflos::server::core::GL::getGLAccountIDFromNumber($data->{'GLIncomeAccountNumber'})) < 1) {
			setError(wiaflos::server::core::GL::Error());
			return $incomeGLAccID;
		}
		my $catMatch = wiaflos::server::core::GL::checkGLAccountFinCat($incomeGLAccID,"D01");
		if ($catMatch == 0) {
			setError("Inventory income account '".$data->{'GLIncomeAccountNumber'}."' is not of type income for item '".
					$data->{'Code'}."'");
			return ERR_USAGE;
		} elsif ($catMatch < 0) {
			setError(wiaflos::server::core::GL::Error());
			return $catMatch;
		}
		push(@updates,sprintf('GLIncomeAccountID = %s',DBQuote($incomeGLAccID)));
	}

	# Check if we have an asset account
	if (defined($data->{'GLAssetAccountNumber'}) && $data->{'GLAssetAccountNumber'} ne "") {
		# Grab inventory account ID
		my $assetGLAccID;
		if (($assetGLAccID = wiaflos::server::core::GL::getGLAccountIDFromNumber($data->{'GLAssetAccountNumber'})) < 1) {
			setError(wiaflos::server::core::GL::Error());
			return $assetGLAccID;
		}

		# Check we an asset account
		my $catMatch = wiaflos::server::core::GL::checkGLAccountFinCat($assetGLAccID,"A01");
		if ($catMatch == 0) {
			setError("Inventory asset account '".$data->{'GLAssetAccountNumber'}."' is not of type asset for item '".$data->{'Code'}."'");
			return ERR_USAGE;
		} elsif ($catMatch < 0) {
			setError(wiaflos::server::core::GL::Error());
			return $catMatch;
		}

		push(@updates,sprintf('GLAssetAccountID = %s',DBQuote($assetGLAccID)));
	}


	# Make sure expense GL account is a expense account
	if (defined($data->{'GLExpenseAccountNumber'}) && $data->{'GLExpenseAccountNumber'} ne "") {
		# Grab expense account ID
		my $expenseGLAccID;
		if (($expenseGLAccID = wiaflos::server::core::GL::getGLAccountIDFromNumber($data->{'GLExpenseAccountNumber'})) < 1) {
			setError(wiaflos::server::core::GL::Error());
			return $expenseGLAccID;
		}

		# Check we an expense account
		my $catMatch = wiaflos::server::core::GL::checkGLAccountFinCat($expenseGLAccID,"E01");
		if ($catMatch == 0) {
			setError("Inventory expense account '".$data->{'GLExpenseAccountNumber'}."' is not of type expense for item '".$data->{'Code'}."'");
			return ERR_USAGE;
		} elsif ($catMatch < 0) {
			setError(wiaflos::server::core::GL::Error());
			return $catMatch;
		}

		push(@updates,sprintf('GLExpenseAccountID = %s',DBQuote($expenseGLAccID)));
	}

	if (defined($data->{'SellPrice'})) {
		push(@updates,sprintf('SellPrice = %s',DBQuote($data->{'SellPrice'})));
	}
	if (defined($data->{'Unit'})) {
		push(@updates,sprintf('Unit = %s',DBQuote($data->{'Unit'})));
	}
	if (defined($data->{'Discountable'})) {
		my $val;
		# Sanitize
		if (lc($data->{'Discountable'}) eq "yes") {
			$val = 1;
		} elsif (lc($data->{'Discountable'}) eq "no") {
			$val = 0;
		} else {
			setError("Unknown value for Discountable '".$data->{'Discountable'}."' regarding item '".$data->{'Code'}."'");
			return ERR_USAGE;
		}
		push(@updates,sprintf('Discountable = %s',DBQuote($val)));
	}

	# Check if inventory item exists
	my $inventoryID;
	if (($inventoryID = getInventoryItemIDFromCode($data->{'Code'})) < 1) {
		setError(Error());
		return $inventoryID;
	}

	# Check we have updates
	if (@updates < 1) {
		setError("Nothing to update for item '".$data->{'Code'}."'");
		return ERR_USAGE;
	}

	my $updateStr = join(',',@updates);

	# Remove inventory item
	my $sth = DBDo("
		UPDATE
			inventory
		SET
			$updateStr
		WHERE
			ID = ".DBQuote($inventoryID)."
	");
	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	return 0;
}



# Link inventory item to expense
# Parameters:
#		Date	 		- Date for transaction
#		Code			- Inventory item to link
#		Reference		- Reference of transaction
#		Quantity 		- Quantity to link
#		GLAccountNumber	- GL account number
# Optional:
#		SerialNumber	- Item serial number
sub linkInventoryItemToExpenseAccount
{
	my ($detail) = @_;


	# Verify params
	if (!defined($detail->{'Date'}) || $detail->{'Date'} eq "") {
		setError("No date provided");
		return ERR_PARAM;
	}
	if (!defined($detail->{'Code'}) || $detail->{'Code'} eq "") {
		setError("No item code provided");
		return ERR_PARAM;
	}
	if (!defined($detail->{'Reference'}) || $detail->{'Reference'} eq "") {
		setError("No reference provided");
		return ERR_PARAM;
	}
	if (!defined($detail->{'GLAccountNumber'}) || $detail->{'GLAccountNumber'} eq "") {
		setError("No description provided");
		return ERR_PARAM;
	}

	# Pull quantity as we do math below, else we wouldn't need to
	my $quantity = Math::BigFloat->new();
	$quantity->precision(-4);
	$quantity->badd( defined($detail->{'Quantity'}) ? $detail->{'Quantity'} : 1 );  # Default to 1 item

	# Grab inventory item and check it
	my $data;
	$data->{'Code'} = $detail->{'Code'};
	my $inventoryItem = getInventoryItem($data);
	if (ref $inventoryItem ne 'HASH') {
		return $inventoryItem;
	}
	# Check item is infact a product
	if ($inventoryItem->{'Type'} ne "product") {
		setError("Can only link a product as an expense");
		return ERR_USAGE;
	}

	# Grab expense account ID
	my $expenseAccID;
	if (($expenseAccID = wiaflos::server::core::GL::getGLAccountIDFromNumber($detail->{'GLAccountNumber'})) < 1) {
		setError(wiaflos::server::core::GL::Error());
		return $expenseAccID;
	}
	# Check we an expense account
	my $catMatch = wiaflos::server::core::GL::checkGLAccountFinCat($expenseAccID,"E01");
	if ($catMatch == 0) {
		setError("Expense account '".$detail->{'GLAccountNumber'}."' is not of type expense");
		return ERR_USAGE;
	} elsif ($catMatch < 0) {
		setError(wiaflos::server::core::GL::Error());
		return $catMatch;
	}

	# Grab inventory stock item
	$data = undef;
	$data->{'ID'} = $inventoryItem->{'ID'};
	$data->{'SerialNumber'} = $detail->{'SerialNumber'};  # Send serial number to ensure we get a cost price for tracked items
	$data->{'Quantity'} = $quantity;
	$inventoryItem = getInventoryStockItem($data);
	if (ref $inventoryItem ne 'HASH') {
		return $inventoryItem;
	}

	DBBegin();

	# Make a nice reference with some details
	my $reference = sprintf('%s (Qty: %s, Code: "%s"%s)',
			$detail->{'Reference'}, $quantity->bstr(), $inventoryItem->{'Code'},
			$detail->{'SerialNumber'} ? ', Serial "'.$detail->{'SerialNumber'}.'"' : '');

	# Create transaction
	$data = undef;
	$data->{'Date'} = $detail->{'Date'};
	$data->{'Reference'} = "Inventory linked expense: $reference";
	my $GLTransActID = wiaflos::server::core::GL::createGLTransaction($data);
	if ($GLTransActID < 1) {
		setError(wiaflos::server::core::GL::Error());
		DBRollback();
		return $GLTransActID;
	}

	# Work out cost price & stock list we require for adjustment
	my $costPrice = Math::BigFloat->new();
	my @stockAdjustmentList;
	# Check if this is a tracked item, if so add up all the stock we need
	foreach my $stockItem (@{$inventoryItem->{'StockRequired'}}) {
		# Get total cost of all items
		$costPrice->badd($stockItem->{'Cost'});

		# Create the stock list to adjust stock
		my $adjustItem;
		$adjustItem->{'InventoryTrackingID'} = $stockItem->{'InventoryTrackingID'};
		$adjustItem->{'Quantity'} = $stockItem->{'QuantityNeeded'};
		$adjustItem->{'Cost'} = $stockItem->{'Cost'};
		push(@stockAdjustmentList,$adjustItem);
	}

	# Adjust stock
	$quantity->bneg(); # Get negative
	$data = undef;
	$data->{'ID'} = $inventoryItem->{'ID'};
	$data->{'GLTransactionID'} = $GLTransActID;
	$data->{'QtyChange'} = $quantity->bstr();
	$data->{'SerialNumber'} = $detail->{'SerialNumber'};
	$data->{'StockAdjustmentList'} = \@stockAdjustmentList;
	# Adjust stock and check for errors
	my $trackingInfo = adjustInventoryStock($data);
	if (ref $trackingInfo ne "ARRAY") {
		DBRollback();
		return $trackingInfo;
	}

	# Data for all transactions below
	$data = undef;
	$data->{'ID'} = $GLTransActID;

	# Link in expenses
	$data->{'GLAccountID'} = $expenseAccID;
	$data->{'Amount'} = $costPrice->bstr();
	$data->{'Reference'} = "Inventory item: $reference";
	if ((my $res = wiaflos::server::core::GL::linkGLTransaction($data)) < 1) {
		setError(wiaflos::server::core::GL::Error());
		DBRollback();
		return $res;
	}

	# Link in asset to GL
	$data->{'GLAccountID'} = $inventoryItem->{'GLAssetAccountID'};
	$data->{'Amount'} = $costPrice->copy()->bneg()->bstr();  # Negate
	$data->{'Reference'} = "Stock control for inventory expense: $reference";
	if ((my $res = wiaflos::server::core::GL::linkGLTransaction($data)) < 1) {
		setError(wiaflos::server::core::GL::Error());
		DBRollback();
		return $res;
	}

	# Post transaction
	$data = undef;
	$data->{'ID'} = $GLTransActID;
	if ((my $res = wiaflos::server::core::GL::postGLTransaction($data)) != 0) {
		setError(wiaflos::server::core::GL::Error());
		DBRollback();
		return $res;
	}

	DBCommit();

	return RES_OK;
}



# Function to get tracking history for a tracking item
# Optional:
#		ID - Inventory tracking ID
sub getInventoryTrackingItemHistory
{
	my $detail = shift;


	# Verify params
	if (!defined($detail->{'ID'}) || $detail->{'ID'} < 1) {
		setError("No item ID given or invalid ID provided");
		return ERR_PARAM;
	}

	# Grab free inventory item list
	my $sth = DBSelect("
			SELECT
				ID, ParentInventoryTrackingID, InventoryID, GLTransactionID, SerialNumber, QtyChange, Price, Closed
			FROM
				inventory_tracking
			WHERE
				(
					ID = ".DBQuote($detail->{'ID'})."
					OR ParentInventoryTrackingID = ".DBQuote($detail->{'ID'})."
				)
	");

	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	# Build result
	my @result;
	while (my $row = hashifyLCtoMC($sth->fetchrow_hashref(),
			qw( ID ParentInventoryTrackingID InventoryID GLTransactionID SerialNumber QtyChange Price Closed )
	)) {
		push(@result,sanitizeRawTrackingItem($row));
	}
	DBFreeRes($sth);

	return \@result;
}


# @fn getInventoryStockBalance($detail)
# Function to get the balances of inventory on hand, qty & price
#
# @param detail Parameter hash ref
# @li EndDate - Optional end date
#
# @returns RES_OK on success, < 0 otherwise
sub getInventoryStockBalance
{
	my $detail = shift;


	# Check if we must use an end date
	my $extraSQL = "";
	if (defined($detail->{'EndDate'}) && $detail->{'EndDate'} ne "") {
		$extraSQL .= " AND gl_transactions.TransactionDate <= ".DBQuote($detail->{'EndDate'});
	}

	# Grab free inventory item list
	my $sth = DBSelect("
			SELECT
				inventory.Code,

				inventory_tracking.GLTransactionID,
				inventory_tracking.SerialNumber, inventory_tracking.QtyChange, inventory_tracking.Price

			FROM
				inventory,
				inventory_tracking,
				gl_transactions

			WHERE
				inventory.Type = 1

				AND inventory_tracking.InventoryID = inventory.ID
				AND inventory_tracking.GLTransactionID = gl_transactions.ID

				$extraSQL
	");

	if (!$sth) {
		setError(awitpt::db::dblayer::Error());
		return ERR_DB;
	}

	my $stockBalances = {};
	# Build result list
	while (my $row = hashifyLCtoMC($sth->fetchrow_hashref(),
			qw( Code GLTransactionID SerialNumber QtyChange Price )
	)) {
		my $item;

		# If there is no serial number, use blank ... this will occur in the case of a bulk item not tracked with no serial
		if (!defined($row->{'SerialNumber'})) {
			$row->{'SerialNumber'} = "";
		}

		# If we have an item, use it
		if (defined($stockBalances->{$row->{'Code'}}) && defined($stockBalances->{$row->{'Code'}}->{$row->{'SerialNumber'}})) {
			$item = $stockBalances->{$row->{'Code'}}->{$row->{'SerialNumber'}};
		}

		# If no item defined, create one
		if (!defined($item)) {
			$item->{'Quantity'} = Math::BigFloat->new();
			$item->{'Quantity'}->precision(-4);
			$item->{'Value'} = Math::BigFloat->new();
		}

		# Add quantity
		$item->{'Quantity'}->badd($row->{'QtyChange'});
		# Add cost
		$item->{'Value'}->badd($row->{'Price'});

		# Save item
		$stockBalances->{ $row->{'Code'} }->{ $row->{'SerialNumber'} } = $item;
	}
	DBFreeRes($sth);

	return $stockBalances;
}





1;
# vim: ts=4
