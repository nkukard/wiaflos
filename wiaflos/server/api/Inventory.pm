# SOAP interface to Inventory module
# Copyright (C) 2009-2014, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


package wiaflos::server::api::Inventory;

use strict;
use warnings;

use wiaflos::server::api::auth;
use wiaflos::constants;
use wiaflos::soap;
use wiaflos::server::core::Inventory;


# Plugin info
our $pluginInfo = {
	Name => "Inventory",
	Init => \&init,
};


# Initialize module
sub init {
	my $server = shift;

	wiaflos::server::api::auth::aclAdd('wiaflos/server/api/Inventory','getInventory','Inventory/Show');
	wiaflos::server::api::auth::aclAdd('wiaflos/server/api/Inventory','getInventoryStockItem','Inventory/Show');
	wiaflos::server::api::auth::aclAdd('wiaflos/server/api/Inventory','getInventoryItem','Inventory/Show');
	wiaflos::server::api::auth::aclAdd('wiaflos/server/api/Inventory','getInventoryItemMovement','Inventory/Show');
	wiaflos::server::api::auth::aclAdd('wiaflos/server/api/Inventory','createInventoryItem','Inventory/Add');
	wiaflos::server::api::auth::aclAdd('wiaflos/server/api/Inventory','removeInventoryItem','Inventory/Remove');
	wiaflos::server::api::auth::aclAdd('wiaflos/server/api/Inventory','updateInventoryItem','Inventory/Update');
	wiaflos::server::api::auth::aclAdd('wiaflos/server/api/Inventory','linkInventoryItemToExpenseAccount','Inventory/Adjust');
}



# Backend function to sanitize a raw inventory item from database
sub sanitizeRawInventoryItem {
	my $rawItem = shift;

	my $tmpItem;

	$tmpItem->{'ID'} = $rawItem->{'ID'};
	$tmpItem->{'Code'} = $rawItem->{'Code'};
	$tmpItem->{'Type'} = $rawItem->{'Type'};
	$tmpItem->{'Mode'} = $rawItem->{'Mode'};
	$tmpItem->{'Description'} = $rawItem->{'Description'};
	$tmpItem->{'GLIncomeAccountID'} = $rawItem->{'GLIncomeAccountID'};
	$tmpItem->{'GLIncomeAccountNumber'} = $rawItem->{'GLIncomeAccountNumber'};
	$tmpItem->{'GLAssetAccountID'} = $rawItem->{'GLAssetAccountID'};
	$tmpItem->{'GLAssetAccountNumber'} = $rawItem->{'GLAssetAccountNumber'};
	$tmpItem->{'GLExpenseAccountID'} = $rawItem->{'GLExpenseAccountID'};
	$tmpItem->{'GLExpenseAccountNumber'} = $rawItem->{'GLExpenseAccountNumber'};
	$tmpItem->{'SellPrice'} = $rawItem->{'SellPrice'};
	$tmpItem->{'TaxTypeID'} = $rawItem->{'TaxTypeID'};
	$tmpItem->{'TaxMode'} = $rawItem->{'TaxMode'};
	$tmpItem->{'Unit'} = $rawItem->{'Unit'};
	$tmpItem->{'Discountable'} = $rawItem->{'Discountable'};

	return $tmpItem;
}


# Backend function to sanitize a raw inventory movement item from database
sub sanitizeRawInventoryMovementItem {
	my $rawItem = shift;

	my $tmpItem;

	$tmpItem->{'ID'} = $rawItem->{'ID'};

	$tmpItem->{'GLTransactionDate'} = $rawItem->{'GLTransactionDate'};
	$tmpItem->{'GLTransactionID'} = $rawItem->{'GLTransactionID'};

	$tmpItem->{'SerialNumber'} = $rawItem->{'SerialNumber'};
	$tmpItem->{'QtyChange'} = $rawItem->{'QtyChange'};
	$tmpItem->{'Price'} = $rawItem->{'Price'};

	return $tmpItem;
}


# Return list of inventory items
sub getInventory {
	my $authInfo = wiaflos::server::api::auth::sessionGetData();

	# Grab and sanitize data
	my $rawData = wiaflos::server::core::Inventory::getInventory();
	if (ref $rawData ne "ARRAY") {
		return SOAPResponse(RES_ERROR,$rawData,wiaflos::server::core::Inventory::Error());
	}

	# Build result
	my @data;
	foreach my $item (@{$rawData}) {
		push(@data,sanitizeRawInventoryItem($item));
	}

	return SOAPResponse(RES_OK,\@data);
}


# Return inventory movement
sub getInventoryItemMovement {
	my (undef,$data) = @_;


	# Check params
	if (!defined($data->{'Code'}) || $data->{'Code'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'Code' invalid");
	}

	my $authInfo = wiaflos::server::api::auth::sessionGetData();

	# Build request
	my $detail;
	$detail->{'Code'} = $data->{'Code'};
	$detail->{'SerialNumber'} = $data->{'SerialNumber'};
	my $rawData = wiaflos::server::core::Inventory::getInventoryItemMovement($detail);
	if (ref $rawData ne "ARRAY") {
		return SOAPResponse(RES_ERROR,$rawData,wiaflos::server::core::Inventory::Error());
	}

	# Sanitize & build result
	my @data;
	foreach my $item (@{$rawData}) {
		push(@data,sanitizeRawInventoryMovementItem($item));
	}

	return SOAPResponse(RES_OK,\@data);
}


# Return list of inventory items
sub getInventoryItem {
	my (undef,$data) = @_;


	# Check params
	if (!defined($data->{'Code'}) || $data->{'Code'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'Code' invalid");
	}

	my $authInfo = wiaflos::server::api::auth::sessionGetData();

	# Do transaction
	my $detail;
	$detail->{'Code'} = $data->{'Code'};
	my $rawData = wiaflos::server::core::Inventory::getInventoryItem($detail);
	if (ref $rawData ne "HASH") {
		return SOAPResponse(RES_ERROR,$rawData,wiaflos::server::core::Inventory::Error());
	}

	my $res = sanitizeRawInventoryItem($rawData);

	return SOAPResponse(RES_OK,$res);
}


# Create inventory item
# Parameters:
#		Code				- Inventory item code
#		Type			- Account type, either "product" or "service"
#		Mode			- Product mode, "bulk" for bulk item, "track" for tracked item. Ignored for service.
#		Description		- Description
#		GLIncomeAccountNumber	- GL income account
#		GLAssetAccountNumber - GL asset account
#		GLExpenseAccountNumber	- GL expense account
#		SellPrice		- Selling price
#		TaxTypeID		- Tax type ID
#		TaxMode			- Tax mode, "incl" or "excl"
# Optional:
#		Unit			- Unit
#		Discountable	- Service is either discountable or not, "yes" or "no"
sub createInventoryItem {
	my (undef,$data) = @_;


	# Check params
	if (!defined($data->{'Code'}) || $data->{'Code'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'Code' invalid");
	}

	if (!defined($data->{'Type'}) || $data->{'Type'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'Type' invalid");
	}

	if ($data->{'Type'} eq "product" && (!defined($data->{'Mode'}) || $data->{'Mode'} eq "")) {
		return SOAPResponse(RES_ERROR,ERR_S_USAGE,"Parameter 'Mode' invalid");
	}

	if (!defined($data->{'Description'}) || $data->{'Description'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'Description' invalid");
	}

	if (!defined($data->{'GLIncomeAccountNumber'}) || $data->{'GLIncomeAccountNumber'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'GLIncomeAccountNumber' invalid");
	}

	if (defined($data->{'GLAssetAccountNumber'}) && $data->{'GLAssetAccountNumber'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'GLAssetAccountNumber' invalid");
	}

	if (defined($data->{'GLExpenseAccountNumber'}) && $data->{'GLExpenseAccountNumber'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'GLExpenseAccountNumber' invalid");
	}

	if (!defined($data->{'SellPrice'}) || $data->{'SellPrice'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'SellPrice' invalid");
	}

	if (!defined($data->{'TaxTypeID'}) || $data->{'TaxTypeID'} < 1) {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'TaxTypeID' invalid");
	}

	if (!defined($data->{'TaxMode'}) || $data->{'TaxMode'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'TaxMode' invalid");
	}

	if (defined($data->{'Discountable'}) && ($data->{'Discountable'} ne "yes" && $data->{'Discountable'} ne "no")) {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'Discountable' invalid");
	}


	my $authInfo = wiaflos::server::api::auth::sessionGetData();

	# Do transaction
	my $detail;
	$detail->{'Code'} = $data->{'Code'};
	$detail->{'Type'} = $data->{'Type'};
	$detail->{'Mode'} = $data->{'Mode'};
	$detail->{'Description'} = $data->{'Description'};
	$detail->{'GLIncomeAccountNumber'} = $data->{'GLIncomeAccountNumber'};
	$detail->{'GLAssetAccountNumber'} = $data->{'GLAssetAccountNumber'};
	$detail->{'GLExpenseAccountNumber'} = $data->{'GLExpenseAccountNumber'};
	$detail->{'SellPrice'} = $data->{'SellPrice'};
	$detail->{'TaxTypeID'} = $data->{'TaxTypeID'};
	$detail->{'TaxMode'} = $data->{'TaxMode'};
	$detail->{'Unit'} = $data->{'Unit'};
	$detail->{'Discountable'} = $data->{'Discountable'};
	my $res = wiaflos::server::core::Inventory::createInventoryItem($detail);

	if ($res < 1) {
		return SOAPResponse(RES_ERROR,$res,wiaflos::server::core::Inventory::Error());
	}

	return SOAPResponse(RES_OK,$res);
}


# Remove inventory item
# Parameters:
#		Code	- Product code
sub removeInventoryItem {
	my (undef,$data) = @_;


	# Check params
	if (!defined($data->{'Code'}) || $data->{'Code'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'Code' invalid");
	}

	my $authInfo = wiaflos::server::api::auth::sessionGetData();

	# Remove item
	my $detail;
	$detail->{'Code'} = $data->{'Code'};
	my $res = wiaflos::server::core::Inventory::removeInventoryItem($detail);

	if ($res < 0) {
		return SOAPResponse(RES_ERROR,$res,wiaflos::server::core::Inventory::Error());
	}

	return SOAPResponse(RES_OK,$res);
}


# Update inventory item
# Parameters:
#		Code			- Product code
# Optional:
#		Description		- Description
#		GLIncomeAccountNumber	- GL income account
#		GLAssetAccountNumber - GL asset account
#		GLExpenseAccountNumber	- GL expense account
#		SellPrice		- Selling price
#		Unit			- Unit
#		Discountable	- Either service is discountable or not
sub updateInventoryItem {
	my (undef,$data) = @_;


	# Check params
	if (!defined($data->{'Code'}) || $data->{'Code'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'Code' invalid");
	}

	if (defined($data->{'Description'}) && $data->{'Description'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'Description' invalid");
	}

	if (defined($data->{'GLIncomeAccountNumber'}) && $data->{'GLIncomeAccountNumber'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'GLIncomeAccountNumber' invalid");
	}

	if (defined($data->{'GLAssetAccountNumber'}) && $data->{'GLAssetAccountNumber'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'GLAssetAccountNumber' invalid");
	}

	if (defined($data->{'GLExpenseAccountNumber'}) && $data->{'GLExpenseAccountNumber'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'GLExpenseAccountNumber' invalid");
	}


	my $authInfo = wiaflos::server::api::auth::sessionGetData();

	# Do transaction
	my $detail;
	$detail->{'Code'} = $data->{'Code'};
	$detail->{'Description'} = $data->{'Description'};
	$detail->{'GLIncomeAccountNumber'} = $data->{'GLIncomeAccountNumber'};
	$detail->{'GLAssetAccountNumber'} = $data->{'GLAssetAccountNumber'};
	$detail->{'GLExpenseAccountNumber'} = $data->{'GLExpenseAccountNumber'};
	$detail->{'SellPrice'} = $data->{'SellPrice'};
	$detail->{'Unit'} = $data->{'Unit'};
	$detail->{'Discountable'} = $data->{'Discountable'};
	my $res = wiaflos::server::core::Inventory::updateInventoryItem($detail);

	if ($res < 0) {
		return SOAPResponse(RES_ERROR,$res,wiaflos::server::core::Inventory::Error());
	}

	return SOAPResponse(RES_OK,$res);
}



# Link inventory item to expense account
# Parameters:
#		Date			- Transaction date
#		Code			- Product code
#		Reference		- Reference for transaction
#		Quantity 		- Quantity to link
#		GLAccountNumber	- GL account number
#
# Optional:
#		SerialNumber	- Item serial number
sub linkInventoryItemToExpenseAccount {
	my (undef,$data) = @_;


	# Check params
	if (!defined($data->{'Date'}) || $data->{'Date'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'Date' invalid");
	}

	if (!defined($data->{'Code'}) || $data->{'Code'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'Code' invalid");
	}

	if (defined($data->{'Reference'}) && $data->{'Reference'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'Reference' invalid");
	}

	if (!defined($data->{'GLAccountNumber'}) || $data->{'GLAccountNumber'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'GLAccountNumber' invalid");
	}


	my $authInfo = wiaflos::server::api::auth::sessionGetData();

	# Do transaction
	my $detail;
	$detail->{'Date'} = $data->{'Date'};
	$detail->{'Code'} = $data->{'Code'};
	$detail->{'Reference'} = $data->{'Reference'};
	$detail->{'Quantity'} = $data->{'Quantity'};
	$detail->{'GLAccountNumber'} = $data->{'GLAccountNumber'};
	$detail->{'SerialNumber'} = $data->{'SerialNumber'};
	my $res = wiaflos::server::core::Inventory::linkInventoryItemToExpenseAccount($detail);

	if ($res < 0) {
		return SOAPResponse(RES_ERROR,$res,wiaflos::server::core::Inventory::Error());
	}

	return SOAPResponse(RES_OK,$res);
}



1;
# vim: ts=4
