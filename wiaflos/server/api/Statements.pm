# SOAP interface to Statements module
# Copyright (C) 2009-2014, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


package wiaflos::server::api::Statements;

use strict;
use warnings;

use wiaflos::server::api::auth;

use wiaflos::constants;
use wiaflos::soap;
use wiaflos::server::core::Statements;


# Plugin info
our $pluginInfo = {
	Name => "Statements",
	Init => \&init,
};


# Initialize module
sub init {
	my $server = shift;


	wiaflos::server::api::auth::aclAdd('wiaflos/server/api/Statements','getStatement','Statements/Show');
	wiaflos::server::api::auth::aclAdd('wiaflos/server/api/Statements','sendStatement','Statements/Send');
}




# Return list of statement entries
# Parameters:
#		ClientCode		- Client code
#		StartDate	- Start date
sub getStatement {
	my (undef,$data) = @_;


	if (!defined($data->{'ClientCode'}) || $data->{'ClientCode'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'ClientCode' invalid");
	}

	my $authInfo = wiaflos::server::api::auth::sessionGetData();

	# Grab and sanitize data
	my $detail;
	$detail->{'ClientCode'} = $data->{'ClientCode'};
	$detail->{'StartDate'} = $data->{'StartDate'};
	my $rawData = wiaflos::server::core::Statements::getStatement($detail);
	if (ref $rawData ne "ARRAY") {
		return SOAPResponse(RES_ERROR,$rawData,wiaflos::server::core::Statements::Error());
	}

	# Build result
	my @data;
	foreach my $item (@{$rawData}) {
		my $tmpItem;

		$tmpItem->{'ID'} = $item->{'ID'};
		$tmpItem->{'GLTransactionID'} = $item->{'GLTransactionID'};
		$tmpItem->{'Reference'} = $item->{'Reference'};
		$tmpItem->{'TransactionDate'} = $item->{'TransactionDate'};
		$tmpItem->{'Amount'} = $item->{'Amount'};
		$tmpItem->{'TransactionReference'} = $item->{'TransactionReference'};

		push(@data,$tmpItem);
	}

	return SOAPResponse(RES_OK,\@data);
}


# Build client statement
# Parameters:
#		ClientCode	- Client ref
#		SendTo		- Send statement to here
# Optional:
#		StartDate	- Start date
#		Subject		- Statement subject
sub sendStatement {
	my (undef,$data) = @_;


	# Check params
	if (!defined($data->{'ClientCode'}) || $data->{'ClientCode'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'ClientCode' invalid");
	}

	if (!defined($data->{'SendTo'}) || $data->{'SendTo'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'SendTo' invalid");
	}

	my $authInfo = wiaflos::server::api::auth::sessionGetData();

	# Do transaction
	my $detail;
	$detail->{'ClientCode'} = $data->{'ClientCode'};
	$detail->{'SendTo'} = $data->{'SendTo'};
	$detail->{'StartDate'} = $data->{'StartDate'};
	$detail->{'Subject'} = $data->{'Subject'};
	my $res = wiaflos::server::core::Statements::sendStatement($detail);

	if ($res < 0) {
		return SOAPResponse(RES_ERROR,$res,wiaflos::server::core::Statements::Error());
	}

	return SOAPResponse(RES_OK,$res);
}



1;
# vim: ts=4
