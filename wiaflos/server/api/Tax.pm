# SOAP interface to Tax module
# Copyright (C) 2009-2014, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


package wiaflos::server::api::Tax;

use strict;
use warnings;

use wiaflos::server::api::auth;
use wiaflos::constants;
use wiaflos::soap;
use wiaflos::server::core::Tax;


# Plugin info
our $pluginInfo = {
	Name => "Tax",
	Init => \&init,
};


# Initialize module
sub init {
	my $server = shift;


	wiaflos::server::api::auth::aclAdd('wiaflos/server/api/Tax','createTaxType','Tax/Type/Add');
}




# Create tax type
# Parameters:
#		Description		- Description
#		GLAccountNumber		- GL account
#		Rate		- Tax rate
sub createTaxType {
	my (undef,$data) = @_;


	if (!defined($data->{'Description'}) || $data->{'Description'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'Description' invalid");
	}

	if (!defined($data->{'GLAccountNumber'}) || $data->{'GLAccountNumber'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'GLAccountNumber' invalid");
	}

	if (!defined($data->{'Rate'}) || $data->{'Rate'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'Rate' invalid");
	}

	my $authInfo = wiaflos::server::api::auth::sessionGetData();

	# Grab and sanitize data
	my $detail;
	$detail->{'Description'} = $data->{'Description'};
	$detail->{'GLAccountNumber'} = $data->{'GLAccountNumber'};
	$detail->{'Rate'} = $data->{'Rate'};
	my $res = wiaflos::server::core::Tax::createTaxType($detail);

	if ($res < 1) {
		return SOAPResponse(RES_ERROR,$res,wiaflos::server::core::Tax::Error());
	}

	return SOAPResponse(RES_OK,$res);
}



1;
# vim: ts=4
