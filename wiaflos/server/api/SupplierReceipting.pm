# SOAP interface to SupplierReceipting module
# Copyright (C) 2009-2014, AllWorldIT
# Copyright (C) 2008, LinuxRulz
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


package wiaflos::server::api::SupplierReceipting;

use strict;
use warnings;

use wiaflos::server::api::auth;
use wiaflos::constants;
use wiaflos::soap;
use wiaflos::server::core::SupplierReceipting;


# Plugin info
our $pluginInfo = {
	Name => "SupplierReceipting",
	Init => \&init,
};



# Initialize module
sub init {
	my $server = shift;

	wiaflos::server::api::auth::aclAdd('wiaflos/server/api/SupplierReceipting','createSupplierReceipt','SupplierReceipting/Add');
	wiaflos::server::api::auth::aclAdd('wiaflos/server/api/SupplierReceipting','getSupplierReceipts','SupplierReceipting/Show');
	wiaflos::server::api::auth::aclAdd('wiaflos/server/api/SupplierReceipting','getSupplierReceipt','SupplierReceipting/Show');
	wiaflos::server::api::auth::aclAdd('wiaflos/server/api/SupplierReceipting','postSupplierReceipt','SupplierReceipting/Post');
	wiaflos::server::api::auth::aclAdd('wiaflos/server/api/SupplierReceipting','sendSupplierReceipt','SupplierReceipting/Send');
	wiaflos::server::api::auth::aclAdd('wiaflos/server/api/SupplierReceipting','createSupplierReceiptAllocation','SupplierReceipting/Allocation/Add');
	wiaflos::server::api::auth::aclAdd('wiaflos/server/api/SupplierReceipting','getSupplierReceiptAllocations','SupplierReceipting/Allocation/Show');
	wiaflos::server::api::auth::aclAdd('wiaflos/server/api/SupplierReceipting','postSupplierReceiptAllocation','SupplierReceipting/Allocation/Post');
}



# Sanitize supplier receipt allocation
sub sanitizeSupplierReceiptAllocation
{
	my $param = shift;


	my $tmpItem;

	$tmpItem->{'ID'} = $param->{'ID'};

	$tmpItem->{'SupplierReceiptNumber'} = $param->{'SupplierReceiptNumber'};

	$tmpItem->{'SupplierCreditNoteID'} = $param->{'SupplierCreditNoteID'};
	$tmpItem->{'SupplierCreditNoteNumber'} = $param->{'SupplierCreditNoteNumber'};

	$tmpItem->{'Amount'} = $param->{'Amount'};

	$tmpItem->{'GLTransactionID'} = $param->{'GLTransactionID'};
	$tmpItem->{'Posted'} = $param->{'Posted'};

	return $tmpItem;
}




# Create receipt
# Parameters:
#		SupplierCode	- Supplier code
#		GLAccountNumber	- GL account where money was paid from
#		Number	- Receipt number
#		Date		- Date of receipt
#		Reference			- GL account entry reference (bank statement reference for example)
#		Amount		- Amount
sub createSupplierReceipt {
	my (undef,$data) = @_;


	# Check params
	if (!defined($data->{'SupplierCode'}) || $data->{'SupplierCode'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'SupplierCode' invalid");
	}

	if (!defined($data->{'GLAccountNumber'}) || $data->{'GLAccountNumber'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'GLAccountNumber' invalid");
	}

	if (!defined($data->{'Number'}) || $data->{'Number'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'Number' invalid");
	}

	if (!defined($data->{'Date'}) || $data->{'Date'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'Date' invalid");
	}

	if (!defined($data->{'Reference'}) || $data->{'Reference'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'Reference' invalid");
	}

	if (!defined($data->{'Amount'}) || $data->{'Amount'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'Amount' invalid");
	}

	my $authInfo = wiaflos::server::api::auth::sessionGetData();

	# Do transaction
	my $detail;
	$detail->{'SupplierCode'} = $data->{'SupplierCode'};
	$detail->{'GLAccountNumber'} = $data->{'GLAccountNumber'};
	$detail->{'Number'} = $data->{'Number'};
	$detail->{'Date'} = $data->{'Date'};
	$detail->{'Reference'} = $data->{'Reference'};
	$detail->{'Amount'} = $data->{'Amount'};
	my $res = wiaflos::server::core::SupplierReceipting::createSupplierReceipt($detail);
	if ($res < 1) {
		return SOAPResponse(RES_ERROR,$res,wiaflos::server::core::SupplierReceipting::Error());
	}

	return SOAPResponse(RES_OK,$res);
}


# Return list of receipts
sub getSupplierReceipts {
	my (undef,$data) = @_;


	my $authInfo = wiaflos::server::api::auth::sessionGetData();

	# Grab and sanitize data
	my $detail;
	$detail->{'Type'} = $data->{'Type'};
	my $rawData = wiaflos::server::core::SupplierReceipting::getSupplierReceipts($detail);
	if (ref $rawData ne "ARRAY") {
		return SOAPResponse(RES_ERROR,$rawData,wiaflos::server::core::SupplierReceipting::Error());
	}

	# Build result
	my @data;
	foreach my $item (@{$rawData}) {
		my $tmpItem;

		$tmpItem->{'ID'} = $item->{'ID'};

		$tmpItem->{'SupplierID'} = $item->{'SupplierID'};
		$tmpItem->{'SupplierCode'} = $item->{'SupplierCode'};

		$tmpItem->{'GLAccountID'} = $item->{'GLAccountID'};
		$tmpItem->{'GLAccountNumber'} = $item->{'GLAccountNumber'};

		$tmpItem->{'Number'} = $item->{'Number'};

		$tmpItem->{'TransactionDate'} = $item->{'TransactionDate'};
		$tmpItem->{'Reference'} = $item->{'Reference'};
		$tmpItem->{'Amount'} = $item->{'Amount'};

		$tmpItem->{'GLTransactionID'} = $item->{'GLTransactionID'};
		$tmpItem->{'Posted'} = $item->{'Posted'};

		$tmpItem->{'Closed'} = $item->{'Closed'};

		push(@data,$tmpItem);
	}

	return SOAPResponse(RES_OK,\@data);
}


# Return a receipt
sub getSupplierReceipt {
	my (undef,$data) = @_;


	my $authInfo = wiaflos::server::api::auth::sessionGetData();

	# Grab and sanitize data
	my $detail;
	$detail->{'Number'} = $data->{'Number'};
	$detail->{'ID'} = $data->{'ID'};
	my $rawData = wiaflos::server::core::SupplierReceipting::getSupplierReceipt($detail);
	if (ref $rawData ne "HASH") {
		return SOAPResponse(RES_ERROR,$rawData,wiaflos::server::core::SupplierReceipting::Error());
	}

	my $tmpItem;

	$tmpItem->{'ID'} = $rawData->{'ID'};

	$tmpItem->{'SupplierID'} = $rawData->{'SupplierID'};
	$tmpItem->{'SupplierCode'} = $rawData->{'SupplierCode'};

	$tmpItem->{'GLAccountID'} = $rawData->{'GLAccountID'};
	$tmpItem->{'GLAccountNumber'} = $rawData->{'GLAccountNumber'};

	$tmpItem->{'Number'} = $rawData->{'Number'};

	$tmpItem->{'TransactionDate'} = $rawData->{'TransactionDate'};
	$tmpItem->{'Reference'} = $rawData->{'Reference'};
	$tmpItem->{'Amount'} = $rawData->{'Amount'};

	$tmpItem->{'GLTransactionID'} = $rawData->{'GLTransactionID'};
	$tmpItem->{'Posted'} = $rawData->{'Posted'};

	$tmpItem->{'Closed'} = $rawData->{'Closed'};


	return SOAPResponse(RES_OK,$tmpItem);
}


# Post receipt
# Parameters:
#		Number	- Supplier receipt number
sub postSupplierReceipt
{
	my (undef,$data) = @_;


	# Check params
	if (!defined($data->{'Number'}) || $data->{'Number'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'Number' invalid");
	}

	my $authInfo = wiaflos::server::api::auth::sessionGetData();

	# Do transaction
	my $detail;
	$detail->{'Number'} = $data->{'Number'};
	my $res = wiaflos::server::core::SupplierReceipting::postSupplierReceipt($detail);
	if ($res < 0) {
		return SOAPResponse(RES_ERROR,$res,wiaflos::server::core::SupplierReceipting::Error());
	}

	return SOAPResponse(RES_OK,$res);
}


# Post allocation
# Parameters:
#		ID	- Allocation ID to post
sub postSupplierReceiptAllocation
{
	my (undef,$data) = @_;


	# Check params
	if (!defined($data->{'ID'}) || $data->{'ID'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'ID' invalid");
	}

	my $authInfo = wiaflos::server::api::auth::sessionGetData();

	# Do transaction
	my $detail;
	$detail->{'ID'} = $data->{'ID'};
	my $res = wiaflos::server::core::SupplierReceipting::postSupplierReceiptAllocation($detail);
	if ($res < 0) {
		return SOAPResponse(RES_ERROR,$res,wiaflos::server::core::SupplierReceipting::Error());
	}

	return SOAPResponse(RES_OK,$res);
}


# Create allocation
# Parameters:
#		SupplierReceiptNumber	- Receipt number
#		SupplierCreditNoteNumber	- Invoice number
#		Amount		- Amount
sub createSupplierReceiptAllocation {
	my (undef,$data) = @_;


	# Check params
	if (!defined($data->{'ReceiptNumber'}) || $data->{'ReceiptNumber'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'ReceiptNumber' invalid");
	}

	if (!defined($data->{'SupplierCreditNoteNumber'}) || $data->{'SupplierCreditNoteNumber'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'SupplierCreditNoteNumber' invalid");
	}

	if (!defined($data->{'Amount'}) || $data->{'Amount'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'Amount' invalid");
	}

	my $authInfo = wiaflos::server::api::auth::sessionGetData();

	# Do transaction
	my $detail;
	$detail->{'ReceiptNumber'} = $data->{'ReceiptNumber'};
	$detail->{'SupplierCreditNoteNumber'} = $data->{'SupplierCreditNoteNumber'};
	$detail->{'Amount'} = $data->{'Amount'};
	my $res = wiaflos::server::core::SupplierReceipting::createSupplierReceiptAllocation($detail);
	if ($res < 1) {
		return SOAPResponse(RES_ERROR,$res,wiaflos::server::core::SupplierReceipting::Error());
	}

	return SOAPResponse(RES_OK,$res);
}


# Return list of receipt allocations
sub getSupplierReceiptAllocations {
	my (undef,$data) = @_;


	# Check params
	if (!defined($data->{'SupplierReceiptNumber'}) || $data->{'SupplierReceiptNumber'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'SupplierReceiptNumber' invalid");
	}

	my $authInfo = wiaflos::server::api::auth::sessionGetData();

	# Grab and sanitize data
	my $detail;
	$detail->{'SupplierReceiptNumber'} = $data->{'SupplierReceiptNumber'};
	my $rawData = wiaflos::server::core::SupplierReceipting::getSupplierReceiptAllocations($detail);
	if (ref $rawData ne "ARRAY") {
		return SOAPResponse(RES_ERROR,$rawData,wiaflos::server::core::SupplierReceipting::Error());
	}

	# Build result
	my @data;
	foreach my $item (@{$rawData}) {
		push(@data,sanitizeSupplierReceiptAllocation($item));
	}

	return SOAPResponse(RES_OK,\@data);
}



# Build client receipt
# Parameters:
#		Number	- Supplier eceipt number
#		SendTo		- Send receipt here
sub sendSupplierReceipt {
	my (undef,$data) = @_;


	# Check params
	if (!defined($data->{'Number'}) || $data->{'Number'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'Number' invalid");
	}

	if (!defined($data->{'SendTo'}) || $data->{'SendTo'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'SendTo' invalid");
	}

	my $authInfo = wiaflos::server::api::auth::sessionGetData();

	# Do transaction
	my $detail;
	$detail->{'Number'} = $data->{'Number'};
	$detail->{'SendTo'} = $data->{'SendTo'};
	my $res = wiaflos::server::core::SupplierReceipting::sendSupplierReceipt($detail);
	if ($res < 0) {
		return SOAPResponse(RES_ERROR,$res,wiaflos::server::core::SupplierReceipting::Error());
	}

	return SOAPResponse(RES_OK,$res);
}




1;
# vim: ts=4
