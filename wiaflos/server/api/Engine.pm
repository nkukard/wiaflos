# SOAP interface to Engine module
# Copyright (C) 2009-2014, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


package wiaflos::server::api::Engine;

use strict;
use warnings;

use wiaflos::constants;
use wiaflos::soap;
use wiaflos::server::api::auth;
use wiaflos::server::core::Engine;


# Plugin info
our $pluginInfo = {
	Name => "Engine",
	Init => \&init,
};


# Initialize module
sub init {
	my $server = shift;


	wiaflos::server::api::auth::aclAdd('wiaflos/server/api/Engine','cacheGetStats','Engine/Cache/Stats');
}



# Get cache stats
sub cacheGetStats {
	# Do transaction
	my $res = wiaflos::server::core::Engine::cacheGetStats();
	if (ref $res ne "HASH") {
		return SOAPResponse(RES_ERROR,$res,wiaflos::server::core::Engine::Error());
	}

	return SOAPResponse(RES_OK,$res);
}





1;
# vim: ts=4
