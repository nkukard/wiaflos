# SOAP interface to Reporting module
# Copyright (C) 2009-2014, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


package wiaflos::server::api::Reporting;

use strict;
use warnings;

use wiaflos::server::api::auth;

use wiaflos::constants;
use wiaflos::soap;
use wiaflos::server::core::Reporting;


# Plugin info
our $pluginInfo = {
	Name => "Reporting",
	Init => \&init,
};


# Initialize module
sub init {
	my $server = shift;


	wiaflos::server::api::auth::aclAdd('wiaflos/server/api/Reporting','getChartOfAccounts','Reporting/ChartOfAccounts/Show');

	# should be split up
	wiaflos::server::api::auth::aclAdd('wiaflos/server/api/Reporting','sendReport','Reporting/ChartOfAccounts/Show');
}




## @fn getChartOfAccounts
# Function to return the chart of accounts with balances
#
# @param detail Parameter hash ref
# @li StartDate Optional statement start date
# @li EndDate Optional statement end date
# @li Levels Optional depth to go into on each parent account
#
# @returns Array ref of hash refs containing the accounts and their balances
# @li ID Account ID
# @li Name Name of account
# @li Number Account number
# @li Balance Account balance
# @li DebitAmount Debit amount
# @li CreditAmount Credit amount
sub getChartOfAccounts {
	my (undef,$data) = @_;


	my $authInfo = wiaflos::server::api::auth::sessionGetData();

	# Grab and sanitize data
	my $detail;
	$detail->{'StartDate'} = $data->{'StartDate'};
	$detail->{'EndDate'} = $data->{'EndDate'};
	$detail->{'Levels'} = $data->{'Levels'};
	my $rawData = wiaflos::server::core::Reporting::getChartOfAccounts($detail);
	if (ref $rawData ne "ARRAY") {
		return SOAPResponse(RES_ERROR,$rawData,wiaflos::server::core::Reporting::Error());
	}

	# Build result
	my @data;
	foreach my $item (@{$rawData}) {
		my $tmpItem;

		$tmpItem->{'ID'} = $item->{'ID'};
		$tmpItem->{'Name'} = $item->{'Name'};
		$tmpItem->{'Number'} = $item->{'Number'};
		$tmpItem->{'RwCatCode'} = $item->{'RwCatCode'};
		$tmpItem->{'RwCatDescription'} = $item->{'RwCatDescription'};
		$tmpItem->{'FinCatCode'} = $item->{'FinCatCode'};
		$tmpItem->{'FinCatDescription'} = $item->{'FinCatDescription'};
		$tmpItem->{'Level'} = $item->{'Level'};
		$tmpItem->{'Balance'} = $item->{'Balance'};
		$tmpItem->{'DebitAmount'} = $item->{'DebitAmount'};
		$tmpItem->{'CreditAmount'} = $item->{'CreditAmount'};

		push(@data,$tmpItem);
	}

	return SOAPResponse(RES_OK,\@data);
}


# Send report
# Parameters:
#		Template	- Report template to use
#		SendTo		- Send statement to here
#		GLAccountNumber	- GL account number
#		StartDate	- Start date
#		EndDate		- End date
#		Subject		- Statement subject
#		Background	- Background this report generation
sub sendReport {
	my (undef,$data) = @_;


	# Check params
	if (!defined($data->{'Template'}) || $data->{'Template'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'Template' invalid");
	}

	if (!defined($data->{'SendTo'}) || $data->{'SendTo'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'SendTo' invalid");
	}

	# Do transaction
	my $detail;
	$detail->{'Template'} = $data->{'Template'};
	$detail->{'SendTo'} = $data->{'SendTo'};
	$detail->{'GLAccountNumber'} = $data->{'GLAccountNumber'};
	$detail->{'StartDate'} = $data->{'StartDate'};
	$detail->{'EndDate'} = $data->{'EndDate'};
	$detail->{'Subject'} = $data->{'Subject'};
	$detail->{'Background'} = $data->{'Background'};
	my $res = wiaflos::server::core::Reporting::sendReport($detail);

	if ($res < 0) {
		return SOAPResponse(RES_ERROR,$res,wiaflos::server::core::Reporting::Error());
	}

	return SOAPResponse(RES_OK,$res);
}



1;
# vim: ts=4
