# SOAP interface to Invoicing module
# Copyright (C) 2009-2014, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


package wiaflos::server::api::Invoicing;

use strict;
use warnings;

use wiaflos::server::api::auth;
use wiaflos::constants;
use wiaflos::soap;
use wiaflos::server::core::Invoicing;


# Plugin info
our $pluginInfo = {
	Name => "Invoicing",
	Init => \&init,
};

# Initialize module
sub init {
	my $server = shift;


	wiaflos::server::api::auth::aclAdd('wiaflos/server/api/Invoicing','getInvoices','Invoicing/Show');
	wiaflos::server::api::auth::aclAdd('wiaflos/server/api/Invoicing','getInvoice','Invoicing/Show');
	wiaflos::server::api::auth::aclAdd('wiaflos/server/api/Invoicing','getInvoiceItems','Invoicing/Show');
	wiaflos::server::api::auth::aclAdd('wiaflos/server/api/Invoicing','getInvoiceTransactions','Invoicing/Show');
	wiaflos::server::api::auth::aclAdd('wiaflos/server/api/Invoicing','createInvoice','Invoicing/Add');
	wiaflos::server::api::auth::aclAdd('wiaflos/server/api/Invoicing','linkInvoiceItem','Invoicing/Add');

	wiaflos::server::api::auth::aclAdd('wiaflos/server/api/Invoicing','postInvoice','Invoicing/Post');
	wiaflos::server::api::auth::aclAdd('wiaflos/server/api/Invoicing','sendInvoice','Invoicing/Send');
}




# Return list of client invoices
sub getInvoices {
	my (undef,$data) = @_;


	my $authInfo = wiaflos::server::api::auth::sessionGetData();

	# Grab and sanitize data
	my $detail;
	$detail->{'Type'} = $data->{'Type'};
	my $rawData = wiaflos::server::core::Invoicing::getInvoices($detail);
	if (ref $rawData ne "ARRAY") {
		return SOAPResponse(RES_ERROR,$rawData,wiaflos::server::core::Invoicing::Error());
	}

	# Build result
	my @data;
	foreach my $item (@{$rawData}) {
		my $tmpItem;

		$tmpItem->{'ID'} = $item->{'ID'};

		$tmpItem->{'ClientID'} = $item->{'ClientID'};
		$tmpItem->{'Number'} = $item->{'Number'};

		$tmpItem->{'IssueDate'} = $item->{'IssueDate'};
		$tmpItem->{'DueDate'} = $item->{'DueDate'};
		$tmpItem->{'OrderNumber'} = $item->{'OrderNumber'};

		$tmpItem->{'DiscountTotal'} = $item->{'DiscountTotal'};
		$tmpItem->{'SubTotal'} = $item->{'SubTotal'};
		$tmpItem->{'TaxTotal'} = $item->{'TaxTotal'};
		$tmpItem->{'Total'} = $item->{'Total'};

		$tmpItem->{'Posted'} = $item->{'Posted'};

		$tmpItem->{'Paid'} = $item->{'Paid'};

		push(@data,$tmpItem);
	}

	return SOAPResponse(RES_OK,\@data);
}



# Return client invoice
# Parameters:
#		Number		- Client invoice number
sub getInvoice {
	my (undef,$data) = @_;


	# Check params
	if (!defined($data->{'Number'}) || $data->{'Number'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'Number' invalid");
	}

	my $authInfo = wiaflos::server::api::auth::sessionGetData();

	# Grab and sanitize data
	my $detail;
	$detail->{'Number'} = $data->{'Number'};
	my $rawData = wiaflos::server::core::Invoicing::getInvoice($detail);
	if (ref $rawData ne "HASH") {
		return SOAPResponse(RES_ERROR,$rawData,wiaflos::server::core::Invoicing::Error());
	}

	# Build result
	my $tmpItem;

	$tmpItem->{'ID'} = $rawData->{'ID'};

	$tmpItem->{'ClientID'} = $rawData->{'ClientID'};
	$tmpItem->{'Number'} = $rawData->{'Number'};

	$tmpItem->{'ShippingAddress'} = $rawData->{'ShippingAddress'};
	$tmpItem->{'TaxReference'} = $rawData->{'TaxReference'};

	$tmpItem->{'IssueDate'} = $rawData->{'IssueDate'};
	$tmpItem->{'DueDate'} = $rawData->{'DueDate'};
	$tmpItem->{'OrderNumber'} = $rawData->{'OrderNumber'};

	$tmpItem->{'DiscountTotal'} = $rawData->{'DiscountTotal'};
	$tmpItem->{'SubTotal'} = $rawData->{'SubTotal'};
	$tmpItem->{'TaxTotal'} = $rawData->{'TaxTotal'};
	$tmpItem->{'Total'} = $rawData->{'Total'};

	$tmpItem->{'GLTransactionID'} = $rawData->{'GLTransactionID'};

	$tmpItem->{'Paid'} = $rawData->{'Paid'};

	return SOAPResponse(RES_OK,$tmpItem);
}



# Return list of items relating to an invoice
# Parameters:
#		Number		- Client invoice number
sub getInvoiceItems {
	my (undef,$data) = @_;


	# Check params
	if (!defined($data->{'Number'}) || $data->{'Number'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'Number' invalid");
	}

	my $authInfo = wiaflos::server::api::auth::sessionGetData();

	# Grab and sanitize data
	my $detail;
	$detail->{'Number'} = $data->{'Number'};
	my $rawData = wiaflos::server::core::Invoicing::getInvoiceItems($detail);
	if (ref $rawData ne "ARRAY") {
		return SOAPResponse(RES_ERROR,$rawData,wiaflos::server::core::Invoicing::Error());
	}

	# Build result
	my @data;
	foreach my $item (@{$rawData}) {
		my $tmpItem;

		$tmpItem->{'ID'} = $item->{'ID'};
		$tmpItem->{'SerialNumber'} = $item->{'SerialNumber'};
		$tmpItem->{'InventoryID'} = $item->{'InventoryID'};
		$tmpItem->{'InventoryCode'} = $item->{'InventoryCode'};
		$tmpItem->{'Description'} = $item->{'Description'};
		$tmpItem->{'Quantity'} = $item->{'Quantity'};
		$tmpItem->{'Unit'} = $item->{'Unit'};
		$tmpItem->{'UnitPrice'} = $item->{'UnitPrice'};
		$tmpItem->{'Price'} = $item->{'Price'};
		$tmpItem->{'TotalPrice'} = $item->{'TotalPrice'};
		$tmpItem->{'Discount'} = $item->{'Discount'};
		$tmpItem->{'DiscountAmount'} = $item->{'DiscountAmount'};
		$tmpItem->{'TaxRate'} = $item->{'TaxRate'};
		$tmpItem->{'TaxAmount'} = $item->{'TaxAmount'};

		push(@data,$tmpItem);
	}

	return SOAPResponse(RES_OK,\@data);
}



# Create client invoice
# Parameters:
#		ClientCode		- Client code
#		Number			- Invoice number
#		IssueDate		- Issue date
#		DueDate			- Due date
# Optional:
#		OrderNumber		- Order number
#		ShippingAddress	- Shipping address
#		Note			- Notes
sub createInvoice {
	my (undef,$data) = @_;


	# Check params
	if (!defined($data->{'ClientCode'}) || $data->{'ClientCode'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'ClientCode' invalid");
	}

	if (!defined($data->{'Number'}) || $data->{'Number'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'Number' invalid");
	}

	if (!defined($data->{'IssueDate'}) || $data->{'IssueDate'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'IssueDate' invalid");
	}

	if (!defined($data->{'DueDate'}) || $data->{'DueDate'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'DueDate' invalid");
	}

	my $authInfo = wiaflos::server::api::auth::sessionGetData();

	# Do transaction
	my $detail;
	$detail->{'ClientCode'} = $data->{'ClientCode'};
	$detail->{'Number'} = $data->{'Number'};
	$detail->{'ShippingAddress'} = $data->{'ShippingAddress'};
	$detail->{'IssueDate'} = $data->{'IssueDate'};
	$detail->{'DueDate'} = $data->{'DueDate'};
	$detail->{'OrderNumber'} = $data->{'OrderNumber'};
	$detail->{'Note'} = $data->{'Note'};
	my $res = wiaflos::server::core::Invoicing::createInvoice($detail);
	if ($res < 1) {
		return SOAPResponse(RES_ERROR,$res,wiaflos::server::core::Invoicing::Error());
	}


	return SOAPResponse(RES_OK,$res);
}



# Link inventory item
# Parameters:
#		Number				- Client invoice number
#		InventoryCode		- Inventory code
# Optional:
#		Description			- Description
#		UnitPrice			- Unit price
#		Quantity			- Quantity
#		Discount			- Discount %
#		SerialNumber		- Used by tracked inventory products, this is normally the serial number of the item. Or in any other case
#							can be used a a batch number, or anything as a matter of fact.
sub linkInvoiceItem {
	my (undef,$data) = @_;


	# Check params
	if (!defined($data->{'Number'}) || $data->{'Number'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'Number' invalid");
	}

	if (!defined($data->{'InventoryCode'}) || $data->{'InventoryCode'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'InventoryCode' invalid");
	}

	my $authInfo = wiaflos::server::api::auth::sessionGetData();

	# Do transaction
	my $detail;
	$detail->{'Number'} = $data->{'Number'};
	$detail->{'InventoryCode'} = $data->{'InventoryCode'};
	$detail->{'Description'} = $data->{'Description'};
	$detail->{'SerialNumber'} = $data->{'SerialNumber'};
	$detail->{'Quantity'} = $data->{'Quantity'};
	$detail->{'TaxTypeID'} = $data->{'TaxTypeID'};
	$detail->{'Discount'} = $data->{'Discount'};
	$detail->{'UnitPrice'} = $data->{'UnitPrice'};
	my $res = wiaflos::server::core::Invoicing::linkInvoiceItem($detail);
	if ($res < 1) {
		return SOAPResponse(RES_ERROR,$res,wiaflos::server::core::Invoicing::Error());
	}

	return SOAPResponse(RES_OK,$res);
}



# Post client invoice
# Parameters:
#		Number	- Client invoice number
sub postInvoice {
	my (undef,$data) = @_;


	# Check params
	if (!defined($data->{'Number'}) || $data->{'Number'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'Number' invalid");
	}

	my $authInfo = wiaflos::server::api::auth::sessionGetData();

	# Grab and sanitize data
	my $detail;
	$detail->{'Number'} = $data->{'Number'};
	my $res = wiaflos::server::core::Invoicing::postInvoice($detail);
	if ($res < 0) {
		return SOAPResponse(RES_ERROR,$res,wiaflos::server::core::Invoicing::Error());
	}

	return SOAPResponse(RES_OK,$res);
}



# Build client invoice
# Parameters:
#		Number		- Invoice ref
#		SendTo			- Send invoice to here
sub sendInvoice {
	my (undef,$data) = @_;


	# Check params
	if (!defined($data->{'Number'}) || $data->{'Number'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'Number' invalid");
	}

	if (!defined($data->{'SendTo'}) || $data->{'SendTo'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'SendTo' invalid");
	}

	my $authInfo = wiaflos::server::api::auth::sessionGetData();

	# Do transaction
	my $detail;
	$detail->{'Number'} = $data->{'Number'};
	$detail->{'SendTo'} = $data->{'SendTo'};
	my $res = wiaflos::server::core::Invoicing::sendInvoice($detail);
	if ($res < 0) {
		return SOAPResponse(RES_ERROR,$res,wiaflos::server::core::Invoicing::Error());
	}

	return SOAPResponse(RES_OK,$res);
}



# Return list of invoice transactions
sub getInvoiceTransactions {
	my (undef,$data) = @_;


	my $authInfo = wiaflos::server::api::auth::sessionGetData();

	# Grab and sanitize data
	my $detail;
	$detail->{'Number'} = $data->{'Number'};
	$detail->{'Mode'} = "resolve_all";
	my $rawData = wiaflos::server::core::Invoicing::getInvoiceTransactions($detail);
	if (ref $rawData ne "ARRAY") {
		return SOAPResponse(RES_ERROR,$rawData,wiaflos::server::core::Invoicing::Error());
	}

	# Build result
	my @data;
	foreach my $item (@{$rawData}) {
		my $tmpItem;

		$tmpItem->{'ID'} = $item->{'ID'};

		$tmpItem->{'Amount'} = $item->{'Amount'};

		$tmpItem->{'ReceiptAllocationID'} = $item->{'ReceiptAllocationID'};
		$tmpItem->{'ReceiptAllocation'} = $item->{'ReceiptAllocation'};
		$tmpItem->{'Receipt'} = $item->{'Receipt'};

		$tmpItem->{'CreditNoteID'} = $item->{'CreditNoteID'};
		$tmpItem->{'CreditNote'} = $item->{'CreditNote'};

		push(@data,$tmpItem);
	}

	return SOAPResponse(RES_OK,\@data);
}





1;
# vim: ts=4
