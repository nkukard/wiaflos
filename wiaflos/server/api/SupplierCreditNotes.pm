# SOAP interface to SupplierCreditNotes module
# Copyright (C) 2009-2014, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


package wiaflos::server::api::SupplierCreditNotes;

use strict;
use warnings;

use wiaflos::server::api::auth;
use wiaflos::constants;
use wiaflos::soap;
use wiaflos::server::core::SupplierCreditNotes;


# Plugin info
our $pluginInfo = {
	Name => "SupplierCreditNotes",
	Init => \&init,
};



# Initialize module
sub init {
	my $server = shift;

	wiaflos::server::api::auth::aclAdd('wiaflos/server/api/SupplierCreditNotes','getSupplierCreditNotes','SupplierCreditNotes/Show');
	wiaflos::server::api::auth::aclAdd('wiaflos/server/api/SupplierCreditNotes','getSupplierCreditNote','SupplierCreditNotes/Show');
	wiaflos::server::api::auth::aclAdd('wiaflos/server/api/SupplierCreditNotes','getSupplierCreditNoteItems','SupplierCreditNotes/Show');
	wiaflos::server::api::auth::aclAdd('wiaflos/server/api/SupplierCreditNotes','getSupplierCreditNoteTransactions','SupplierCreditNotes/Show');

	wiaflos::server::api::auth::aclAdd('wiaflos/server/api/SupplierCreditNotes','createSupplierCreditNote','SupplierCreditNotes/Add');
	wiaflos::server::api::auth::aclAdd('wiaflos/server/api/SupplierCreditNotes','linkSupplierCreditNoteInventoryItem','SupplierCreditNotes/Add');
	wiaflos::server::api::auth::aclAdd('wiaflos/server/api/SupplierCreditNotes','linkSupplierCreditNoteExpenseItem','SupplierCreditNotes/Add');
	wiaflos::server::api::auth::aclAdd('wiaflos/server/api/SupplierCreditNotes','postSupplierCreditNote','SupplierCreditNotes/Post');
}



# Sanitize supplier credit note item
sub sanitizeSupplierCreditNote
{
	my $param = shift;

	my $tmpItem;
	$tmpItem->{'ID'} = $param->{'ID'};

	$tmpItem->{'SupplierID'} = $param->{'SupplierID'};
	$tmpItem->{'SupplierCode'} = $param->{'SupplierCode'};
	$tmpItem->{'Number'} = $param->{'Number'};

	$tmpItem->{'TaxReference'} = $param->{'TaxReference'};

	$tmpItem->{'SupplierCreditNoteNumber'} = $param->{'SupplierCreditNoteNumber'};
	$tmpItem->{'IssueDate'} = $param->{'IssueDate'};

	$tmpItem->{'SubTotal'} = $param->{'Subtotal'};
	$tmpItem->{'TaxTotal'} = $param->{'TaxTotal'};
	$tmpItem->{'Total'} = $param->{'Total'};

	$tmpItem->{'GLTransactionID'} = $param->{'GLTransactionID'};

	$tmpItem->{'Posted'} = $param->{'Posted'};

	$tmpItem->{'Closed'} = $param->{'Closed'};

	return $tmpItem;
}



# Sanitize supplier invoice
sub sanitizeSupplierInvoice
{
	my $param = shift;

	my $tmpItem;

	$tmpItem->{'ID'} = $param->{'ID'};

	$tmpItem->{'SupplierID'} = $param->{'SupplierID'};
	$tmpItem->{'Number'} = $param->{'Number'};

	$tmpItem->{'TaxReference'} = $param->{'TaxReference'};

	$tmpItem->{'SupplierInvoiceNumber'} = $param->{'SupplierInvoiceNumber'};
	$tmpItem->{'IssueDate'} = $param->{'IssueDate'};
	$tmpItem->{'DueDate'} = $param->{'DueDate'};
	$tmpItem->{'OrderNumber'} = $param->{'OrderNumber'};

	$tmpItem->{'SubTotal'} = $param->{'Subtotal'};
	$tmpItem->{'TaxTotal'} = $param->{'TaxTotal'};
	$tmpItem->{'Total'} = $param->{'Total'};

	$tmpItem->{'GLTransactionID'} = $param->{'GLTransactionID'};

	$tmpItem->{'Posted'} = $param->{'Posted'};

	return $tmpItem;
}


# Sanitize supplier invoice transaction
sub sanitizeSupplierInvoiceTransaction
{
	my $param = shift;

	my $tmpItem;

	$tmpItem->{'ID'} = $param->{'ID'};

	$tmpItem->{'Amount'} = $param->{'Amount'};

	$tmpItem->{'PaymentAllocationID'} = $param->{'PaymentAllocationID'};

	$tmpItem->{'SupplierCreditNoteID'} = $param->{'SupplierCreditNoteID'};

	return $tmpItem;
}



# Sanitize supplier receipt allocation
sub sanitizeSupplierReceiptAllocation
{
	my $param = shift;


	my $tmpItem;

	$tmpItem->{'ID'} = $param->{'ID'};

	$tmpItem->{'SupplierReceiptNumber'} = $param->{'SupplierReceiptNumber'};

	$tmpItem->{'CreditNoteID'} = $param->{'CreditNoteID'};
	$tmpItem->{'CreditNoteNumber'} = $param->{'CreditNoteNumber'};

	$tmpItem->{'Amount'} = $param->{'Amount'};

	$tmpItem->{'GLTransactionID'} = $param->{'GLTransactionID'};
	$tmpItem->{'Posted'} = $param->{'Posted'};

	return $tmpItem;
}


# Sanitize receipt
sub sanitizeReceipt
{
	my $param = shift;


	my $tmpItem;

	$tmpItem->{'ID'} = $param->{'ID'};

	$tmpItem->{'ClientID'} = $param->{'ClientID'};
	$tmpItem->{'ClientCode'} = $param->{'ClientCode'};

	$tmpItem->{'GLAccountID'} = $param->{'GLAccountID'};
	$tmpItem->{'GLAccountNumber'} = $param->{'GLAccountNumber'};

	$tmpItem->{'Number'} = $param->{'Number'};

	$tmpItem->{'TransactionDate'} = $param->{'TransactionDate'};
	$tmpItem->{'Reference'} = $param->{'Reference'};
	$tmpItem->{'Amount'} = $param->{'Amount'};

	$tmpItem->{'GLTransactionID'} = $param->{'GLTransactionID'};
	$tmpItem->{'Posted'} = $param->{'Posted'};

	$tmpItem->{'Closed'} = $param->{'Closed'};

	return $tmpItem;
}



# Return list of supplier credit notes
# Optional:
# 	Type	- "open" and "all"
sub getSupplierCreditNotes {
	my (undef,$data) = @_;


	my $authInfo = wiaflos::server::api::auth::sessionGetData();

	# Grab and sanitize data
	my $detail;
	$detail->{'Type'} = $data->{'Type'};
	my $rawData = wiaflos::server::core::SupplierCreditNotes::getSupplierCreditNotes($detail);
	if (ref $rawData ne "ARRAY") {
		return SOAPResponse(RES_ERROR,$rawData,wiaflos::server::core::SupplierCreditNotes::Error());
	}

	# Build result
	my @data;
	foreach my $item (@{$rawData}) {
		push(@data,sanitizeSupplierCreditNote($item));
	}

	return SOAPResponse(RES_OK,\@data);
}



# Create client invoice
# Parameters:
#		SupplierCode	- Supplier code
#		Number			- Credit note number
#		SupplierCreditNoteNumber	- Supplier credit note number
#		IssueDate		- Issue date of credit note
# Optional:
# 		SupplierInvoiceNumber	- Supplier invoice number
# 		Note			- Notes
sub createSupplierCreditNote {
	my (undef,$data) = @_;


	# Check params
	if (!defined($data->{'SupplierCode'}) || $data->{'SupplierCode'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'SupplierCode' invalid");
	}

	if (!defined($data->{'Number'}) || $data->{'Number'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'Number' invalid");
	}

	if (!defined($data->{'IssueDate'}) || $data->{'IssueDate'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'IssueDate' invalid");
	}

	my $authInfo = wiaflos::server::api::auth::sessionGetData();

	# Do transaction
	my $detail;
	$detail->{'SupplierCode'} = $data->{'SupplierCode'};
	$detail->{'Number'} = $data->{'Number'};
	$detail->{'SupplierCreditNoteNumber'} = $data->{'SupplierCreditNoteNumber'};
	$detail->{'IssueDate'} = $data->{'IssueDate'};
	$detail->{'SupplierInvoiceNumber'} = $data->{'SupplierInvoiceNumber'};
	$detail->{'Note'} = $data->{'Note'};
	my $res = wiaflos::server::core::SupplierCreditNotes::createSupplierCreditNote($detail);
	if ($res < 1) {
		return SOAPResponse(RES_ERROR,$res,wiaflos::server::core::SupplierCreditNotes::Error());
	}


	return SOAPResponse(RES_OK,$res);
}




# Link inventory item
# Parameters:
#		Number			- Supplier credit note number
#		InventoryCode			- Inventory code
#		UnitPrice			- Unit price
# Optional:
#		Description			- Description
#		Quantity			- Quantity
#		SerialNumber		- Used by tracked inventory products, this is normally the serial number of the item. Or in any other case
#							can be used a a batch number, or anything as a matter of fact.
#		ExpenseGLAccountNumber - Expense account to post any difference on qty = 0 vs. price > 0 to
sub linkSupplierCreditNoteInventoryItem {
	my (undef,$data) = @_;


	# Check params
	if (!defined($data->{'Number'}) || $data->{'Number'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'Number' invalid");
	}

	if (!defined($data->{'InventoryCode'}) || $data->{'InventoryCode'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'InventoryCode' invalid");
	}

	if (!defined($data->{'UnitPrice'}) || $data->{'UnitPrice'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'UnitPrice' invalid");
	}

	my $authInfo = wiaflos::server::api::auth::sessionGetData();

	# Do transaction
	my $detail;
	$detail->{'Number'} = $data->{'Number'};
	$detail->{'InventoryCode'} = $data->{'InventoryCode'};
	$detail->{'Description'} = $data->{'Description'};
	$detail->{'SerialNumber'} = $data->{'SerialNumber'};
	$detail->{'Quantity'} = $data->{'Quantity'};
	$detail->{'UnitPrice'} = $data->{'UnitPrice'};
	$detail->{'GLExpenseAccountNumber'} = $data->{'GLExpenseAccountNumber'};
	my $res = wiaflos::server::core::SupplierCreditNotes::linkSupplierCreditNoteItem($detail);
	if ($res < 1) {
		return SOAPResponse(RES_ERROR,$res,wiaflos::server::core::SupplierCreditNotes::Error());
	}

	return SOAPResponse(RES_OK,$res);
}



# Link expense item
# Parameters:
#		Number	- Supplier credit note number
#		GLAccountNumber				- GL account number
#		UnitPrice			- UnitPrice
# Optional:
#		Description			- Description
#		Quantity			- Quantity
#		SerialNumber		- Used by tracked inventory products, this is normally the serial number of the item. Or in any other case
#							can be used a a batch number, or anything as a matter of fact.
sub linkSupplierCreditNoteExpenseItem {
	my (undef,$data) = @_;


	# Check params
	if (!defined($data->{'Number'}) || $data->{'Number'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'Number' invalid");
	}

	if (!defined($data->{'GLAccountNumber'}) || $data->{'GLAccountNumber'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'GLAccountNumber' invalid");
	}

	if (!defined($data->{'UnitPrice'}) || $data->{'UnitPrice'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'UnitPrice' invalid");
	}

	my $authInfo = wiaflos::server::api::auth::sessionGetData();

	# Do transaction
	my $detail;
	$detail->{'Number'} = $data->{'Number'};
	$detail->{'GLAccountNumber'} = $data->{'GLAccountNumber'};
	$detail->{'Description'} = $data->{'Description'};
	$detail->{'SerialNumber'} = $data->{'SerialNumber'};
	$detail->{'Quantity'} = $data->{'Quantity'};
	$detail->{'UnitPrice'} = $data->{'UnitPrice'};
	$detail->{'GLExpenseAccountNumber'} = $data->{'GLExpenseAccountNumber'};
	my $res = wiaflos::server::core::SupplierCreditNotes::linkSupplierCreditNoteItem($detail);
	if ($res < 1) {
		return SOAPResponse(RES_ERROR,$res,wiaflos::server::core::SupplierCreditNotes::Error());
	}

	return SOAPResponse(RES_OK,$res);
}



# Post credit note
# Parameters:
#		Number	- Supplier credit note number
sub postSupplierCreditNote {
	my (undef,$data) = @_;



	# Check params
	if (!defined($data->{'Number'}) || $data->{'Number'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'Number' invalid");
	}

	my $authInfo = wiaflos::server::api::auth::sessionGetData();

	# Grab and sanitize data
	my $detail;
	$detail->{'Number'} = $data->{'Number'};
	my $res = wiaflos::server::core::SupplierCreditNotes::postSupplierCreditNote($detail);
	if ($res < 0) {
		return SOAPResponse(RES_ERROR,$res,wiaflos::server::core::SupplierCreditNotes::Error());
	}

	return SOAPResponse(RES_OK,$res);
}



# Return supplier credit note
# Parameters:
#		Number		- Supplier credit note number
sub getSupplierCreditNote {
	my (undef,$data) = @_;


	# Check params
	if (!defined($data->{'Number'}) || $data->{'Number'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'Number' invalid");
	}

	my $authInfo = wiaflos::server::api::auth::sessionGetData();

	# Grab and sanitize data
	my $detail;
	$detail->{'Number'} = $data->{'Number'};
	my $rawData = wiaflos::server::core::SupplierCreditNotes::getSupplierCreditNote($detail);
	if (ref $rawData ne "HASH") {
		return SOAPResponse(RES_ERROR,$rawData,wiaflos::server::core::SupplierCreditNotes::Error());
	}

	return SOAPResponse(RES_OK,sanitizeSupplierCreditNote($rawData));
}


# Return list of items relating to a supplier credit note
# Parameters:
#		Number		- Supplier credit note number
sub getSupplierCreditNoteItems {
	my (undef,$data) = @_;


	# Check params
	if (!defined($data->{'Number'}) || $data->{'Number'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'Number' invalid");
	}

	my $authInfo = wiaflos::server::api::auth::sessionGetData();

	# Grab and sanitize data
	my $detail;
	$detail->{'Number'} = $data->{'Number'};
	my $rawData = wiaflos::server::core::SupplierCreditNotes::getSupplierCreditNoteItems($detail);
	if (ref $rawData ne "ARRAY") {
		return SOAPResponse(RES_ERROR,$rawData,wiaflos::server::core::SupplierCreditNotes::Error());
	}

	# Build result
	my @data;
	foreach my $item (@{$rawData}) {
		my $tmpItem;

		$tmpItem->{'ID'} = $item->{'ID'};

		$tmpItem->{'SupplierInvoiceItemID'} = $item->{'SupplierInvoiceItemID'};

		$tmpItem->{'InventoryID'} = $item->{'InventoryID'};
		$tmpItem->{'GLAccountID'} = $item->{'GLAccountID'};
		$tmpItem->{'GLAccountNumber'} = $item->{'GLAccountNumber'};

		$tmpItem->{'Description'} = $item->{'Description'};
		$tmpItem->{'SerialNumber'} = $item->{'SerialNumber'};

		$tmpItem->{'Quantity'} = $item->{'Quantity'};
		$tmpItem->{'Unit'} = $item->{'Unit'};
		$tmpItem->{'UnitPrice'} = $item->{'UnitPrice'};
		$tmpItem->{'Price'} = $item->{'Price'};

		$tmpItem->{'TaxRate'} = $item->{'TaxRate'};
		$tmpItem->{'TaxAmount'} = $item->{'TaxAmount'};
		$tmpItem->{'TaxMode'} = $item->{'TaxMode'};

		$tmpItem->{'TotalPrice'} = $item->{'TotalPrice'};

		push(@data,$tmpItem);
	}

	return SOAPResponse(RES_OK,\@data);
}


# Return list of supplier credit note transactions
sub getSupplierCreditNoteTransactions {
	my (undef,$data) = @_;


	my $authInfo = wiaflos::server::api::auth::sessionGetData();

	# Grab and sanitize data
	my $detail;
	$detail->{'Number'} = $data->{'Number'};
	$detail->{'Mode'} = "resolve_all";
	my $rawData = wiaflos::server::core::SupplierCreditNotes::getSupplierCreditNoteTransactions($detail);
	if (ref $rawData ne "ARRAY") {
		return SOAPResponse(RES_ERROR,$rawData,wiaflos::server::core::SupplierCreditNotes::Error());
	}

	# Build result
	my @data;
	foreach my $item (@{$rawData}) {
		my $tmpItem;

		$tmpItem->{'ID'} = $item->{'ID'};

		$tmpItem->{'Amount'} = $item->{'Amount'};

		# Check what we have an make it pretty
		if (defined($item->{'SupplierReceiptAllocationID'})) {
			$tmpItem->{'SupplierReceiptAllocationID'} = $item->{'SupplierReceiptAllocationID'};
			$tmpItem->{'SupplierReceiptAllocation'} = sanitizeSupplierReceiptAllocation($item->{'SupplierReceiptAllocation'});
			$tmpItem->{'Receipt'} = sanitizeReceipt($item->{'Receipt'});
		}
		if (defined($item->{'SupplierInvoiceTransactionID'})) {
			$tmpItem->{'SupplierInvoiceTransactionID'} = $item->{'SupplierInvoiceTransactionID'};
			$tmpItem->{'SupplierInvoiceTransaction'} = sanitizeSupplierInvoiceTransaction($item->{'SupplierInvoiceTransaction'});
			$tmpItem->{'SupplierInvoice'} = sanitizeSupplierInvoice($item->{'SupplierInvoice'});
		}

		push(@data,$tmpItem);
	}

	return SOAPResponse(RES_OK,\@data);
}




1;
# vim: ts=4
