# SOAP interface to Purchasing module
# Copyright (C) 2009-2014, AllWorldIT
# Copyright (C) 2008, LinuxRulz
# Copyright (C) 2007 Nigel Kukard  <nkukard@lbsd.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


package wiaflos::server::api::Purchasing;

use strict;
use warnings;

use wiaflos::server::api::auth;
use wiaflos::constants;
use wiaflos::soap;
use wiaflos::server::core::Purchasing;


# Plugin info
our $pluginInfo = {
	Name => "Purchasing",
	Init => \&init,
};


# Initialize module
sub init {
	my $server = shift;

	wiaflos::server::api::auth::aclAdd('wiaflos/server/api/Purchasing','getSupplierInvoices','Purchasing/Show');
	wiaflos::server::api::auth::aclAdd('wiaflos/server/api/Purchasing','getSupplierInvoice','Purchasing/Show');
	wiaflos::server::api::auth::aclAdd('wiaflos/server/api/Purchasing','getSupplierInvoiceItems','Purchasing/Show');
	wiaflos::server::api::auth::aclAdd('wiaflos/server/api/Purchasing','getSupplierInvoiceTransactions','Purchasing/Show');
	wiaflos::server::api::auth::aclAdd('wiaflos/server/api/Purchasing','createSupplierInvoice','Purchasing/Add');
	wiaflos::server::api::auth::aclAdd('wiaflos/server/api/Purchasing','linkSupplierInvoiceInventoryItem','Purchasing/Add');
	wiaflos::server::api::auth::aclAdd('wiaflos/server/api/Purchasing','linkSupplierInvoiceExpenseItem','Purchasing/Add');
	wiaflos::server::api::auth::aclAdd('wiaflos/server/api/Purchasing','postSupplierInvoice','Purchasing/Post');
}



# Sanitize payment
sub sanitizePayment {
	my $params = shift;

	my $tmpItem;

	$tmpItem->{'ID'} = $params->{'ID'};
	$tmpItem->{'SupplierID'} = $params->{'SupplierID'};
	$tmpItem->{'SupplierCode'} = $params->{'SupplierCode'};

	$tmpItem->{'GLAccountID'} = $params->{'GLAccountID'};
	$tmpItem->{'GLAccountNumber'} = $params->{'GLAccountNumber'};

	$tmpItem->{'Number'} = $params->{'Number'};

	$tmpItem->{'TransactionDate'} = $params->{'TransactionDate'};
	$tmpItem->{'Reference'} = $params->{'Reference'};
	$tmpItem->{'Amount'} = $params->{'Amount'};

	$tmpItem->{'GLTransactionID'} = $params->{'GLTransactionID'};
	$tmpItem->{'Posted'} = $params->{'Posted'};

	$tmpItem->{'Closed'} = $params->{'Closed'};

	return $tmpItem;
}


# Sanitize raw payment allocation
sub sanitizePaymentAllocation
{
	my $param = shift;


	my $tmpItem;

	$tmpItem->{'ID'} = $param->{'ID'};

	$tmpItem->{'Number'} = $param->{'Number'};

	$tmpItem->{'SupplierInvoiceID'} = $param->{'SupplierInvoiceID'};
	$tmpItem->{'SupplierInvoiceNumber'} = $param->{'SupplierInvoiceNumber'};

	$tmpItem->{'Amount'} = $param->{'Amount'};

	$tmpItem->{'GLTransactionID'} = $param->{'GLTransactionID'};
	$tmpItem->{'Posted'} = $param->{'Posted'};

	return $tmpItem;
}



# Sanitize supplier credit note item
sub sanitizeSupplierCreditNote
{
	my $param = shift;

	my $tmpItem;
	$tmpItem->{'ID'} = $param->{'ID'};

	$tmpItem->{'SupplierID'} = $param->{'SupplierID'};
	$tmpItem->{'SupplierCode'} = $param->{'SupplierCode'};
	$tmpItem->{'Number'} = $param->{'Number'};

	$tmpItem->{'TaxReference'} = $param->{'TaxReference'};

	$tmpItem->{'SupplierCreditNoteNumber'} = $param->{'SupplierCreditNoteNumber'};
	$tmpItem->{'IssueDate'} = $param->{'IssueDate'};

	$tmpItem->{'SubTotal'} = $param->{'Subtotal'};
	$tmpItem->{'TaxTotal'} = $param->{'TaxTotal'};
	$tmpItem->{'Total'} = $param->{'Total'};

	$tmpItem->{'GLTransactionID'} = $param->{'GLTransactionID'};

	$tmpItem->{'Posted'} = $param->{'Posted'};

	$tmpItem->{'Closed'} = $param->{'Closed'};

	return $tmpItem;
}


# Sanitize supplier invoice
sub sanitizeSupplierInvoice
{
	my $param = shift;

	my $tmpItem;

	$tmpItem->{'ID'} = $param->{'ID'};

	$tmpItem->{'SupplierID'} = $param->{'SupplierID'};
	$tmpItem->{'Number'} = $param->{'Number'};

	$tmpItem->{'TaxReference'} = $param->{'TaxReference'};

	$tmpItem->{'SupplierInvoiceNumber'} = $param->{'SupplierInvoiceNumber'};
	$tmpItem->{'IssueDate'} = $param->{'IssueDate'};
	$tmpItem->{'DueDate'} = $param->{'DueDate'};
	$tmpItem->{'OrderNumber'} = $param->{'OrderNumber'};

	$tmpItem->{'SubTotal'} = $param->{'Subtotal'};
	$tmpItem->{'TaxTotal'} = $param->{'TaxTotal'};
	$tmpItem->{'Total'} = $param->{'Total'};

	$tmpItem->{'GLTransactionID'} = $param->{'GLTransactionID'};

	$tmpItem->{'Posted'} = $param->{'Posted'};

	return $tmpItem;
}


# Return list of supplier invoices
# Optional:
# 	Type	- "paid" and "all"
sub getSupplierInvoices {
	my (undef,$data) = @_;


	my $authInfo = wiaflos::server::api::auth::sessionGetData();

	# Grab and sanitize data
	my $detail;
	$detail->{'Type'} = $data->{'Type'};
	my $rawData = wiaflos::server::core::Purchasing::getSupplierInvoices($detail);
	if (ref $rawData ne "ARRAY") {
		return SOAPResponse(RES_ERROR,$rawData,wiaflos::server::core::Purchasing::Error());
	}

	# Build result
	my @data;
	foreach my $item (@{$rawData}) {
		push(@data,sanitizeSupplierInvoice($item));
	}

	return SOAPResponse(RES_OK,\@data);
}



# Return supplier invoice
# Parameters:
#		ID		- Supplier invoice ID
#		Number	- Supplier invoice number
sub getSupplierInvoice {
	my (undef,$data) = @_;


	# Check params
	if (!defined($data->{'Number'}) || $data->{'Number'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'Number' invalid");
	}

	my $authInfo = wiaflos::server::api::auth::sessionGetData();

	# Grab and sanitize data
	my $detail;
	$detail->{'Number'} = $data->{'Number'};
	my $rawData = wiaflos::server::core::Purchasing::getSupplierInvoice($detail);
	if (ref $rawData ne "HASH") {
		return SOAPResponse(RES_ERROR,$rawData,wiaflos::server::core::Purchasing::Error());
	}

	# Build result
	my $tmpItem;

	$tmpItem->{'ID'} = $rawData->{'ID'};

	$tmpItem->{'SupplierID'} = $rawData->{'SupplierID'};
	$tmpItem->{'Number'} = $rawData->{'Number'};

	$tmpItem->{'TaxReference'} = $rawData->{'TaxReference'};

	$tmpItem->{'SupplierInvoiceNumber'} = $rawData->{'SupplierInvoiceNumber'};
	$tmpItem->{'IssueDate'} = $rawData->{'IssueDate'};
	$tmpItem->{'DueDate'} = $rawData->{'DueDate'};
	$tmpItem->{'OrderNumber'} = $rawData->{'OrderNumber'};

	$tmpItem->{'DiscountTotal'} = $rawData->{'DiscountTotal'};
	$tmpItem->{'SubTotal'} = $rawData->{'SubTotal'};
	$tmpItem->{'TaxTotal'} = $rawData->{'TaxTotal'};
	$tmpItem->{'Total'} = $rawData->{'Total'};

	$tmpItem->{'GLTransactionID'} = $rawData->{'GLTransactionID'};
	$tmpItem->{'Posted'} = $rawData->{'Posted'};
	$tmpItem->{'Paid'} = $rawData->{'Paid'};

	return SOAPResponse(RES_OK,$tmpItem);
}



# Return list of items relating to an invoice
# Parameters:
#		Number	- Supplier invoice number
sub getSupplierInvoiceItems {
	my (undef,$data) = @_;


	# Check params
	if (!defined($data->{'Number'}) || $data->{'Number'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'Number' invalid");
	}

	my $authInfo = wiaflos::server::api::auth::sessionGetData();

	# Grab and sanitize data
	my $detail;
	$detail->{'Number'} = $data->{'Number'};
	my $rawData = wiaflos::server::core::Purchasing::getSupplierInvoiceItems($detail);
	if (ref $rawData ne "ARRAY") {
		return SOAPResponse(RES_ERROR,$rawData,wiaflos::server::core::Purchasing::Error());
	}

	# Build result
	my @data;
	foreach my $item (@{$rawData}) {
		my $tmpItem;

		$tmpItem->{'ID'} = $item->{'ID'};
		$tmpItem->{'InventoryID'} = $item->{'InventoryID'};
		$tmpItem->{'GLAccountNumber'} = $item->{'GLAccountNumber'};
		$tmpItem->{'GLAccountID'} = $item->{'GLAccountID'};
		$tmpItem->{'Description'} = $item->{'Description'};
		$tmpItem->{'Quantity'} = $item->{'Quantity'};
		$tmpItem->{'Unit'} = $item->{'Unit'};
		$tmpItem->{'UnitPrice'} = $item->{'UnitPrice'};
		$tmpItem->{'Price'} = $item->{'Price'};
		$tmpItem->{'Total'} = $item->{'Total'};
		$tmpItem->{'Discount'} = $item->{'Discount'};
		$tmpItem->{'DiscountAmount'} = $item->{'DiscountAmount'};
		$tmpItem->{'TaxRate'} = $item->{'TaxRate'};
		$tmpItem->{'TaxAmount'} = $item->{'TaxAmount'};

		push(@data,$tmpItem);
	}

	return SOAPResponse(RES_OK,\@data);
}



# Create supplier invoice
# Parameters:
#		SupplierCode		- Supplier code
#		Number				- Invoice number
#		SupplierInvoiceNumber	- Suppliers invoice number
#		IssueDate		- Issue date
# Optional:
#		DueDate			- Due date
#		OrderNumber		- Order number
#		SubTotal		- Sub total
#		TaxTotal		- Tax total
#		Total			- Invoice total
#		Note			- Notes
sub createSupplierInvoice {
	my (undef,$data) = @_;


	# Check params
	if (!defined($data->{'SupplierCode'}) || $data->{'SupplierCode'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'SupplierCode' invalid");
	}

	if (!defined($data->{'Number'}) || $data->{'Number'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'Number' invalid");
	}

	if (!defined($data->{'SupplierInvoiceNumber'}) || $data->{'SupplierInvoiceNumber'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'SupplierInvoiceNumber' invalid");
	}

	if (!defined($data->{'IssueDate'}) || $data->{'IssueDate'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'IssueDate' invalid");
	}

	my $authInfo = wiaflos::server::api::auth::sessionGetData();

	# Do transaction
	my $detail;
	$detail->{'SupplierCode'} = $data->{'SupplierCode'};
	$detail->{'Number'} = $data->{'Number'};
	$detail->{'SupplierInvoiceNumber'} = $data->{'SupplierInvoiceNumber'};
	$detail->{'IssueDate'} = $data->{'IssueDate'};

	$detail->{'SubTotal'} = $data->{'SubTotal'};
	$detail->{'TaxTotal'} = $data->{'TaxTotal'};
	$detail->{'Total'} = $data->{'Total'};

	$detail->{'DueDate'} = $data->{'DueDate'};
	$detail->{'OrderNumber'} = $data->{'OrderNumber'};
	$detail->{'Note'} = $data->{'Note'};

	my $res = wiaflos::server::core::Purchasing::createSupplierInvoice($detail);
	if ($res < 1) {
		return SOAPResponse(RES_ERROR,$res,wiaflos::server::core::Purchasing::Error());
	}

	return SOAPResponse(RES_OK,$res);
}



# Link inventory item
# Parameters:
#		Number			- Supplier invoice number
#		InventoryCode			- Inventory code
#		Description			- Description
#		UnitPrice			- Unit price
#		TaxTypeID			- Tax type ID
#		TaxMode				- Tax mode
# Optional:
#		Quantity			- Quantity
#		Discount			- Discount %
#		SerialNumber		- Used by tracked inventory products, this is normally the serial number of the item. Or in any other case
#							can be used a a batch number, or anything as a matter of fact.
sub linkSupplierInvoiceInventoryItem {
	my (undef,$data) = @_;


	# Check params
	if (!defined($data->{'Number'}) || $data->{'Number'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'Number' invalid");
	}

	if (!defined($data->{'InventoryCode'}) || $data->{'InventoryCode'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'InventoryCode' invalid");
	}

	if (!defined($data->{'Description'}) || $data->{'Description'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'Description' invalid");
	}

	if (!defined($data->{'UnitPrice'}) || $data->{'UnitPrice'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'UnitPrice' invalid");
	}

	if (!defined($data->{'TaxTypeID'}) || $data->{'TaxTypeID'} < 1) {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'TaxTypeID' invalid");
	}

	if (!defined($data->{'TaxMode'}) || $data->{'TaxMode'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'TaxMode' invalid");
	}

	my $authInfo = wiaflos::server::api::auth::sessionGetData();

	# Do transaction
	my $detail;
	$detail->{'Number'} = $data->{'Number'};
	$detail->{'InventoryCode'} = $data->{'InventoryCode'};
	$detail->{'Description'} = $data->{'Description'};
	$detail->{'SerialNumber'} = $data->{'SerialNumber'};
	$detail->{'Unit'} = $data->{'Unit'};
	$detail->{'Quantity'} = $data->{'Quantity'};
	$detail->{'UnitPrice'} = $data->{'UnitPrice'};
	$detail->{'Discount'} = $data->{'Discount'};
	$detail->{'TaxTypeID'} = $data->{'TaxTypeID'};
	$detail->{'TaxMode'} = $data->{'TaxMode'};
	my $res = wiaflos::server::core::Purchasing::linkSupplierInvoiceItem($detail);
	if ($res < 1) {
		return SOAPResponse(RES_ERROR,$res,wiaflos::server::core::Purchasing::Error());
	}

	return SOAPResponse(RES_OK,$res);
}



# Link expense item
# Parameters:
#		Number	- Supplier invoice number
#		GLAccountNumber				- GL account number
#		Description			- Description
#		UnitPrice			- UnitPrice
#		TaxTypeID			- Tax type ID
#		TaxMode				- Tax mode
# Optional:
#		Quantity			- Quantity
#		Discount			- Discount %
#		SerialNumber		- Used by tracked inventory products, this is normally the serial number of the item. Or in any other case
#							can be used a a batch number, or anything as a matter of fact.
sub linkSupplierInvoiceExpenseItem {
	my (undef,$data) = @_;


	# Check params
	if (!defined($data->{'Number'}) || $data->{'Number'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'Number' invalid");
	}

	if (!defined($data->{'GLAccountNumber'}) || $data->{'GLAccountNumber'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'GLAccountNumber' invalid");
	}

	if (!defined($data->{'Description'}) || $data->{'Description'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'Description' invalid");
	}

	if (!defined($data->{'UnitPrice'}) || $data->{'UnitPrice'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'UnitPrice' invalid");
	}

	if (!defined($data->{'TaxTypeID'}) || $data->{'TaxTypeID'} < 1) {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'TaxTypeID' invalid");
	}

	if (!defined($data->{'TaxMode'}) || $data->{'TaxMode'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'TaxMode' invalid");
	}

	my $authInfo = wiaflos::server::api::auth::sessionGetData();

	# Do transaction
	my $detail;
	$detail->{'Number'} = $data->{'Number'};
	$detail->{'GLAccountNumber'} = $data->{'GLAccountNumber'};
	$detail->{'Description'} = $data->{'Description'};
	$detail->{'SerialNumber'} = $data->{'SerialNumber'};
	$detail->{'Quantity'} = $data->{'Quantity'};
	$detail->{'UnitPrice'} = $data->{'UnitPrice'};
	$detail->{'Discount'} = $data->{'Discount'};
	$detail->{'TaxTypeID'} = $data->{'TaxTypeID'};
	$detail->{'TaxMode'} = $data->{'TaxMode'};
	my $res = wiaflos::server::core::Purchasing::linkSupplierInvoiceItem($detail);
	if ($res < 1) {
		return SOAPResponse(RES_ERROR,$res,wiaflos::server::core::Purchasing::Error());
	}

	return SOAPResponse(RES_OK,$res);
}



# Post supplier invoice
# Parameters:
#		Number	- Supplier invoice number
sub postSupplierInvoice {
	my (undef,$data) = @_;



	# Check params
	if (!defined($data->{'Number'}) || $data->{'Number'} eq "") {
		return SOAPResponse(RES_ERROR,ERR_S_PARAM,"Parameter 'Number' invalid");
	}

	my $authInfo = wiaflos::server::api::auth::sessionGetData();

	# Grab and sanitize data
	my $detail;
	$detail->{'Number'} = $data->{'Number'};
	my $res = wiaflos::server::core::Purchasing::postSupplierInvoice($detail);
	if ($res < 0) {
		return SOAPResponse(RES_ERROR,$res,wiaflos::server::core::Purchasing::Error());
	}

	return SOAPResponse(RES_OK,$res);
}



# Return list of supplier invoice transactions
sub getSupplierInvoiceTransactions {
	my (undef,$data) = @_;


	my $authInfo = wiaflos::server::api::auth::sessionGetData();

	# Grab and sanitize data
	my $detail;
	$detail->{'Number'} = $data->{'Number'};
	$detail->{'Mode'} = "resolve_all";
	my $rawData = wiaflos::server::core::Purchasing::getSupplierInvoiceTransactions($detail);
	if (ref $rawData ne "ARRAY") {
		return SOAPResponse(RES_ERROR,$rawData,wiaflos::server::core::Purchasing::Error());
	}

	# Build result
	my @data;
	foreach my $item (@{$rawData}) {
		my $tmpItem;

		$tmpItem->{'ID'} = $item->{'ID'};

		$tmpItem->{'Amount'} = $item->{'Amount'};

		# Depending on how we came ... sanitize a bit
		if (defined($item->{'PaymentAllocationID'})) {
			$tmpItem->{'PaymentAllocationID'} = $item->{'PaymentAllocationID'};
			$tmpItem->{'PaymentAllocation'} = sanitizePaymentAllocation($item->{'PaymentAllocation'});
			$tmpItem->{'Payment'} = sanitizePayment($item->{'Payment'});
		}
		if (defined($item->{'SupplierCreditNoteID'})) {
			$tmpItem->{'SupplierCreditNoteID'} = $item->{'SupplierCreditNoteID'};
			$tmpItem->{'SupplierCreditNote'} = sanitizeSupplierCreditNote($item->{'SupplierCreditNote'});
		}

		push(@data,$tmpItem);
	}

	return SOAPResponse(RES_OK,\@data);
}



1;
# vim: ts=4
